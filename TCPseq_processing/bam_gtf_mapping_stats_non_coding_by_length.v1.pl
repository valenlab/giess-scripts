#!/usr/bin/perl -w
use strict;

#to do 09/03/2018
#script to take a bam file and to count reads mapping to transcripts, or ncRNA per read length

my $gtf=$ARGV[0];
my $nc_fasta=$ARGV[1];
my $bam=$ARGV[2];

###################################################################################################

#open gtf and get start codons
my %TIS;

open(GENES,$gtf) || die "can't open $gtf";
while (<GENES>){
    unless(/^#/){
        my @b=split("\t");
        my $class=$b[2];
        my $chr=$b[0];
        my $start=$b[3];
        my $end=$b[4];
        my $dir=$b[6];

        my ($gene_id) = $b[8] =~ /gene_id\s"([^\"]+)"/;

        if ($class eq "start_codon"){
            if ($dir eq "+"){
                $TIS{$gene_id}=$start;
            }else{
                $TIS{$gene_id}=$end;
            }
        }
    }
}
close(GENES);

###################################################################################################
#open gtf file setup genomic positions types
my %rRNA; #key1 = chr, key2 = position, value = 1
my %cds; #key1 = chr, key2 = position, value = 1
my %utr5; #key1 = chr, key2 = position, value = 1
my %utr3; #key1 = chr, key2 = position, value = 1

open(GENES2,$gtf) || die "can't open $gtf";
while (<GENES2>){
    unless(/^#/){
        my @b=split("\t");
        my $class=$b[2];
        my $chr=$b[0];
        my $start=$b[3];
        my $end=$b[4];
        my $dir=$b[6];

        my ($gene_id) = $b[8] =~ /gene_id\s"([^\"]+)"/;
        my ($biotype) = $b[8] =~ /gene_biotype\s"([^\"]+)"/;

        if ($chr=~/chr(.*)/){ #ensembl chromosome names do not contain the "chr" prefix
            $chr=$1;
        }

#        if ($b[8] =~ /GRCh38:([^:]+):(\d+):(\d+).*gene_biotype:\s"rRNA"/){
        if ($b[8] =~ /gene_biotype\s"rRNA"/){  #this misses mt_RNA + TR_J_gene
            for ($start .. $end){
                 $rRNA{$chr}{$_}=1;
            }
        }

        if ($class eq "CDS"){
            if ($b[8] =~ /gene_biotype\s"protein_coding"/){
               for ($start .. $end){
                   $cds{$chr}{$_}=1;
               }
           }
        }

        if ($class eq "UTR"){
            if ($b[8] =~ /gene_biotype\s"protein_coding"/){
                if (exists ($TIS{$gene_id})){
                    if ($dir eq "+"){
                        if ($start < $TIS{$gene_id}){
                            for ($start .. $end){
                                $utr5{$chr}{$_}=1;
                            }
                        }else{
                            for ($start .. $end){
                                $utr3{$chr}{$_}=1;
                            }
                        }
                    }else{
                        if ($end > $TIS{$gene_id}){
                            for ($start .. $end){
                                $utr5{$chr}{$_}=1;
                            }
                        }else{
                            for ($start .. $end){
                                $utr3{$chr}{$_}=1;
                            }
                        }
                    }
                }
            }
        }

        #there are no annotated UTRs in the yeast genome, additionally gene regions do not extend beyond CDSs
        #if ($class eq "start_codon"){
        #    if ($b[8] =~ /gene_biotype\s"protein_coding"/){
        #        if (exists ($transcript_start{$gene_id})){
        #            if ($dir eq "+"){
        #                if ($start < $transcript_start{$gene_id}){
        #                    print "$gene_id\n";
        #                    for ($start .. ($transcript_start{$gene_id}-1)){
        #                        $utr5{$chr}{$_}=1;
        #                    }
        #                }
        #            }else{
        #                if ($end > $transcript_start{$gene_id}){
        #                    print "$gene_id\n";
        #                    for ($transcript_start{$gene_id} .. $end){
        #                        $utr5{$chr}{$_}=1;
        #                    }
        #                }
        #            }
        #        }
        #    }
        #}


    }
}
close(GENES2);

###################################################################################################
#open fasta of nc rna's
my %nc;

open(NCF,$nc_fasta) || die "can't open $nc_fasta";
while (<NCF>){
    if (/^\>/){
        if (/chromosome:GRCz10:([^:]+):(\d+):(\d+).*gene_biotype:([^\s]+)\s/){ 
        #if (/chromosome:R64-1-1:([^:]+):(\d+):(\d+).*gene_biotype:([^\s]+)\s/){

            my $chr=$1;
            my $start=$2;
            my $end=$3;
            my $bioType=$4;
            #print "$bioType,$chr,$start,$end\n";

            if ($chr=~/chr(.*)/){ #ensembl chromosome names do not contain the "chr" prefix
                $chr=$1;
            }

            for ($start .. $end){
                $nc{$chr}{$_}=$bioType;
            }
        }
    }
}

###################################################################################################
#open sam file
my %ribosomal_count;
my %coding_count;
my %read_count;
my %nc_count;
my %lead_count;
my %trail_count;

open BAM,"samtools view $bam |";
while(<BAM>){

    next if(/^(\@)/);        # skipping the header lines (if you used -h in the samtools command)
    s/\n//;  s/\r//;         # removing new line
    my @sam = split(/\t+/);  # splitting SAM line into array

    my $leftMost=$sam[3];    #leftmost position of match 5' for fwd, 3' for rev
    my $flag=$sam[1];
    my $chr=$sam[2];
    my $mapq=$sam[4];
    my $cigar=$sam[5];
    my $seq=$sam[9];
    my $threePrime;
    my $fivePrime;

    if ($chr=~/chr(.*)/){ #ensembl chromosome names do not contain the "chr" prefix
        $chr=$1;
    }

    unless ($flag & 0x4){   #if aligned
        my $readLength=length($seq);
        $read_count{$readLength}++;

        if ($flag & 0x10){  #if rev calculate 3' == sam coordinate (leftmost)

            $threePrime=$leftMost;

            #parse cigar for indels and adjust the length of the alignment             
            my $length=length($seq);
            while ($cigar =~/(\d+)I/g){   #add to length for insertions
                $length+=$1;
            }
            while ($cigar =~/(\d+)D/g){   #substact from length for deletions
                $length-=$1;
            }
            $fivePrime=$leftMost+($length-1);              #SAM is 1 based

            my $r=0;
            my $c=0;
            my $u5=0;
            my $u3=0;
            my $nc_type=0;
            for my $pos ($threePrime .. $fivePrime){
                if (exists ($rRNA{$chr}{$pos})){
                    $r=1;
                    last;
                }
                   
                if (exists ($cds{$chr}{$pos})){
                    $c=1;
                    last;
                }
 
                if (exists ($utr5{$chr}{$pos})){
                    $u5=1;
                    last;
                }

                if (exists ($utr3{$chr}{$pos})){
                    $u3=1;
                    last;
                }

                if (exists ($nc{$chr}{$pos})){
                   #$nc_type=$nc{$chr}{$pos};
                    $nc_type=1;
                    last;
                }
            }

            if ($r){        $ribosomal_count{$readLength}++; }
            if ($c){        $coding_count{$readLength}++;    }
            if ($u5){       $lead_count{$readLength}++;      }
            if ($u3){       $trail_count{$readLength}++;     }
            if ($nc_type) { $nc_count{$readLength}++;        }

        }else{ #if fwd 3' == sam coordinate (leftmost) + read length 

            #parse cigar for indels and adjust the length of the alignment             
            my $length=length($seq);
            while ($cigar =~/(\d+)I/g){   #add to length for insertions
                $length+=$1;
            }
            while ($cigar =~/(\d+)D/g){   #substact from length for deletions
                $length-=$1;
            }

            $threePrime=$leftMost+($length-1);              #SAM is 1 based
            $fivePrime=$leftMost;

            my $r=0;
            my $c=0;
            my $u5=0;
            my $u3=0;
            my $nc_type=0;
            for my $pos ($fivePrime .. $threePrime){
                if (exists ($rRNA{$chr}{$pos})){
                    $r=1;
                    last;
                }

                if (exists ($cds{$chr}{$pos})){
                    $c=1;
                    last;
                }

                if (exists ($utr5{$chr}{$pos})){
                    $u5=1;
                    last;
                }

                if (exists ($utr3{$chr}{$pos})){
                    $u3=1;
                    last;
                }

                if (exists ($nc{$chr}{$pos})){
                    #$nc_type=$nc{$chr}{$pos};
                    $nc_type=1;
                    last;
                }

            }
            if ($r){        $ribosomal_count{$readLength}++; }
            if ($c){        $coding_count{$readLength}++;    }
            if ($u5){       $lead_count{$readLength}++;      }
            if ($u3){       $trail_count{$readLength}++;     }
            if ($nc_type) { $nc_count{$readLength}++;        }
        }
    }                    
}
close (BAM);

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
print "insert_size,read_count,leader_count,coding_count,trailer_count,ribosomal_count,nc_count\n"; 
for (1 .. 100){
   print "$_";
   if (exists ($read_count{$_}) ){ print ",$read_count{$_}"; }else{ print ",0"; }
   if (exists ($lead_count{$_}) ){ print ",$lead_count{$_}"; }else{ print ",0"; }
   if (exists ($coding_count{$_}) ){ print ",$coding_count{$_}"; }else{ print ",0"; }
   if (exists ($trail_count{$_}) ){ print ",$trail_count{$_}"; }else{ print ",0"; }
   if (exists ($ribosomal_count{$_}) ){ print ",$ribosomal_count{$_}"; }else{ print ",0"; }
   if (exists ($nc_count{$_}) ){ print ",$nc_count{$_}"; }else{ print ",0"; }
   print "\n";
}
exit;
