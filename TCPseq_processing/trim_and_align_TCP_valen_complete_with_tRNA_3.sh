#!/bin/bash

#AG 25/04/18
#script to process genomic non paired fastq datasets 
#1 Trim adaptor
#2 Remove PhiX
#3 Remove rRNA
#4 Remove organism specific ncRNA (zebrafish does not contain tRNA)
#5 Remove organism specific tRNA (from GtRNAdb, zebrafish does not include iMet subgroup)
#6 Map to organism specific reference

#PATH=$PATH:/Home/ii/adamg/bin/
#top2=/net/apps/cbu/stow/tophat-2.0.13/bin/tophat2 
top2=/Home/ii/adamg/bin/tophat2

usage(){
cat << EOF
usage: $0 options

script to trim and align riboseq fastq datasets 

OPTIONS:
	-f	path to directory containing input fastq files (gzipped)
	-o	path to output dir
	-s	genome - one of:
			GRCz10	(GRCz10_zebrabish)
			Zv9		(Zv9_zebrafish)
			GRCm38	(GRCm38_mouse)
			GRCh38	(GRCh38_human)
			R64_1_1	(R64_1_1_yeast)
			BDGP6	(BDGP6_fruitfly)
	-a	path to file containing adaptor sequences (defaults to those I have seen in other riboseq datasets)
	-h	this help message

example usage: trim_and_align.sh -f <in.fasta.dir> -o <out_dir> -s Zv9

EOF
}

while getopts ":f:o:s:a:h" opt; do
	case $opt in 
		f)
			in_dir=$OPTARG
		  	echo "-f input folder $OPTARG"
		  	;;
        o)
            out_dir=$OPTARG
            echo "-o output folder $OPTARG"
            ;;
        s)
            species=$OPTARG
            echo "-s genome $OPTARG"
            ;;
		a)	
			adapt=$OPTARG
			echo "-a adaptor file $OPTARG"
			;;
		h)
			usage
			exit
			;;
		?) 
			echo "Invalid option: -$OPTARG"
			usage
			exit 1
		  	;;
	esac
done

if [ ! -e $in_dir ]  || [ ! $out_dir ] || [ ! $species ]; then
		echo "something's missing - check your command lines args"
        usage
        exit 1
fi

if [ ! -d $out_dir ]; then
    mkdir $out_dir
fi

phix=/export/valenfs/data/references/phiX/phiX
rRNA=/export/valenfs/data/references/rrna/SILVA_119_bothSURef

if [ $species == "GRCz10" ]; then
    ncRNA=/export/valenfs/data/references/Zv10_zebrafish/ncrna_edited/Danio_rerio.GRCz10.ncrna
    genome=/export/valenfs/data/references/Zv10_zebrafish/bowtie2_index/Danio_rerio.GRCz10
    transome=/export/valenfs/data/references/Zv10_zebrafish/tophat2_transcriptome/Danio_rerio.GRCz10
    tRNAome=//export/valenfs/data/references/Zv10_zebrafish/tRNAscan-SE-2.0/GRcZ10_tRNA_scan_output
elif [ $species == "R64_1_1" ]; then
    ncRNA=/export/valenfs/data/references/R64_1_1_yeast/ncrna_edited/Saccharomyces_cerevisiae.R64-1-1.ncrna
    genome=/export/valenfs/data/references/R64_1_1_yeast/bowtie2_index/Saccharomyces_cerevisiae.R64-1-1.dna.toplevel
    transome=/export/valenfs/data/references/R64_1_1_yeast/tophat2_transcriptome/Saccharomyces_cerevisiae.R64-1-1.79
    tRNAome=/export/valenfs/data/references/R64_1_1_yeast/tRNAscan-SE-2.0/R64_tRNA_scan_output
else 
	echo "invalid species selected : $species"
	usage
	exit 1
fi

#if [ ! $adapt ]; then
#	echo "using default adaptor file"
#	adapt=/export/valenfs/data/references/adaptor_files/riboseq_adaptor_list.fa
#fi

for file in ${in_dir}/*.fastq.gz; do

	name=$(basename $file)
	prefix=${name%.fastq.gz}
	echo $prefix

	#------------------------------------------------------------------------------------------
	#1 Trim adaptors
	#------------------------------------------------------------------------------------------
	#-n 4 = 4 iterations of adaptor removal
	#-e 0.1 = error rate, 1 missmatch per 10 bp
	#-O 5 = minimium overlap with an adaptor sequence
	#-m -15 = discard reads shorter than 20 bp
    #-u 3 = trim the first 3 bases

	if [ ! -d ${out_dir}/trim ]; then
		mkdir ${out_dir}/trim
	fi

#	cutadapt -u 3 -a "AAAAAAAAAA" -n 1 -e 0.1 -O 5 -m 15 -o ${out_dir}/trim/${prefix}_trim.fastq.gz $file #zebrafish (trim first 3nt)
    #cutadapt -a "AAAAAAAAAA" -n 1 -e 0.1 -O 5 -m 15 -o ${out_dir}/trim/${prefix}_trim.fastq.gz $file #yeast (don't trim)

    #------------------------------------------------------------------------------------------
    #2 Remove PhiX
    #------------------------------------------------------------------------------------------
    #-p 8 = 8 processors
    #-N 1 = allow 1 missmatch in seed
    #-L 15 = use 15bp seeds

    if [ ! -d ${out_dir}/phix_depletion ]; then
        mkdir ${out_dir}/phix_depletion
    fi

    if [ ! -d ${out_dir}/phix_match ]; then
        mkdir ${out_dir}/phix_match
    fi

#    bowtie2 -p 16 -N 1 -L 15 --un-gz ${out_dir}/phix_depletion/${prefix}_no_phix.fastq.gz -x $phix -U ${out_dir}/trim/${prefix}_trim.fastq.gz | samtools view -bSu - | samtools sort - ${out_dir}/phix_match/${prefix}_phix
	
	#------------------------------------------------------------------------------------------
	#3 Remove rRNA
	#------------------------------------------------------------------------------------------
	#-p 8 = 8 processors
	#-N 1 = allow 1 missmatch in seed
	#-L 15 = use 15bp seeds

	if [ ! -d ${out_dir}/rRNA_depletion ]; then
		mkdir ${out_dir}/rRNA_depletion
	fi

	if [ ! -d ${out_dir}/rRNA_match ]; then
		mkdir ${out_dir}/rRNA_match
	fi

#	bowtie2 -p 16 -N 1 -L 15 --un-gz ${out_dir}/rRNA_depletion/${prefix}_no_rRNA.fastq.gz -x $rRNA -U ${out_dir}/phix_depletion/${prefix}_no_phix.fastq.gz | samtools view -bSu - | samtools sort - ${out_dir}/rRNA_match/${prefix}_rRNA

        #------------------------------------------------------------------------------------------
        #4 Remove organism specific tRNA
        #------------------------------------------------------------------------------------------
        #-p 8 = use 8 processors
        #-N 1 = allow 1 missmatch in seed
        #-L 15 = use 15bp seeds

        if [ ! -d ${out_dir}/tRNAscan_depletion_$species ]; then
            mkdir ${out_dir}/tRNAscan_depletion_$species
        fi

        if [ ! -d ${out_dir}/tRNAscan_match_$species ]; then
            mkdir ${out_dir}/tRNAscan_match_$species
        fi

        bowtie2 -p 8 -N 1 -L 15 --un-gz ${out_dir}/tRNAscan_depletion_${species}/${prefix}_no_tRNA.fastq.gz -x $tRNAome -U ${out_dir}/rRNA_depletion/${prefix}_no_rRNA.fastq.gz | samtools view -bSu - | samtools sort - ${out_dir}/tRNAscan_match_${species}/${prefix}_tRNA

	#------------------------------------------------------------------------------------------
	#5 Remove organism specific ncRNA
	#------------------------------------------------------------------------------------------
	#-p 8 = use 8 processors
	#-N 1 = allow 1 missmatch in seed
	#-L 15 = use 15bp seeds

	if [ ! -d ${out_dir}/ncRNA_depletion_$species ]; then
		mkdir ${out_dir}/ncRNA_depletion_$species
	fi

	if [ ! -d ${out_dir}/ncRNA_match_$species ]; then
		mkdir ${out_dir}/ncRNA_match_$species
	fi

#   bowtie2 -p 16 -N 1 -L 15 --un-gz ${out_dir}/ncRNA_depletion_${species}/${prefix}_no_ncRNA.fastq.gz -x $ncRNA -U ${out_dir}/tRNA_depletion_${species}/${prefix}_no_tRNA.fastq.gz | samtools view -bSu - | samtools sort - ${out_dir}/ncRNA_match_${species}/${prefix}_ncRNA

	#------------------------------------------------------------------------------------------
	#6 Map to organism specfic reference
	#------------------------------------------------------------------------------------------
	#-g 1 = max genome multihits 1
	#-x 1 = max transcriptome multihits 1
	#--prefilter-multihits = don't pass multihits to transcriptome
	#-p 8 = use 8 processors
	#--transcriptome-index = use pre built transriptome (from tophat2 -G <<org.gtf>> --transcriptome-index=<<org.trans>>)

	if [ ! -d ${out_dir}/aligned_${species}_tidy_tRNA ]; then
		mkdir ${out_dir}/aligned_${species}_tidy_tRNA
	fi

	if [ ! -d ${out_dir}/aligned_${species}_tidy_tRNA/$prefix ]; then
		mkdir ${out_dir}/aligned_${species}_tidy_tRNA/$prefix
    fi
 
#   $top2 --transcriptome-index=$transome -g 1 -x 1 -p 16 -o ${out_dir}/aligned_${species}_tidy_tRNA/$prefix $genome ${out_dir}/ncRNA_depletion_${species}/${prefix}_no_ncRNA.fastq.gz

done
