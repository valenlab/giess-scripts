#!/bin/bash

#--#

gtf_yeast=/export/valenfs/data/references/R64_1_1_yeast/Saccharomyces_cerevisiae.R64-1-1.79.gtf
fa_yeast=/export/valenfs/data/references/R64_1_1_yeast/Saccharomyces_cerevisiae.R64-1-1.dna.toplevel.fa
leaders_yeast=~/analysis/R_working/Cage_r_R64_1_1/TCPseq_leaders_Jan2018/TCP_matrix/matrix_yeast_leaders.csv

perl /export/valenfs/projects/adam/final_results/scripts/find_all_uORF_and_dORF.pl $gtf_yeast $fa_yeast $leaders_yeast /export/valenfs/projects/adam/TCP_seq/archer_yeast_2016/peaks_removed_tRNA/WT_RS_peaks_removed.bam /export/valenfs/projects/adam/TCP_seq/archer_yeast_2016/peaks_removed_tRNA/WT_SSU_peaks_removed.bam /Home/ii/adamg/analysis/TCP/Hakon_ORFs/uORF_dORF_stats/yeast/

echo "yeast reafy"

#--#

gtf=/export/valenfs/data/references/Zv10_zebrafish/Danio_rerio.GRCz10.81.gtf
fa=/export/valenfs/data/references/Zv10_zebrafish/Danio_rerio.GRCz10.dna.toplevel.fa
leaders_shield=~/analysis/R_working/Cage_r_zv10/TCPseq_leaders_Jan2018/TCP_matrix_kozak_trailer_update_histones_directional/matrix_zebrafish_shield_leaders.csv
leaders_1k=~/analysis/R_working/Cage_r_zv10/TCPseq_leaders_Jan2018/TCP_matrix_kozak_trailer_update_histones_directional/matrix_zebrafish_1kCell_leaders.csv

perl /export/valenfs/projects/adam/final_results/scripts/find_all_uORF_and_dORF.pl $gtf $fa $leaders_1k /export/valenfs/projects/adam/TCP_seq/valen_1_2_merge_15nt/tidy_alignments/1kCell/peaks_removed_tRNA/40S_merged_1kcell_peaks_removed.bam /export/valenfs/projects/adam/TCP_seq/valen_1_2_merge_15nt/tidy_alignments/1kCell/peaks_removed_tRNA/80S_merged_1kcell_peaks_removed.bam /Home/ii/adamg/analysis/TCP/Hakon_ORFs/uORF_dORF_stats/1kcell/

echo "1Kcell ready"

perl /export/valenfs/projects/adam/final_results/scripts/find_all_uORF_and_dORF.pl $gtf $fa $leaders_shield /export/valenfs/projects/adam/TCP_seq/valen_1_2_merge_15nt/tidy_alignments/bud/peaks_removed_tRNA/40S_merged_bud_peaks_removed.bam /export/valenfs/projects/adam/TCP_seq/valen_1_2_merge_15nt/tidy_alignments/bud/peaks_removed_tRNA/80S_merged_bud_peaks_removed.bam /Home/ii/adamg/analysis/TCP/Hakon_ORFs/uORF_dORF_stats/bud/

echo "Bud ready"

perl /export/valenfs/projects/adam/final_results/scripts/find_all_uORF_and_dORF.pl $gtf $fa $leaders_shield /export/valenfs/projects/adam/TCP_seq/valen5/peaks_removed_tRNA/SSU_fractions_13_14_trim3_15nt_tidy_peaks_removed.bam /export/valenfs/projects/adam/TCP_seq/valen5/peaks_removed_tRNA/LSU_fractions_18_19_trim3_15nt_tidy_peaks_removed.bam /Home/ii/adamg/analysis/TCP/Hakon_ORFs/uORF_dORF_stats/sheild/

echo "Shield ready"

perl /export/valenfs/projects/adam/final_results/scripts/find_all_uORF_and_dORF.pl $gtf $fa $leaders_shield /export/valenfs/projects/adam/TCP_seq/valen_4/peaks_removed_tRNA/2-ribo-zero-48S-12_S2_R1_001_peaks_removed.bam /export/valenfs/projects/adam/TCP_seq/valen_4/peaks_removed_tRNA/4-ribo-zero-80S-17_S4_R1_001_peaks_removed.bam /Home/ii/adamg/analysis/TCP/Hakon_ORFs/uORF_dORF_stats/75epi/

echo "75perc epibpdy ready"
