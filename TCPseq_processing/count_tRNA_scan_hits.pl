#!/usr/bin/perl -w
use strict;

#25/04/18
#script to assign FMPK counts to uoRFs
#calculates kozak scores for zebrafish uORFS

my $fasta=$ARGV[0];
my $in_bam=$ARGV[1];  
my ($prefix)=$in_bam=~/([^\/]+).bam$/;

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#open fasta for codon sequeces
my %aa_count;


my %fasta_names2aa; #key = sequence_name, value=amino acid
my $name;
open (FA, $fasta) || die "can't open $fasta";
while (<FA>){
    chomp;

    if (/^>/){ 
        if (/^>([^\s]+)\s([^\s]+)\s([^\s]+)\s([^\s]+)\s/){
            my $name=$1;
            my $amino_acid=$4;
            $fasta_names2aa{$name}=$amino_acid;
            $aa_count{$amino_acid}++;
        }else{
            print "not_parsed: $_\n";
        }
    }
}
close(FA);

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#open BAM and count tRNA hits
my %tRNA_hits;
my $tRNA_match=0;
my $unassigned=0;

my $count_iMET=0;
my $count_MET=0;

open BAM,"samtools view $in_bam |";
while(<BAM>){

    next if(/^(\@)/);  ## skipping the header lines (if you used -h in the samools command)
    s/\n//;  s/\r//;  ## removing new line
    my @sam = split(/\t+/);  ## splitting SAM line into array

    my $leftMost=$sam[3]; #leftmost position of match 5' for fwd, 3' for rev
    my $flag=$sam[1];
    my $chr=$sam[2];
    my $mapq=$sam[4];
    my $cigar=$sam[5];
    my $seq=$sam[9];
    my $threePrime;
    my $fivePrime;

    unless ($flag & 0x4){   #if aligned

#        if ($mapq >= 5){     #mapping uniqnes filter remove a lot of the hits

            $tRNA_match++;

            if ($flag & 0x10){  #Reverse reads. Starting from the leftmost position parse the cigar and check if it matches a uORF

                if (exists($fasta_names2aa{$chr})){
                    $tRNA_hits{$fasta_names2aa{$chr}}++;

                    if ($fasta_names2aa{$chr} eq "iMet"){
                        $count_iMET++;
                    }elsif($fasta_names2aa{$chr} eq "Met"){
                        $count_MET++;
                    }

                }else{
                    $unassigned++;
                }

            }else{ #Forward reads. Starting from the leftmost position parse the cigar and check if it matches a uORF

                if (exists($fasta_names2aa{$chr})){
                    $tRNA_hits{$fasta_names2aa{$chr}}++;

                    if ($fasta_names2aa{$chr} eq "iMet"){
                        $count_iMET++;
                    }elsif($fasta_names2aa{$chr} eq "Met"){
                        $count_MET++;
                    }

                }else{
                    $unassigned++;
                }
            }
#        }
    }
}
close(BAM);

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#for (sort keys %tRNA_hits){
#   print " $_,$aa_count{$_},$tRNA_hits{$_}\n";
#}

#print "tRNA_hits:$tRNA_match\nunassigned:$unassigned\n";

print "$prefix,$tRNA_match,$count_MET,$count_iMET\n";

exit;
