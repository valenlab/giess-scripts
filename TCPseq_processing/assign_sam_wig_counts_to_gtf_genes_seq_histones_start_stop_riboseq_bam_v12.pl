#!/usr/bin/perl -w
use strict;

#07/03/18
#script to assign FMPK counts to leader (CAGE updated), start_codon, CDS, stop_codon, 3'trailer
#calculates kozak scores for zebrafish and yeast
#mark histone genes based on biomart GO terms

my $inGtf=$ARGV[0]; 
my $fasta=$ARGV[1];
my $bam_RIBOseq=$ARGV[2];
my $bam_RNAseq=$ARGV[3];
my $bam_TCPseq_SSU=$ARGV[4];
my $bam_TCPseq_LSU=$ARGV[5];
my $leaders=$ARGV[6]; #from cageR (cage_peaks_assignment_gtf_transcript_coords_for_TCPseq_v8.pl)
my $go=$ARGV[7]; #biomart genes with GO term name and WikiGene description

#excluding:
#genes where the TSS is downstream of the start codon
#genes without a detectable cage peak 
#genes that are annotated as protien_coding

#reads are assigned to leaders/CDS on a stand specific basis (including the RNA-seq) as follows:
#Ribo-seq:  Using a shoelaces shifted bed, exclude reads that overlap TIS and stop codons 
#RNA-seq:   Assign fragments that overlap leader and CDS to both.
#TCP-seq_SSU:   Exclude fragments that overlap the TIS or stop codon.
#TCP-seq_LSU:   Exclude fragments that overlap the TIS or stop codon.

#Reads are normalised by the total number of reads that were found to map to leader, CDS and trailer regions of genes as defined above.

#Flags:
#overlapping_gene: The longest transcript of this gene overlaps with the longest transcript of another gene
#leader_potentially_overlaps_upstream_gene: There is an upstream gene within 500nt of the start codon of this gene 
#gene_potentially_overlaps_downstream_leader: There is an downstream gene start codon withing 500nt of the 3' most position of this gene (in yeast this is the stop codon).

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#Zebrafish Zozak position frequency matrix (PFM) from Grzegorski et. al, 2015                   
#Position   -4  -3  -2  -1  4   5
#A  35  62  39  28  24  27
#C  32  5   23  36  12  42
#G  19  28  17  27  46  16
#T  14  5   21  10  17  15

my %raw_kozak =
   (
       "A" => [ 35, 62, 39, 28, 24, 27 ],
       "C" => [ 32, 5, 23, 36, 12, 42 ],
       "G" => [ 19, 28, 17, 27, 46, 16 ],
       "T" => [ 14, 5, 21, 10, 17, 15 ],
   );

my %PWM; #key1: position, key2: base, value: weight
for my $pos (0 .. 5){

    #0 == -4 #nucleotide position in relation to start codon
    #1 == -3
    #2 == -2
    #3 == -1
    #4 == +4
    #5 == +5

    my $pos_sum=0;
    my $pwm_sum=0;
    for my $base (keys %raw_kozak){ #sum the nucleotide frequencies per position
        $pos_sum+=$raw_kozak{$base}[$pos];
    }

    for my $base(keys %raw_kozak){ #score the PWM
        my $psudo_count= sqrt($pos_sum);
        my $background_probability=0.25; #no base preference
        my $pwm=&log2( ($raw_kozak{$base}[$pos] + $psudo_count * $background_probability) / ($pos_sum + $psudo_count * $background_probability));
        $PWM{$pos}{$base}=$pwm;
        $pwm_sum+=$pwm;
    }

    $PWM{$pos}{"N"}=($pwm_sum/4); #set "N" to be equal to the column mean. For genes with short leaders, missing upstream positions 
} 

#Yeast 2011	The mRNA landscape at yeast translation initiation sites				
#my %yeast_matrix =
#   (            #-4   -3    -2    -1     4     5
#       "A" => [ 0.53, 1.02, 0.47, 0.58, -0.11, -0.6 ],
#       "C" => [ 0.16, -1.16, 0.16, -0.12, -0.47, 1.26 ],
#       "G" => [ -0.3, 0.2, -0.52, -0.15, 0.77, -0.11 ],
#       "T" => [ -0.74, -2.1, -0.49, -0.71, -0.18, -0.75 ],
#   );

my %PWM_yeast =
(
    0 => { 
        "A" => 0.53,
        "C" => 0.16,
        "G" => -0.3,
        "T" => -0.74
    },
    1 => {
        "A" => 1.02,
        "C" => -1.16,
        "G" => 0.2,
        "T" => -2.1
    },
    2 => { 
        "A" => 0.47,
        "C" => 0.16,
        "G" => -0.52,
        "T" => -0.49
    },
    3 => { 
        "A" => 0.58,
        "C" => -0.12,
        "G" => -0.15,
        "T" => -0.71
    },
    4 => {
        "A" => -0.11,
        "C" => -0.47,
        "G" => 0.77,
        "T" => -0.18
    },
    5 => {
        "A" => -0.6,
        "C" => 1.26,
        "G" => -0.11,
        "T" => -0.75
    }
);

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#open gtf and get transcript lengths function
#my %transcripts; #key = gene_id, transcript_id, #value = sum_exon_lengths;
#get_transcript_lengths($inGtf, \%transcripts);

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#open gtf and get transcript lengths
my %transcripts; #key = gene_id, transcript_id, #value = sum_exon_lengths;

open(GENES1,$inGtf) || die "can't open $inGtf";      #gft is 1 based
while (<GENES1>){
    unless(/^#/){
        my @b=split("\t");
        my $chr=$b[0];
        my $class=$b[2];
        my $start=$b[3];
        my $end=$b[4];
        my $dir=$b[6];
        my ($gene_id) = $b[8] =~ /gene_id\s"([^\"]+)";/;
        my ($transcript_id) = $b[8] =~ /transcript_id\s"([^\"]+)";/;
        my $gene_biotype="NA";
        if ($b[8] =~ /gene_biotype\s"([^\"]+)";/){
            $gene_biotype=$1;
        }

        if ($gene_id && $transcript_id){

            if ($gene_biotype eq "protein_coding"){ #restrict to protien coding genes (control for ncRNAs)

                if ($class eq "exon"){
                    if ($dir eq "+"){
                        for ($start .. $end){
                            $transcripts{$gene_id}{$transcript_id}++;
                        }
                    }else{
                        for ($start .. $end){
                            $transcripts{$gene_id}{$transcript_id}++;
                        }
                    }
                }
            }
        }
    }
}
close (GENES1);

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#select longest transcripts per gene
my %longest_transcript; #key=gene_id, value=transcript_id

for my $gene (keys %transcripts){
    my $longest=0;
    for my $transcript (keys %{ $transcripts{$gene}} ){
        if ($transcripts{$gene}{$transcript} > $longest) {
            $longest_transcript{$gene}=$transcript;
            $longest=$transcripts{$gene}{$transcript};
        }
    }
}

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#second pass through the genome, find annotated start codons and setup transcript models for longest transcript of each gene
my %gene_start_codon_fwd;
my %gene_stop_codon_fwd;
my %gene_exons_fwd;
my %gene_start_codon_rev;
my %gene_stop_codon_rev;
my %gene_exons_rev;
my %gene_2_chr; #key = gene_id; value = chr

open(GENES2,$inGtf) || die "can't open $inGtf";      #gft is 1 based
while (<GENES2>){
    unless(/^#/){
        my @b=split("\t");
        my $chr=$b[0];
        my $class=$b[2];
        my $start=$b[3];
        my $end=$b[4];
        my $dir=$b[6];
        my ($gene_id) = $b[8] =~ /gene_id\s"([^\"]+)";/;
        my ($transcript_id) = $b[8] =~ /transcript_id\s"([^\"]+)";/;

        if ($gene_id && $transcript_id){

            if (exists ( $longest_transcript{$gene_id} )){    #if the transcript is in the list of longest transcripts

                if ($transcript_id eq $longest_transcript{$gene_id}){

                    $gene_2_chr{$gene_id}=$chr;

                    if ($dir eq "+"){ #fwd cases. Use start positions as 5'

                        if ($class eq "start_codon"){
                            $gene_start_codon_fwd{$gene_id}=$start;
                        }
                        if ($class eq "stop_codon"){
                            $gene_stop_codon_fwd{$gene_id}=$start;
                        }
                        if ($class eq "exon"){
                            $gene_exons_fwd{$gene_id}{$start}=$end;
                        }

                    }else{ #revese cases use end as 5'

                        if ($class eq "start_codon"){
                            $gene_start_codon_rev{$gene_id}=$end;
                        }
                        if ($class eq "stop_codon"){
                            $gene_stop_codon_rev{$gene_id}=$end;
                        }
                        if ($class eq "exon"){
                            $gene_exons_rev{$gene_id}{$start}=$end;
                        }
                    }
                }
            }
        }
    }
}
close(GENES2);

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#open fasta for codon sequeces

my %fasta_sequences; #key = sequence_name, value=sequence
my $name;
open (FA, $fasta) || die "can't open $fasta";
while (<FA>){
    chomp;
    if (/^>([^\s]+)/){ #take header up to the first space
        $name=$1;
        if ($name =~ /^chr(.*)/){
           $name=$1; #if the chr name have a chr* prefix, remove it 
        }
    }else{
        $fasta_sequences{$name}.=$_;
    }
}
close(FA);

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#Store histone genes
my %histone; #key=gene_id; value = 1

open (GO, $go) || die "can't open $go";
while (<GO>){
   chomp();
   my @lines=split(",");

    my $gene=$lines[0];
    my $go_terms=$lines[3];
    my $wiki_desc=$lines[13];

    if ($gene){
        if ($go_terms){
            if ($go_terms =~ /^nucleosome\sassembly$/i){
                $histone{$gene}=1;
            }elsif ($go_terms =~ /^nucleosome$/i){
                $histone{$gene}=1;    
            }
        }
        if ($wiki_desc){
            if ($wiki_desc =~ /^histone\sfamily$/i){
                $histone{$gene}=1;
            }
        }
    }
}
close(GO);

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#setup trascript models

my %gene_model_fwd;
my %start_coord_fwd;
my %stop_coord_fwd;

#5' is the #gene_model{$gene}{0}
#3' is the last coord
my %three_prime_most_coord_fwd;

#gene_model{$gene}{0}=12345   #1st nt of start codon
#gene_model{$gene}{1}=12346
#gene_model{$gene}{2}=12347
#gene_model{$gene}{3}=12348
#gene_model{$gene}{4}=12349
#...
#to end of exons             #last nt of stop codon

for my $gene (keys %gene_exons_fwd){
    if ( (exists ($gene_start_codon_fwd{$gene})) && (exists ($gene_stop_codon_fwd{$gene})) ) { #restrict to genes with annotated start + stop codon

        my $model_pos=0;

        for my $exon_start (sort {$a <=> $b} keys %{ $gene_exons_fwd{$gene} } ){
            my $exon_end=$gene_exons_fwd{$gene}{$exon_start};

            #fwd exons are in ascending order
            # start(-1)-> 100958 100975
            #             101077 101715 <-end(+1)

            for ($exon_start .. $exon_end){
                $gene_model_fwd{$gene}{$model_pos}=$_;

                if ($_ == $gene_stop_codon_fwd{$gene}){
                    $stop_coord_fwd{$gene}=$model_pos;    #find the index of the stop codon per gene
                }

                if ($_ == $gene_start_codon_fwd{$gene}){
                    $start_coord_fwd{$gene}=$model_pos;    #find the index of the start codon per gene
                }
                $model_pos++;
            }
        }
        $three_prime_most_coord_fwd{$gene}=$model_pos-1; #store the 3 prime most position of each gene
    }
}

my %gene_model_rev;
my %start_coord_rev;
my %stop_coord_rev;

#5' is the #gene_model{$gene}{0}
#3' is the last coord
my %three_prime_most_coord_rev;

for my $gene (keys %gene_exons_rev){
    if ( (exists ($gene_start_codon_rev{$gene})) && (exists ($gene_stop_codon_rev{$gene})) ) { #restrict to genes with annotated start + stop codon

        my $model_pos=0;

        for my $exon_end (reverse (sort {$a <=> $b} keys %{ $gene_exons_rev{$gene} } )){
            my $exon_start=$gene_exons_rev{$gene}{$exon_end};

            #rev exons are sorted in decending order  
            #           447087 447794 <-start(+1)
            # end(-1)-> 446060 446254

            while ($exon_start >= $exon_end){
                $gene_model_rev{$gene}{$model_pos}=$exon_start;

                if ($exon_start == $gene_stop_codon_rev{$gene}){
                    $stop_coord_rev{$gene}=$model_pos;    #find the index of the stop codon per gene
                }
                if ($exon_start == $gene_start_codon_rev{$gene}){
                    $start_coord_rev{$gene}=$model_pos;    #find the index of the start codon per gene
                }
                $model_pos++;
                $exon_start--;
            }
        }
        $three_prime_most_coord_rev{$gene}=$model_pos-1; #store the 3 prime most position of each gene
    }
}

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#parse leaders
my %leader_positions_fwd;
my %leader_positions_rev;

#filters:
my %leader_length;
my %cage_peak_value;
my %overlaps_inframe_gene;
my %leader_overlaps_upstream;
my %gene_overlaps_downstream_leader;

#gene_id,       0
#transcript_id, 1
#chr,           2
#dir,           3
#overlaps_inframe_gene,           4
#leader_overlaps_upstream,        5
#gene_overlaps_downstream_leader, 6
#highest_cage_peak,               7
#count_at_highest_cage_peak,      8
#leader_length                    9

#ENSDARG00000037917,ENSDART00000161963,3,fwd,FALSE,FALSE,FALSE,34716685,2.30440468389324,143
#ENSDARG00000104069,ENSDART00000167982,5,fwd,FALSE,FALSE,FALSE,337237,9.98331428397882,122
#ENSDARG00000037925,ENSDART00000130591,3,fwd,FALSE,FALSE,FALSE,36250098,0.346429817638395,-679
#ENSDARG00000029263,ENSDART00000078466,3,fwd,FALSE,FALSE,FALSE,NaN,0,NaN

open(LEAD, $leaders) || die "can't open $leaders";
while (<LEAD>){
    unless(/^#/){
 
        chomp;
        my @b=split(",");

        my $gene=$b[0];
        my $transcript=$b[1];
        my $chr=$b[2];
        my $dir=$b[3];
        my $overlaps_inframe_gene=$b[4];
        my $leader_overlaps_upstream=$b[5];
        my $gene_overlaps_downstream_leader=$b[6];
        my $highest_cage_peak=$b[7];
        my $count_at_highest_cage_peak=$b[8];
        my $leader_length=$b[9];

        if ($overlaps_inframe_gene eq "TRUE"){ $overlaps_inframe_gene{$gene}=1; }
        if ($leader_overlaps_upstream eq "TRUE"){ $leader_overlaps_upstream=1; }
        if ($gene_overlaps_downstream_leader eq "TRUE"){ $gene_overlaps_downstream_leader=1; }

        unless ($leader_length eq "NaN"){  #only take genes that have a detectable cage peak

            unless ($leader_length < 0){  #exlude genes with negative leader sizes, as they cuase problems with FPKM

                if ($dir eq "fwd"){ 
                    if (exists ($start_coord_fwd{$gene})){
                        unless ($highest_cage_peak >=  $gene_model_fwd{$gene}{$start_coord_fwd{$gene}}){  #exclude genes where the TSS is downstream of the start codon
                            $leader_positions_fwd{$gene}=$highest_cage_peak;
                            $cage_peak_value{$gene}=$count_at_highest_cage_peak;
                            $leader_length{$gene}=$leader_length;
                        } 
                    }
                }else{
                    if (exists ($start_coord_rev{$gene})){
                        unless ($highest_cage_peak <=  $gene_model_rev{$gene}{$start_coord_rev{$gene}}){  #exclude genes where the TSS is downstream of the start codon
                            $leader_positions_rev{$gene}=$highest_cage_peak;
                            $cage_peak_value{$gene}=$count_at_highest_cage_peak;
                            $leader_length{$gene}=$leader_length;
                        }
                    }
                }             
            }
        }
    }
}
close(LEAD);

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#extend transcript models to incorportate cage derived leaders
my %leader_start_coord; #key=gene, value=coord

for my $gene (keys %leader_positions_fwd){

    if (exists ($gene_2_chr{$gene})){  #to restict to protien_coding genes
        my $leader_start=$leader_positions_fwd{$gene};
        my $three_prime_coord=$three_prime_most_coord_fwd{$gene};
        my $three_prime_pos=$gene_model_fwd{$gene}{$three_prime_coord};
        my $five_prime_coord=0;
        my $five_prime_pos=$gene_model_fwd{$gene}{$five_prime_coord};

        if ($leader_start >= $five_prime_pos){  #great, just find and save the coordinate
 
            for my $coord ($five_prime_coord .. $three_prime_coord){
                my $pos=$gene_model_fwd{$gene}{$coord};       
                if ($pos == $leader_start){
                    $leader_start_coord{$gene}=$coord;
                    last;
                }
            }

        }else{  #extend the coords

            my $extended_coord=0; 
            while ($five_prime_pos > $leader_start){        
                $extended_coord--;
                $five_prime_pos--;
                $gene_model_fwd{$gene}{$extended_coord}=$five_prime_pos;
            }
            $leader_start_coord{$gene}=$extended_coord;
        }
    }
} 

for my $gene (keys %leader_positions_rev){

    if (exists ($gene_2_chr{$gene})){  #to restict to protien_coding genes
        my $leader_start=$leader_positions_rev{$gene};
        my $three_prime_coord=$three_prime_most_coord_rev{$gene};
        my $three_prime_pos=$gene_model_rev{$gene}{$three_prime_coord};
        my $five_prime_coord=0;
        my $five_prime_pos=$gene_model_rev{$gene}{$five_prime_coord};
 
        if ($leader_start <= $five_prime_pos){  #great, just find and save the coordinate

            for my $coord ($five_prime_coord .. $three_prime_coord){
                my $pos=$gene_model_rev{$gene}{$coord};
                if ($pos == $leader_start){
                    $leader_start_coord{$gene}=$coord;
                    last;
                }
            }

        }else{   #extend the coords

            my $extended_coord=0;
            while ($five_prime_pos < $leader_start){
                $extended_coord--;
                $five_prime_pos++;
                $gene_model_rev{$gene}{$extended_coord}=$five_prime_pos;
            }
            $leader_start_coord{$gene}=$extended_coord;
        }
    }
}

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#Transcript coord key
#Transcript 5' coord   0                                  #1st nt of annotated transcript
#Transcript 3' coord   $three_prime_most_coord_???{$gene} #last nt of transcript
#Start codon coord     $start_coord_???{$gene}            #1st nt in start codon
#Stop codon coord      $stop_coord_???{$gene}             #1st nt in stop codon
#Leader start coord    $leader_start_coord{$gene}         #1st nt of cage defined leader

#$gene_model_fwd{$gene}{$coord}==genomic position

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#loop though genes and assign leader, CDS and trailer regions to hashes for quick searching. (added start + stop codons).
my %leader_search_fwd; #key1=chr, key2=pos, value=gene
my %leader_search_rev; #key1=chr, key2=pos, value=gene
my %CDS_search_fwd; #key1=chr, key2=pos, value=gene
my %CDS_search_rev; #key1=chr, key2=pos, value=gene
my %trailer_search_fwd; #key1=chr, key2=pos, value=gene
my %trailer_search_rev; #key1=chr, key2=pos, value=gene

my %start_codons_search_fwd; #key1=chr, key2=pos, value=gene
my %start_codons_search_rev; #key1=chr, key2=pos, value=gene
my %stop_codons_search_fwd; #key1=chr, key2=pos, value=gene
my %stop_codons_search_rev; #key1=chr, key2=pos, value=gene
 
my %CDS_counts_RIBOseq; #key = gene, value = count sum
my %leader_counts_RIBOseq; #key, value = count sum
my %trailer_counts_RIBOseq; #key, value = count sum
my %start_codon_counts_RIBOseq;
my %stop_codon_counts_RIBOseq;

my %CDS_counts_RNAseq; 
my %leader_counts_RNAseq; 
my %trailer_counts_RNAseq;
my %start_codon_counts_RNAseq;
my %stop_codon_counts_RNAseq;

my %CDS_counts_TCPseq_SSU; 
my %leader_counts_TCPseq_SSU;
my %trailer_counts_TCPseq_SSU;
my %start_codon_counts_TCPseq_SSU;
my %stop_codon_counts_TCPseq_SSU;

my %CDS_counts_TCPseq_LSU;
my %leader_counts_TCPseq_LSU;
my %trailer_counts_TCPseq_LSU;
my %start_codon_counts_TCPseq_LSU;
my %stop_codon_counts_TCPseq_LSU;

for my $gene (keys %gene_model_fwd){
    my $chr=$gene_2_chr{$gene};
    my $start_coord=$start_coord_fwd{$gene};
    my $stop_coord=$stop_coord_fwd{$gene};

    $CDS_counts_RIBOseq{$gene}=0;
    $leader_counts_RIBOseq{$gene}=0;
    $trailer_counts_RIBOseq{$gene}=0; 
    $start_codon_counts_RIBOseq{$gene}=0;
    $stop_codon_counts_RIBOseq{$gene}=0;

    $CDS_counts_RNAseq{$gene}=0; 
    $leader_counts_RNAseq{$gene}=0; 
    $trailer_counts_RNAseq{$gene}=0;
    $start_codon_counts_RNAseq{$gene}=0;
    $stop_codon_counts_RNAseq{$gene}=0;

    $CDS_counts_TCPseq_SSU{$gene}=0;
    $leader_counts_TCPseq_SSU{$gene}=0;
    $trailer_counts_TCPseq_SSU{$gene}=0;
    $start_codon_counts_TCPseq_SSU{$gene}=0;
    $stop_codon_counts_TCPseq_SSU{$gene}=0;

    $CDS_counts_TCPseq_LSU{$gene}=0;
    $leader_counts_TCPseq_LSU{$gene}=0;
    $trailer_counts_TCPseq_LSU{$gene}=0;
    $start_codon_counts_TCPseq_LSU{$gene}=0;
    $stop_codon_counts_TCPseq_LSU{$gene}=0;

    for my $coord (sort {$a <=> $b} keys %{ $gene_model_fwd{$gene} } ){
        my $pos=$gene_model_fwd{$gene}{$coord};

        if ($coord == $start_coord){   $start_codons_search_fwd{$chr}{$pos}=$gene; } 
        if ($coord == $start_coord+1){ $start_codons_search_fwd{$chr}{$pos}=$gene; }
        if ($coord == $start_coord+2){ $start_codons_search_fwd{$chr}{$pos}=$gene; }

        if ($coord == $stop_coord){   $stop_codons_search_fwd{$chr}{$pos}=$gene; }
        if ($coord == $stop_coord+1){ $stop_codons_search_fwd{$chr}{$pos}=$gene; }
        if ($coord == $stop_coord+2){ $stop_codons_search_fwd{$chr}{$pos}=$gene; }
           
        if ($coord < $start_coord){
            $leader_search_fwd{$chr}{$pos}=$gene;
        }elsif($coord <= ($stop_coord+2)){  #limit to the stop codon
            $CDS_search_fwd{$chr}{$pos}=$gene;
        }elsif( $coord > ($stop_coord+2)){
            $trailer_search_fwd{$chr}{$pos}=$gene;
        }
    }
}

for my $gene (keys %gene_model_rev){
    my $chr=$gene_2_chr{$gene};
    my $start_coord=$start_coord_rev{$gene};
    my $stop_coord=$stop_coord_rev{$gene};

    $CDS_counts_RIBOseq{$gene}=0;    
    $leader_counts_RIBOseq{$gene}=0;
    $trailer_counts_RIBOseq{$gene}=0;
    $start_codon_counts_RIBOseq{$gene}=0;
    $stop_codon_counts_RIBOseq{$gene}=0;

    $CDS_counts_RNAseq{$gene}=0;
    $leader_counts_RNAseq{$gene}=0;
    $trailer_counts_RNAseq{$gene}=0;
    $start_codon_counts_RNAseq{$gene}=0;
    $stop_codon_counts_RNAseq{$gene}=0;

    $CDS_counts_TCPseq_SSU{$gene}=0;
    $leader_counts_TCPseq_SSU{$gene}=0;
    $trailer_counts_TCPseq_SSU{$gene}=0;
    $start_codon_counts_TCPseq_SSU{$gene}=0;
    $stop_codon_counts_TCPseq_SSU{$gene}=0;

    $CDS_counts_TCPseq_LSU{$gene}=0;
    $leader_counts_TCPseq_LSU{$gene}=0;
    $trailer_counts_TCPseq_LSU{$gene}=0;
    $start_codon_counts_TCPseq_LSU{$gene}=0;
    $stop_codon_counts_TCPseq_LSU{$gene}=0;

    for my $coord (sort {$a <=> $b} keys %{ $gene_model_rev{$gene} } ){
        my $pos=$gene_model_rev{$gene}{$coord};

        if ($coord == $start_coord){   $start_codons_search_rev{$chr}{$pos}=$gene; }
        if ($coord == $start_coord+1){ $start_codons_search_rev{$chr}{$pos}=$gene; }
        if ($coord == $start_coord+2){ $start_codons_search_rev{$chr}{$pos}=$gene; }

        if ($coord == $stop_coord){   $stop_codons_search_rev{$chr}{$pos}=$gene; }
        if ($coord == $stop_coord+1){ $stop_codons_search_rev{$chr}{$pos}=$gene; }
        if ($coord == $stop_coord+2){ $stop_codons_search_rev{$chr}{$pos}=$gene; }

        if ($coord < $start_coord){
            $leader_search_rev{$chr}{$pos}=$gene;
        }elsif($coord <= ($stop_coord+2)){
            $CDS_search_rev{$chr}{$pos}=$gene;
        }elsif( $coord > ($stop_coord+2)){
            $trailer_search_rev{$chr}{$pos}=$gene;
        }
    }
}

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#open riboseq and assign
#my $total_RIBOseq_wig_count_fwd=0;
#my $total_RIBOseq_wig_count_rev=0;
my $total_RIBOseq_bam_count=0;

open BAM0,"samtools view $bam_RIBOseq |";
while(<BAM0>){

    next if(/^(\@)/);  ## skipping the header lines (if you used -h in the samools command)
    s/\n//;  s/\r//;  ## removing new line
    my @sam = split(/\t+/);  ## splitting SAM line into array

    my $leftMost=$sam[3]; #leftmost position of match 5' for fwd, 3' for rev
    my $flag=$sam[1];
    my $chr=$sam[2];
    my $mapq=$sam[4];
    my $cigar=$sam[5];
    my $CDS_hit=0;
    my $leader_hit=0;
    my $trailer_hit=0;
    my $start_hit=0;
    my $stop_hit=0;

    if ($chr=~/chr(.*)/){ #ensembl chromosome names do not contain the "chr" prefix
        $chr=$1;
    }

    unless ($flag & 0x4){   #if aligned

        #both chew_2013 and subtelney_2014 riboseq are directional
        if ($flag & 0x10){  #Reverse reads. Starting from the leftmost position parse the cigar and check if matching positions overlap leaders or cds's

            while ($cigar !~ /^$/){
                if ($cigar =~ /^([0-9]+[MIDN])/){
                    my $cigar_part = $1;
                    if ($cigar_part =~ /(\d+)M/){   #alignment matching 
                        for my $pos ($leftMost .. ($leftMost+$1-1)){ #search though this position

                            if (exists ($start_codons_search_rev{$chr}{$pos})){
                                $start_hit=$start_codons_search_rev{$chr}{$pos};
                                last;
                            }elsif (exists ($stop_codons_search_rev{$chr}{$pos})){
                                $stop_hit=$stop_codons_search_rev{$chr}{$pos};
                                last;
                            }

                            if (exists ( $leader_search_rev{$chr}{$pos})){ 
                                $leader_hit=$leader_search_rev{$chr}{$pos};
                            }elsif( exists($CDS_search_rev{$chr}{$pos})){
                                $CDS_hit=$CDS_search_rev{$chr}{$pos};
                            }elsif( exists($trailer_search_rev{$chr}{$pos})){
                                $trailer_hit=$trailer_search_rev{$chr}{$pos};
                            }

                            #if (exists ($start_codons_search_fwd{$chr}{$pos})){
                            #    $start_hit=$start_codons_search_fwd{$chr}{$pos};
                            #    last;
                            #}elsif (exists ($stop_codons_search_fwd{$chr}{$pos})){
                            #    $stop_hit=$stop_codons_search_fwd{$chr}{$pos};
                            #    last;
                            #}

                            #if (exists ( $leader_search_fwd{$chr}{$pos})){   
                            #    $leader_hit=$leader_search_fwd{$chr}{$pos};
                            #}elsif( exists($CDS_search_fwd{$chr}{$pos})){
                            #    $CDS_hit=$CDS_search_fwd{$chr}{$pos};
                            #}elsif( exists($trailer_search_fwd{$chr}{$pos})){
                            #    $trailer_hit=$trailer_search_fwd{$chr}{$pos};
                            #}
                        }
                        $leftMost+=$1;
                    } elsif ($cigar_part =~ /(\d+)I/){  #insertion (to the reference) #do nothing this region is not in the reference
                    } elsif ($cigar_part =~ /(\d+)D/){  #deletion (from the reference)
                        $leftMost+=$1; #skip this position. Add to position count but do not search
                    } elsif ($cigar_part =~ /(\d+)N/){  #Skipped region from the reference
                        $leftMost+=$1; #skip this position. Add to position count but do not search  
                    }
                    $cigar =~ s/$cigar_part//;
                }
            }

        }else{ #Forward reads. Starting from the leftmost position parse the cigar and check if matching positions overlap leaders or cds's
             while ($cigar !~ /^$/){
                if ($cigar =~ /^([0-9]+[MIDN])/){
                    my $cigar_part = $1;
                    if ($cigar_part =~ /(\d+)M/){   #alignment matching 
                        for my $pos ($leftMost .. ($leftMost+$1-1)){ #search though this position

                            if (exists ($start_codons_search_fwd{$chr}{$pos})){
                                $start_hit=$start_codons_search_fwd{$chr}{$pos};
                                last;
                            }elsif (exists ($stop_codons_search_fwd{$chr}{$pos})){
                                $stop_hit=$stop_codons_search_fwd{$chr}{$pos};
                                last;
                            }

                            if (exists ( $leader_search_fwd{$chr}{$pos})){   
                                $leader_hit=$leader_search_fwd{$chr}{$pos};
                            }elsif( exists($CDS_search_fwd{$chr}{$pos})){
                                $CDS_hit=$CDS_search_fwd{$chr}{$pos};
                            }elsif( exists($trailer_search_fwd{$chr}{$pos})){
                                $trailer_hit=$trailer_search_fwd{$chr}{$pos};
                            }

                            #if (exists ($start_codons_search_rev{$chr}{$pos})){
                            #    $start_hit=$start_codons_search_rev{$chr}{$pos};
                            #    last;
                            #}elsif (exists ($stop_codons_search_rev{$chr}{$pos})){
                            #    $stop_hit=$stop_codons_search_rev{$chr}{$pos};
                            #    last;
                            #}

                            #if (exists ( $leader_search_rev{$chr}{$pos})){  
                            #    $leader_hit=$leader_search_rev{$chr}{$pos};
                            #}elsif( exists($CDS_search_rev{$chr}{$pos})){
                            #    $CDS_hit=$CDS_search_rev{$chr}{$pos};
                            #}elsif( exists($trailer_search_rev{$chr}{$pos})){
                            #    $trailer_hit=$trailer_search_rev{$chr}{$pos};
                            #}
                        }
                        $leftMost+=$1;
                    } elsif ($cigar_part =~ /(\d+)I/){  #insertion (to the reference) #do nothing this region is not in the reference
                    } elsif ($cigar_part =~ /(\d+)D/){  #deletion (from the reference)
                        $leftMost+=$1; #skip this position. Add to position count but do not search
                    } elsif ($cigar_part =~ /(\d+)N/){  #Skipped region from the reference
                        $leftMost+=$1; #skip this position. Add to position count but do not search  
                    }
                    $cigar =~ s/$cigar_part//;
                }
            }
        }

        if ($start_hit){
            $start_codon_counts_RIBOseq{$start_hit}+=1;
            $total_RIBOseq_bam_count++;
        }elsif ($stop_hit){
            $stop_codon_counts_RIBOseq{$stop_hit}+=1;
            $total_RIBOseq_bam_count++;
        }else{
            if ($leader_hit){ #now contains gene name   
                $leader_counts_RIBOseq{$leader_hit}+=1;
                $total_RIBOseq_bam_count++;
            }
            if ($CDS_hit){ #now contains gene name
                 $CDS_counts_RIBOseq{$CDS_hit}+=1;
                 $total_RIBOseq_bam_count++;
            }
            if ($trailer_hit){ #now contains gene name
                $trailer_counts_RIBOseq{$trailer_hit}+=1;
                $total_RIBOseq_bam_count++;
            }
        }
    }
}
close(BAM0);

#my $chr;
#open (WIG1, $wig_RIBOseq_fwd) || die "Can't open $wig_RIBOseq_fwd";
#while (<WIG1>){
#
#    if (/variableStep\schrom=(.*)/){
#        $chr=$1;
#    }else{
#
#        chomp();
#        #split line
#        my @line=split("\t");
#
#        my $pos=$line[0];
#        my $count=$line[1];
#
#        #This is gtf dependant (required for Zv10)
#        if ($chr=~/chr(.*)/){ #ensembl chromosome names do not contain the "chr" prefix
#            $chr=$1;
#        }
#
#        #skip reads that overlap the TIS ans Stop codon (both in CDS region)
#        #this is easy with shifted ribo-seq beds as reads are already at single nucleotide resolution
#        #   :. elsif is ok here as only one of these will be true
#        unless ( (exists ($start_codons_search_fwd{$chr}{$pos})) || (exists ($stop_codons_search_fwd{$chr}{$pos})) ){
#            if (exists ( $leader_search_fwd{$chr}{$pos})){   #is riboseq directional?
#                $leader_counts_RIBOseq{$leader_search_fwd{$chr}{$pos}}+=$count;
#                $total_RIBOseq_wig_count_fwd+=$count;
#            }
#            if( exists($CDS_search_fwd{$chr}{$pos})){
#                $CDS_counts_RIBOseq{$CDS_search_fwd{$chr}{$pos}}+=$count;
#                $total_RIBOseq_wig_count_fwd+=$count;
#            }
#            if( exists($trailer_search_fwd{$chr}{$pos})){
#                $trailer_counts_RIBOseq{$trailer_search_fwd{$chr}{$pos}}+=$count;
#                $total_RIBOseq_wig_count_fwd+=$count;
#            }
#        }else{
#            if (exists ($start_codons_search_fwd{$chr}{$pos})){
#                $start_codon_counts_RIBOseq{$start_codons_search_fwd{$chr}{$pos}}+=$count;
#                $total_RIBOseq_wig_count_fwd+=$count;
#            }elsif (exists ($stop_codons_search_fwd{$chr}{$pos})){
#                $stop_codon_counts_RIBOseq{$stop_codons_search_fwd{$chr}{$pos}}+=$count;
#                $total_RIBOseq_wig_count_fwd+=$count;
#            }
#        }
#    }
#}
#close(WIG1);

#open (WIG2, $wig_RIBOseq_rev) || die "Can't open $wig_RIBOseq_rev";
#while (<WIG2>){
#
#    if (/variableStep\schrom=(.*)/){
#        $chr=$1;
#    }else{
#        chomp();
#        #split line
#        my @line=split("\t");
#
#        my $pos=$line[0];
#        my $count=$line[1];
#
#        #This is gtf dependant (required for Zv10)
#        if ($chr=~/chr(.*)/){ #ensembl chromosome names do not contain the "chr" prefix
#            $chr=$1;
#        }
#
#        #skip reads that overlap the TIS ans Stop codon (both in CDS region)
#        #this is easy with shifted ribo-seq beds as reads are already at single nucleotide resolution
#        #   :. elsif is ok here as only one of these will be true
#        unless ( (exists ($start_codons_search_rev{$chr}{$pos})) || (exists ($stop_codons_search_rev{$chr}{$pos})) ){
#            if (exists ( $leader_search_rev{$chr}{$pos})){   #riboseq is directional so only search in strand
#                $leader_counts_RIBOseq{$leader_search_rev{$chr}{$pos}}+=$count;
#                $total_RIBOseq_wig_count_rev+=$count; 
#            }
#            if( exists($CDS_search_rev{$chr}{$pos})){
#                $CDS_counts_RIBOseq{$CDS_search_rev{$chr}{$pos}}+=$count;
#                $total_RIBOseq_wig_count_rev+=$count; 
#            }
#            if( exists($trailer_search_rev{$chr}{$pos})){
#                 $trailer_counts_RIBOseq{$trailer_search_rev{$chr}{$pos}}+=$count;
#                 $total_RIBOseq_wig_count_rev+=$count; 
#            }
#        }else{
#            if (exists ($start_codons_search_rev{$chr}{$pos})){
#                $start_codon_counts_RIBOseq{$start_codons_search_rev{$chr}{$pos}}+=$count;
#                $total_RIBOseq_wig_count_rev+=$count;
#            }elsif (exists ($stop_codons_search_rev{$chr}{$pos})){
#                $stop_codon_counts_RIBOseq{$stop_codons_search_rev{$chr}{$pos}}+=$count;
#                $total_RIBOseq_wig_count_rev+=$count;
#            }
#        }
#    }
#}
#close(WIG2);

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤t¤
#open and store RNAseq counts
my $total_RNAseq_count=0;

open BAM1,"samtools view $bam_RNAseq |";

while(<BAM1>){

    next if(/^(\@)/);  ## skipping the header lines (if you used -h in the samools command)
    s/\n//;  s/\r//;  ## removing new line
    my @sam = split(/\t+/);  ## splitting SAM line into array

    my $leftMost=$sam[3]; #leftmost position of match 5' for fwd, 3' for rev
    my $flag=$sam[1];
    my $chr=$sam[2];
    my $mapq=$sam[4];
    my $cigar=$sam[5];
    my $CDS_hit=0;
    my $leader_hit=0;
    my $trailer_hit=0;
    #my $start_hit=0; #RNAseq should not be different over start and stop codons
    #my $stop_hit=0;

    if ($chr=~/chr(.*)/){ #ensembl chromosome names do not contain the "chr" prefix
        $chr=$1;
    }

    unless ($flag & 0x4){   #if aligned

#       $total_RNAseq_count++;

        if ($flag & 0x10){  #Reverse reads. Starting from the leftmost position parse the cigar and check if matching positions overlap leaders or cds's

            while ($cigar !~ /^$/){
                if ($cigar =~ /^([0-9]+[MIDN])/){
                    my $cigar_part = $1;
                    if ($cigar_part =~ /(\d+)M/){   #alignment matching 
                        for my $pos ($leftMost .. ($leftMost+$1-1)){ #search though this position

	#trouble here

                            if (exists ( $leader_search_rev{$chr}{$pos})){   #The yeast rnaseq is directional so only search in strand
                                $leader_hit=$leader_search_rev{$chr}{$pos};
                            }elsif( exists($CDS_search_rev{$chr}{$pos})){
                                $CDS_hit=$CDS_search_rev{$chr}{$pos};
                            }elsif( exists($trailer_search_rev{$chr}{$pos})){
                                $trailer_hit=$trailer_search_rev{$chr}{$pos};
                            }

    #allow both srands (i.e.lee total RNA mappoes to the opposite only)

                            if (exists ( $leader_search_fwd{$chr}{$pos})){   #The yeast rnaseq is directional so only search in strand
                                $leader_hit=$leader_search_fwd{$chr}{$pos};
                            }elsif( exists($CDS_search_fwd{$chr}{$pos})){
                                $CDS_hit=$CDS_search_fwd{$chr}{$pos};
                            }elsif( exists($trailer_search_fwd{$chr}{$pos})){
                                $trailer_hit=$trailer_search_fwd{$chr}{$pos};
                            }

                        }
                        $leftMost+=$1;
                    } elsif ($cigar_part =~ /(\d+)I/){  #insertion (to the reference) #do nothing this region is not in the reference
                    } elsif ($cigar_part =~ /(\d+)D/){  #deletion (from the reference)
                        $leftMost+=$1; #skip this position. Add to position count but do not search
                    } elsif ($cigar_part =~ /(\d+)N/){  #Skipped region from the reference
                        $leftMost+=$1; #skip this position. Add to position count but do not search  
                    }
                    $cigar =~ s/$cigar_part//;
                }
            }

        }else{ #Forward reads. Starting from the leftmost position parse the cigar and check if matching positions overlap leaders or cds's
             while ($cigar !~ /^$/){
                if ($cigar =~ /^([0-9]+[MIDN])/){
                    my $cigar_part = $1;
                    if ($cigar_part =~ /(\d+)M/){   #alignment matching 
                        for my $pos ($leftMost .. ($leftMost+$1-1)){ #search though this position
                            if (exists ( $leader_search_fwd{$chr}{$pos})){   #The yeast rnaseq is directional so only search in strand
                                $leader_hit=$leader_search_fwd{$chr}{$pos};
                            }elsif( exists($CDS_search_fwd{$chr}{$pos})){
                                $CDS_hit=$CDS_search_fwd{$chr}{$pos};
                            }elsif( exists($trailer_search_fwd{$chr}{$pos})){
                                $trailer_hit=$trailer_search_fwd{$chr}{$pos};
                            }

    #allow both srands (i.e.lee total RNA mappoes to the opposite only)
    
                            if (exists ( $leader_search_rev{$chr}{$pos})){   #The yeast rnaseq is directional so only search in strand
                                $leader_hit=$leader_search_rev{$chr}{$pos};
                            }elsif( exists($CDS_search_rev{$chr}{$pos})){
                                $CDS_hit=$CDS_search_rev{$chr}{$pos};
                            }elsif( exists($trailer_search_rev{$chr}{$pos})){
                                $trailer_hit=$trailer_search_rev{$chr}{$pos};
                            }



                        }
                        $leftMost+=$1;
                    } elsif ($cigar_part =~ /(\d+)I/){  #insertion (to the reference) #do nothing this region is not in the reference
                    } elsif ($cigar_part =~ /(\d+)D/){  #deletion (from the reference)
                        $leftMost+=$1; #skip this position. Add to position count but do not search
                    } elsif ($cigar_part =~ /(\d+)N/){  #Skipped region from the reference
                        $leftMost+=$1; #skip this position. Add to position count but do not search  
                    }
                    $cigar =~ s/$cigar_part//;
                }
            }
        }

        #a read can be assigned to more than one feature. Should I also increase the total read count when this happends?

        if ($leader_hit){ #now contains gene name
            $leader_counts_RNAseq{$leader_hit}+=1;
            $total_RNAseq_count++;
        }

        if ($CDS_hit){ #now contains gene name
            $CDS_counts_RNAseq{$CDS_hit}+=1;
            $total_RNAseq_count++;
        }

        if ($trailer_hit){ #now contains gene name
            $trailer_counts_RNAseq{$trailer_hit}+=1;
            $total_RNAseq_count++;
        }
    }
}
close (BAM1);

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#open and store TCPseq SSU counts
my $total_TCPseq_SSU_count=0;

open BAM2,"samtools view $bam_TCPseq_SSU |";

while(<BAM2>){

    next if(/^(\@)/);  ## skipping the header lines (if you used -h in the samools command)
    s/\n//;  s/\r//;  ## removing new line
    my @sam = split(/\t+/);  ## splitting SAM line into array

    my $leftMost=$sam[3]; #leftmost position of match 5' for fwd, 3' for rev
    my $flag=$sam[1];
    my $chr=$sam[2];
    my $mapq=$sam[4];
    my $cigar=$sam[5];
    my $seq=$sam[9];
    my $CDS_hit=0;
    my $leader_hit=0;
    my $trailer_hit=0;
    my $start_hit=0;
    my $stop_hit=0;

    if ($chr=~/chr(.*)/){ #ensembl chromosome names do not contain the "chr" prefix
        $chr=$1;
    }

    unless (length($seq) < 17){

    unless ($flag & 0x4){   #if aligned

#       $total_TCPseq_SSU_count++;

        if ($flag & 0x10){  #Reverse reads. Starting from the leftmost position parse the cigar and check if matching positions overlap leaders or cds's

            while ($cigar !~ /^$/){
                if ($cigar =~ /^([0-9]+[MIDN])/){
                    my $cigar_part = $1;
                    if ($cigar_part =~ /(\d+)M/){   #alignment matching 
                        for my $pos ($leftMost .. ($leftMost+$1-1)){ #search though this position

                            #flag TCP-seq reads that overalap with TIS's or stop codons
                            if (exists ($start_codons_search_rev{$chr}{$pos})){
                                $start_hit=$start_codons_search_rev{$chr}{$pos};
                                last;
                            }elsif (exists ($stop_codons_search_rev{$chr}{$pos})){
                                $stop_hit=$stop_codons_search_rev{$chr}{$pos};
                                last;
                            }
 
                            if (exists ( $leader_search_rev{$chr}{$pos})){                                 
                                $leader_hit=$leader_search_rev{$chr}{$pos};
                            }elsif( exists($CDS_search_rev{$chr}{$pos})){
                                $CDS_hit=$CDS_search_rev{$chr}{$pos};
                            }elsif( exists($trailer_search_rev{$chr}{$pos})){
                                $trailer_hit=$trailer_search_rev{$chr}{$pos};
                            }
                        }
                        $leftMost+=$1;
                    } elsif ($cigar_part =~ /(\d+)I/){  #insertion (to the reference) #do nothing this region is not in the reference
                    } elsif ($cigar_part =~ /(\d+)D/){  #deletion (from the reference)
                        $leftMost+=$1; #skip this position. Add to position count but do not search
                    } elsif ($cigar_part =~ /(\d+)N/){  #Skipped region from the reference
                        $leftMost+=$1; #skip this position. Add to position count but do not search  
                    }
                    $cigar =~ s/$cigar_part//;
                }
            }

        }else{ #Forward reads. Starting from the leftmost position parse the cigar and check if matching positions overlap leaders or cds's
             while ($cigar !~ /^$/){
                if ($cigar =~ /^([0-9]+[MIDN])/){
                    my $cigar_part = $1;
                    if ($cigar_part =~ /(\d+)M/){   #alignment matching 
                        for my $pos ($leftMost .. ($leftMost+$1-1)){ #search though this position

                            #flag TCP-seq reads that overalap with TIS's or stop codons
                            if (exists ($start_codons_search_fwd{$chr}{$pos})){
                                $start_hit=$start_codons_search_fwd{$chr}{$pos};
                                last;
                            }elsif (exists ($stop_codons_search_fwd{$chr}{$pos})){
                                $stop_hit=$stop_codons_search_fwd{$chr}{$pos};
                                last;
                            }

                            if (exists ( $leader_search_fwd{$chr}{$pos})){                                 
                                $leader_hit=$leader_search_fwd{$chr}{$pos};
                            }elsif( exists($CDS_search_fwd{$chr}{$pos})){
                                $CDS_hit=$CDS_search_fwd{$chr}{$pos};
                            }elsif( exists($trailer_search_fwd{$chr}{$pos})){
                                $trailer_hit=$trailer_search_fwd{$chr}{$pos};
                            }
                        }
                        $leftMost+=$1;
                    } elsif ($cigar_part =~ /(\d+)I/){  #insertion (to the reference) #do nothing this region is not in the reference
                    } elsif ($cigar_part =~ /(\d+)D/){  #deletion (from the reference)
                        $leftMost+=$1; #skip this position. Add to position count but do not search
                    } elsif ($cigar_part =~ /(\d+)N/){  #Skipped region from the reference
                        $leftMost+=$1; #skip this position. Add to position count but do not search  
                    }
                    $cigar =~ s/$cigar_part//;
                }
            }
        }

        #reads should not be able to be assigned to multiple features, becuase I am seperating the reads that overlap start and stop codons (i.e. the boundaries between features)

        if ($start_hit){
            $start_codon_counts_TCPseq_SSU{$start_hit}+=1;
            $total_TCPseq_SSU_count++;
        }elsif ($stop_hit){
            $stop_codon_counts_TCPseq_SSU{$stop_hit}+=1;
            $total_TCPseq_SSU_count++;
        }else{
            if ($leader_hit){ #now contains gene name   
                $leader_counts_TCPseq_SSU{$leader_hit}+=1;
                $total_TCPseq_SSU_count++;
            }
            if ($CDS_hit){ #now contains gene name
                 $CDS_counts_TCPseq_SSU{$CDS_hit}+=1;
                 $total_TCPseq_SSU_count++;
            }
            if ($trailer_hit){ #now contains gene name
                $trailer_counts_TCPseq_SSU{$trailer_hit}+=1;
                $total_TCPseq_SSU_count++;
            }
        }
    }
    }
}
close (BAM2);

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#open and store TPCseq LSU counts
my $total_TCPseq_LSU_count=0;

open BAM3,"samtools view $bam_TCPseq_LSU |";

while(<BAM3>){

    next if(/^(\@)/);  ## skipping the header lines (if you used -h in the samools command)
    s/\n//;  s/\r//;  ## removing new line
    my @sam = split(/\t+/);  ## splitting SAM line into array

    my $leftMost=$sam[3]; #leftmost position of match 5' for fwd, 3' for rev
    my $flag=$sam[1];
    my $chr=$sam[2];
    my $mapq=$sam[4];
    my $cigar=$sam[5];
    my $seq=$sam[9];
    my $CDS_hit=0;
    my $leader_hit=0;
    my $trailer_hit=0;
    my $start_hit=0;
    my $stop_hit=0;

    if ($chr=~/chr(.*)/){ #ensembl chromosome names do not contain the "chr" prefix
        $chr=$1;
    }

    unless (length($seq) < 17){

    unless ($flag & 0x4){   #if aligned

#        $total_TCPseq_LSU_count++;
        
        if ($flag & 0x10){  #Reverse reads. Starting from the leftmost position parse the cigar and check if matching positions overlap leaders or cds's
            
            while ($cigar !~ /^$/){
                if ($cigar =~ /^([0-9]+[MIDN])/){
                    my $cigar_part = $1;
                    if ($cigar_part =~ /(\d+)M/){   #alignment matching 
                        for my $pos ($leftMost .. ($leftMost+$1-1)){ #search though this position

                            #flag TCP-seq reads that overalap with TIS's or stop codons
                            if (exists ($start_codons_search_rev{$chr}{$pos})){
                                $start_hit=$start_codons_search_rev{$chr}{$pos};
                                last;
                            }elsif (exists ($stop_codons_search_rev{$chr}{$pos})){
                                $stop_hit=$stop_codons_search_rev{$chr}{$pos};
                                last;
                            }

                            if (exists ( $leader_search_rev{$chr}{$pos})){                                
                                $leader_hit=$leader_search_rev{$chr}{$pos};
                            }elsif( exists($CDS_search_rev{$chr}{$pos})){
                                $CDS_hit=$CDS_search_rev{$chr}{$pos};
                            }elsif( exists($trailer_search_rev{$chr}{$pos})){
                                $trailer_hit=$trailer_search_rev{$chr}{$pos};
                            }
                        }
                        $leftMost+=$1;
                    } elsif ($cigar_part =~ /(\d+)I/){  #insertion (to the reference) #do nothing this region is not in the reference
                    } elsif ($cigar_part =~ /(\d+)D/){  #deletion (from the reference)
                        $leftMost+=$1; #skip this position. Add to position count but do not search
                    } elsif ($cigar_part =~ /(\d+)N/){  #Skipped region from the reference
                        $leftMost+=$1; #skip this position. Add to position count but do not search  
                    }
                    $cigar =~ s/$cigar_part//;
                }
            }
        
        }else{ #Forward reads. Starting from the leftmost position parse the cigar and check if matching positions overlap leaders or cds's
             while ($cigar !~ /^$/){
                if ($cigar =~ /^([0-9]+[MIDN])/){
                    my $cigar_part = $1;
                    if ($cigar_part =~ /(\d+)M/){   #alignment matching 
                        for my $pos ($leftMost .. ($leftMost+$1-1)){ #search though this position

                            if (exists ($start_codons_search_fwd{$chr}{$pos})){
                                $start_hit=$start_codons_search_fwd{$chr}{$pos};
                                last;
                            }elsif (exists ($stop_codons_search_fwd{$chr}{$pos})){
                                $stop_hit=$stop_codons_search_fwd{$chr}{$pos};
                                last;
                            }


                            if (exists ( $leader_search_fwd{$chr}{$pos})){                               
                                $leader_hit=$leader_search_fwd{$chr}{$pos};
                            }elsif( exists($CDS_search_fwd{$chr}{$pos})){
                                $CDS_hit=$CDS_search_fwd{$chr}{$pos};
                            }elsif( exists($trailer_search_fwd{$chr}{$pos})){
                                $trailer_hit=$trailer_search_fwd{$chr}{$pos};
                            }
                        }
                        $leftMost+=$1;
                    } elsif ($cigar_part =~ /(\d+)I/){  #insertion (to the reference) #do nothing this region is not in the reference
                    } elsif ($cigar_part =~ /(\d+)D/){  #deletion (from the reference)
                        $leftMost+=$1; #skip this position. Add to position count but do not search
                    } elsif ($cigar_part =~ /(\d+)N/){  #Skipped region from the reference
                        $leftMost+=$1; #skip this position. Add to position count but do not search  
                    }
                    $cigar =~ s/$cigar_part//;
                }
            }
        }

        #reads should not be able to be assigned to multiple features, because I am preferencially assigning overlaps to start and stop codons first (i.e. the boundaries between features)
 
        if ($start_hit){
            $start_codon_counts_TCPseq_LSU{$start_hit}+=1;
            $total_TCPseq_LSU_count++;
        }elsif ($stop_hit){
            $stop_codon_counts_TCPseq_LSU{$stop_hit}+=1;
            $total_TCPseq_LSU_count++;
        }else{
            if ($CDS_hit){ #now contains gene name  #assign counts preferencially to CDSs
                $CDS_counts_TCPseq_LSU{$CDS_hit}+=1;
                $total_TCPseq_LSU_count++;
            }
            if($leader_hit){
                $leader_counts_TCPseq_LSU{$leader_hit}+=1;
                $total_TCPseq_LSU_count++;
            }
            if($trailer_hit){
                $trailer_counts_TCPseq_LSU{$trailer_hit}+=1;
                $total_TCPseq_LSU_count++;
            }
        }
    }
    }
}
close (BAM3);

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#output
my $total_RIBOseq_aligned_sum=$total_RIBOseq_bam_count;
#my $total_RIBOseq_aligned_sum=$total_RIBOseq_wig_count_fwd+$total_RIBOseq_wig_count_rev;
my $total_RNAseq_aligned_sum=$total_RNAseq_count;
my $total_TCPseq_SSU_aligned_sum=$total_TCPseq_SSU_count;
my $total_TCPseq_LSU_aligned_sum=$total_TCPseq_LSU_count;

#header
print "#gene_id,transcript_id,cage_tags_at_leader_peak,leader_length,CDS_length,trailer_length,leader_RNA_FPKM,leader_RFP_FPKM,leader_SSU_FPKM,leader_LSU_FPKM,Start_codon_RNA_FPKM,Start_codon_RFP_FPKM,Start_codon_SSU_FPKM,Start_codon_LSU_FPKM,CDS_RNA_FPKM,CDS_RFP_FPKM,CDS_SSU_FPKM,CDS_LSU_FPKM,Stop_codon_RNA_FPKM,Stop_codon_RFP_FPKM,Stop_codon_SSU_FPKM,Stop_codon_LSU_FPKM,trailer_RNA_FPKM,trailer_RFP_FPKM,trailer_SSU_FPKM,trailer_LSU_FPKM,overlapping_gene,leader_potentially_overlaps_upstream_gene,gene_potentially_overlaps_downstream_leader,upstream_sequence,TIS_seqeuence,downstream_sequence,stop_codon_sequence,kozak_strength,histone\n";

for my $gene (keys %leader_positions_fwd){ #restrict to genes with cage defined leaders

    if (exists ($gene_2_chr{$gene})){  #restict to protien_coding genes
        my $chr=$gene_2_chr{$gene};
        my $transcript=$longest_transcript{$gene};
        my $start_coord=$start_coord_fwd{$gene};
        my $stop_coord=$stop_coord_fwd{$gene};
        my $CDS_length=(($stop_coord+2)-$start_coord)+1;
        my $leader_length=$leader_length{$gene};
        my $trailer_length=($three_prime_most_coord_fwd{$gene}-($stop_coord+3))+1;
        my $highest_cage_peak_value=$cage_peak_value{$gene};
        my $leader_start=$leader_positions_fwd{$gene};
        my $overlapping_gene="FALSE";
        my $leader_potentially_overlaps_upstream_gene="FALSE";
        my $gene_potentially_overlaps_downstream_leader="FALSE";
        my $is_histone="FALSE";
        
        if (exists ($overlaps_inframe_gene{$gene})){           $overlapping_gene="TRUE"; }
        if (exists ($leader_overlaps_upstream{$gene})){        $leader_potentially_overlaps_upstream_gene="TRUE"; }
        if (exists ($gene_overlaps_downstream_leader{$gene})){ $gene_potentially_overlaps_downstream_leader="TRUE"; }
        if (exists ($histone{$gene})){                         $is_histone="TRUE"; }

        my $leader_count_RIBOseq=$leader_counts_RIBOseq{$gene};
        my $start_codon_count_RIBOseq=$start_codon_counts_RIBOseq{$gene};
        my $CDS_count_RIBOseq=$CDS_counts_RIBOseq{$gene};
        my $stop_codon_count_RIBOseq=$stop_codon_counts_RIBOseq{$gene};
        my $trailer_count_RIBOseq=$trailer_counts_RIBOseq{$gene};

        my $leader_count_RNAseq=$leader_counts_RNAseq{$gene};
        my $start_codon_count_RNAseq=$start_codon_counts_RNAseq{$gene};
        my $CDS_count_RNAseq=$CDS_counts_RNAseq{$gene};
        my $stop_codon_count_RNAseq=$stop_codon_counts_RNAseq{$gene};
        my $trailer_count_RNAseq=$trailer_counts_RNAseq{$gene};

        my $leader_count_TCPseq_SSU=$leader_counts_TCPseq_SSU{$gene};
        my $start_codon_count_TCPseq_SSU=$start_codon_counts_TCPseq_SSU{$gene};
        my $CDS_count_TCPseq_SSU=$CDS_counts_TCPseq_SSU{$gene};
        my $stop_codon_count_TCPseq_SSU=$stop_codon_counts_TCPseq_SSU{$gene};
        my $trailer_count_TCPseq_SSU=$trailer_counts_TCPseq_SSU{$gene};

        my $leader_count_TCPseq_LSU=$leader_counts_TCPseq_LSU{$gene};
        my $start_codon_count_TCPseq_LSU=$start_codon_counts_TCPseq_LSU{$gene};
        my $CDS_count_TCPseq_LSU=$CDS_counts_TCPseq_LSU{$gene};
        my $stop_codon_count_TCPseq_LSU=$stop_codon_counts_TCPseq_LSU{$gene};
        my $trailer_count_TCPseq_LSU=$trailer_counts_TCPseq_LSU{$gene};

        #for start and stop codons should I use a length of 3nt?

        my $leader_RIBOseq_FPKM=eval{ $leader_count_RIBOseq/($leader_length*$total_RIBOseq_aligned_sum)*1000000000 } || 0;
        my $start_codon_RIBOseq_FPKM=eval{ $start_codon_count_RIBOseq/($total_RIBOseq_aligned_sum)*1000000 } || 0;
        my $CDS_RIBOseq_FPKM=eval{ $CDS_count_RIBOseq/($CDS_length*$total_RIBOseq_aligned_sum)*1000000000 } || 0;
        my $stop_codon_RIBOseq_FPKM=eval{ $stop_codon_count_RIBOseq/($total_RIBOseq_aligned_sum)*1000000 } || 0;
        my $trailer_RIBOseq_FPKM=eval{ $trailer_count_RIBOseq/($trailer_length*$total_RIBOseq_aligned_sum)*1000000000 } || 0;

        my $leader_RNAseq_FPKM=eval{ $leader_count_RNAseq/($leader_length*$total_RNAseq_aligned_sum)*1000000000 } || 0;
        my $start_codon_RNAseq_FPKM=eval{ $start_codon_count_RNAseq/($total_RNAseq_aligned_sum)*1000000 } || 0;
        my $CDS_RNAseq_FPKM=eval{ $CDS_count_RNAseq/($CDS_length*$total_RNAseq_aligned_sum)*1000000000 } || 0;
        my $stop_codon_RNAseq_FPKM=eval{ $stop_codon_count_RNAseq/($total_RNAseq_aligned_sum)*1000000 } || 0;
        my $trailer_RNAseq_FPKM=eval{ $trailer_count_RNAseq/($trailer_length*$total_RNAseq_aligned_sum)*1000000000 } || 0;

        my $leader_TCPseq_SSU_FPKM=eval{ $leader_count_TCPseq_SSU/($leader_length*$total_TCPseq_SSU_aligned_sum)*1000000000 } || 0;
        my $start_codon_TCPseq_SSU_FPKM=eval{ $start_codon_count_TCPseq_SSU/($total_TCPseq_SSU_aligned_sum)*1000000 } || 0;
        my $CDS_TCPseq_SSU_FPKM=eval{ $CDS_count_TCPseq_SSU/($CDS_length*$total_TCPseq_SSU_aligned_sum)*1000000000 } || 0;
        my $stop_codon_TCPseq_SSU_FPKM=eval{ $stop_codon_count_TCPseq_SSU/($total_TCPseq_SSU_aligned_sum)*1000000 } || 0;
        my $trailer_TCPseq_SSU_FPKM=eval{ $trailer_count_TCPseq_SSU/($trailer_length*$total_TCPseq_SSU_aligned_sum)*1000000000 } || 0;

        my $leader_TCPseq_LSU_FPKM=eval{ $leader_count_TCPseq_LSU/($leader_length*$total_TCPseq_LSU_aligned_sum)*1000000000 } || 0;
        my $start_codon_TCPseq_LSU_FPKM=eval{ $start_codon_count_TCPseq_LSU/($total_TCPseq_LSU_aligned_sum)*1000000 } || 0;
        my $CDS_TCPseq_LSU_FPKM=eval{ $CDS_count_TCPseq_LSU/($CDS_length*$total_TCPseq_LSU_aligned_sum)*1000000000 } || 0;
        my $stop_codon_TCPseq_LSU_FPKM=eval{ $stop_codon_count_TCPseq_LSU/($total_TCPseq_LSU_aligned_sum)*1000000 } || 0;
        my $trailer_TCPseq_LSU_FPKM=eval{ $trailer_count_TCPseq_LSU/($trailer_length*$total_TCPseq_LSU_aligned_sum)*1000000000 } || 0;

        #find the downstream, TIS and upstream sequences, if there is no anotated leader, the sequences will be called "N"
        my @TIS;
        my @up;
        my @down;
        my @stop; 

        for(0 .. 2){
            if (exists ($gene_model_fwd{$gene}{ ($stop_coord+$_) } )){
                push (@stop, substr($fasta_sequences{$chr}, ($gene_model_fwd{$gene}{ ($stop_coord+$_) } -1), 1) );
            }else{
                push (@stop, "N");
            }
        }
    
        for(0 .. 2){
            if (exists ($gene_model_fwd{$gene}{ ($start_coord+$_) } )){
                push (@TIS, substr($fasta_sequences{$chr}, ($gene_model_fwd{$gene}{ ($start_coord+$_) } -1), 1) );
            }else{
                push (@TIS, "N");
            }
        }

        #further upstream sequence needs to be added in a seperate loop and then concaternated (becuase the PWM sequences neecds to start at -4) 
        for (-4 .. -1){
            if (exists ($gene_model_fwd{$gene}{ ($start_coord+$_) } )){
                if (exists ($leader_start_coord{$gene})){ #for genes with CAGE defined (updated) leaders
                    if (($start_coord+$_) >= ($leader_start_coord{$gene}-1)){ #check we're not going behyond the CAGE defined leader
                        push (@up, substr($fasta_sequences{$chr}, ($gene_model_fwd{$gene}{ ($start_coord+$_) } -1), 1) );
                    }else{
                        push (@up, "N");
                    } 
                }
            }else{
                push (@up, "N");
            }
            
        }

        #this can be freely extended in the 3' direction
        for (3 .. 4){
            if (exists ($gene_model_fwd{$gene}{ ($start_coord+$_) } )){
                push (@down, substr($fasta_sequences{$chr}, ($gene_model_fwd{$gene}{ ($start_coord+$_) } -1), 1) );
            }else{
                push (@down, "N");
            }
        }

        #score the kozak context of the gene
        #if a gene has no upstream sequence an "N" will be assigned with the mean PWM score of A,C,G,T  for that position
          
        my $seq_up=join("", @up);
        my $seq_TIS=join("", @TIS);
        my $seq_down=join("", @down);
        my $seq_stop=join("", @stop);

        my $kozak_strength=&score_kozak($seq_up,$seq_down); 

       print "$gene,$transcript,$highest_cage_peak_value,$leader_length,$CDS_length,$trailer_length,$leader_RNAseq_FPKM,$leader_RIBOseq_FPKM,$leader_TCPseq_SSU_FPKM,$leader_TCPseq_LSU_FPKM,$start_codon_RNAseq_FPKM,$start_codon_RIBOseq_FPKM,$start_codon_TCPseq_SSU_FPKM,$start_codon_TCPseq_LSU_FPKM,$CDS_RNAseq_FPKM,$CDS_RIBOseq_FPKM,$CDS_TCPseq_SSU_FPKM,$CDS_TCPseq_LSU_FPKM,$stop_codon_RNAseq_FPKM,$stop_codon_RIBOseq_FPKM,$stop_codon_TCPseq_SSU_FPKM,$stop_codon_TCPseq_LSU_FPKM,$trailer_RNAseq_FPKM,$trailer_RIBOseq_FPKM,$trailer_TCPseq_SSU_FPKM,$trailer_TCPseq_LSU_FPKM,$overlapping_gene,$leader_potentially_overlaps_upstream_gene,$gene_potentially_overlaps_downstream_leader,$seq_up,$seq_TIS,$seq_down,$seq_stop,$kozak_strength,$is_histone\n";
    }
}

for my $gene (keys %leader_positions_rev){ #restrict to genes with cage defined leaders

    if (exists ($gene_2_chr{$gene})){  #restict to protien_coding genes
        my $chr=$gene_2_chr{$gene};
        my $transcript=$longest_transcript{$gene};
        my $start_coord=$start_coord_rev{$gene};
        my $stop_coord=$stop_coord_rev{$gene};
        my $CDS_length=(($stop_coord+2)-$start_coord)+1;
        my $leader_length=$leader_length{$gene};
        my $trailer_length=($three_prime_most_coord_rev{$gene}-($stop_coord+3))+1;
        my $highest_cage_peak_value=$cage_peak_value{$gene};
        my $leader_start=$leader_positions_rev{$gene};
        my $overlapping_gene="FALSE";
        my $leader_potentially_overlaps_upstream_gene="FALSE";
        my $gene_potentially_overlaps_downstream_leader="FALSE";
        my $is_histone="FALSE";
        
        if (exists ($overlaps_inframe_gene{$gene})){           $overlapping_gene="TRUE"; }
        if (exists ($leader_overlaps_upstream{$gene})){        $leader_potentially_overlaps_upstream_gene="TRUE"; }
        if (exists ($gene_overlaps_downstream_leader{$gene})){ $gene_potentially_overlaps_downstream_leader="TRUE"; }
        if (exists ($histone{$gene})){                         $is_histone="TRUE"; }

        my $leader_count_RIBOseq=$leader_counts_RIBOseq{$gene};
        my $start_codon_count_RIBOseq=$start_codon_counts_RIBOseq{$gene};
        my $CDS_count_RIBOseq=$CDS_counts_RIBOseq{$gene};
        my $stop_codon_count_RIBOseq=$stop_codon_counts_RIBOseq{$gene};
        my $trailer_count_RIBOseq=$trailer_counts_RIBOseq{$gene};

        my $leader_count_RNAseq=$leader_counts_RNAseq{$gene};
        my $start_codon_count_RNAseq=$start_codon_counts_RNAseq{$gene};
        my $CDS_count_RNAseq=$CDS_counts_RNAseq{$gene};
        my $stop_codon_count_RNAseq=$stop_codon_counts_RNAseq{$gene};
        my $trailer_count_RNAseq=$trailer_counts_RNAseq{$gene};

        my $leader_count_TCPseq_SSU=$leader_counts_TCPseq_SSU{$gene};
        my $start_codon_count_TCPseq_SSU=$start_codon_counts_TCPseq_SSU{$gene};
        my $CDS_count_TCPseq_SSU=$CDS_counts_TCPseq_SSU{$gene};
        my $stop_codon_count_TCPseq_SSU=$stop_codon_counts_TCPseq_SSU{$gene};
        my $trailer_count_TCPseq_SSU=$trailer_counts_TCPseq_SSU{$gene};

        my $leader_count_TCPseq_LSU=$leader_counts_TCPseq_LSU{$gene};
        my $start_codon_count_TCPseq_LSU=$start_codon_counts_TCPseq_LSU{$gene};
        my $CDS_count_TCPseq_LSU=$CDS_counts_TCPseq_LSU{$gene};
        my $stop_codon_count_TCPseq_LSU=$stop_codon_counts_TCPseq_LSU{$gene};
        my $trailer_count_TCPseq_LSU=$trailer_counts_TCPseq_LSU{$gene};

        my $leader_RIBOseq_FPKM=eval{ $leader_count_RIBOseq/($leader_length*$total_RIBOseq_aligned_sum)*1000000000 } || 0;
        my $start_codon_RIBOseq_FPKM=eval{ $start_codon_count_RIBOseq/($total_RIBOseq_aligned_sum)*1000000 } || 0;
        my $CDS_RIBOseq_FPKM=eval{ $CDS_count_RIBOseq/($CDS_length*$total_RIBOseq_aligned_sum)*1000000000 } || 0;
        my $stop_codon_RIBOseq_FPKM=eval{ $stop_codon_count_RIBOseq/($total_RIBOseq_aligned_sum)*1000000 } || 0;
        my $trailer_RIBOseq_FPKM=eval{ $trailer_count_RIBOseq/($trailer_length*$total_RIBOseq_aligned_sum)*1000000000 } || 0;

        my $leader_RNAseq_FPKM=eval{ $leader_count_RNAseq/($leader_length*$total_RNAseq_aligned_sum)*1000000000 } || 0;
        my $start_codon_RNAseq_FPKM=eval{ $start_codon_count_RNAseq/($total_RNAseq_aligned_sum)*1000000 } || 0;
        my $CDS_RNAseq_FPKM=eval{ $CDS_count_RNAseq/($CDS_length*$total_RNAseq_aligned_sum)*1000000000 } || 0;
        my $stop_codon_RNAseq_FPKM=eval{ $stop_codon_count_RNAseq/($total_RNAseq_aligned_sum)*1000000 } || 0;
        my $trailer_RNAseq_FPKM=eval{ $trailer_count_RNAseq/($trailer_length*$total_RNAseq_aligned_sum)*1000000000 } || 0;

        my $leader_TCPseq_SSU_FPKM=eval{ $leader_count_TCPseq_SSU/($leader_length*$total_TCPseq_SSU_aligned_sum)*1000000000 } || 0;
        my $start_codon_TCPseq_SSU_FPKM=eval{ $start_codon_count_TCPseq_SSU/($total_TCPseq_SSU_aligned_sum)*1000000 } || 0;
        my $CDS_TCPseq_SSU_FPKM=eval{ $CDS_count_TCPseq_SSU/($CDS_length*$total_TCPseq_SSU_aligned_sum)*1000000000 } || 0;
        my $stop_codon_TCPseq_SSU_FPKM=eval{ $stop_codon_count_TCPseq_SSU/($total_TCPseq_SSU_aligned_sum)*1000000 } || 0;
        my $trailer_TCPseq_SSU_FPKM=eval{ $trailer_count_TCPseq_SSU/($trailer_length*$total_TCPseq_SSU_aligned_sum)*1000000000 } || 0;

        my $leader_TCPseq_LSU_FPKM=eval{ $leader_count_TCPseq_LSU/($leader_length*$total_TCPseq_LSU_aligned_sum)*1000000000 } || 0;
        my $start_codon_TCPseq_LSU_FPKM=eval{ $start_codon_count_TCPseq_LSU/($total_TCPseq_LSU_aligned_sum)*1000000 } || 0;
        my $CDS_TCPseq_LSU_FPKM=eval{ $CDS_count_TCPseq_LSU/($CDS_length*$total_TCPseq_LSU_aligned_sum)*1000000000 } || 0;
        my $stop_codon_TCPseq_LSU_FPKM=eval{ $stop_codon_count_TCPseq_LSU/($total_TCPseq_LSU_aligned_sum)*1000000 } || 0;
        my $trailer_TCPseq_LSU_FPKM=eval{ $trailer_count_TCPseq_LSU/($trailer_length*$total_TCPseq_LSU_aligned_sum)*1000000000 } || 0;

        #Find the downstream, TIS and upstream squences, if there is no anotated leader, the sequences will be called "N"
        my @TIS;
        my @up;
        my @down;
        my @stop;

        for(0 .. 2){
            if (exists ($gene_model_rev{$gene}{ ($stop_coord+$_) } )){
                push (@stop, substr($fasta_sequences{$chr}, ($gene_model_rev{$gene}{ ($stop_coord+$_) } -1), 1) );
            }else{
                push (@stop, "N");
            }
        }

        for(0 .. 2){
            if (exists ($gene_model_rev{$gene}{ ($start_coord+$_) } )){
                push (@TIS, substr($fasta_sequences{$chr}, ($gene_model_rev{$gene}{ ($start_coord+$_) } -1), 1) );
            }else{
                push (@TIS, "N");
            }
        }

        #further upstream sequence needs to be added in a seperate loop and then concaternated (becuase the PWM sequences neecds to start at -4) 
        my $upstream_coord=$start_coord;
        for (-4 .. -1){
            if (exists ($gene_model_rev{$gene}{ ($start_coord+$_) } )){
                if (exists ($leader_start_coord{$gene})){ #for genes with CAGE defined (updated) leaders
                    if (($start_coord+$_) >= ($leader_start_coord{$gene}-1)){ #check we're not going behyond the CAGE defined leader
                        push (@up, substr($fasta_sequences{$chr}, ($gene_model_rev{$gene}{ ($start_coord+$_) } -1), 1) );
                    }else{
                        push (@up, "N");
                    }
                }
            }else{
                push (@up, "N");
            }
        }

        #this can be freely extended in the 3' direction
        for (3 .. 4){
            if (exists ($gene_model_rev{$gene}{ ($start_coord+$_) } )){
                push (@down, substr($fasta_sequences{$chr}, ($gene_model_rev{$gene}{ ($start_coord+$_) } -1), 1) );
            }else{
                push (@down, "N");
            }
        }

        my $seq_up=join("", @up);
        my $seq_TIS=join("", @TIS);
        my $seq_down=join("", @down);
        my $seq_stop=join("", @stop);
        $seq_up=~tr/ACGTacgt/TGCAtgca/;
        $seq_TIS=~tr/ACGTacgt/TGCAtgca/;
        $seq_down=~tr/ACGTacgt/TGCAtgca/;
        $seq_stop=~tr/ACGTacgt/TGCAtgca/;

        my $kozak_strength=&score_kozak($seq_up,$seq_down);

        print "$gene,$transcript,$highest_cage_peak_value,$leader_length,$CDS_length,$trailer_length,$leader_RNAseq_FPKM,$leader_RIBOseq_FPKM,$leader_TCPseq_SSU_FPKM,$leader_TCPseq_LSU_FPKM,$start_codon_RNAseq_FPKM,$start_codon_RIBOseq_FPKM,$start_codon_TCPseq_SSU_FPKM,$start_codon_TCPseq_LSU_FPKM,$CDS_RNAseq_FPKM,$CDS_RIBOseq_FPKM,$CDS_TCPseq_SSU_FPKM,$CDS_TCPseq_LSU_FPKM,$stop_codon_RNAseq_FPKM,$stop_codon_RIBOseq_FPKM,$stop_codon_TCPseq_SSU_FPKM,$stop_codon_TCPseq_LSU_FPKM,$trailer_RNAseq_FPKM,$trailer_RIBOseq_FPKM,$trailer_TCPseq_SSU_FPKM,$trailer_TCPseq_LSU_FPKM,$overlapping_gene,$leader_potentially_overlaps_upstream_gene,$gene_potentially_overlaps_downstream_leader,$seq_up,$seq_TIS,$seq_down,$seq_stop,$kozak_strength,$is_histone\n";

    }
}

exit;

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#open a gtf file and get the transcript lengths of the protien coding genes
#sub transcript_lengths{
#
#    my $in_gtf = shift;
#    my %transcripts_ref = shift;

#    my $in_gtf = $_[0];
#    my $transcripts_ref = $_[1];            #key = gene_id, transcript_id, #value = sum_exon_lengths;
#
#    #to dereferece (makes a new copy of the hash)
#    my %transcripts=%{$transcripts_ref};
#
#    open(GENES1,$inGtf) || die "can't open $inGtf";      #gft is 1 based
#    while (<GENES1>){
#        unless(/^#/){
#            my @b=split("\t");
#            my $chr=$b[0];
#            my $class=$b[2];
#            my $start=$b[3];
#            my $end=$b[4];
#            my $dir=$b[6];
#            my ($gene_id) = $b[8] =~ /gene_id\s"([^\"]+)";/;
#            my ($transcript_id) = $b[8] =~ /transcript_id\s"([^\"]+)";/;
#            my $gene_biotype="NA";
#            if ($b[8] =~ /gene_biotype\s"([^\"]+)";/){
#               $gene_biotype=$1;
#            }
#
#            if ($gene_id && $transcript_id){
#                if ($gene_biotype eq "protein_coding"){ #restrict to protien coding genes (control for ncRNAs)
#                    if ($class eq "exon"){
#                        if ($dir eq "+"){
#                            for ($start .. $end){
#                                $transcripts{$gene_id}{$transcript_id}++;
#                           }
#                        }else{
#                            for ($start .. $end){
#                                $transcripts{$gene_id}{$transcript_id}++;
#                            }
#                        }
#                    }
#                }
#            }
#        }
#    }
#    close (GENES1);
#
#    return #I don't need to 
#    return (\%in_frame_fwd, \%in_frame_rev);
#}

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#caclulate log2
sub log2 {
    my $n = shift;
    my $l = log($n)/log(2);
    #$l = sprintf("%.2f",$l);
    return $l;
}

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#score the tis surrounding sequence against the Zebrafish kozak PWM
sub score_kozak{
    my $upstream = shift;
    my $downstream = shift;
    my $score=0;
    my $seq_to_score=uc($upstream.$downstream); #concaternate and set to uppercase   
    my @seq_to_score=split("",$seq_to_score);
    my $count=0;

    for my $base (@seq_to_score){
        if (exists ($PWM{$count}{$base} )){
            $score+=$PWM{$count}{$base};
        }
        $count++;
    }
    return $score;
}
#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
