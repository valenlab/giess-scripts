#08/05/2018
TCP-seq

1) #Align for ncRNA counting

	i) trim_and_align_TCP_edited.sh

	#rename bams
	for file in */accepted_hits.bam; do postfix=${file%/*}; echo ${postfix}; mv $file ${postfix}.bam; done

	ii) #read length plots

	iii) #%exonic or non coding by length

        for file in *.bam; do prefix=${file%_R1_001.bam}; echo $prefix;  perl /export/valenfs/projects/adam/final_results/scripts/bam_gtf_mapping_stats_non_coding_v1.pl /export/valenfs/data/references/Zv10_zebrafish/Danio_rerio.GRCz10.81.gtf /export/valenfs/data/references/Zv10_zebrafish/ncrna/Danio_rerio.GRCz10.ncrna.fa $file > ${prefix}.count.csv; done

        for file in *.bam; do prefix=${file%_R1_001.bam}; echo $prefix;  perl /export/valenfs/projects/adam/final_results/scripts/bam_gtf_mapping_stats_non_coding_by_length.v1.pl /export/valenfs/data/references/Zv10_zebrafish/Danio_rerio.GRCz10.81.gtf /export/valenfs/data/references/Zv10_zebrafish/ncrna/Danio_rerio.GRCz10.ncrna.fa $file > length_counts/${prefix}.length.count.csv; done

2) #Align for heatmaps (removing phiX, rRNA, tRNA, ncRNA)

	i) nice -n 10 bash /export/valenfs/data/references/scripts/trim_and_align_TCP_valen_complete_with_tRNA_3.sh -f /export/valenfs//data/raw_data/TCP-seq/valen_zebrafish_2018_5/ -s GRCz10 -o /export/valenfs/data/processed_data/TCP-seq/valen_2017_zebrafish_5_trim3_15nt/

	ii) Filter bams to remove peaks + fragment length distributions + plot hetmaps 
        /export/valenfs/projects/adam/final_results/scripts/run_TCP_bam_tidy.sh

		nice -n 10 perl /export/valenfs/projects/adam/final_results/scripts/remove_peaks_from_TCP_seq_bam.pl $gtf $fa $file $leaders_shield | samtools view -o $out_dir/${prefix}_peaks_removed.bam -Sb -
		
		for file in *.bam; do prefix=${file%.bam}; echo $prefix; nice -n 10 perl /export/valenfs/projects/adam/final_results/scripts/TCP_length_distributions_per_feature_v2.pl /export/valenfs/data/references/Zv10_zebrafish/Danio_rerio.GRCz10.81_chr.gtf /export/valenfs/data/references/Zv10_zebrafish/Danio_rerio.GRCz10.fa $file ~/analysis/R_working/Cage_r_zv10/TCPseq_leaders_Jan2018/all_leaders/matrix_zebrafish_09_shield_leaders.csv > lengths_per_feature_TSS/${prefix}_lengths.csv; done

		bash /export/valenfs/projects/adam/final_results/scripts/repo3/giess-scripts/TCPseq_heatmaps_eukaryotic/run_transcript_coord.sh -b /export/valenfs/data/processed_data/TCP-seq/valen_2017_zebrafish_4_trim3_15nt/aligned_GRCz10/to_do/ -o /export/valenfs/projects/adam/TCP_seq/valen_4/heatmap_15nt_for_nc_90perc/ -g /export/valenfs/data/references/Zv10_zebrafish/Danio_rerio.GRCz10.81_chr.gtf -l ~/analysis/R_working/Cage_r_zv10/TCPseq_leaders_Jan2018/all_leaders/matrix_zebrafish_09_shield_leaders.csv

	iii) #counts hits from bams	
	for file in *.bam; do samtools flagstat $file | awk NR==5 | awk {'print $1'}; done

	iv) #count tRNA's  perl /export/valenfs/projects/adam/final_results/scripts/count_tRNA_scan_hits.pl /Home/ii/adamg/analysis/genome/GRcZ10_tRNA_scan_output.fa /export/valenfs/data/processed_data/TCP-seq/valen_2017_zebrafish_5_trim3_15nt/tRNAscan_match_GRCz10/Fraction10_S7_R1_001_tRNA.bam 

3) Matricies
	
	i ) #Per transcript
	~/analysis/R_working/Cage_r_zv10/TCPseq_leaders_Jan2018/make_matrix_GRCz10_total_RNA.sh 

		perl /export/valenfs/projects/adam/final_results/scripts/cage_peaks_assignment_gtf_transcript_coords_for_TCPseq_v8.pl $gtf $cage_fwd $cage_rev > ${outdir}/${prefix}_leaders.csv
		perl /export/valenfs/projects/adam/final_results/scripts/assign_sam_wig_counts_to_gtf_genes_seq_histones_start_stop_v11.pl $gtf $fa $ribo_fwd $ribo_rev $rna_bam $ssu_bam $lsu_bam ${outdir}/${prefix}_leaders.csv $go > ${outdir}/${prefix}_matrix.csv
		perl /export/valenfs/projects/adam/final_results/scripts/assign_sam_wig_counts_to_gtf_genes_seq_histones_start_stop_v11.pl $gtf $fa $ribo_fwd $ribo_rev $rna_bam $ssu_bam $lsu_bam ${outdir}/${prefix}_leaders.csv $go > ${outdir}/${prefix}_matrix.csv

	ii) #uORF dORF stats:
	bash ~/analysis/TCP/Hakon_ORFs/uORF_dORF_stats/run_uorf_dorf_stats.sh 

		/export/valenfs/projects/adam/final_results/scripts/find_all_uORF_and_dORF.pl
	
		perl /export/valenfs/projects/adam/final_results/scripts/find_all_uORF_for_tcpseq_v7_uORF_search.pl /export/valenfs/data/references/Zv10_zebrafish/Danio_rerio.GRCz10.81.gtf /export/valenfs/data/references/Zv10_zebrafish/Danio_rerio.GRCz10.fa ~/analysis/R_working/Cage_r_zv10/TCPseq_leaders_Jan2018/TCP_matrix_kozak_trailer_update_histones_directional/matrix_zebrafish_shield_leaders.csv /export/valenfs/projects/adam/TCP_seq/valen_4/peaks_removed/2-ribo-zero-48S-12_S2_R1_001_peaks_removed.bam /export/valenfs/projects/adam/TCP_seq/valen_4/peaks_removed/4-ribo-zero-80S-17_S4_R1_001_peaks_removed.bam /Home/ii/adamg/analysis/TCP/Hakon_ORFs/uORF_stats_shield/

		perl /export/valenfs/projects/adam/final_results/scripts/find_all_uORF_for_tcpseq_v7_uORF_search_yeast.pl /export/valenfs/data/references/R64_1_1_yeast/Saccharomyces_cerevisiae.R64-1-1.79.gtf /export/valenfs/data/references/R64_1_1_yeast/Saccharomyces_cerevisiae.R64-1-1.dna.toplevel.fa ~/analysis/R_working/Cage_r_R64_1_1/TCPseq_leaders_Jan2018/TCP_matrix/matrix_yeast_leaders.csv /export/valenfs/projects/adam/TCP_seq/archer_yeast_2016/peaks_removed/SSU_peaks_removed.bam /export/valenfs/projects/adam/TCP_seq/archer_yeast_2016/peaks_removed/RS_peaks_removed.bam /Home/ii/adamg/analysis/TCP/Hakon_ORFs/uORF_stats_yeast/

	iii)	uORF coverage plots:
	perl /export/valenfs/projects/adam/final_results/scripts/find_all_uORF_for_tcpseq_v6_coverage.pl /export/valenfs/data/references/Zv10_zebrafish/Danio_rerio.GRCz10.81.gtf /export/valenfs/data/references/Zv10_zebrafish/Danio_rerio.GRCz10.fa ~/analysis/R_working/Cage_r_zv10/TCPseq_leaders_Jan2018/TCP_matrix_kozak_trailer_update_histones_directional/matrix_zebrafish_shield_leaders.csv /Home/ii/adamg/analysis/TCP/Hakon_ORFs/uORFS_shield.bed /export/valenfs/projects/adam/TCP_seq/valen_4/peaks_removed/4-ribo-zero-80S-17_S4_R1_001_peaks_removed.bam /Home/ii/adamg/analysis/TCP/Hakon_ORFs/shield_test_50_window_50per/

	iv) leader, cds, trailer plots (scaled to 100nt)
	perl /export/valenfs/projects/adam/final_results/scripts/scaled_window_plots_TCPseq.pl /export/valenfs/data/references/R64_1_1_yeast/Saccharomyces_cerevisiae.R64-1-1.79.gtf /export/valenfs/data/references/R64_1_1_yeast/Saccharomyces_cerevisiae.R64-1-1.dna.toplevel.fa ~/analysis/R_working/Cage_r_R64_1_1/TCPseq_leaders_Jan2018/TCP_matrix/matrix_yeast_leaders.csv /export/valenfs/projects/adam/TCP_seq/archer_yeast_2016/peaks_removed/SSU_peaks_removed.bam /export/valenfs/projects/adam/TCP_seq/archer_yeast_2016/peaks_removed/RS_peaks_removed.bam /Home/ii/adamg/analysis/TCP/Hakon_ORFs/uORF_stats_yeast_test/
		
	#rRNA-seq ribo-seq to compare (chew, subtelney sheild)
	perl /export/valenfs/projects/adam/final_results/scripts/scaled_window_plots_TCPseq.pl /export/valenfs/data/references/Zv10_zebrafish/Danio_rerio.GRCz10.81.gtf /export/valenfs/data/references/Zv10_zebrafish/Danio_rerio.GRCz10.fa ~/analysis/R_working/Cage_r_zv10/TCPseq_leaders_Jan2018/TCP_matrix_total_RNA_kozak_trailer_update_histones_directional/matrix_zebrafish_shield_leaders.csv /export/valenfs/data/processed_data/Ribo-seq/subtelny_2014_zebrafish_frog_yeast_mouse_human_fly/final_results/aligned_GRCz10/6hpf_mock_RPF_Danio_rerio.bam /export/valenfs/data/processed_data/Ribo-seq/chew_2013_zebrafish/final_results/aligned_GRCz10/Shield_trimmed.bam /Home/ii/adamg/analysis/TCP/Hakon_ORFs/uORF_stats_ribo_test/
