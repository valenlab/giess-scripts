#!/usr/bin/perl -w
use strict;

#11/04/18
#script to assign FMPK counts to uoRFs
#calculates kozak scores for zebrafish uORFS
#select the uORF with the highest Kozak score for each uORF (most upstream is tied)

my $inGtf=$ARGV[0]; 
my $fasta=$ARGV[1];
my $leaders=$ARGV[2];
my $bam_SSU=$ARGV[3];  
my $bam_LSU=$ARGV[4];  
my $outDir=$ARGV[5];
my ($prefix)=$bam_SSU=~/([^\/]+).bam$/;

#excluding:
#genes where the TSS is downstream of the start codon
#genes without a detectable cage peak 
#genes that are annotated as protien_coding

#Flags:
#overlapping_gene: The longest transcript of this gene overlaps with the longest transcript of another gene
#leader_potentially_overlaps_upstream_gene: There is an upstream gene within 500nt of the start codon of this gene 
#gene_potentially_overlaps_downstream_leader: There is an downstream gene start codon withing 500nt of the 3' most position of this gene (in yeast this is the stop codon).

#my $PERCENTAGE_TO_EXCLUDE=0.1;
#my $PERCENTAGE_TO_EXCLUDE=0.5;

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#Zebrafish Zozak position frequency matrix (PFM) from Grzegorski et. al, 2015                   
#Position   -4  -3  -2  -1  4   5
#A  35  62  39  28  24  27
#C  32  5   23  36  12  42
#G  19  28  17  27  46  16
#T  14  5   21  10  17  15

my %raw_kozak =
   (
       "A" => [ 35, 62, 39, 28, 24, 27 ],
       "C" => [ 32, 5, 23, 36, 12, 42 ],
       "G" => [ 19, 28, 17, 27, 46, 16 ],
       "T" => [ 14, 5, 21, 10, 17, 15 ],
   );

my %PWM; #key1: position, key2: base, value: weight
for my $pos (0 .. 5){

    #0 == -4 #nucleotide position in relation to start codon
    #1 == -3
    #2 == -2
    #3 == -1
    #4 == +4
    #5 == +5

    my $pos_sum=0;
    my $pwm_sum=0;
    for my $base (keys %raw_kozak){ #sum the nucleotide frequencies per position
        $pos_sum+=$raw_kozak{$base}[$pos];
    }

    for my $base(keys %raw_kozak){ #score the PWM
        my $psudo_count= sqrt($pos_sum);
        my $background_probability=0.25; #no base preference
        my $pwm=&log2( ($raw_kozak{$base}[$pos] + $psudo_count * $background_probability) / ($pos_sum + $psudo_count * $background_probability));
        $PWM{$pos}{$base}=$pwm;
        $pwm_sum+=$pwm;
    }

    $PWM{$pos}{"N"}=($pwm_sum/4); #set "N" to be equal to the column mean. For genes with short leaders, missing upstream positions 
} 

#Yeast 2011	The mRNA landscape at yeast translation initiation sites				
#my %yeast_matrix =
#   (            #-4   -3    -2    -1     4     5
#       "A" => [ 0.53, 1.02, 0.47, 0.58, -0.11, -0.6 ],
#       "C" => [ 0.16, -1.16, 0.16, -0.12, -0.47, 1.26 ],
#       "G" => [ -0.3, 0.2, -0.52, -0.15, 0.77, -0.11 ],
#       "T" => [ -0.74, -2.1, -0.49, -0.71, -0.18, -0.75 ],
#   );

my %PWM_yeast =
(
    0 => { 
        "A" => 0.53,
        "C" => 0.16,
        "G" => -0.3,
        "T" => -0.74
    },
    1 => {
        "A" => 1.02,
        "C" => -1.16,
        "G" => 0.2,
        "T" => -2.1
    },
    2 => { 
        "A" => 0.47,
        "C" => 0.16,
        "G" => -0.52,
        "T" => -0.49
    },
    3 => { 
        "A" => 0.58,
        "C" => -0.12,
        "G" => -0.15,
        "T" => -0.71
    },
    4 => {
        "A" => -0.11,
        "C" => -0.47,
        "G" => 0.77,
        "T" => -0.18
    },
    5 => {
        "A" => -0.6,
        "C" => 1.26,
        "G" => -0.11,
        "T" => -0.75
    }
);

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#open gtf and get transcript lengths
my %transcripts; #key = gene_id, transcript_id, #value = sum_exon_lengths;

open(GENES1,$inGtf) || die "can't open $inGtf";      #gft is 1 based
while (<GENES1>){
    unless(/^#/){
        my @b=split("\t");
        my $chr=$b[0];
        my $class=$b[2];
        my $start=$b[3];
        my $end=$b[4];
        my $dir=$b[6];
        my ($gene_id) = $b[8] =~ /gene_id\s"([^\"]+)";/;
        my ($transcript_id) = $b[8] =~ /transcript_id\s"([^\"]+)";/;
        my $gene_biotype="NA";
        if ($b[8] =~ /gene_biotype\s"([^\"]+)";/){
            $gene_biotype=$1;
        }

        if ($gene_id && $transcript_id){

            if ($gene_biotype eq "protein_coding"){ #restrict to protien coding genes (control for ncRNAs)

                if ($class eq "exon"){
                    if ($dir eq "+"){
                        for ($start .. $end){
                            $transcripts{$gene_id}{$transcript_id}++;
                        }
                    }else{
                        for ($start .. $end){
                            $transcripts{$gene_id}{$transcript_id}++;
                        }
                    }
                }
            }
        }
    }
}
close (GENES1);

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#select longest transcripts per gene
my %longest_transcript; #key=gene_id, value=transcript_id
for my $gene (keys %transcripts){
    my $longest=0;
    for my $transcript (keys %{ $transcripts{$gene}} ){
        if ($transcripts{$gene}{$transcript} > $longest) {
            $longest_transcript{$gene}=$transcript;
            $longest=$transcripts{$gene}{$transcript};
        }
    }
}

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#second pass through the genome, find annotated start codons and setup transcript models for longest transcript of each gene
my %gene_start_codon_fwd;
my %gene_stop_codon_fwd;
my %gene_exons_fwd;
my %gene_start_codon_rev;
my %gene_stop_codon_rev;
my %gene_exons_rev;
my %gene_2_chr; #key = gene_id; value = chr

my %overlaps_inframe_gene;
my %leader_overlaps_upstream;
my %gene_overlaps_downstream_leader;
my %has_cage_defined_leader;

open(GENES2,$inGtf) || die "can't open $inGtf";      #gft is 1 based
while (<GENES2>){
    unless(/^#/){
        my @b=split("\t");
        my $chr=$b[0];
        my $class=$b[2];
        my $start=$b[3];
        my $end=$b[4];
        my $dir=$b[6];
        my ($gene_id) = $b[8] =~ /gene_id\s"([^\"]+)";/;
        my ($transcript_id) = $b[8] =~ /transcript_id\s"([^\"]+)";/;

        if ($gene_id && $transcript_id){

            if (exists ( $longest_transcript{$gene_id} )){    #if the transcript is in the list of longest transcripts

                if ($transcript_id eq $longest_transcript{$gene_id}){

                    $gene_2_chr{$gene_id}=$chr;
                    $overlaps_inframe_gene{$gene_id}=0;
                    $leader_overlaps_upstream{$gene_id}=0;
                    $gene_overlaps_downstream_leader{$gene_id}=0;
                    $has_cage_defined_leader{$gene_id}=0;

                    if ($dir eq "+"){ #fwd cases. Use start positions as 5'

                        if ($class eq "start_codon"){
                            $gene_start_codon_fwd{$gene_id}=$start;
                        }
                        if ($class eq "stop_codon"){
                            $gene_stop_codon_fwd{$gene_id}=$start;     #the 1st nt of the stop codon
                        }
                        if ($class eq "exon"){
                            $gene_exons_fwd{$gene_id}{$start}=$end;
                        }

                    }else{ #revese cases use end as 5'

                        if ($class eq "start_codon"){
                            $gene_start_codon_rev{$gene_id}=$end;
                        }
                        if ($class eq "stop_codon"){
                            $gene_stop_codon_rev{$gene_id}=$end;     #the 1st nt of the stop codon
                        }
                        if ($class eq "exon"){
                            $gene_exons_rev{$gene_id}{$start}=$end;
                        }
                    }
                }
            }
        }
    }
}
close(GENES2);

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#open fasta for codon sequeces

my %fasta_sequences; #key = sequence_name, value=sequence
my $name;
open (FA, $fasta) || die "can't open $fasta";
while (<FA>){
    chomp;
    if (/^>([^\s]+)/){ #take header up to the first space
        $name=$1;
        if ($name =~ /^chr(.*)/){
           $name=$1; #if the chr name have a chr* prefix, remove it 
        }
    }else{
        $fasta_sequences{$name}.=$_;
    }
}
close(FA);

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#setup transcript models

my %gene_model_fwd;
my %start_coord_fwd;
my %stop_coord_fwd;

#5' is the #gene_model{$gene}{0}
#3' is the last coord
my %three_prime_most_coord_fwd;
my %five_prime_most_coord_fwd;

#gene_model{$gene}{0}=12345   #1st nt of start codon
#gene_model{$gene}{1}=12346
#gene_model{$gene}{2}=12347
#gene_model{$gene}{3}=12348
#gene_model{$gene}{4}=12349
#...
#to end of exons             #last nt of stop codon

for my $gene (keys %gene_exons_fwd){
    if ( (exists ($gene_start_codon_fwd{$gene})) && (exists ($gene_stop_codon_fwd{$gene})) ) { #restrict to genes with annotated start + stop codon

        my $model_pos=0;
        $five_prime_most_coord_fwd{$gene}=$model_pos;  #initalise the 5' to the first coord coord

        for my $exon_start (sort {$a <=> $b} keys %{ $gene_exons_fwd{$gene} } ){
            my $exon_end=$gene_exons_fwd{$gene}{$exon_start};

            #fwd exons are in ascending order
            # start(-1)-> 100958 100975
            #             101077 101715 <-end(+1)

            for ($exon_start .. $exon_end){
                $gene_model_fwd{$gene}{$model_pos}=$_;

                if ($_ == $gene_stop_codon_fwd{$gene}){
                    $stop_coord_fwd{$gene}=$model_pos;    #find the index of the stop codon per gene
                }

                if ($_ == $gene_start_codon_fwd{$gene}){
                    $start_coord_fwd{$gene}=$model_pos;    #find the index of the start codon per gene
                }
                $model_pos++;
            }
        }
        $three_prime_most_coord_fwd{$gene}=$model_pos-1; #store the 3 prime most position of each gene
    }
}

my %gene_model_rev;
my %start_coord_rev;
my %stop_coord_rev;

#5' is the #gene_model{$gene}{0}
#3' is the last coord
my %three_prime_most_coord_rev;
my %five_prime_most_coord_rev;

for my $gene (keys %gene_exons_rev){
    if ( (exists ($gene_start_codon_rev{$gene})) && (exists ($gene_stop_codon_rev{$gene})) ) { #restrict to genes with annotated start + stop codon

        my $model_pos=0;

        $five_prime_most_coord_rev{$gene}=$model_pos;  #initalise the 5' to the first coord coord
 
        for my $exon_end (reverse (sort {$a <=> $b} keys %{ $gene_exons_rev{$gene} } )){
            my $exon_start=$gene_exons_rev{$gene}{$exon_end};

            #rev exons are sorted in decending order  
            #           447087 447794 <-start(+1)
            # end(-1)-> 446060 446254

            while ($exon_start >= $exon_end){
                $gene_model_rev{$gene}{$model_pos}=$exon_start;

                if ($exon_start == $gene_stop_codon_rev{$gene}){
                    $stop_coord_rev{$gene}=$model_pos;    #find the index of the stop codon per gene
                }
                if ($exon_start == $gene_start_codon_rev{$gene}){
                    $start_coord_rev{$gene}=$model_pos;    #find the index of the start codon per gene
                }
                $model_pos++;
                $exon_start--;
            }
        }
        $three_prime_most_coord_rev{$gene}=$model_pos-1; #store the 3 prime most position of each gene
    }
}

#for my $gene (keys %gene_model_fwd){          #test for intron
#    if ($gene eq "ENSDARG00000005026"){
#        for my $pos (sort {$a <=> $b} keys %{ $gene_model_fwd{$gene} }) {
#            print "$gene,$pos,$gene_model_fwd{$gene}{$pos}\n";
#        }
#    }
#}

#exit;


#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#parse leaders
my %leader_positions_fwd;
my %leader_positions_rev;

#filters:
my %leader_length;
my %cage_peak_value;

#gene_id,       0
#transcript_id, 1
#chr,           2
#dir,           3
#overlaps_inframe_gene,           4
#leader_overlaps_upstream,        5
#gene_overlaps_downstream_leader, 6
#highest_cage_peak,               7
#count_at_highest_cage_peak,      8
#leader_length                    9

#ENSDARG00000037917,ENSDART00000161963,3,fwd,FALSE,FALSE,FALSE,34716685,2.30440468389324,143
#ENSDARG00000104069,ENSDART00000167982,5,fwd,FALSE,FALSE,FALSE,337237,9.98331428397882,122
#ENSDARG00000037925,ENSDART00000130591,3,fwd,FALSE,FALSE,FALSE,36250098,0.346429817638395,-679
#ENSDARG00000029263,ENSDART00000078466,3,fwd,FALSE,FALSE,FALSE,NaN,0,NaN

open(LEAD, $leaders) || die "can't open $leaders";
while (<LEAD>){
    unless(/^#/){
 
        chomp;
        my @b=split(",");

        my $gene=$b[0];
        my $transcript=$b[1];
        my $chr=$b[2];
        my $dir=$b[3];
        my $overlaps_inframe_gene=$b[4];
        my $leader_overlaps_upstream=$b[5];
        my $gene_overlaps_downstream_leader=$b[6];
        my $highest_cage_peak=$b[7];
        my $count_at_highest_cage_peak=$b[8];
        my $leader_length=$b[9];

        if ($overlaps_inframe_gene eq "TRUE"){ $overlaps_inframe_gene{$gene}=1; }
        if ($leader_overlaps_upstream eq "TRUE"){ $leader_overlaps_upstream{$gene}=1; }
        if ($gene_overlaps_downstream_leader eq "TRUE"){ $gene_overlaps_downstream_leader{$gene}=1; }

        unless ($leader_length eq "NaN"){  #only take genes that have a detectable cage peak

            unless ($leader_length < 0 ){  #exlude genes with negative leader sizes, as they cuase problems with FPKM

                $has_cage_defined_leader{$gene}=1;

                if ($dir eq "fwd"){ 
                    if (exists ($start_coord_fwd{$gene})){
                        unless ($highest_cage_peak >=  $gene_model_fwd{$gene}{$start_coord_fwd{$gene}}){  #exclude genes where the TSS is downstream of the start codon
                            $leader_positions_fwd{$gene}=$highest_cage_peak;
                            $cage_peak_value{$gene}=$count_at_highest_cage_peak;
                            $leader_length{$gene}=$leader_length;
                            $has_cage_defined_leader{$gene}=1;
                        } 
                    }
                }else{
                    if (exists ($start_coord_rev{$gene})){
                        unless ($highest_cage_peak <=  $gene_model_rev{$gene}{$start_coord_rev{$gene}}){  #exclude genes where the TSS is downstream of the start codon
                            $leader_positions_rev{$gene}=$highest_cage_peak;
                            $cage_peak_value{$gene}=$count_at_highest_cage_peak;
                            $leader_length{$gene}=$leader_length;
                            $has_cage_defined_leader{$gene}=1;
                        }
                    }
                }     
            }
        }
    }
}
close(LEAD);

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#extend transcript models to incorportate cage derived leaders
for my $gene (keys %leader_positions_fwd){

    if (exists ($gene_2_chr{$gene})){  #to restict to protien_coding genes
        my $leader_start=$leader_positions_fwd{$gene};
        my $three_prime_coord=$three_prime_most_coord_fwd{$gene};
        my $three_prime_pos=$gene_model_fwd{$gene}{$three_prime_coord};
        my $five_prime_coord=0;
        my $five_prime_pos=$gene_model_fwd{$gene}{$five_prime_coord};
 
        if ($leader_start >= $five_prime_pos){  #great, just find and save the coordinate
 
            for my $coord ($five_prime_coord .. $three_prime_coord){
                my $pos=$gene_model_fwd{$gene}{$coord};       
                if ($pos == $leader_start){
                    $five_prime_most_coord_fwd{$gene}=$coord;
#                    if ($gene eq "ENSDARG00000005026"){ print "new_5 shorter: $coord\n"; }
                    last; 
                }
            }

        }else{  #extend the coords

            my $extended_coord=0; 
            while ($five_prime_pos > $leader_start){        
                $extended_coord--;
                $five_prime_pos--;
                $gene_model_fwd{$gene}{$extended_coord}=$five_prime_pos;
            }
            $five_prime_most_coord_fwd{$gene}=$extended_coord;
#            if ($gene eq "ENSDARG00000005026"){ print "new_5 extended: $extended_coord\n"; }
        }
    }
} 

for my $gene (keys %leader_positions_rev){

    if (exists ($gene_2_chr{$gene})){  #to restict to protien_coding genes
        my $leader_start=$leader_positions_rev{$gene};
        my $three_prime_coord=$three_prime_most_coord_rev{$gene};
        my $three_prime_pos=$gene_model_rev{$gene}{$three_prime_coord};
        my $five_prime_coord=0;
        my $five_prime_pos=$gene_model_rev{$gene}{$five_prime_coord};
 
        if ($leader_start <= $five_prime_pos){  #great, just find and save the coordinate

            for my $coord ($five_prime_coord .. $three_prime_coord){
                my $pos=$gene_model_rev{$gene}{$coord};
                if ($pos == $leader_start){
                    $five_prime_most_coord_rev{$gene}=$coord;
                    last;
                }
            }

        }else{   #extend the coords

            my $extended_coord=0;
            while ($five_prime_pos < $leader_start){
                $extended_coord--;
                $five_prime_pos++;
                $gene_model_rev{$gene}{$extended_coord}=$five_prime_pos;
            }
            $five_prime_most_coord_rev{$gene}=$extended_coord;
        }
    }
}

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#Transcript coord key
#Transcript 5' coord   $five_prime_most_coord_fwd{$gene}  #1st nt of annotated transcript
#Transcript 3' coord   $three_prime_most_coord_???{$gene} #last nt of transcript
#Start codon coord     $start_coord_???{$gene}            #1st nt in start codon
#Stop codon coord      $stop_coord_???{$gene}             #1st nt in stop codon
#$gene_model_fwd{$gene}{$coord}==genomic position

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#loop though genes and assign leader, CDS and trailer regions to hashes for quick searching. (added start + stop codons).

my %leader_search_fwd;
my %leader_search_rev;

my %start_codons_search_fwd; #key1=chr, key2=pos, value=gene
my %start_codons_search_rev; #key1=chr, key2=pos, value=gene
my %stop_codons_search_fwd; #key1=chr, key2=pos, value=gene
my %stop_codons_search_rev; #key1=chr, key2=pos, value=gene
 
for my $gene (keys %gene_model_fwd){
    my $chr=$gene_2_chr{$gene};
    my $start_coord=$start_coord_fwd{$gene};
    my $stop_coord=$stop_coord_fwd{$gene};

    for my $coord (sort {$a <=> $b} keys %{ $gene_model_fwd{$gene} } ){
        my $pos=$gene_model_fwd{$gene}{$coord};

        if ($coord == $start_coord){   $start_codons_search_fwd{$chr}{$pos}=$gene; } 
        if ($coord == $start_coord+1){ $start_codons_search_fwd{$chr}{$pos}=$gene; }
        if ($coord == $start_coord+2){ $start_codons_search_fwd{$chr}{$pos}=$gene; }

        if ($coord == $stop_coord){   $stop_codons_search_fwd{$chr}{$pos}=$gene; }
        if ($coord == $stop_coord+1){ $stop_codons_search_fwd{$chr}{$pos}=$gene; }
        if ($coord == $stop_coord+2){ $stop_codons_search_fwd{$chr}{$pos}=$gene; }
           
        if ($coord < $start_coord){
            $leader_search_fwd{$chr}{$pos}=$gene;
        }
    }
}

for my $gene (keys %gene_model_rev){
    my $chr=$gene_2_chr{$gene};
    my $start_coord=$start_coord_rev{$gene};
    my $stop_coord=$stop_coord_rev{$gene};

    for my $coord (sort {$a <=> $b} keys %{ $gene_model_rev{$gene} } ){
        my $pos=$gene_model_rev{$gene}{$coord};

        if ($coord == $start_coord){   $start_codons_search_rev{$chr}{$pos}=$gene; }
        if ($coord == $start_coord+1){ $start_codons_search_rev{$chr}{$pos}=$gene; }
        if ($coord == $start_coord+2){ $start_codons_search_rev{$chr}{$pos}=$gene; }

        if ($coord == $stop_coord){   $stop_codons_search_rev{$chr}{$pos}=$gene; }
        if ($coord == $stop_coord+1){ $stop_codons_search_rev{$chr}{$pos}=$gene; }
        if ($coord == $stop_coord+2){ $stop_codons_search_rev{$chr}{$pos}=$gene; }

        if ($coord < $start_coord){
            $leader_search_rev{$chr}{$pos}=$gene;
        }
    }
}

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#parse the leader regions and identify uORFs
#   mark those that overlap with gene CDSs
#   mark CDS extensions (inframe?) or ignore

my %uORF_start_fwd;   #uORF_ID = $start_coord
my %uORF_stop_fwd;    #uORF_ID = $stop_coord
my %uORF_start_rev;   #uORF_ID = $start_coord
my %uORF_stop_rev;    #uORF_ID = $stop_coord
my %uORF_to_chr;
my %uORF_to_gene;

my %uORF_start_coord;
my %uORF_stop_coord;
my %uORF_start_codon;
my %uORF_stop_codon;
my %uORF_kozak_score; #uORF_ID = score

my %uORF_overlaps_CDS;

my %SSU_hits; #initalise
my %LSU_hits;

for my $gene (keys %gene_model_fwd){
    my $chr=$gene_2_chr{$gene};
    my $start_coord=$start_coord_fwd{$gene};
    my $stop_coord=$stop_coord_fwd{$gene};
    my $five_prime_coord=$five_prime_most_coord_fwd{$gene};
    my $three_prime_coord=$three_prime_most_coord_fwd{$gene};

    my $uORF_id_count=0;
    my $orf_overlaps_CDS=0;

    for my $coord (sort {$a <=> $b} keys %{ $gene_model_fwd{$gene} } ){

        if ($coord > ($five_prime_coord+2)){  #+2 to allow enugh room for the first codon

            if ($coord < $start_coord ){ #uORFs need to be upstream

                my @TIS;
                for(-2 .. 0){
                    if (exists ($gene_model_fwd{$gene}{ ($coord+$_) } )){
                         push (@TIS, substr($fasta_sequences{$chr}, ($gene_model_fwd{$gene}{ ($coord+$_) } -1), 1) );
                    }else{
                         push (@TIS, "N");
                    }
                }
                my $seq_TIS=join("", @TIS);
 
                if ( ($seq_TIS eq "ATG") || ($seq_TIS eq "CTG") || ($seq_TIS eq "GTG") || ($seq_TIS eq "TTG") ){

                    my $uORF_start=$gene_model_fwd{$gene}{($coord-2)}; 
                    my $uORF_start_coord=$coord;
                    my $search_coord=$coord+3;
                    while ($search_coord < $three_prime_coord-2){ #-2 to allow enough room for the last codon
             
                        my @STOP;
                        for(-2 .. 0){
                            if (exists ($gene_model_fwd{$gene}{ ($search_coord+$_) } )){
                                push (@STOP, substr($fasta_sequences{$chr}, ($gene_model_fwd{$gene}{ ($search_coord+$_) } -1), 1) );
                            }else{
                                push (@STOP, "N");
                            }
                        }
                        my $seq_STOP=join("", @STOP);
                        if (($seq_STOP eq "TAA") || ($seq_STOP eq "TAG") || ($seq_STOP eq "TGA") ){

                            unless (($search_coord-2) == $stop_coord){  #I am not intersted in extensions to the CDS

                                my $kozak_score=0; #calculate kozak score here
                                my @up;
                                my @down;

                                for (-6 .. -3){
                                    if (exists ($gene_model_fwd{$gene}{ ($uORF_start_coord+$_) } )){
                                        if (exists ($five_prime_most_coord_fwd{$gene})){
                                            if (($coord+$_) >= ($five_prime_most_coord_fwd{$gene}-1)){ #check we're not extending behyond leader
                                                push (@up, substr($fasta_sequences{$chr}, ($gene_model_fwd{$gene}{ ($uORF_start_coord+$_) } -1), 1) );
                                            }else{
                                                push (@up, "N");
                                            }
                                        }
                                    }else{
                                        push (@up, "N");
                                    }
                                }

                                for (1 .. 2){
                                    if (exists ($gene_model_fwd{$gene}{ ($uORF_start_coord+$_) } )){
                                        push (@down, substr($fasta_sequences{$chr}, ($gene_model_fwd{$gene}{ ($uORF_start_coord+$_) } -1), 1) );
                                    }else{
                                        push (@down, "N");
                                    }
                                }

                                my $seq_up=join("", @up);
                                my $seq_down=join("", @down);
                                $kozak_score=&score_kozak($seq_up,$seq_down);
                                                        
                                my $uORF_id=$gene."_".$uORF_id_count;
                                my $uORF_stop=$gene_model_fwd{$gene}{$search_coord};  #the last nt of the stop codon

                                $uORF_start_fwd{$uORF_id}=$uORF_start;
                                $uORF_stop_fwd{$uORF_id}=$uORF_stop;
                                $uORF_start_coord{$gene}{$uORF_id}=$uORF_start_coord;
                                $uORF_stop_coord{$gene}{$uORF_id}=$search_coord;
                                $uORF_start_codon{$uORF_id}=$seq_TIS;
                                $uORF_stop_codon{$uORF_id}=$seq_STOP;
                                $SSU_hits{$uORF_id}=1; 
                                $LSU_hits{$uORF_id}=1;
                                $uORF_to_gene{$uORF_id}=$gene;
                                $uORF_kozak_score{$uORF_id}=$kozak_score;
                                $uORF_overlaps_CDS{$uORF_id}=0;

                                if ($search_coord > $start_coord){
                                     $orf_overlaps_CDS=1;
                                     $uORF_overlaps_CDS{$uORF_id}=1;
                                }
  
#                                print "$uORF_id_count,$seq_TIS,$seq_STOP,$chr,$uORF_start,$uORF_stop,$orf_overlaps_CDS,$search_coord,$stop_coord,$kozak_score,$seq_up,$seq_down\n";
                                $uORF_id_count++;
                            }
                            last;
                        }
                        $search_coord+=3;      
                    }         
                }
            }
        }
    }
}

for my $gene (keys %gene_model_rev){
    my $chr=$gene_2_chr{$gene};
    my $start_coord=$start_coord_rev{$gene};
    my $stop_coord=$stop_coord_rev{$gene};
    my $five_prime_coord=$five_prime_most_coord_rev{$gene};
    my $three_prime_coord=$three_prime_most_coord_rev{$gene};

    my $uORF_id_count=0;
    my $orf_overlaps_CDS=0;

    for my $coord (sort {$a <=> $b} keys %{ $gene_model_rev{$gene} } ){

        if ($coord > ($five_prime_coord+2)){  #+2 to allow enugh room for the first codon

            if ($coord < $start_coord ){ #uORFs need to be upstream

                my @TIS;
                for(-2 .. 0){
                    if (exists ($gene_model_rev{$gene}{ ($coord+$_) } )){
                         push (@TIS, substr($fasta_sequences{$chr}, ($gene_model_rev{$gene}{ ($coord+$_) } -1), 1) );
                    }else{
                         push (@TIS, "N");
                    }
                }
                my $seq_TIS=join("", @TIS);
                $seq_TIS=~tr/ACGTacgt/TGCAtgca/;
                if ( ($seq_TIS eq "ATG") || ($seq_TIS eq "CTG") || ($seq_TIS eq "GTG") || ($seq_TIS eq "TTG") ){

                    my $uORF_start=$gene_model_rev{$gene}{($coord-2)};
                    my $uORF_start_coord=$coord;
                    my $search_coord=$coord+3;
                    while ($search_coord < $three_prime_coord-2){ #-2 to allow enough room for the last codon

                        my @STOP;
                        for(-2 .. 0){
                            if (exists ($gene_model_rev{$gene}{ ($search_coord+$_) } )){
                                push (@STOP, substr($fasta_sequences{$chr}, ($gene_model_rev{$gene}{ ($search_coord+$_) } -1), 1) );
                            }else{
                                push (@STOP, "N");
                            }
                        }
                        my $seq_STOP=join("", @STOP);
                        $seq_STOP=~tr/ACGTacgt/TGCAtgca/;
                        if (($seq_STOP eq "TAA") || ($seq_STOP eq "TAG") || ($seq_STOP eq "TGA") ){

                            unless (($search_coord-2) == $stop_coord){  #I am not intersted in extensions to the CDS

                                my $kozak_score=0; #calculate kozak score here
                                my @up;
                                my @down;

                                for (-6 .. -3){
                                    if (exists ($gene_model_rev{$gene}{ ($uORF_start_coord+$_) } )){
                                        if (exists ($five_prime_most_coord_rev{$gene})){
                                            if (($coord+$_) >= ($five_prime_most_coord_rev{$gene}-1)){ #check we're not extending behyond leader
                                                push (@up, substr($fasta_sequences{$chr}, ($gene_model_rev{$gene}{ ($uORF_start_coord+$_) } -1), 1) );
                                            }else{
                                                push (@up, "N");
                                            }
                                        }
                                    }else{
                                        push (@up, "N");
                                    }
                                }

                                for (1 .. 2){
                                    if (exists ($gene_model_rev{$gene}{ ($uORF_start_coord+$_) } )){
                                        push (@down, substr($fasta_sequences{$chr}, ($gene_model_rev{$gene}{ ($uORF_start_coord+$_) } -1), 1) );
                                    }else{
                                        push (@down, "N");
                                    }
                                }

                                my $seq_up=join("", @up);
                                my $seq_down=join("", @down);
                                $seq_up=~tr/ACGTacgt/TGCAtgca/;
                                $seq_down=~tr/ACGTacgt/TGCAtgca/;

                                $kozak_score=&score_kozak($seq_up,$seq_down);

                                my $uORF_id=$gene."_".$uORF_id_count;
                                my $uORF_stop=$gene_model_rev{$gene}{$search_coord};  #the last nt of the stop codon

                                $uORF_start_rev{$uORF_id}=$uORF_start;
                                $uORF_stop_rev{$uORF_id}=$uORF_stop;
                                $uORF_start_coord{$gene}{$uORF_id}=$uORF_start_coord;
                                $uORF_stop_coord{$gene}{$uORF_id}=$search_coord;
                                $uORF_start_codon{$uORF_id}=$seq_TIS;
                                $uORF_stop_codon{$uORF_id}=$seq_STOP;
                                $SSU_hits{$uORF_id}=1;
                                $LSU_hits{$uORF_id}=1;
                                $uORF_to_gene{$uORF_id}=$gene;
                                $uORF_kozak_score{$uORF_id}=$kozak_score;
                                $uORF_overlaps_CDS{$uORF_id}=0;

                                if ($search_coord > $start_coord){
                                     $orf_overlaps_CDS=1;
                                     $uORF_overlaps_CDS{$uORF_id}=1;
                                }

                                #print "$uORF_id_count,$seq_TIS,$seq_STOP,$chr,$uORF_start,$uORF_stop,$orf_overlaps_CDS,$search_coord,$stop_coord,$kozak_score,$seq_up,$seq_down\n";
                                $uORF_id_count++;
                            }
                            last;
                        }
                        $search_coord+=3;
                    }
                }
            }
        }
    }
}

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#best uORF here

my %best_uORF;
for my $gene (keys %uORF_start_coord){
    my $best_score=-100;
    for my $uorf (sort keys %{$uORF_start_coord{$gene}}){ #sorting is important as the id's are listed 5' to 3'
        my $kozak=$uORF_kozak_score{$uorf};
        if ($kozak > $best_score){
            $best_uORF{$gene}=$uorf;
            $best_score=$kozak
        }
    }
}


#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#assign uORF search regions

my %uORF_search_fwd; #{$chr}{$pos}=$uorf1;uorf2; ... ; ...
my %uORF_search_rev; #{$chr}{$pos}=$uorf1;uorf2; ... ; ...

for my $gene (keys %gene_model_fwd){

    my $chr=$gene_2_chr{$gene};
 
    if (exists ($uORF_start_coord{$gene})){

        for my $uorf (keys %{$uORF_start_coord{$gene}}){

            if (exists ($uORF_stop_coord{$gene}{$uorf})){

                my $ustart_coord=$uORF_start_coord{$gene}{$uorf};
                my $ustop_coord=$uORF_stop_coord{$gene}{$uorf};

                for my $coord ($uORF_start_coord{$gene}{$uorf} .. $uORF_stop_coord{$gene}{$uorf} ){              

                    my $genomic_pos=$gene_model_fwd{$gene}{$coord}; 
                    if (exists ($uORF_search_fwd{$chr}{$genomic_pos})){  #contaternate overlapping uorf
                        $uORF_search_fwd{$chr}{$genomic_pos}.=";".$uorf;                            
                    }else{ #initalise
                        $uORF_search_fwd{$chr}{$genomic_pos}=$uorf;
                    }                   
                }
            }
        }
    }
}

#for my $c (keys %uORF_search_fwd){
#    for my $p (sort {$a <=> $b} keys %{$uORF_search_fwd{$c}}){
#        print "fwd,$c,$p,$uORF_search_fwd{$c}{$p}\n";
#    }
#}

for my $gene (keys %gene_model_rev){

    my $chr=$gene_2_chr{$gene};

    if (exists ($uORF_start_coord{$gene})){

        for my $uorf (keys %{$uORF_start_coord{$gene}}){
 
            if (exists ($uORF_stop_coord{$gene}{$uorf})){

                my $ustart_coord=$uORF_start_coord{$gene}{$uorf};
                my $ustop_coord=$uORF_stop_coord{$gene}{$uorf};

                for my $coord ($uORF_start_coord{$gene}{$uorf} .. $uORF_stop_coord{$gene}{$uorf} ){

                    my $genomic_pos=$gene_model_rev{$gene}{$coord};

                    if (exists ($uORF_search_rev{$chr}{$genomic_pos})){  #contaternate overlapping uorf
                        $uORF_search_rev{$chr}{$genomic_pos}.=";".$uorf;                         
                    }else{ #initalise
                        $uORF_search_rev{$chr}{$genomic_pos}=$uorf;
                    }   
                }
            }
        }
    }
}

#for my $c (keys %uORF_search_rev){
#    for my $p (sort {$a <=> $b} keys %{$uORF_search_rev{$c}}){
#        print "rev,$c,$p,$uORF_search_rev{$c}{$p}\n";
#    }
#}


#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#open riboseq and assign
my $SSU_total;

open SSU,"samtools view $bam_SSU |";
while(<SSU>){

    next if(/^(\@)/);  ## skipping the header lines (if you used -h in the samools command)
    s/\n//;  s/\r//;  ## removing new line
    my @sam = split(/\t+/);  ## splitting SAM line into array

    my $leftMost=$sam[3]; #leftmost position of match 5' for fwd, 3' for rev
    my $flag=$sam[1];
    my $chr=$sam[2];
    my $mapq=$sam[4];
    my $cigar=$sam[5];
    my $seq=$sam[9];

    if ($chr=~/chr(.*)/){ #ensembl chromosome names do not contain the "chr" prefix
        $chr=$1;
    }

    unless ($flag & 0x4){   #if aligned

        if ($mapq >= 10){     #mapping uniqnes filter

            #both chew_2013 and subtelney_2014 riboseq are directional
            if ($flag & 0x10){  #Reverse reads. Starting from the leftmost position parse the cigar and check if it matches a uORF

                while ($cigar !~ /^$/){
                    if ($cigar =~ /^([0-9]+[MIDN])/){
                        my $cigar_part = $1;
                        if ($cigar_part =~ /(\d+)M/){   #alignment matching 
                            for my $pos ($leftMost .. ($leftMost+$1-1)){ #search though this position
                                if (exists ($uORF_search_rev{$chr}{$pos})){
                                    my @uORF=split(";",$uORF_search_rev{$chr}{$pos});
                                    for my $u (@uORF){
                                        $SSU_hits{$u}++;
                                        $SSU_total++;
                                    } 
                                }
                            }
                            $leftMost+=$1;
                        } elsif ($cigar_part =~ /(\d+)I/){  #insertion (to the reference) #do nothing this region is not in the reference
                        } elsif ($cigar_part =~ /(\d+)D/){  #deletion (from the reference)
                            $leftMost+=$1; #skip this position. Add to position count but do not search
                        } elsif ($cigar_part =~ /(\d+)N/){  #Skipped region from the reference
                            $leftMost+=$1; #skip this position. Add to position count but do not search  
                        }
                        $cigar =~ s/$cigar_part//;
                    }
                }

            }else{ #Forward reads. Starting from the leftmost position parse the cigar and check if it matches a uORF

                while ($cigar !~ /^$/){
                    if ($cigar =~ /^([0-9]+[MIDN])/){
                        my $cigar_part = $1;
                        if ($cigar_part =~ /(\d+)M/){   #alignment matching 
                            for my $pos ($leftMost .. ($leftMost+$1-1)){ #search though this position
                                if (exists ($uORF_search_fwd{$chr}{$pos})){
                                    my @uORF=split(";",$uORF_search_fwd{$chr}{$pos});
                                    for my $u (@uORF){
                                        $SSU_hits{$u}++;
                                        $SSU_total++;
                                    }
                                }
                            }
                            $leftMost+=$1;
                        } elsif ($cigar_part =~ /(\d+)I/){  #insertion (to the reference) #do nothing this region is not in the reference                     
                        } elsif ($cigar_part =~ /(\d+)D/){  #deletion (from the reference)
                            $leftMost+=$1; #skip this position. Add to position count but do not search
                        } elsif ($cigar_part =~ /(\d+)N/){  #Skipped region from the reference
                            $leftMost+=$1; #skip this position. Add to position count but do not search  
                        }
                        $cigar =~ s/$cigar_part//;
                    }
                }
            }
        }
    }
}
close(SSU);

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#again for LSU
my $LSU_total;

open LSU,"samtools view $bam_LSU |";
while(<LSU>){

    next if(/^(\@)/);  ## skipping the header lines (if you used -h in the samools command)
    s/\n//;  s/\r//;  ## removing new line
    my @sam = split(/\t+/);  ## splitting SAM line into array

    my $leftMost=$sam[3]; #leftmost position of match 5' for fwd, 3' for rev
    my $flag=$sam[1];
    my $chr=$sam[2];
    my $mapq=$sam[4];
    my $cigar=$sam[5];
    my $seq=$sam[9];

    if ($chr=~/chr(.*)/){ #ensembl chromosome names do not contain the "chr" prefix
        $chr=$1;
    }

    unless ($flag & 0x4){   #if aligned

        if ($mapq >= 10){     #mapping uniqnes filter

            if ($flag & 0x10){  #Reverse reads. Starting from the leftmost position parse the cigar and check if it matches a uORF

                while ($cigar !~ /^$/){
                    if ($cigar =~ /^([0-9]+[MIDN])/){
                        my $cigar_part = $1;
                        if ($cigar_part =~ /(\d+)M/){   #alignment matching 
                            for my $pos ($leftMost .. ($leftMost+$1-1)){ #search though this position
                                if (exists ($uORF_search_rev{$chr}{$pos})){
                                    my @uORF=split(";",$uORF_search_rev{$chr}{$pos});
                                    for my $u (@uORF){
                                        $LSU_hits{$u}++;
                                        $LSU_total++;
                                    }
                                }
                            }
                            $leftMost+=$1;
                        } elsif ($cigar_part =~ /(\d+)I/){  #insertion (to the reference) #do nothing this region is not in the reference
                        } elsif ($cigar_part =~ /(\d+)D/){  #deletion (from the reference)
                            $leftMost+=$1; #skip this position. Add to position count but do not search
                        } elsif ($cigar_part =~ /(\d+)N/){  #Skipped region from the reference
                            $leftMost+=$1; #skip this position. Add to position count but do not search  
                        }
                        $cigar =~ s/$cigar_part//;
                    }
                }

            }else{ #Forward reads. Starting from the leftmost position parse the cigar and check if it matches a uORF

                while ($cigar !~ /^$/){
                    if ($cigar =~ /^([0-9]+[MIDN])/){
                        my $cigar_part = $1;
                        if ($cigar_part =~ /(\d+)M/){   #alignment matching 
                            for my $pos ($leftMost .. ($leftMost+$1-1)){ #search though this position
                                if (exists ($uORF_search_fwd{$chr}{$pos})){
                                    my @uORF=split(";",$uORF_search_fwd{$chr}{$pos});
                                    for my $u (@uORF){
                                        $LSU_hits{$u}++;
                                        $LSU_total++;
                                    }
                                }
                            }
                            $leftMost+=$1;
                        } elsif ($cigar_part =~ /(\d+)I/){  #insertion (to the reference) #do nothing this region is not in the reference                     
                        } elsif ($cigar_part =~ /(\d+)D/){  #deletion (from the reference)
                            $leftMost+=$1; #skip this position. Add to position count but do not search
                        } elsif ($cigar_part =~ /(\d+)N/){  #Skipped region from the reference
                            $leftMost+=$1; #skip this position. Add to position count but do not search  
                        }
                        $cigar =~ s/$cigar_part//;
                    }
                }
            }
        }
    }
}
close(LSU);

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#Outpur uORF stats
#start                                         $uORF_start_fwd{$uorf_id} / $uORF_start_rev{$uorf_id}
#stop                                          $uORF_stop_fwd{$uorf_id} / $uORF_stop_rev{$uorf_id} #or coordinate based
#distance start to TSS                         five_prime_most_coord_fwd{$gene} - $uORF_start_coord{$gene}{$uorf_id}
#distance start to TIS                         $start_coord_fwd{$gene} - $uORF_start_coord{$gene}{$uorf_id}
#distance stop to TIS                          $start_coord_fwd{$gene} - $uORF_stop_coord{$gene}{$uorf_id}
#uORF_length                                   $uORF_start_coord{$gene}{$uorf_id} - $uORF_stop_coord{$gene}{$uorf_id}
#Kozak score                                   $uORF_kozak_score{$gene}{$uorf_id}
#Start codon                                   $uORF_start_codon{$uorf}
#Stop codon                                    $uORF_stop_codon{$uorf}
#SSU FPKM                                      $SSU_hits{$u}/SSU_total * uorf_length
#LSU FPKM
#Gene overlaps another gene                    $overlaps_inframe_gene{$gene}
#Leader overlap an upstream gene               $leader_overlaps_upstream{$gene}
#Trailer overlaps a downstream gene            $gene_overlaps_downstream_leader{$gene}
#Gene has a CAGE updated leader                $has_cage_defined_leader{$gene}
#uORF is most 5' best Kozak context in Gene    $best_uORF{$gene}

my $out_matrix=$outDir."/".$prefix."_uORF_matrix.csv";
open (OUT1,">$out_matrix")  || die "can't open $out_matrix\n";

print OUT1 "uorf_id,gene,chr,leader_length,dir,start,stop,distance_start_to_TSS,distance_start_to_TIS,distance_stop_to_TIS,uORF_length,kozak_score,start_codon,stop_codon,SSU_sum,LSU_sum,SSU_FPKM,LSU_FPKM,gene_overlaps_another_gene,leader_overlap_an_upstream_gene,trailer_overlaps_a_downstream_gene,gene_has_a_CAGE_updated_leader,uORF_overlaps_CDS,uORF_is_best_kozak_context_in_gene\n";

for my $uorf_id (keys %uORF_start_fwd){

    my $gene=$uORF_to_gene{$uorf_id};
    my $chr=$gene_2_chr{$gene};

    if ((exists ($uORF_start_coord{$gene}{$uorf_id})) && (exists ($uORF_stop_coord{$gene}{$uorf_id}))){

        my $leader_length=$start_coord_fwd{$gene}-$five_prime_most_coord_fwd{$gene};
        my $dir="fwd";
        my $start=$uORF_start_fwd{$uorf_id}; 
        my $stop=$uORF_stop_fwd{$uorf_id};
        my $distance_start_to_TSS=$five_prime_most_coord_fwd{$gene} - $uORF_start_coord{$gene}{$uorf_id};
        my $distance_start_to_TIS=$start_coord_fwd{$gene} - $uORF_start_coord{$gene}{$uorf_id};
        my $distance_stop_to_TIS=$start_coord_fwd{$gene} - $uORF_stop_coord{$gene}{$uorf_id};    
        my $uORF_length=$uORF_stop_coord{$gene}{$uorf_id} - $uORF_start_coord{$gene}{$uorf_id};
        my $kozak_score=$uORF_kozak_score{$uorf_id};
        my $start_codon=$uORF_start_codon{$uorf_id};
        my $stop_codon=$uORF_stop_codon{$uorf_id};
        my $SSU_sum=$SSU_hits{$uorf_id};
        my $LSU_sum=$LSU_hits{$uorf_id};
        my $SSU_FPKM=eval{ $SSU_hits{$uorf_id} / ($uORF_length * $SSU_total) * 1000000000 } || 0;
        my $LSU_FPKM=eval{ $LSU_hits{$uorf_id} / ($uORF_length * $LSU_total) * 1000000000 } || 0;
        my $gene_overlaps_another_gene=$overlaps_inframe_gene{$gene};
        my $leader_overlap_an_upstream_gene=$leader_overlaps_upstream{$gene};
        my $trailer_overlaps_a_downstream_gene=$gene_overlaps_downstream_leader{$gene};
        my $gene_has_a_CAGE_updated_leader=$has_cage_defined_leader{$gene};
        my $uorf_overlaps_CDS=$uORF_overlaps_CDS{$uorf_id};
        my $uORF_is_best_kozak_context_in_gene=0;
        if ($uorf_id eq $best_uORF{$gene}){ $uORF_is_best_kozak_context_in_gene=1; }

        print OUT1 "$uorf_id,$gene,$chr,$leader_length,$dir,$start,$stop,$distance_start_to_TSS,$distance_start_to_TIS,$distance_stop_to_TIS,$uORF_length,$kozak_score,$start_codon,$stop_codon,$SSU_sum,$LSU_sum,$SSU_FPKM,$LSU_FPKM,$gene_overlaps_another_gene,$leader_overlap_an_upstream_gene,$trailer_overlaps_a_downstream_gene,$gene_has_a_CAGE_updated_leader,$uorf_overlaps_CDS,$uORF_is_best_kozak_context_in_gene\n";
    }
}

for my $uorf_id (keys %uORF_start_rev){
    my $gene=$uORF_to_gene{$uorf_id};
    my $chr=$gene_2_chr{$gene};

    if ((exists ($uORF_start_coord{$gene}{$uorf_id})) && (exists ($uORF_stop_coord{$gene}{$uorf_id}))){

        my $leader_length=$start_coord_rev{$gene}-$five_prime_most_coord_rev{$gene};
        my $dir="rev";
        my $start=$uORF_start_rev{$uorf_id};
        my $stop=$uORF_stop_rev{$uorf_id};
        my $distance_start_to_TSS=$five_prime_most_coord_rev{$gene}-$uORF_start_coord{$gene}{$uorf_id};
        my $distance_start_to_TIS=$start_coord_rev{$gene} - $uORF_start_coord{$gene}{$uorf_id};
        my $distance_stop_to_TIS=$start_coord_rev{$gene} - $uORF_stop_coord{$gene}{$uorf_id};
        my $uORF_length=$uORF_stop_coord{$gene}{$uorf_id} - $uORF_start_coord{$gene}{$uorf_id};
        my $kozak_score=$uORF_kozak_score{$uorf_id};
        my $start_codon=$uORF_start_codon{$uorf_id};
        my $stop_codon=$uORF_stop_codon{$uorf_id};
        my $SSU_sum=$SSU_hits{$uorf_id};  #initalise
        my $LSU_sum=$LSU_hits{$uorf_id};
        my $SSU_FPKM=eval{ $SSU_hits{$uorf_id} / ($uORF_length * $SSU_total) * 1000000000 } || 0; 
        my $LSU_FPKM=eval{ $LSU_hits{$uorf_id} / ($uORF_length * $LSU_total) * 1000000000 } || 0;
        my $gene_overlaps_another_gene=$overlaps_inframe_gene{$gene};
        my $leader_overlap_an_upstream_gene=$leader_overlaps_upstream{$gene};
        my $trailer_overlaps_a_downstream_gene=$gene_overlaps_downstream_leader{$gene};
        my $gene_has_a_CAGE_updated_leader=$has_cage_defined_leader{$gene};
        my $uorf_overlaps_CDS=$uORF_overlaps_CDS{$uorf_id};
        my $uORF_is_best_kozak_context_in_gene=0;
        if ($uorf_id eq $best_uORF{$gene}){ $uORF_is_best_kozak_context_in_gene=1; }

        print OUT1 "$uorf_id,$gene,$chr,$leader_length,$dir,$start,$stop,$distance_start_to_TSS,$distance_start_to_TIS,$distance_stop_to_TIS,$uORF_length,$kozak_score,$start_codon,$stop_codon,$SSU_sum,$LSU_sum,$SSU_FPKM,$LSU_FPKM,$gene_overlaps_another_gene,$leader_overlap_an_upstream_gene,$trailer_overlaps_a_downstream_gene,$gene_has_a_CAGE_updated_leader,$uorf_overlaps_CDS,$uORF_is_best_kozak_context_in_gene\n";
   }
}


#output the coverage plots






#output 5/3' length plots for heatmaps




#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
##caclulate log2
sub log2 {
    my $n = shift;
    my $l = log($n)/log(2);
    #$l = sprintf("%.2f",$l);
    return $l;
}

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#score the tis surrounding sequence against the Zebrafish kozak PWM
sub score_kozak{
    my $upstream = shift;
    my $downstream = shift;
    my $score=0;
    my $seq_to_score=uc($upstream.$downstream); #concaternate and set to uppercase   
    my @seq_to_score=split("",$seq_to_score);
    my $count=0;

    for my $base (@seq_to_score){
        if (exists ($PWM{$count}{$base} )){
            $score+=$PWM{$count}{$base};
        }
        $count++;
    }
    return $score;
}

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
sub score_kozak_fwd{
    my $gene=$_[0];
    my $pos=$_[1];
    my $chr=$_[2];

    my @up;

    #can I do this in coord space?

    for (-4 .. -1){
        push (@up, substr($fasta_sequences{$chr}, (($pos+$_)-1), 1));
    }

    my @down;
    for (3 .. 4){
        push (@down, substr($fasta_sequences{$chr}, (($pos+$_)-1), 1));
    }

    my $seq_up=join("", @up);
    my $seq_down=join("", @down);
    my $kozak_strength=&score_kozak($seq_up,$seq_down);

    return $kozak_strength;
}

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
sub score_kozak_rev{
    my $gene=$_[0];
    my $pos=$_[1];
    my $chr=$_[2];

    my @up;

    for (1 .. 4){
        push (@up, substr($fasta_sequences{$chr}, (($pos+$_)-1), 1));
    }

    my @down;
    for (-4 .. -3){
        push (@down, substr($fasta_sequences{$chr}, (($pos+$_)-1), 1));
    }

    my $seq_up=reverse(join("", @up));
    my $seq_down=reverse(join("", @down));
    $seq_up=~tr/ACGTacgt/TGCAtgca/;
    $seq_down=~tr/ACGTacgt/TGCAtgca/;
    my $kozak_strength=&score_kozak($seq_up,$seq_down);

    return $kozak_strength;
}

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
