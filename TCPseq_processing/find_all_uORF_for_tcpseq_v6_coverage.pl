#!/usr/bin/perl -w
use strict;

#17/04/18
#script to assign FMPK counts to uoRFs
#calculates kozak scores for zebrafish uORFS
#select the uORF with the highest Kozak score for each uORF (most upstream is tied)

my $inGtf=$ARGV[0]; 
my $fasta=$ARGV[1];
my $leaders=$ARGV[2];
my $uORFS=$ARGV[3];
my $bam_RIBOseq=$ARGV[4];  #edited to take one input bam at a time
my $outDir=$ARGV[5];
my ($prefix)=$bam_RIBOseq=~/([^\/]+).bam$/;

#excluding:
#genes where the TSS is downstream of the start codon
#genes without a detectable cage peak 
#genes that are annotated as protien_coding

#Flags:
#overlapping_gene: The longest transcript of this gene overlaps with the longest transcript of another gene
#leader_potentially_overlaps_upstream_gene: There is an upstream gene within 500nt of the start codon of this gene 
#gene_potentially_overlaps_downstream_leader: There is an downstream gene start codon withing 500nt of the 3' most position of this gene (in yeast this is the stop codon).

my $PERCENTAGE_TO_EXCLUDE=0.1;
#my $PERCENTAGE_TO_EXCLUDE=0.5;

my $KOZAK_MIN=-5;  #-15
my $KOZAK_MAX=-20; #-20;

#my $STOP_CODON="TTA";

my %accepted_codons =
    (
        "TAA" => 1,
        "TAG" => 1,
        "TGA" => 1,
    );

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#Zebrafish Zozak position frequency matrix (PFM) from Grzegorski et. al, 2015                   
#Position   -4  -3  -2  -1  4   5
#A  35  62  39  28  24  27
#C  32  5   23  36  12  42
#G  19  28  17  27  46  16
#T  14  5   21  10  17  15

my %raw_kozak =
   (
       "A" => [ 35, 62, 39, 28, 24, 27 ],
       "C" => [ 32, 5, 23, 36, 12, 42 ],
       "G" => [ 19, 28, 17, 27, 46, 16 ],
       "T" => [ 14, 5, 21, 10, 17, 15 ],
   );

my %PWM; #key1: position, key2: base, value: weight
for my $pos (0 .. 5){

    #0 == -4 #nucleotide position in relation to start codon
    #1 == -3
    #2 == -2
    #3 == -1
    #4 == +4
    #5 == +5

    my $pos_sum=0;
    my $pwm_sum=0;
    for my $base (keys %raw_kozak){ #sum the nucleotide frequencies per position
        $pos_sum+=$raw_kozak{$base}[$pos];
    }

    for my $base(keys %raw_kozak){ #score the PWM
        my $psudo_count= sqrt($pos_sum);
        my $background_probability=0.25; #no base preference
        my $pwm=&log2( ($raw_kozak{$base}[$pos] + $psudo_count * $background_probability) / ($pos_sum + $psudo_count * $background_probability));
        $PWM{$pos}{$base}=$pwm;
        $pwm_sum+=$pwm;
    }

    $PWM{$pos}{"N"}=($pwm_sum/4); #set "N" to be equal to the column mean. For genes with short leaders, missing upstream positions 
} 

#Yeast 2011	The mRNA landscape at yeast translation initiation sites				
#my %yeast_matrix =
#   (            #-4   -3    -2    -1     4     5
#       "A" => [ 0.53, 1.02, 0.47, 0.58, -0.11, -0.6 ],
#       "C" => [ 0.16, -1.16, 0.16, -0.12, -0.47, 1.26 ],
#       "G" => [ -0.3, 0.2, -0.52, -0.15, 0.77, -0.11 ],
#       "T" => [ -0.74, -2.1, -0.49, -0.71, -0.18, -0.75 ],
#   );

my %PWM_yeast =
(
    0 => { 
        "A" => 0.53,
        "C" => 0.16,
        "G" => -0.3,
        "T" => -0.74
    },
    1 => {
        "A" => 1.02,
        "C" => -1.16,
        "G" => 0.2,
        "T" => -2.1
    },
    2 => { 
        "A" => 0.47,
        "C" => 0.16,
        "G" => -0.52,
        "T" => -0.49
    },
    3 => { 
        "A" => 0.58,
        "C" => -0.12,
        "G" => -0.15,
        "T" => -0.71
    },
    4 => {
        "A" => -0.11,
        "C" => -0.47,
        "G" => 0.77,
        "T" => -0.18
    },
    5 => {
        "A" => -0.6,
        "C" => 1.26,
        "G" => -0.11,
        "T" => -0.75
    }
);

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#open gtf and get transcript lengths
my %transcripts; #key = gene_id, transcript_id, #value = sum_exon_lengths;

open(GENES1,$inGtf) || die "can't open $inGtf";      #gft is 1 based
while (<GENES1>){
    unless(/^#/){
        my @b=split("\t");
        my $chr=$b[0];
        my $class=$b[2];
        my $start=$b[3];
        my $end=$b[4];
        my $dir=$b[6];
        my ($gene_id) = $b[8] =~ /gene_id\s"([^\"]+)";/;
        my ($transcript_id) = $b[8] =~ /transcript_id\s"([^\"]+)";/;
        my $gene_biotype="NA";
        if ($b[8] =~ /gene_biotype\s"([^\"]+)";/){
            $gene_biotype=$1;
        }

        if ($gene_id && $transcript_id){

            if ($gene_biotype eq "protein_coding"){ #restrict to protien coding genes (control for ncRNAs)

                if ($class eq "exon"){
                    if ($dir eq "+"){
                        for ($start .. $end){
                            $transcripts{$gene_id}{$transcript_id}++;
                        }
                    }else{
                        for ($start .. $end){
                            $transcripts{$gene_id}{$transcript_id}++;
                        }
                    }
                }
            }
        }
    }
}
close (GENES1);

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#select longest transcripts per gene
my %longest_transcript; #key=gene_id, value=transcript_id
for my $gene (keys %transcripts){
    my $longest=0;
    for my $transcript (keys %{ $transcripts{$gene}} ){
        if ($transcripts{$gene}{$transcript} > $longest) {
            $longest_transcript{$gene}=$transcript;
            $longest=$transcripts{$gene}{$transcript};
        }
    }
}

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#second pass through the genome, find annotated start codons and setup transcript models for longest transcript of each gene
my %gene_start_codon_fwd;
my %gene_stop_codon_fwd;
my %gene_exons_fwd;
my %gene_start_codon_rev;
my %gene_stop_codon_rev;
my %gene_exons_rev;
my %gene_2_chr; #key = gene_id; value = chr

my %overlaps_inframe_gene;
my %leader_overlaps_upstream;
my %gene_overlaps_downstream_leader;
my %has_cage_defined_leader;

open(GENES2,$inGtf) || die "can't open $inGtf";      #gft is 1 based
while (<GENES2>){
    unless(/^#/){
        my @b=split("\t");
        my $chr=$b[0];
        my $class=$b[2];
        my $start=$b[3];
        my $end=$b[4];
        my $dir=$b[6];
        my ($gene_id) = $b[8] =~ /gene_id\s"([^\"]+)";/;
        my ($transcript_id) = $b[8] =~ /transcript_id\s"([^\"]+)";/;

        if ($gene_id && $transcript_id){

            if (exists ( $longest_transcript{$gene_id} )){    #if the transcript is in the list of longest transcripts

                if ($transcript_id eq $longest_transcript{$gene_id}){

                    $gene_2_chr{$gene_id}=$chr;
                    $overlaps_inframe_gene{$gene_id}=0;
                    $leader_overlaps_upstream{$gene_id}=0;
                    $gene_overlaps_downstream_leader{$gene_id}=0;
                    $has_cage_defined_leader{$gene_id}=0;

                    if ($dir eq "+"){ #fwd cases. Use start positions as 5'

                        if ($class eq "start_codon"){
                            $gene_start_codon_fwd{$gene_id}=$start;
                        }
                        if ($class eq "stop_codon"){
                            $gene_stop_codon_fwd{$gene_id}=$start;
                        }
                        if ($class eq "exon"){
                            $gene_exons_fwd{$gene_id}{$start}=$end;
                        }

                    }else{ #revese cases use end as 5'

                        if ($class eq "start_codon"){
                            $gene_start_codon_rev{$gene_id}=$end;
                        }
                        if ($class eq "stop_codon"){
                            $gene_stop_codon_rev{$gene_id}=$end;
                        }
                        if ($class eq "exon"){
                            $gene_exons_rev{$gene_id}{$start}=$end;
                        }
                    }
                }
            }
        }
    }
}
close(GENES2);

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#open fasta for codon sequeces

my %fasta_sequences; #key = sequence_name, value=sequence
my $name;
open (FA, $fasta) || die "can't open $fasta";
while (<FA>){
    chomp;
    if (/^>([^\s]+)/){ #take header up to the first space
        $name=$1;
        if ($name =~ /^chr(.*)/){
           $name=$1; #if the chr name have a chr* prefix, remove it 
        }
    }else{
        $fasta_sequences{$name}.=$_;
    }
}
close(FA);

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#setup transcript models

my %gene_model_fwd;
my %start_coord_fwd;
my %stop_coord_fwd;

#5' is the #gene_model{$gene}{0}
#3' is the last coord
my %three_prime_most_coord_fwd;
my %five_prime_most_coord_fwd;

#gene_model{$gene}{0}=12345   #1st nt of start codon
#gene_model{$gene}{1}=12346
#gene_model{$gene}{2}=12347
#gene_model{$gene}{3}=12348
#gene_model{$gene}{4}=12349
#...
#to end of exons             #last nt of stop codon

for my $gene (keys %gene_exons_fwd){
    if ( (exists ($gene_start_codon_fwd{$gene})) && (exists ($gene_stop_codon_fwd{$gene})) ) { #restrict to genes with annotated start + stop codon

        my $model_pos=0;
        $five_prime_most_coord_fwd{$gene}=$model_pos;  #initalise the 5' to the first coord coord

        for my $exon_start (sort {$a <=> $b} keys %{ $gene_exons_fwd{$gene} } ){
            my $exon_end=$gene_exons_fwd{$gene}{$exon_start};

            #fwd exons are in ascending order
            # start(-1)-> 100958 100975
            #             101077 101715 <-end(+1)

            for ($exon_start .. $exon_end){
                $gene_model_fwd{$gene}{$model_pos}=$_;

                if ($_ == $gene_stop_codon_fwd{$gene}){
                    $stop_coord_fwd{$gene}=$model_pos;    #find the index of the stop codon per gene
                }

                if ($_ == $gene_start_codon_fwd{$gene}){
                    $start_coord_fwd{$gene}=$model_pos;    #find the index of the start codon per gene
                }
                $model_pos++;
            }
        }
        $three_prime_most_coord_fwd{$gene}=$model_pos-1; #store the 3 prime most position of each gene
    }
}

my %gene_model_rev;
my %start_coord_rev;
my %stop_coord_rev;

#5' is the #gene_model{$gene}{0}
#3' is the last coord
my %three_prime_most_coord_rev;
my %five_prime_most_coord_rev;

for my $gene (keys %gene_exons_rev){
    if ( (exists ($gene_start_codon_rev{$gene})) && (exists ($gene_stop_codon_rev{$gene})) ) { #restrict to genes with annotated start + stop codon

        my $model_pos=0;
        $five_prime_most_coord_rev{$gene}=$model_pos;  #initalise the 5' to the first coord coord

        for my $exon_end (reverse (sort {$a <=> $b} keys %{ $gene_exons_rev{$gene} } )){
            my $exon_start=$gene_exons_rev{$gene}{$exon_end};

            #rev exons are sorted in decending order  
            #           447087 447794 <-start(+1)
            # end(-1)-> 446060 446254

            while ($exon_start >= $exon_end){
                $gene_model_rev{$gene}{$model_pos}=$exon_start;

                if ($exon_start == $gene_stop_codon_rev{$gene}){
                    $stop_coord_rev{$gene}=$model_pos;    #find the index of the stop codon per gene
                }
                if ($exon_start == $gene_start_codon_rev{$gene}){
                    $start_coord_rev{$gene}=$model_pos;    #find the index of the start codon per gene
                }
                $model_pos++;
                $exon_start--;
            }
        }
        $three_prime_most_coord_rev{$gene}=$model_pos-1; #store the 3 prime most position of each gene
    }
}

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#check transcript models
#for my $g (keys %gene_model_fwd){
#    for my $p (sort {$a <=> $b} keys %{$gene_model_fwd{$g}}){
#        print "fwd,$g,$gene_2_chr{$g},$p,$gene_model_fwd{$g}{$p}\n";
#    }
#}

#for my $g (keys %gene_model_rev){
#    for my $p (sort {$a <=> $b} keys %{$gene_model_rev{$g}}){
#        print "rev,$g,$gene_2_chr{$g},$p,$gene_model_rev{$g}{$p}\n";
#    }
#}

#exit;

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#parse leaders
my %leader_positions_fwd;
my %leader_positions_rev;

#filters:
my %leader_length;
my %cage_peak_value;

#gene_id,       0
#transcript_id, 1
#chr,           2
#dir,           3
#overlaps_inframe_gene,           4
#leader_overlaps_upstream,        5
#gene_overlaps_downstream_leader, 6
#highest_cage_peak,               7
#count_at_highest_cage_peak,      8
#leader_length                    9

#ENSDARG00000037917,ENSDART00000161963,3,fwd,FALSE,FALSE,FALSE,34716685,2.30440468389324,143
#ENSDARG00000104069,ENSDART00000167982,5,fwd,FALSE,FALSE,FALSE,337237,9.98331428397882,122
#ENSDARG00000037925,ENSDART00000130591,3,fwd,FALSE,FALSE,FALSE,36250098,0.346429817638395,-679
#ENSDARG00000029263,ENSDART00000078466,3,fwd,FALSE,FALSE,FALSE,NaN,0,NaN

open(LEAD, $leaders) || die "can't open $leaders";
while (<LEAD>){
    unless(/^#/){
 
        chomp;
        my @b=split(",");

        my $gene=$b[0];
        my $transcript=$b[1];
        my $chr=$b[2];
        my $dir=$b[3];
        my $overlaps_inframe_gene=$b[4];
        my $leader_overlaps_upstream=$b[5];
        my $gene_overlaps_downstream_leader=$b[6];
        my $highest_cage_peak=$b[7];
        my $count_at_highest_cage_peak=$b[8];
        my $leader_length=$b[9];

        if ($overlaps_inframe_gene eq "TRUE"){ $overlaps_inframe_gene{$gene}=1; }
        if ($leader_overlaps_upstream eq "TRUE"){ $leader_overlaps_upstream{$gene}=1; }
        if ($gene_overlaps_downstream_leader eq "TRUE"){ $gene_overlaps_downstream_leader{$gene}=1; }

        unless ($leader_length eq "NaN"){  #only take genes that have a detectable cage peak

 #I have exlcuded long leaders

            unless ($leader_length < 0 ){  #exlude genes with negative leader sizes, as they cuase problems with FPKM

                if ($dir eq "fwd"){ 
                    if (exists ($start_coord_fwd{$gene})){
                        unless ($highest_cage_peak >=  $gene_model_fwd{$gene}{$start_coord_fwd{$gene}}){  #exclude genes where the TSS is downstream of the start codon
                            $leader_positions_fwd{$gene}=$highest_cage_peak;
                            $cage_peak_value{$gene}=$count_at_highest_cage_peak;
                            $leader_length{$gene}=$leader_length;
                            $has_cage_defined_leader{$gene}=1;
                        } 
                    }
                }else{
                    if (exists ($start_coord_rev{$gene})){
                        unless ($highest_cage_peak <=  $gene_model_rev{$gene}{$start_coord_rev{$gene}}){  #exclude genes where the TSS is downstream of the start codon
                            $leader_positions_rev{$gene}=$highest_cage_peak;
                            $cage_peak_value{$gene}=$count_at_highest_cage_peak;
                            $leader_length{$gene}=$leader_length;
                            $has_cage_defined_leader{$gene}=1;
                        }
                    }
                }     
            }
        }
    }
}
close(LEAD);

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#extend transcript models to incorportate cage derived leaders
my %leader_start_coord; #key=gene, value=coord

for my $gene (keys %leader_positions_fwd){

    if (exists ($gene_2_chr{$gene})){  #to restict to protien_coding genes
        my $leader_start=$leader_positions_fwd{$gene};
        my $three_prime_coord=$three_prime_most_coord_fwd{$gene};
        my $three_prime_pos=$gene_model_fwd{$gene}{$three_prime_coord};
        my $five_prime_coord=0;
        my $five_prime_pos=$gene_model_fwd{$gene}{$five_prime_coord};

        if ($leader_start >= $five_prime_pos){  #great, just find and save the coordinate
 
            for my $coord ($five_prime_coord .. $three_prime_coord){
                my $pos=$gene_model_fwd{$gene}{$coord};       
                if ($pos == $leader_start){
                    $five_prime_most_coord_fwd{$gene}=$coord;
                    last;
                }
            }

        }else{  #extend the coords

            my $extended_coord=0; 
            while ($five_prime_pos > $leader_start){        
                $extended_coord--;
                $five_prime_pos--;
                $gene_model_fwd{$gene}{$extended_coord}=$five_prime_pos;
            }
            $five_prime_most_coord_fwd{$gene}=$extended_coord;
        }
    }
} 

for my $gene (keys %leader_positions_rev){

    if (exists ($gene_2_chr{$gene})){  #to restict to protien_coding genes
        my $leader_start=$leader_positions_rev{$gene};
        my $three_prime_coord=$three_prime_most_coord_rev{$gene};
        my $three_prime_pos=$gene_model_rev{$gene}{$three_prime_coord};
        my $five_prime_coord=0;
        my $five_prime_pos=$gene_model_rev{$gene}{$five_prime_coord};
 
        if ($leader_start <= $five_prime_pos){  #great, just find and save the coordinate

            for my $coord ($five_prime_coord .. $three_prime_coord){
                my $pos=$gene_model_rev{$gene}{$coord};
                if ($pos == $leader_start){
                    $leader_start_coord{$gene}=$coord;
                    $five_prime_most_coord_rev{$gene}=$coord;
                    last;
                }
            }

        }else{   #extend the coords

            my $extended_coord=0;
            while ($five_prime_pos < $leader_start){
                $extended_coord--;
                $five_prime_pos++;
                $gene_model_rev{$gene}{$extended_coord}=$five_prime_pos;
            }
            $five_prime_most_coord_rev{$gene}=$extended_coord;
        }
    }
}

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#Transcript coord key
#Transcript 5' coord   0                                  #1st nt of annotated transcript
#Transcript 3' coord   $three_prime_most_coord_???{$gene} #last nt of transcript
#Start codon coord     $start_coord_???{$gene}            #1st nt in start codon
#Stop codon coord      $stop_coord_???{$gene}             #1st nt in stop codon
#Leader start coord    $leader_start_coord{$gene}         #1st nt of cage defined leader

#$gene_model_fwd{$gene}{$coord}==genomic position

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#loop though genes and assign leader, CDS and trailer regions to hashes for quick searching. (added start + stop codons).

my %leader_search_fwd;
my %leader_search_rev;

my %start_codons_search_fwd; #key1=chr, key2=pos, value=gene
my %start_codons_search_rev; #key1=chr, key2=pos, value=gene
my %stop_codons_search_fwd; #key1=chr, key2=pos, value=gene
my %stop_codons_search_rev; #key1=chr, key2=pos, value=gene
 
for my $gene (keys %gene_model_fwd){
    my $chr=$gene_2_chr{$gene};
    my $start_coord=$start_coord_fwd{$gene};
    my $stop_coord=$stop_coord_fwd{$gene};

    for my $coord (sort {$a <=> $b} keys %{ $gene_model_fwd{$gene} } ){
        my $pos=$gene_model_fwd{$gene}{$coord};

        if ($coord == $start_coord){   $start_codons_search_fwd{$chr}{$pos}=$gene; } 
        if ($coord == $start_coord+1){ $start_codons_search_fwd{$chr}{$pos}=$gene; }
        if ($coord == $start_coord+2){ $start_codons_search_fwd{$chr}{$pos}=$gene; }

        if ($coord == $stop_coord){   $stop_codons_search_fwd{$chr}{$pos}=$gene; }
        if ($coord == $stop_coord+1){ $stop_codons_search_fwd{$chr}{$pos}=$gene; }
        if ($coord == $stop_coord+2){ $stop_codons_search_fwd{$chr}{$pos}=$gene; }
           
        if ($coord < $start_coord){
            $leader_search_fwd{$chr}{$pos}=$gene;
        }
    }
}

for my $gene (keys %gene_model_rev){
    my $chr=$gene_2_chr{$gene};
    my $start_coord=$start_coord_rev{$gene};
    my $stop_coord=$stop_coord_rev{$gene};

    for my $coord (sort {$a <=> $b} keys %{ $gene_model_rev{$gene} } ){
        my $pos=$gene_model_rev{$gene}{$coord};

        if ($coord == $start_coord){   $start_codons_search_rev{$chr}{$pos}=$gene; }
        if ($coord == $start_coord+1){ $start_codons_search_rev{$chr}{$pos}=$gene; }
        if ($coord == $start_coord+2){ $start_codons_search_rev{$chr}{$pos}=$gene; }

        if ($coord == $stop_coord){   $stop_codons_search_rev{$chr}{$pos}=$gene; }
        if ($coord == $stop_coord+1){ $stop_codons_search_rev{$chr}{$pos}=$gene; }
        if ($coord == $stop_coord+2){ $stop_codons_search_rev{$chr}{$pos}=$gene; }

        if ($coord < $start_coord){
            $leader_search_rev{$chr}{$pos}=$gene;
        }
    }
}

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#Get the uORFs per longest transcript

my %uORF_start_fwd;   #uORF_ID = $start_coord
my %uORF_stop_fwd;    #uORF_ID = $stop_coord
my %uORF_start_rev;   #uORF_ID = $start_coord
my %uORF_stop_rev;    #uORF_ID = $stop_coord
my %uORF_to_chr;
my %uORF_to_gene;

open(UORFS, $uORFS) || die "can't open $uORFS";
while (<UORFS>){

    #there are no headers in the bed files from orfick
    #uorfs are listed as transcriptId_<rank from 5' accending>.transcriptID
    #uorfs containg introns will be split over multiple lines with the same id

    chomp;
    my @u=split("\t");
    my $chr=$u[0];
    my $start=$u[1]+1;     #start is zero based
    my $stop=$u[2];
    my $uorf_id=$u[3];
    my $dir=$u[5];

    my ($transcript, $rank)=$uorf_id=~/^(\w+)\_(\d+)\.\w+$/;

    if ($dir eq "+"){

        if (exists ($leader_search_fwd{$chr}{$start})){
            my $gene=$leader_search_fwd{$chr}{$start};

            unless (exists ($uORF_start_fwd{$uorf_id})){  #for forward cases take the first start position
                $uORF_start_fwd{$uorf_id}=$start;
            }

            $uORF_stop_fwd{$uorf_id}=$stop; #take the last stop position ##assumes the gtf is sorted 5 -> 3 
            $uORF_to_chr{$uorf_id}=$chr;
            $uORF_to_gene{$uorf_id}=$gene;
       }
    }else{

       if (exists ($leader_search_rev{$chr}{$stop})){
            my $gene=$leader_search_rev{$chr}{$stop};

            $uORF_start_rev{$uorf_id}=$stop; #take the last start posotion
            unless (exists ($uORF_stop_rev{$uorf_id})){  #for reverse cases take the first stop position
                $uORF_stop_rev{$uorf_id}=$start; 
                #this can be problematic of the stio codon itself is split over introns
            }

            $uORF_to_gene{$uorf_id}=$gene;
            $uORF_to_chr{$uorf_id}=$chr;
        }
    }
}
close(UORFS);

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#assign orf feaatures from  coords
my %uORF_kozak_score; #uORF_ID = score
my %uORF_start_coord;
my %uORF_stop_coord;
my %uORF_start_codon;
my %uORF_stop_codon;

for my $uorf_id (keys %uORF_start_fwd){
    my $gene=$uORF_to_gene{$uorf_id};
    my $chr=$gene_2_chr{$gene};

    $uORF_kozak_score{$uorf_id}=0;
    $uORF_stop_codon{$uorf_id}="NNN";
    $uORF_start_codon{$uorf_id}="NNN";

    my @TIS;
    my @up;
    my @down;
    my @stop;
    my $seq_up;
    my $seq_TIS;
    my $seq_down;
    my $seq_stop;

    my $score="NA";

    for my $coord (sort {$a <=> $b} keys %{ $gene_model_fwd{$gene} } ){
        my $pos=$gene_model_fwd{$gene}{$coord};

        if ($pos == $uORF_start_fwd{$uorf_id}){
            $uORF_start_coord{$gene}{$uorf_id}=$coord;

            for(0 .. 2){
                if (exists ($gene_model_fwd{$gene}{ ($coord+$_) } )){
                     push (@TIS, substr($fasta_sequences{$chr}, ($gene_model_fwd{$gene}{ ($coord+$_) } -1), 1) );
                }else{
                     push (@TIS, "N");
                }
            }

            for (-4 .. -1){
                if (exists ($gene_model_fwd{$gene}{ ($coord+$_) } )){
                    if (exists ($five_prime_most_coord_fwd{$gene})){
                        if (($coord+$_) >= ($five_prime_most_coord_fwd{$gene}-1)){ #check we're not extending behyond leader
                            push (@up, substr($fasta_sequences{$chr}, ($gene_model_fwd{$gene}{ ($coord+$_) } -1), 1) );
                        }else{
                            push (@up, "N");
                        }
                    }
                }else{
                     push (@up, "N");
                }
            }

            for (3 .. 4){
                if (exists ($gene_model_fwd{$gene}{ ($coord+$_) } )){
                    push (@down, substr($fasta_sequences{$chr}, ($gene_model_fwd{$gene}{ ($coord+$_) } -1), 1) );
                }else{
                    push (@down, "N");
                }
            }

            $seq_up=join("", @up);
            $seq_TIS=join("", @TIS);
            $seq_down=join("", @down);
            $score=&score_kozak($seq_up,$seq_down);
        }

        if ($pos == $uORF_stop_fwd{$uorf_id}){
            $uORF_stop_coord{$gene}{$uorf_id}=$coord;

            for(-2 .. 0){
                if (exists ($gene_model_fwd{$gene}{ ($coord+$_) } )){
                     push (@stop, substr($fasta_sequences{$chr}, ($gene_model_fwd{$gene}{ ($coord+$_) } -1), 1) );
                }else{
                     push (@stop, "N");
                }
            }
            $seq_stop=join("", @stop);
        }
    }
    $uORF_kozak_score{$uorf_id}=$score;
    $uORF_start_codon{$uorf_id}=$seq_TIS;
    $uORF_stop_codon{$uorf_id}=$seq_stop;
}

for my $uorf_id (keys %uORF_start_rev){
    my $gene=$uORF_to_gene{$uorf_id};
    my $chr=$gene_2_chr{$gene};

    $uORF_kozak_score{$uorf_id}=0;
    $uORF_stop_codon{$uorf_id}="NNN";
    $uORF_start_codon{$uorf_id}="NNN";

    my @TIS;
    my @up;
    my @down;
    my @stop;
    my $seq_up;
    my $seq_TIS;
    my $seq_down;
    my $seq_stop;

    my $score="NA";

    for my $coord (sort {$a <=> $b} keys %{ $gene_model_rev{$gene} } ){
        my $pos=$gene_model_rev{$gene}{$coord};

        if ($pos == $uORF_start_rev{$uorf_id}){
            $uORF_start_coord{$gene}{$uorf_id}=$coord;

            for(0 .. 2){
                if (exists ($gene_model_rev{$gene}{ ($coord+$_) } )){
                     push (@TIS, substr($fasta_sequences{$chr}, ($gene_model_rev{$gene}{ ($coord+$_) } -1), 1) );
                }else{
                     push (@TIS, "N");
                }
            }

            for (-4 .. -1){
                if (exists ($gene_model_rev{$gene}{ ($coord+$_) } )){
                    if (exists ($five_prime_most_coord_rev{$gene})){
                        if (($coord+$_) >= ($five_prime_most_coord_rev{$gene}-1)){ #check we're not extending behyond leader
                            push (@up, substr($fasta_sequences{$chr}, ($gene_model_rev{$gene}{ ($coord+$_) } -1), 1) );
                        }else{
                            push (@up, "N");
                        }
                    }
                }else{
                     push (@up, "N");
                }
            }

            for (3 .. 4){
                if (exists ($gene_model_rev{$gene}{ ($coord+$_) } )){
                    push (@down, substr($fasta_sequences{$chr}, ($gene_model_rev{$gene}{ ($coord+$_) } -1), 1) );
                }else{
                    push (@down, "N");
                }
            }

            $seq_up=join("", @up);
            $seq_TIS=join("", @TIS);
            $seq_down=join("", @down);
            $seq_up=~tr/ACGTacgt/TGCAtgca/;
            $seq_TIS=~tr/ACGTacgt/TGCAtgca/;
            $seq_down=~tr/ACGTacgt/TGCAtgca/;
            $score=&score_kozak($seq_up,$seq_down);
        }

        if ($pos == $uORF_stop_rev{$uorf_id}){
            $uORF_stop_coord{$gene}{$uorf_id}=$coord;


            for(-2 .. 0){
                if (exists ($gene_model_rev{$gene}{ ($coord+$_) } )){
                     push (@stop, substr($fasta_sequences{$chr}, ($gene_model_rev{$gene}{ ($coord+$_) } -1), 1) );
                }else{
                     push (@stop, "N");
                }
            }
            $seq_stop=join("", @stop);
            $seq_stop=~tr/ACGTacgt/TGCAtgca/;
        }
    }
    $uORF_kozak_score{$uorf_id}=$score;
    $uORF_start_codon{$uorf_id}=$seq_TIS;
    $uORF_stop_codon{$uorf_id}=$seq_stop;;
}

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#assign uORF search regions

my %start_uORF_search_fwd; #{$chr}{$pos}=$uorf."_".$coord_2; 
my %start_uORF_search_rev; #{$chr}{$pos}=$uorf."_".$coord_2; 

for my $gene (keys %gene_model_fwd){
    my $chr=$gene_2_chr{$gene};

    if ((exists ($five_prime_most_coord_fwd{$gene})) && (exists ($three_prime_most_coord_fwd{$gene} ))) {
        my $transcript_coord_5=$five_prime_most_coord_fwd{$gene};
        my $transcript_coord_3=$three_prime_most_coord_fwd{$gene};

        if (exists ($uORF_start_coord{$gene})){
            for my $uorf (keys %{$uORF_start_coord{$gene}}){
                if ((exists ($uORF_start_coord{$gene}{$uorf})) && (exists ($uORF_stop_coord{$gene}{$uorf}))){
                     my $ustart_coord=$uORF_start_coord{$gene}{$uorf};
                     my $ustop_coord=$uORF_stop_coord{$gene}{$uorf};

                     if (($ustop_coord - $ustart_coord) >= 30){  #resrict to uORFs long enough to plot start and stop codons seperatly
                         if ( (($ustart_coord-51) >= $transcript_coord_5) && (($ustop_coord+51) <= $transcript_coord_3) ) {  

                             for my $coord (sort {$a <=> $b} keys %{ $gene_model_fwd{$gene} } ){
                                 if ($coord == $ustart_coord){   

                                    my $relational_position=-50;
 
                                    for my $inner_coord ($coord-49 .. $coord+15){   #30nt upstream, 15nt downstream of "T" in ATG. "T" = position 0 on plot
                                        my $pos=$gene_model_fwd{$gene}{$inner_coord};
                                        $start_uORF_search_fwd{$chr}{$pos}=$uorf.";".$relational_position;
                                        $relational_position++;
     
                                        #The positions around start codons willl be as follows
                                        #A -1
                                        #T 0
                                        #G +1
                                    }
                                }elsif($coord == $ustop_coord){
     
                                    my $relational_position=15;
 
                                    for my $inner_coord ($coord-16 .. $coord+49){  #15nt downstream, 30nt upstream of "A" in TAG. "A" = position 30 on plot
                                        my $pos=$gene_model_fwd{$gene}{$inner_coord};

#                                       print "$gene,$uorf,$chr,$inner_coord,$pos,$relational_position\n";

                                        $start_uORF_search_fwd{$chr}{$pos}=$uorf.";".$relational_position;
                                        $relational_position++;

                                        #The positions around stop codons willl be as follows
                                        #T -1
                                        #A  0
                                        #A +1
                                    }
                                }
                            }
                        }    
                    }                   
                }
            }
        }
    }
}

#xit;

for my $gene (keys %gene_model_rev){
    my $chr=$gene_2_chr{$gene};

    if ((exists ($five_prime_most_coord_rev{$gene})) && (exists ($three_prime_most_coord_rev{$gene} ))) {
        my $transcript_coord_5=$five_prime_most_coord_rev{$gene};
        my $transcript_coord_3=$three_prime_most_coord_rev{$gene};

        if (exists ($uORF_start_coord{$gene})){
            for my $uorf (keys %{$uORF_start_coord{$gene}}){
                if ((exists ($uORF_start_coord{$gene}{$uorf})) && (exists ($uORF_stop_coord{$gene}{$uorf}))){
                    my $ustart_coord=$uORF_start_coord{$gene}{$uorf};
                    my $ustop_coord=$uORF_stop_coord{$gene}{$uorf};

                    if (($ustop_coord - $ustart_coord) >= 30){  #resrict to uORFs long enough to plot start and stop codons seperatly
                         if ( (($ustart_coord-51) >= $transcript_coord_5) && (($ustop_coord+51) <= $transcript_coord_3) ){

                            for my $coord (sort {$a <=> $b} keys %{ $gene_model_rev{$gene} } ){
                                if ($coord == $ustart_coord){

                                    my $relational_position=-50;

                                    for my $inner_coord ($coord-49 .. $coord+15){  #30nt upstream, 15nt downstream of "T" in ATG. "T" = position 0 on plot
                                        my $pos=$gene_model_rev{$gene}{$inner_coord};
                                        $start_uORF_search_rev{$chr}{$pos}=$uorf.";".$relational_position;
                                        $relational_position++;
                                    }
                                }elsif($coord == $ustop_coord){

                                    my $relational_position=15;
     
                                    for my $inner_coord ($coord-16 .. $coord+49){ #15nt downstream, 30nt upstream of 1st "A" in TAA. 1st "A" = position 30 on plot
                                        my $pos=$gene_model_rev{$gene}{$inner_coord};
                                        $start_uORF_search_rev{$chr}{$pos}=$uorf.";".$relational_position;
                                        $relational_position++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#open riboseq and assign

my %uorf_coverage_by_positon;
my %uorf_coverage_sum;

open BAM0,"samtools view $bam_RIBOseq |";
while(<BAM0>){

    next if(/^(\@)/);  ## skipping the header lines (if you used -h in the samools command)
    s/\n//;  s/\r//;  ## removing new line
    my @sam = split(/\t+/);  ## splitting SAM line into array

    my $leftMost=$sam[3]; #leftmost position of match 5' for fwd, 3' for rev
    my $flag=$sam[1];
    my $chr=$sam[2];
    my $mapq=$sam[4];
    my $cigar=$sam[5];
    my $seq=$sam[9];
    my $threePrime;
    my $fivePrime;

    if ($chr=~/chr(.*)/){ #ensembl chromosome names do not contain the "chr" prefix
        $chr=$1;
    }

    unless ($flag & 0x4){   #if aligned

        if ($mapq >= 10){     #mapping uniqnes filter

            #both chew_2013 and subtelney_2014 riboseq are directional
            if ($flag & 0x10){  #Reverse reads. Starting from the leftmost position parse the cigar and check if matching positions overlap leaders or cds's

                while ($cigar !~ /^$/){
                    if ($cigar =~ /^([0-9]+[MIDN])/){
                        my $cigar_part = $1;
                        if ($cigar_part =~ /(\d+)M/){   #alignment matching 
                            for my $pos ($leftMost .. ($leftMost+$1-1)){ #search though this position

                               if (exists ($start_uORF_search_rev{$chr}{$pos})){
                                   my ($uorf,$meta_pos)=$start_uORF_search_rev{$chr}{$pos}=~/(^[^;]+);(-?\w+)$/; 
                                   $uorf_coverage_sum{$uorf}++;
                                   $uorf_coverage_by_positon{$uorf}{$meta_pos}++;
                               }
                            }
                            $leftMost+=$1;
                        } elsif ($cigar_part =~ /(\d+)I/){  #insertion (to the reference) #do nothing this region is not in the reference
                        } elsif ($cigar_part =~ /(\d+)D/){  #deletion (from the reference)
                            $leftMost+=$1; #skip this position. Add to position count but do not search
                        } elsif ($cigar_part =~ /(\d+)N/){  #Skipped region from the reference
                            $leftMost+=$1; #skip this position. Add to position count but do not search  
                        }
                        $cigar =~ s/$cigar_part//;
                    }
                }
            }else{ #Forward reads. Starting from the leftmost position parse the cigar and check if matching positions overlap leaders or cds's

                while ($cigar !~ /^$/){
                    if ($cigar =~ /^([0-9]+[MIDN])/){
                        my $cigar_part = $1;
                        if ($cigar_part =~ /(\d+)M/){   #alignment matching 
                            for my $pos ($leftMost .. ($leftMost+$1-1)){ #search though this position
 
                                if (exists ($start_uORF_search_fwd{$chr}{$pos})){
                                    my ($uorf,$meta_pos)=$start_uORF_search_fwd{$chr}{$pos}=~/(^[^;]+);(-?\w+)$/;
                                    $uorf_coverage_sum{$uorf}++;
                                    $uorf_coverage_by_positon{$uorf}{$meta_pos}++;
                                }
                            }
                            $leftMost+=$1;
                        } elsif ($cigar_part =~ /(\d+)I/){  #insertion (to the reference) #do nothing this region is not in the reference
                        } elsif ($cigar_part =~ /(\d+)D/){  #deletion (from the reference)
                             $leftMost+=$1; #skip this position. Add to position count but do not search
                        } elsif ($cigar_part =~ /(\d+)N/){  #Skipped region from the reference
                            $leftMost+=$1; #skip this position. Add to position count but do not search  
                        }
                        $cigar =~ s/$cigar_part//;
                    }
                }
            }
        }
    }
}
close(BAM0);


#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#kozak filter
# %KOZAK_MIN-5;
# %KOZAK_MAX-20;   #-9
my %coverage_for_output;

#$overlaps_inframe_gene{$gene_id}=0;
#$leader_overlaps_upstream{$gene_id}=0;
#$gene_overlaps_downstream_leader{$gene_id}=0;
#$has_cage_defined_leader{$gene_id}=0;

my $count_before_kozak=keys (%uorf_coverage_sum);

print "$count_before_kozak, uorfs processed\n";

my $count_kozak=0;

for my $uorf (keys %uorf_coverage_sum){
    if (($uORF_kozak_score{$uorf} < $KOZAK_MIN) && ($uORF_kozak_score{$uorf} > $KOZAK_MAX)){ 

        my $stop_codon=$uORF_stop_codon{$uorf};
        if (exists ($accepted_codons{$stop_codon})){

            my $gene=$uORF_to_gene{$uorf};
            unless ($overlaps_inframe_gene{$gene}){
                unless ($leader_overlaps_upstream{$gene}){

                    my $distance_stop_to_TIS;
                    if (exists ($start_coord_rev{$gene})){
                        $distance_stop_to_TIS=$start_coord_rev{$gene} - $uORF_stop_coord{$gene}{$uorf};
                    }elsif (exists ($start_coord_fwd{$gene})){
                        $distance_stop_to_TIS=$start_coord_fwd{$gene} - $uORF_stop_coord{$gene}{$uorf};
                    }

                    if ($distance_stop_to_TIS >= 1){  #could look at making sure the windows doesn't overlap the TIS?
                        $coverage_for_output{$uorf}=$uorf_coverage_sum{$uorf};    
                        $count_kozak++;
                    }
                }
            }
        }
    }
}

print "$count_kozak, uorfs after kozak + stop codon selection\n";

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#rank the uorfs by start and stop window expression, to exclude the bottom n%
my %black_list;

#filter on kozak here or stop codon

my $count=0;
my $ten_percent=(keys %coverage_for_output)*$PERCENTAGE_TO_EXCLUDE;
for my $uorf ( sort { $coverage_for_output{$a} <=> $coverage_for_output{$b} } keys(%coverage_for_output) ){
    if ($count <= $ten_percent){ $black_list{$uorf}=1; }
    $count++;
}

print "$count, total uorfs\n";

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
my %uorf_barchart;
my %uorf_barchart_scaled;

my $count_used=0;
for my $uorf_id (keys %coverage_for_output){
    unless (exists ($black_list{$uorf_id})){  #blacklisted then remove
        if (exists ($uorf_coverage_by_positon{$uorf_id})){
            $count_used++;
            for my $pos (sort { $a <=> $b } keys %{ $uorf_coverage_by_positon{$uorf_id}} ){
                $uorf_barchart{$pos}+=$uorf_coverage_by_positon{$uorf_id}{$pos};
                $uorf_barchart_scaled{$pos}+=eval{ $uorf_coverage_by_positon{$uorf_id}{$pos}/$uorf_coverage_sum{$uorf_id}; } || 0;
            }
        }
    }
}

print "$count_used, uorfs for plotting\n";

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
my $out_coverage=$outDir."/".$prefix."best_uorf_coverage_by_position.csv";
my $out_coverage_scaled=$outDir."/".$prefix."best_uorf_coverage_by_position_scale.csv";

open (OUT1,">$out_coverage")  || die "can't open $out_coverage\n";
open (OUT2,">$out_coverage_scaled")  || die "can't open $out_coverage_scaled\n";

for my $pos (sort {$a <=> $b} keys %uorf_barchart){
    print OUT1 "$pos,$uorf_barchart{$pos}\n";
}

for my $pos (sort {$a <=> $b} keys %uorf_barchart_scaled){
    print OUT2 "$pos,$uorf_barchart_scaled{$pos}\n";
}

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
##caclulate log2
sub log2 {
    my $n = shift;
    my $l = log($n)/log(2);
    #$l = sprintf("%.2f",$l);
    return $l;
}

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#score the tis surrounding sequence against the Zebrafish kozak PWM
sub score_kozak{
    my $upstream = shift;
    my $downstream = shift;
    my $score=0;
    my $seq_to_score=uc($upstream.$downstream); #concaternate and set to uppercase   
    my @seq_to_score=split("",$seq_to_score);
    my $count=0;

    for my $base (@seq_to_score){
        if (exists ($PWM{$count}{$base} )){
            $score+=$PWM{$count}{$base};
        }
        $count++;
    }
    return $score;
}

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
sub score_kozak_fwd{
    my $gene=$_[0];
    my $pos=$_[1];
    my $chr=$_[2];

    my @up;

    #can I do this in coord space?

    for (-4 .. -1){
        push (@up, substr($fasta_sequences{$chr}, (($pos+$_)-1), 1));
    }

    my @down;
    for (3 .. 4){
        push (@down, substr($fasta_sequences{$chr}, (($pos+$_)-1), 1));
    }

    my $seq_up=join("", @up);
    my $seq_down=join("", @down);

#    print "fwd,$gene,$chr,$pos,$seq_up,$seq_down\n";

    my $kozak_strength=&score_kozak($seq_up,$seq_down);

    return $kozak_strength;
}

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
sub score_kozak_rev{
    my $gene=$_[0];
    my $pos=$_[1];
    my $chr=$_[2];

    my @up;

    for (1 .. 4){
        push (@up, substr($fasta_sequences{$chr}, (($pos+$_)-1), 1));
    }

    my @down;
    for (-4 .. -3){
        push (@down, substr($fasta_sequences{$chr}, (($pos+$_)-1), 1));
    }

    my $seq_up=reverse(join("", @up));
    my $seq_down=reverse(join("", @down));

    $seq_up=~tr/ACGTacgt/TGCAtgca/;
    $seq_down=~tr/ACGTacgt/TGCAtgca/;

#    print "rev,$gene,$chr,$pos,$seq_up,$seq_down\n";

    my $kozak_strength=&score_kozak($seq_up,$seq_down);

    return ($kozak_strength);
}

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
