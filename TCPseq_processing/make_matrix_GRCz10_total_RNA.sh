#/bin/bash

#make a matrix of RNA, ribo-seq and TCP-seq over GRCz10 leader and CDS regions
#5000nt leader search

gtf=/export/valenfs/data/references/Zv10_zebrafish/Danio_rerio.GRCz10.81.gtf
fa=/export/valenfs/data/references/Zv10_zebrafish/Danio_rerio.GRCz10.fa
go=~/analysis/genome/GRCz10_GO_terms_ensembl_mart_export.csv

###################################################################################################
# 1KCell TCPseq
###################################################################################################

cage_fwd=~/analysis/R_working/Cage_r_zv10/Normalised_values/zf_04_cells_512.CTSS.normalized.plus.bedGraph
cage_rev=~/analysis/R_working/Cage_r_zv10/Normalised_values/zf_04_cells_512.CTSS.normalized.minus.bedGraph
ribo_fwd=/export/valenfs/data/processed_data/Ribo-seq/chew_2013_zebrafish/final_results/aligned_GRCz10/shoelaces_GRCz10/1KCell_trimmed.wig-forward.wig
ribo_rev=/export/valenfs/data/processed_data/Ribo-seq/chew_2013_zebrafish/final_results/aligned_GRCz10/shoelaces_GRCz10/1KCell_trimmed.wig-reverse.wig
rna_bam=/export/valenfs/data/processed_data/RNA-seq/lee_2013_zebrafish/total_RNA/aligned_GRCz10/WT_4hpf_Total_mRNA.bam
ssu_bam=/export/valenfs/projects/adam/TCP_seq/valen_1_2_merge_15nt/tidy_alignments/1kCell/40S_merged_1kcell.bam
lsu_bam=/export/valenfs/projects/adam/TCP_seq/valen_1_2_merge_15nt/tidy_alignments/1kCell/80S_merged_1kcell.bam

outdir=~/analysis/R_working/Cage_r_zv10/TCPseq_leaders_Jan2018/TCP_matrix_total_RNA_kozak_trailer_update_histones_directional/
prefix=matrix_zebrafish_1kCell

#perl /export/valenfs/projects/adam/final_results/scripts/cage_peaks_assignment_gtf_transcript_coords_for_TCPseq_v8.pl $gtf $cage_fwd $cage_rev > ${outdir}/${prefix}_leaders.csv

perl /export/valenfs/projects/adam/final_results/scripts/assign_sam_wig_counts_to_gtf_genes_seq_histones_start_stop_v11.pl $gtf $fa $ribo_fwd $ribo_rev $rna_bam $ssu_bam $lsu_bam ${outdir}/${prefix}_leaders.csv $go > ${outdir}/${prefix}_matrix.csv 

echo "1Kcell finished"

###################################################################################################
# Bud TCPseq
###################################################################################################

cage_fwd=~/analysis/R_working/Cage_r_zv10/Normalised_values/zf_09_shield.CTSS.normalized.plus.bedGraph
cage_rev=~/analysis/R_working/Cage_r_zv10/Normalised_values/zf_09_shield.CTSS.normalized.minus.bedGraph
ribo_fwd=/export/valenfs/data/processed_data/Ribo-seq/chew_2013_zebrafish/final_results/aligned_GRCz10/shoelaces_GRCz10/Bud_trimmed.wig-forward.wig
ribo_rev=/export/valenfs/data/processed_data/Ribo-seq/chew_2013_zebrafish/final_results/aligned_GRCz10/shoelaces_GRCz10/Bud_trimmed.wig-reverse.wig
rna_bam=/export/valenfs/data/processed_data/RNA-seq/lee_2013_zebrafish/total_RNA/aligned_GRCz10/WT_6hpf_Total_mRNA_merged.bam
ssu_bam=/export/valenfs/projects/adam/TCP_seq/valen_1_2_merge_15nt/tidy_alignments/bud/40S_merged_bud.bam
lsu_bam=/export/valenfs/projects/adam/TCP_seq/valen_1_2_merge_15nt/tidy_alignments/bud/80S_merged_bud.bam

outdir=~/analysis/R_working/Cage_r_zv10/TCPseq_leaders_Jan2018/TCP_matrix_total_RNA_kozak_trailer_update_histones_directional/
prefix=matrix_zebrafish_bud

perl /export/valenfs/projects/adam/final_results/scripts/cage_peaks_assignment_gtf_transcript_coords_for_TCPseq_v8.pl $gtf $cage_fwd $cage_rev > ${outdir}/${prefix}_leaders.csv

perl /export/valenfs/projects/adam/final_results/scripts/assign_sam_wig_counts_to_gtf_genes_seq_histones_start_stop_v11.pl $gtf $fa $ribo_fwd $ribo_rev $rna_bam $ssu_bam $lsu_bam ${outdir}/${prefix}_leaders.csv $go > ${outdir}/${prefix}_matrix.csv 

echo "Bud finished"

###################################################################################################
# 75% Epibody TCPseq
###################################################################################################

cage_fwd=~/analysis/R_working/Cage_r_zv10/Normalised_values/zf_09_shield.CTSS.normalized.plus.bedGraph
cage_rev=~/analysis/R_working/Cage_r_zv10/Normalised_values/zf_09_shield.CTSS.normalized.minus.bedGraph
ribo_fwd=/export/valenfs/data/processed_data/Ribo-seq/chew_2013_zebrafish/final_results/aligned_GRCz10/shoelaces_GRCz10/Shield_trimmed.wig-forward.wig
ribo_rev=/export/valenfs/data/processed_data/Ribo-seq/chew_2013_zebrafish/final_results/aligned_GRCz10/shoelaces_GRCz10/Shield_trimmed.wig-reverse.wig
rna_bam=/export/valenfs/data/processed_data/RNA-seq/lee_2013_zebrafish/total_RNA/aligned_GRCz10/WT_6hpf_Total_mRNA_merged.bam
ssu_bam=/export/valenfs/data/processed_data/TCP-seq/valen_2017_zebrafish_4_trim3_15nt/aligned_GRCz10_tidy/2-ribo-zero-48S-12_S2_R1_001.bam
lsu_bam=/export/valenfs/data/processed_data/TCP-seq/valen_2017_zebrafish_4_trim3_15nt/aligned_GRCz10_tidy/4-ribo-zero-80S-17_S4_R1_001.bam

outdir=~/analysis/R_working/Cage_r_zv10/TCPseq_leaders_Jan2018/TCP_matrix_total_RNA_kozak_trailer_update_histones_directional/
prefix=matrix_zebrafish_75_epibody

perl /export/valenfs/projects/adam/final_results/scripts/cage_peaks_assignment_gtf_transcript_coords_for_TCPseq_v8.pl $gtf $cage_fwd $cage_rev > ${outdir}/${prefix}_leaders.csv

perl /export/valenfs/projects/adam/final_results/scripts/assign_sam_wig_counts_to_gtf_genes_seq_histones_start_stop_v11.pl $gtf $fa $ribo_fwd $ribo_rev $rna_bam $ssu_bam $lsu_bam ${outdir}/${prefix}_leaders.csv $go > ${outdir}/${prefix}_matrix.csv 

echo "75% epibody finished"

###################################################################################################
# Shield TCPseq
###################################################################################################

cage_fwd=~/analysis/R_working/Cage_r_zv10/Normalised_values/zf_09_shield.CTSS.normalized.plus.bedGraph
cage_rev=~/analysis/R_working/Cage_r_zv10/Normalised_values/zf_09_shield.CTSS.normalized.minus.bedGraph
ribo_fwd=/export/valenfs/data/processed_data/Ribo-seq/chew_2013_zebrafish/final_results/aligned_GRCz10/shoelaces_GRCz10/Shield_trimmed.wig-forward.wig
ribo_rev=/export/valenfs/data/processed_data/Ribo-seq/chew_2013_zebrafish/final_results/aligned_GRCz10/shoelaces_GRCz10/Shield_trimmed.wig-reverse.wig
rna_bam=/export/valenfs/data/processed_data/RNA-seq/lee_2013_zebrafish/total_RNA/aligned_GRCz10/WT_6hpf_Total_mRNA_merged.bam
ssu_bam=/export/valenfs/projects/adam/TCP_seq/valen5/SSU_fractions_13_14_trim3_15nt_tidy.bam
lsu_bam=/export/valenfs/projects/adam/TCP_seq/valen5/LSU_fractions_18_19_trim3_15nt_tidy.bam

outdir=~/analysis/R_working/Cage_r_zv10/TCPseq_leaders_Jan2018/TCP_matrix_total_RNA_kozak_trailer_update_histones_directional/
prefix=matrix_zebrafish_shield

perl /export/valenfs/projects/adam/final_results/scripts/cage_peaks_assignment_gtf_transcript_coords_for_TCPseq_v8.pl $gtf $cage_fwd $cage_rev > ${outdir}/${prefix}_leaders.csv

perl /export/valenfs/projects/adam/final_results/scripts/assign_sam_wig_counts_to_gtf_genes_seq_histones_start_stop_v11.pl $gtf $fa $ribo_fwd $ribo_rev $rna_bam $ssu_bam $lsu_bam ${outdir}/${prefix}_leaders.csv $go > ${outdir}/${prefix}_matrix.csv

echo "Shield finished"

###################################################################################################

#1+2 = 1kcell + bud
#4 ribozero = 75% epibody (8hours)

#shield=6h
#bud=10h

#~/analysis/R_working/Cage_r_zv10/Normalised_values/zf_04_cells_512.CTSS.normalized.plus.bedGraph
#~/analysis/R_working/Cage_r_zv10/Normalised_values/zf_04_cells_512.CTSS.normalized.minus.bedGraph

#~/analysis/R_working/Cage_r_zv10/Normalised_values/zf_09_shield.CTSS.normalized.plus.bedGraph
#~/analysis/R_working/Cage_r_zv10/Normalised_values/zf_09_shield.CTSS.normalized.minus.bedGraph

#/export/valenfs/data/processed_data/Ribo-seq/chew_2013_zebrafish/final_results/aligned_GRCz10/shoelaces_GRCz10/1KCell_trimmed.wig-forward.wig
#/export/valenfs/data/processed_data/Ribo-seq/chew_2013_zebrafish/final_results/aligned_GRCz10/shoelaces_GRCz10/1KCell_trimmed.wig-reverse.wig

#/export/valenfs/data/processed_data/Ribo-seq/chew_2013_zebrafish/final_results/aligned_GRCz10/shoelaces_GRCz10/Bud_trimmed.wig-forward.wig
#/export/valenfs/data/processed_data/Ribo-seq/chew_2013_zebrafish/final_results/aligned_GRCz10/shoelaces_GRCz10/Bud_trimmed.wig-reverse.wig

#/export/valenfs/data/processed_data/Ribo-seq/chew_2013_zebrafish/final_results/aligned_GRCz10/shoelaces_GRCz10/Shield_Squint_trimmed.wig-forward.wig
#/export/valenfs/data/processed_data/Ribo-seq/chew_2013_zebrafish/final_results/aligned_GRCz10/shoelaces_GRCz10/Shield_Squint_trimmed.wig-reverse.wig

#/export/valenfs/data/processed_data/Ribo-seq/chew_2013_zebrafish/final_results/aligned_GRCz10/shoelaces_GRCz10/Shield_trimmed.wig-forward.wig
#/export/valenfs/data/processed_data/Ribo-seq/chew_2013_zebrafish/final_results/aligned_GRCz10/shoelaces_GRCz10/Shield_trimmed.wig-reverse.wig

#/export/valenfs/data/processed_data/RNA-seq/chew_2013_and_pauli_2012_zebrafish/final_results/aligned_GRCz10/1KCell.bam

#/export/valenfs/data/processed_data/RNA-seq/chew_2013_and_pauli_2012_zebrafish/final_results/aligned_GRCz10/Bud.bam

#/export/valenfs/data/processed_data/RNA-seq/chew_2013_and_pauli_2012_zebrafish/final_results/aligned_GRCz10/Shield.bam

#/export/valenfs/projects/adam/TCP_seq/valen_2017_merge/1_40S_merged.bam
#/export/valenfs/projects/adam/TCP_seq/valen_2017_merge/2_60S_merged.bam
#/export/valenfs/projects/adam/TCP_seq/valen_2017_merge/3_80S_merged.bam

#/export/valenfs/projects/adam/TCP_seq/valen_2017_merge/4_40S_merged.bam
#/export/valenfs/projects/adam/TCP_seq/valen_2017_merge/5_60S_merged.bam
#/export/valenfs/projects/adam/TCP_seq/valen_2017_merge/6_80S_merged.bam

#/export/valenfs/data/processed_data/TCP-seq/valen_2017_zebrafish_4_trim3/aligned_GRCz10/1-ribo-zero-43S.bam
#/export/valenfs/data/processed_data/TCP-seq/valen_2017_zebrafish_4_trim3/aligned_GRCz10/2-ribo-zero-48S.bam
#/export/valenfs/data/processed_data/TCP-seq/valen_2017_zebrafish_4_trim3/aligned_GRCz10/3-ribo-zero-60S.bam
#/export/valenfs/data/processed_data/TCP-seq/valen_2017_zebrafish_4_trim3/aligned_GRCz10/4-ribo-zero-80S.bam

