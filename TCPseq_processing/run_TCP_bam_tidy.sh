#!/bin/bash

#AG 30/03/18

#--------#

#export PATH=$PATH:/net/apps/cbu/bin/
#samtools=/net/apps/cbu/bin/samtools_0.1.18

#--------#

gtf=/export/valenfs/data/references/Zv10_zebrafish/Danio_rerio.GRCz10.81.gtf
fa=/export/valenfs/data/references/Zv10_zebrafish/Danio_rerio.GRCz10.dna.toplevel.fa
leaders_shield=~/analysis/R_working/Cage_r_zv10/TCPseq_leaders_Jan2018/TCP_matrix_kozak_trailer_update_histones_directional/matrix_zebrafish_shield_leaders.csv
leaders_1k=~/analysis/R_working/Cage_r_zv10/TCPseq_leaders_Jan2018/TCP_matrix_kozak_trailer_update_histones_directional/matrix_zebrafish_1kCell_leaders.csv

#--#

in_dir=/export/valenfs/projects/adam/TCP_seq/valen5/merged_tidy/
out_dir=/export/valenfs/projects/adam/TCP_seq/valen5/peaks_removed_tRNA/

for file in $in_dir/*.bam; do
    name=$(basename $file)
    prefix=${name%.bam};
    echo $prefix

    nice -n 10 perl /export/valenfs/projects/adam/final_results/scripts/remove_peaks_from_TCP_seq_bam.pl $gtf $fa $file $leaders_shield | samtools view -o ${out_dir}/${prefix}_peaks_removed.bam -Sb -

    #fragment length plots
    nice -n 10 perl /export/valenfs/projects/adam/final_results/scripts/TCP_length_distributions_per_feature_v2.pl $gtf $fa $out_dir/${prefix}_peaks_removed.bam $leaders_shield > ${out_dir}/${prefix}_lengths.csv;

done
        
#--#

in_dir=/export/valenfs/data/processed_data/TCP-seq/valen_2017_zebrafish_5_trim3_15nt/aligned_GRCz10_tidy/
out_dir=/export/valenfs/projects/adam/TCP_seq/valen5/peaks_removed_tRNA

for file in $in_dir/*.bam; do
    name=$(basename $file)
    prefix=${name%.bam};
    echo $prefix

    nice -n 10 perl /export/valenfs/projects/adam/final_results/scripts/remove_peaks_from_TCP_seq_bam.pl $gtf $fa $file $leaders_shield | samtools view -o ${out_dir}/${prefix}_peaks_removed.bam -Sb -

    #fragment length plots
    nice -n 10 perl /export/valenfs/projects/adam/final_results/scripts/TCP_length_distributions_per_feature_v2.pl $gtf $fa $out_dir/${prefix}_peaks_removed.bam $leaders_shield > ${out_dir}/${prefix}_lengths.csv;

done

#heatmaps
#bash /export/valenfs/projects/adam/final_results/scripts/repo3/giess-scripts/TCPseq_heatmaps_eukaryotic/run_transcript_coord.sh -b $out_dir -o ${out_dir}/heatmaps_90/ -g $gtf -l $leaders_shield

#--#

in_dir=/export/valenfs/data/processed_data/TCP-seq/valen_2017_zebrafish_4_15nt_with_3nt_trim2/aligned_GRCz10_tidy/
out_dir=/export/valenfs/projects/adam/TCP_seq/valen_4/peaks_removed_tRNA/

for file in $in_dir/*.bam; do
    name=$(basename $file)
    prefix=${name%.bam};
    echo $prefix

    nice -n 10 perl /export/valenfs/projects/adam/final_results/scripts/remove_peaks_from_TCP_seq_bam.pl $gtf $fa $file $leaders_shield | samtools view -o ${out_dir}/${prefix}_peaks_removed.bam -Sb -

    #fragment length plots
    nice -n 10 perl /export/valenfs/projects/adam/final_results/scripts/TCP_length_distributions_per_feature_v2.pl $gtf $fa $out_dir/${prefix}_peaks_removed.bam $leaders_shield > ${out_dir}/${prefix}_lengths.csv

done

#heatmaps
#bash /export/valenfs/projects/adam/final_results/scripts/repo3/giess-scripts/TCPseq_heatmaps_eukaryotic/run_transcript_coord.sh -b $out_dir -o ${out_dir}/heatmaps_90/ -g $gtf -l $leaders_shield

#--#

in_dir=/export/valenfs/projects/adam/TCP_seq/valen_1_2_merge_15nt/tidy_alignments/bud/
out_dir=/export/valenfs/projects/adam/TCP_seq/valen_1_2_merge_15nt/tidy_alignments/bud/peaks_removed_tRNA/

for file in $in_dir/*.bam; do
    name=$(basename $file)
    prefix=${name%.bam};
    echo $prefix

    nice -n 10 perl /export/valenfs/projects/adam/final_results/scripts/remove_peaks_from_TCP_seq_bam.pl $gtf $fa $file $leaders_shield | samtools view -o ${out_dir}/${prefix}_peaks_removed.bam -Sb -

    #fragment length plots
    nice -n 10 perl /export/valenfs/projects/adam/final_results/scripts/TCP_length_distributions_per_feature_v2.pl $gtf $fa $out_dir/${prefix}_peaks_removed.bam $leaders_shield > ${out_dir}/${prefix}_lengths.csv

done

#heatmaps
#bash /export/valenfs/projects/adam/final_results/scripts/repo3/giess-scripts/TCPseq_heatmaps_eukaryotic/run_transcript_coord.sh -b $out_dir -o ${out_dir}/heatmaps_90/ -g $gtf -l $leaders_shield

#--#

in_dir=/export/valenfs/projects/adam/TCP_seq/valen_1_2_merge_15nt/tidy_alignments/1kCell/
out_dir=/export/valenfs/projects/adam/TCP_seq/valen_1_2_merge_15nt/tidy_alignments/1kCell/peaks_removed_tRNA/

for file in $in_dir/*.bam; do
    name=$(basename $file)
    prefix=${name%.bam};
    echo $prefix

    nice -n 10 perl /export/valenfs/projects/adam/final_results/scripts/remove_peaks_from_TCP_seq_bam.pl $gtf $fa $file $leaders_1k | samtools view -o ${out_dir}/${prefix}_peaks_removed.bam -Sb -

    #fragment length plots
    nice -n 10 perl /export/valenfs/projects/adam/final_results/scripts/TCP_length_distributions_per_feature_v2.pl $gtf $fa $out_dir/${prefix}_peaks_removed.bam $leaders_1k > ${out_dir}/${prefix}_lengths.csv

done

#heatmaps
#bash /export/valenfs/projects/adam/final_results/scripts/repo3/giess-scripts/TCPseq_heatmaps_eukaryotic/run_transcript_coord.sh -b $out_dir -o ${out_dir}/heatmaps_90/ -g $gtf -l $leaders_1k

#--#

gtf_yeast=/export/valenfs/data/references/R64_1_1_yeast/Saccharomyces_cerevisiae.R64-1-1.79.gtf
fa_yeast=/export/valenfs/data/references/R64_1_1_yeast/Saccharomyces_cerevisiae.R64-1-1.dna.toplevel.fa
leaders_yeast=~/analysis/R_working/Cage_r_R64_1_1/TCPseq_leaders_Jan2018/TCP_matrix/matrix_yeast_leaders.csv

in_dir=/export/valenfs/data/processed_data/TCP-seq/archer_2016_yeast_15nt_without_3nt_trim/aligned_R64_1_1_tidy/
out_dir=/export/valenfs/projects/adam/TCP_seq/archer_yeast_2016/peaks_removed_tRNA/

for file in $in_dir/*.bam; do
    name=$(basename $file)
    prefix=${name%.bam};
    echo $prefix

    nice -n 10 perl /export/valenfs/projects/adam/final_results/scripts/remove_peaks_from_TCP_seq_bam.pl $gtf_yeast $fa_yeast $file $leaders_yeast | samtools view -o ${out_dir}/${prefix}_peaks_removed.bam -Sb -

    #fragment length plots
    nice -n 10 perl /export/valenfs/projects/adam/final_results/scripts/TCP_length_distributions_per_feature_v2.pl $gtf_yeast $fa_yeast $out_dir/${prefix}_peaks_removed.bam $leaders_yeast > ${out_dir}/${prefix}_lengths.csv

done

#yeast heatmaps (requieres editing of run_transcript_coord.sh
#heatmaps
#bash /export/valenfs/projects/adam/final_results/scripts/repo3/giess-scripts/TCPseq_heatmaps_eukaryotic/run_transcript_coord.sh -b $out_dir -o ${out_dir}/heatmaps_90/ -g $gtf_yeast -l $leaders_yeast

