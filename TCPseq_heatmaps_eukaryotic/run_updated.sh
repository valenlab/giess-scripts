#!/bin/bash

#AG 21/06/17
#script to produce heatmaps of 5' and 3' length distributions from a bam file
#1 bam to sam
#2 get 5' and 3' reads
        #up/downstream of annotated start codon
        #up/downstream of annotated stop codons    
   
#dependanices (must be in path)
#samtools

usage(){
cat << EOF
usage: $0 options

script to trim and align riboseq fastq datasets 

OPTIONS:
    -b    path to the input bam files
    -o    path to output dir
    -g    gtf file (matched to the bam gemone)
    -f    fasta file (also matched to genome)
    -h    this help message

example usage: run_lenghts.sh -b <in.bam> -g in.gtf -f in.fasta -o <out_dir>

EOF
}

while getopts ":b:o:g:f:h" opt; do
    case $opt in 
        b)
            in_bam=$OPTARG
              echo "-b input bam file $OPTARG"
              ;;
        o)
            out_dir=$OPTARG
            echo "-o output folder $OPTARG"
            ;;
        g)
            in_gtf=$OPTARG
            echo "-g gtf file $OPTARG"
            ;;
        f) 
            in_fasta=$OPTARG
            echo "-f fasta file $OPTARG"
            ;;
        h)
            usage
            exit
            ;;
        ?) 
            echo "Invalid option: -$OPTARG"
            usage
            exit 1
              ;;
    esac
done

if [ ! -e $in_bam ]  || [ ! $out_dir ] || [ ! $in_gtf ] || [ ! $in_fasta ] ; then
        echo "something's missing - check your command lines args"
        usage
        exit 1
fi

if [ ! -d $out_dir ]; then
    mkdir $out_dir
fi

if [ ! -d ${out_dir}/tmp ]; then
    mkdir ${out_dir}/tmp
fi

for file in $in_bam/*.bam; do

    name=$(basename $file)
    prefix=${name%.bam}
    echo starting $prefix

    #------------------------------------------------------------------------------------------
    #1 Bam to sam
    #------------------------------------------------------------------------------------------

    samtools view $file > ${out_dir}/tmp/${prefix}.sam
    
    #------------------------------------------------------------------------------------------
    #2 Count 5' ends of reads
    #------------------------------------------------------------------------------------------

    nice -n 10 perl /export/valenfs/projects/adam/final_results/scripts/repo3/giess-scripts/TCPseq_heatmaps_eukaryotic/meta_gene_plot_bam_matrix_scaled_3and5_start_stop_scale_v9.pl $in_gtf ${out_dir}/tmp/${prefix}.sam $in_fasta $out_dir

    #------------------------------------------------------------------------------------------ 
    #4 Plot heatmaps
    #------------------------------------------------------------------------------------------

    Rscript /export/valenfs/projects/adam/final_results/scripts/repo3/giess-scripts/TCPseq_heatmaps_eukaryotic/TCP_heatmaps.R ${out_dir}/${prefix}_start_lengths_scale_5prime.csv ${out_dir}/${prefix}_start_lengths_scale_3prime.csv ${out_dir}/${prefix}_start_5prime_scale.csv ${out_dir}/${prefix}_start_3prime_scale.csv ${out_dir}/${prefix}_heatmaps.pdf

	#I can also add the plots at stop codons here
    Rscript /export/valenfs/projects/adam/final_results/scripts/repo3/giess-scripts/TCPseq_heatmaps_eukaryotic/TCP_heatmaps_stop.R ${out_dir}/${prefix}_stop_lengths_scale_5prime.csv ${out_dir}/${prefix}_stop_lengths_scale_3prime.csv ${out_dir}/${prefix}_stop_5prime_scale.csv ${out_dir}/${prefix}_stop_3prime_scale.csv ${out_dir}/${prefix}_stop_heatmaps.pdf

    #------------------------------------------------------------------------------------------ 
    #5 Tidy up
    #------------------------------------------------------------------------------------------

    rm ${out_dir}/tmp/${prefix}.sam

done
