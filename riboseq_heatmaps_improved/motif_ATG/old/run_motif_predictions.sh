#!/bin/bash

#AG 06/03/17
#script to produce heatmaps of 5' and 3' length distributions from a bam file
#1 bam to sam
#2 get 5' and 3' reads
	#at all SD like sequences
    #further subdevided into:
        #20nt upstream of an annotated start codon
        #Downstream of annotated start codons    
   
#3 count scaled lengths relative to start codons
#4 plot heatmaps

#dependanices (must be in path)
#samtools

usage(){
cat << EOF
usage: $0 options

script to trim and align riboseq fastq datasets 

OPTIONS:
    -b    path to the input bam files
    -o    path to output dir
    -g    gtf file (matched to the bam gemone)
    -f    fasta file (also matched to genome)
    -p    bed of TIS predictions
    -h    this help message

example usage: run_lenghts.sh -b <in.bam> -g in.gtf -f in.fasta -o <out_dir>

EOF
}

while getopts ":b:o:g:f:p:h" opt; do
    case $opt in 
        b)
            in_bam=$OPTARG
              echo "-b input bam file $OPTARG"
              ;;
        o)
            out_dir=$OPTARG
            echo "-o output folder $OPTARG"
            ;;
        g)
            in_gtf=$OPTARG
            echo "-g gtf file $OPTARG"
            ;;
        f) 
            in_fasta=$OPTARG
            echo "-f fasta file $OPTARG"
            ;;
        p) 
            in_pred=$OPTARG
            echo "-p predictions $OPTARG"
            ;;
        h)
            usage
            exit
            ;;
        ?) 
            echo "Invalid option: -$OPTARG"
            usage
            exit 1
              ;;
    esac
done

if [ ! -e $in_bam ]  || [ ! $out_dir ] || [ ! $in_gtf ] || [ ! $in_fasta ] || [ ! $in_pred ] ; then
        echo "something's missing - check your command lines args"
        usage
        exit 1
fi

if [ ! -d $out_dir ]; then
    mkdir $out_dir
fi

if [ ! -d ${out_dir}/tmp ]; then
    mkdir ${out_dir}/tmp
fi

for file in $in_bam/*.bam; do

    name=$(basename $file)
    prefix=${name%.bam}
    echo starting $prefix

    #------------------------------------------------------------------------------------------
    #1 Bam to sam
    #------------------------------------------------------------------------------------------

    samtools view $file > ${out_dir}/tmp/${prefix}.sam
    
    #------------------------------------------------------------------------------------------
    #2 Count 5' ends of reads
    #------------------------------------------------------------------------------------------

    nice -n 10 perl /export/valenfs/projects/adam/final_results/scripts/repo3/giess-scripts/riboseq_heatmaps_improved/motif_ATG/meta_gene_plot_bam_matrix_scaled_3and5_motif_v5.pl $in_gtf ${out_dir}/tmp/${prefix}.sam $in_fasta $out_dir $in_pred

    #------------------------------------------------------------------------------------------ 
    #3 Make metagene plots
    #------------------------------------------------------------------------------------------

	#changed to neither scale nor drop out bottom 10%

    nice -n 10 perl /export/valenfs/projects/adam/final_results/scripts/repo3/giess-scripts/riboseq_heatmaps_improved/motif_ATG/meta_plot_matrix_scale.pl ${out_dir}/${prefix}_upstream_motif_5prime.csv > ${out_dir}/${prefix}_upstream_meta_scaled_5prime.csv
 
    nice -n 10 perl /export/valenfs/projects/adam/final_results/scripts/repo3/giess-scripts/riboseq_heatmaps_improved/motif_ATG/meta_plot_matrix_scale.pl ${out_dir}/${prefix}_downstream_motif_5prime.csv > ${out_dir}/${prefix}_downstream_meta_scaled_5prime.csv

    nice -n 10 perl /export/valenfs/projects/adam/final_results/scripts/repo3/giess-scripts/riboseq_heatmaps_improved/motif_ATG/meta_plot_matrix_scale.pl ${out_dir}/${prefix}_upstream_motif_3prime.csv > ${out_dir}/${prefix}_upstream_meta_scaled_3prime.csv

    nice -n 10 perl /export/valenfs/projects/adam/final_results/scripts/repo3/giess-scripts/riboseq_heatmaps_improved/motif_ATG/meta_plot_matrix_scale.pl ${out_dir}/${prefix}_downstream_motif_3prime.csv > ${out_dir}/${prefix}_downstream_meta_scaled_3prime.csv

    #------------------------------------------------------------------------------------------ 
    #4 Plot heatmaps
    #------------------------------------------------------------------------------------------

    Rscript /export/valenfs/projects/adam/final_results/scripts/repo3/giess-scripts/riboseq_heatmaps_improved/motif_ATG/plot_heatmaps.R ${out_dir}/${prefix}_upstream_codon_lengths_scale_5prime.csv ${out_dir}/${prefix}_upstream_codon_lengths_scale_3prime.csv ${out_dir}/${prefix}_upstream_meta_scaled_5prime.csv ${out_dir}/${prefix}_upstream_meta_scaled_3prime.csv ${out_dir}/${prefix}_upstream_motif_heatmaps.pdf

    Rscript /export/valenfs/projects/adam/final_results/scripts/repo3/giess-scripts/riboseq_heatmaps_improved/motif_ATG/tidy_heatmaps2.R ${out_dir}/${prefix}_upstream_codon_lengths_scale_5prime.csv ${out_dir}/${prefix}_upstream_codon_lengths_scale_3prime.csv ${out_dir}/${prefix}_upstream_meta_scaled_5prime.csv ${out_dir}/${prefix}_upstream_meta_scaled_3prime.csv ${out_dir}/${prefix}_upstream_motif_heatmaps_tidy.pdf


    Rscript /export/valenfs/projects/adam/final_results/scripts/repo3/giess-scripts/riboseq_heatmaps_improved/motif_ATG/plot_heatmaps.R ${out_dir}/${prefix}_downstream_codon_lengths_scale_5prime.csv ${out_dir}/${prefix}_downstream_codon_lengths_scale_3prime.csv ${out_dir}/${prefix}_downstream_meta_scaled_5prime.csv ${out_dir}/${prefix}_downstream_meta_scaled_3prime.csv ${out_dir}/${prefix}_downstream_motif_heatmaps.pdf

    Rscript /export/valenfs/projects/adam/final_results/scripts/repo3/giess-scripts/riboseq_heatmaps_improved/motif_ATG/tidy_heatmaps2.R ${out_dir}/${prefix}_downstream_codon_lengths_scale_5prime.csv ${out_dir}/${prefix}_downstream_codon_lengths_scale_3prime.csv ${out_dir}/${prefix}_downstream_meta_scaled_5prime.csv ${out_dir}/${prefix}_downstream_meta_scaled_3prime.csv ${out_dir}/${prefix}_downstream_motif_heatmaps_tidy.pdf

    #------------------------------------------------------------------------------------------ 
    #5 Tidy up
    #------------------------------------------------------------------------------------------

    rm ${out_dir}/tmp/${prefix}.sam

done
