#!/usr/bin/perl -w
use strict;

#to do 07/03/2016
#script to take gtf get coords around start/stop codons, and assign 5' + 3' regions to them from sam
#scale each gene, so that the reads coming from it sum to 1
#where a read overlaps multiple in strand genes counts it towards all those genes

#input files:
#gtf exons, start, stop codons
#sam = aligned 
my $gtf=$ARGV[0];
my $sam=$ARGV[1];
my $fasta=$ARGV[2];
my $outDir=$ARGV[3];
my ($prefix)=$sam=~/([^\/]+).sam$/;


#my $MOTIF="CGGAGGTG";          
#my $MOTIF="AGGAGG";
#my $MOTIF="AGGAGGTG";
my $MOTIF="ATG";

#the approach that was used in OCnnor 2014 was to use RNAsubopt to find all sequeces that were able to form a duplex with the aSD above some threshold

##################################################################################################
#open fasta and get chr limits
my %fasta_sequences; #key = sequence_name, value=sequence
my $name="";
open (FA, $fasta) || die "can't open $fasta";
while (<FA>){
    chomp;
    if (/^>([^\s]+)/){ #take header up to the first space
        $name=$1;
    }else{
        $fasta_sequences{$name}.=$_;
    }
}
close(FA);

print "fasta parsed\n";

###################################################################################################
#open gtf and get inton positions
my %introns; #key=gene_id, #key2=position, #value = 1;
my %previous_exon; #key=gene_id. value=exon_position

#setup a hash of intron positions
open(GENES2,$gtf) || die "can't open $gtf";      #gft is 1 based
while (<GENES2>){
    unless(/^#/){
        my @b=split("\t");
        my $class=$b[2];
        my $start=$b[3];
        my $end=$b[4];
        my $dir=$b[6];
        if ($class eq "exon"){
  
            my ($gene_id) = $b[8] =~ /gene_id\s"([^\"]+)";/;

            if ($gene_id){
 
                #fwd exons are sorted in ascending order 
                #             100958 100975 <-end(+1)
                # satrt(-1)-> 101077 101715 
                if ($dir eq "+"){
                     if (exists $previous_exon{$gene_id}){   #if there is a previous exon, there is an intron between this one and the last one
                         for my $pos ($previous_exon{$gene_id} .. ($start-1)){   #end of previous exon to start of this one = sorted ascending
                             $introns{$gene_id}{$pos}=1;
                         }
                         $previous_exon{$gene_id}=$end+1;
                     }else{
                         $previous_exon{$gene_id}=$end+1;
                     }

                }else{
                #rev exons are sorted in decending order  
                # end(-1)-> 447087 447794
                #           446060 446254 <-start(+1)
                     if (exists $previous_exon{$gene_id}){   #if there is a previous exon, there is an intron between this one and the last one
                         for my $pos (($end+1) .. $previous_exon{$gene_id}){   #end of this exon to start of the pervious one = sorted decending
                             $introns{$gene_id}{$pos}=1;
                         }
                         $previous_exon{$gene_id}=$start-1;
                    }else{
                         $previous_exon{$gene_id}=$start-1;
                    }
                }
            }
        }
    }
}
close (GENES2);

###################################################################################################
#open gtf file setup genomic cds positions
my %upstreamFwd; #key1 = chr, key2 = position, value = gene_id(s)
my %upstreamRev; #key1 = chr, key2 = position, value = gene_id(s)
my %downstreamFwd; #key1 = chr, key2 = position, value = gene_id(s)
my %downstreamRev; #key1 = chr, key2 = position, value = gene_id(s)
my %cdsFwd; #key1 = chr, key2 = position, value = gene_ids(s)
my %cdsRev; #key1 = chr, key2 = position, value = gene_ids(s)
my %cdsSum_3; #key = gene_id, value=total CDS signal
my %cdsSum_5;
my %upstreamM_3; #key1 = gene_id, key2=metapos, key3=value
my %downstreamM_3; #key1 = gene_id, key2=metapos, key3=value
my %upstreamM_5; #key1 = gene_id, key2=metapos, key3=value
my %downstreamM_5; #key1 = gene_id, key2=metapos, key3=value

my $feature_count=0;
my %motif2gene;
my %motif2location;


open(GENES,$gtf) || die "can't open $gtf";
while (<GENES>){
    unless(/^#/){
        my @b=split("\t");
        my $class=$b[2];
        my $chr=$b[0];
        my $start=$b[3];
        my $end=$b[4];
        my $dir=$b[6];
        my ($gene_id) = $b[8] =~ /gene_id\s"([^\"]+)";/;

        if ($gene_id){

            if ($class eq "CDS"){

                #use the start of the cds for start codons
                #	This will need updating for eukartyotic genomes (which hopefully have annotated start codons)

                if ($dir eq "+"){
                    $cdsSum_3{$gene_id}=0;
                    $cdsSum_5{$gene_id}=0;
                    for ($start .. $end){
                        if (exists ($cdsFwd{$chr}{$_})){
                             $cdsFwd{$chr}{$_}.=",".$gene_id;
                        }else{
                             $cdsFwd{$chr}{$_}=$gene_id;
                        }
                    }

                    #upstream is start-1 to start+2
                    #search for motifs
                    my $seq=substr($fasta_sequences{$chr},$start-1,3);
                    while ($seq =~ /$MOTIF/g){
                        $feature_count++;
                        $motif2gene{$feature_count}=$gene_id;
              #         $motif2location{$feature_count}=$chr.",".(($start-20)+$-[0]);
                        #$-[0]; start position
                        #$+[0]; end position
              #         print "$chr,".(($start-20)+$-[0]).",".((($start-20)+$+[0])-1)."\n";
 
                        #setup the window around the start position of each match
          #Why do this in ATG?????
                        
                        my $match_start=$start;
                        my $posCount=$match_start-100;
                        my $cdsLength=-100;
                        while ($cdsLength <= 500){
                            if (exists ($upstreamFwd{$chr}{$posCount})){
                                $upstreamFwd{$chr}{$posCount}.=",".$feature_count.";".$cdsLength;  #change linker from "_" to ";"
                            }else{
                                $upstreamFwd{$chr}{$posCount}=$feature_count.";".$cdsLength;
                            }
                            $upstreamM_3{$feature_count}{$cdsLength}=0;
                            $upstreamM_5{$feature_count}{$cdsLength}=0;
                            $cdsLength++;
                            $posCount++;
                        }
                        #$posCount++;
                    }    

                    #downstream if start to end
                    $seq=substr($fasta_sequences{$chr},$start+2,($end-($start+2)));
                    my $frame=($start+2)%3;
                    while ($seq =~ /$MOTIF/g){
                        #check if this is inframe
                        if ((($start+2)+$-[0]) %3 == $frame){
                  #          print "inframe,$gene_id,$chr,".((($start+2)+$-[0]+1))."\n";
                            $feature_count++;
                            $motif2gene{$feature_count}=$gene_id;
                  #         $motif2location{$feature_count}=$chr.",".((($start+2)+$-[0])+1);
                  #         print "$chr,".((($start+2)+$-[0])+1).",".(($start+2)+$+[0])."\n";

                            #setup the window around the start position of each match
                            my $match_start=(($start+2)+$-[0])+1;
                            my $posCount=$match_start-100;
                            my $cdsLength=-100;
                            while ($cdsLength <= 500){
                                if (exists ($downstreamFwd{$chr}{$posCount})){
                                    $downstreamFwd{$chr}{$posCount}.=",".$feature_count.";".$cdsLength;  #change linker from "_" to ";"
                                }else{
                                    $downstreamFwd{$chr}{$posCount}=$feature_count.";".$cdsLength;
                                }
                                $downstreamM_3{$feature_count}{$cdsLength}=0;
                                $downstreamM_5{$feature_count}{$cdsLength}=0;
                                $cdsLength++;
                                $posCount++;
                            }
                        }
                    }

                 }else{ #reverse cases

#                     $cdsSum_3{$gene_id}=0;  #change to relate to features => #feature to gene hash?
#                     $cdsSum_5{$gene_id}=0;
#                     for ($start .. $end){
#                         if (exists ($cdsRev{$chr})){
#                            $cdsRev{$chr}{$_}.=",".$gene_id;
#                         }else{
#                            $cdsRev{$chr}{$_}=$gene_id;
#                         }
#                    }
#
#                    #upstream is end to end +20
#                    my $seq=reverse(substr($fasta_sequences{$chr},$end-3,3));
#                    $seq=~tr/ACGTacgt/TGCAtgca/;
#                    while ($seq =~ /$MOTIF/g){
#                        $feature_count++;
#                        $motif2gene{$feature_count}=$gene_id;
#               #        $motif2location{$feature_count}=$chr.",".((($end+20)-$-[0])-1);
#               #        print "$chr,".((($end+20)-$-[0])-1).",".((($end+20)-$+[0])-1)."\n";  #additional -1 for zero base
#
#                        #setup the window around the start position of each match
#                        my $match_start=$end;
#                        my $revPos=$match_start+100;
#                        my $cdsLength=-100;
#                        while ($cdsLength <= 500){
#                            if (exists ($upstreamRev{$chr}{$revPos})){
#                                 $upstreamRev{$chr}{$revPos}.=",".$feature_count.";".$cdsLength;
#                            }else{
#                                 $upstreamRev{$chr}{$revPos}=$feature_count.";".$cdsLength;
#                            }
#                            $upstreamM_3{$feature_count}{$cdsLength}=0;
#                            $upstreamM_5{$feature_count}{$cdsLength}=0;
#                            $cdsLength++;
#                            $revPos--;
#                        }
#                        #$revPos--;
#                    }                 
#
#                    #downstream is end to start
#                    $seq=reverse(substr($fasta_sequences{$chr},$start,(($end-3)-$start)));
#                    $seq=~tr/ACGTacgt/TGCAtgca/;
#             #      print "$gene_id,$seq\n";
#                    my $frame=$end%3;
#                    while ($seq =~ /$MOTIF/g){
#
#                        #check if this is inframe
#                        if ((($end-3)-$-[0]) %3 == $frame){
#                            #print "inframe,$gene_id,$chr,".((($end-3)-$-[0]))."\n";
#                            $feature_count++;
#                            $motif2gene{$feature_count}=$gene_id;
#             #              $motif2location{$feature_count}=$chr.",".(($end-3)-$-[0]);
#             #              print "$chr,".(($end-3)-$-[0]).",".((($end-3)-$+[0])+1)."\n";  #additional +1 for zero base
#
#                            #setup the window around the start position of each match
#                            my $match_start=($end-3)-$-[0];
#                            my $revPos=$match_start+100;
#                            my $cdsLength=-100;
#                            while ($cdsLength <= 500){
#                                 if (exists ($downstreamRev{$chr}{$revPos})){
#                                      $downstreamRev{$chr}{$revPos}.=",".$feature_count.";".$cdsLength;
#                                }else{
#                                     $downstreamRev{$chr}{$revPos}=$feature_count.";".$cdsLength;
#                                }
#                                $downstreamM_3{$feature_count}{$cdsLength}=0;
#                                $downstreamM_5{$feature_count}{$cdsLength}=0;
#                                $cdsLength++;
#                                $revPos--;
#                            }
#                        }
#                    }
                }
            }
        } 
    }
}
close(GENES);

print "gtf parsed\n";

###################################################################################################
#open sam file
my %upstream_lengths_scale_3;     #gene, meta position (-50 to 500), length = count
my %upstream_lengths_scale_5;     #gene, meta position (-50 to 500), length = count

my %downstream_lengths_scale_3;     #gene, meta position (-50 to 500), length = count
my %downstream_lengths_scale_5;     #gene, meta position (-50 to 500), length = count

open (SAM, $sam) || die "can't open $sam";
while (<SAM>){
    #skip headers
    unless(/^#/){
        my @b=split("\t");
        my $leftMost=$b[3]; #leftmost position of match 5' for fwd, 3' for rev
        my $flag=$b[1];
        my $chr=$b[2];
        my $mapq=$b[4];
        my $cig=$b[5];
        my $seq=$b[9];
        my $threePrime;
        my $fivePrime;
        #mapping uniqness filter
        if ($mapq >= 10){
            unless ($flag & 0x4){   #if aligned
                if ($flag & 0x10){  #if rev calculate 3' == sam coordinate (leftmost)

#                   $threePrime=$leftMost;
#
#                   #parse cigar for indels and adjust the length of the alignment             
#                   my $length=length($seq);
#                   while ($cig =~/(\d+)I/g){   #add to length for insertions
#                      $length+=$1;
#                   }
#                   while ($cig =~/(\d+)D/g){   #substact from length for deletions
#                       $length-=$1;
#                   }
#                   $fivePrime=$leftMost+($length-1);              #SAM is 1 based
#
#                   #assign to metaplots
#                   if (exists ($upstreamRev{$chr}{$threePrime})){
#                       my @over1=split(",",$upstreamRev{$chr}{$threePrime});
#                       for (@over1){
#                           my ($motif,$meta_pos)=$_=~/(^[^;]+);(-?\w+)$/;  #/(\w+)_(-?\w+)/;
#                           $upstreamM_3{$motif}{$meta_pos}+=1;            
#                           $upstream_lengths_scale_3{$motif}{$meta_pos}{length($seq)}+=1;   
#                       }    
#                   }
#
#                   if (exists ($downstreamRev{$chr}{$threePrime})){
#                       my @over1=split(",",$downstreamRev{$chr}{$threePrime});
#                       for (@over1){
#                           my ($motif,$meta_pos)=$_=~/(^[^;]+);(-?\w+)$/;  #/(\w+)_(-?\w+)/;
#                           $downstreamM_3{$motif}{$meta_pos}+=1;
#                           $downstream_lengths_scale_3{$motif}{$meta_pos}{length($seq)}+=1;
#                       }
#                   }
#
#                   if (exists ($cdsRev{$chr}{$threePrime})){
#                       my @over3=split(",",$cdsRev{$chr}{$threePrime});
#                       for (@over3){
#                           $cdsSum_3{$_}++;
#                       }    
#                   }
#
#                   #same again for the 5 prime
#                   if (exists ($upstreamRev{$chr}{$fivePrime})){
#                       my @over1=split(",",$upstreamRev{$chr}{$fivePrime});
#                       for (@over1){
#                           my ($motif,$meta_pos)=$_=~/(^[^;]+);(-?\w+)$/;  #/(\w+)_(-?\w+)/;
#                           $upstreamM_5{$motif}{$meta_pos}+=1;
#                           $upstream_lengths_scale_5{$motif}{$meta_pos}{length($seq)}+=1;
#                       }
#                   }
#
#                   if (exists ($downstreamRev{$chr}{$fivePrime})){
#                       my @over1=split(",",$downstreamRev{$chr}{$fivePrime});
#                       for (@over1){
#                           my ($motif,$meta_pos)=$_=~/(^[^;]+);(-?\w+)$/;  #/(\w+)_(-?\w+)/;
#                           $downstreamM_5{$motif}{$meta_pos}+=1;
#                           $downstream_lengths_scale_5{$motif}{$meta_pos}{length($seq)}+=1;
#                       }
#                   }
#
#                   if (exists ($cdsRev{$chr}{$fivePrime})){
#                       my @over3=split(",",$cdsRev{$chr}{$fivePrime});
#                       for (@over3){
#                           $cdsSum_5{$_}++;
#                       }
#                   }

                }else{ #if fwd 3' == sam coordinate (leftmost) + read length 

                    #parse cigar for indels and adjust the length of the alignment             
                    my $length=length($seq);
                    while ($cig =~/(\d+)I/g){   #add to length for insertions
                        $length+=$1;
                    }
                    while ($cig =~/(\d+)D/g){   #substact from length for deletions
                        $length-=$1;
                    }

                    $threePrime=$leftMost+($length-1);              #SAM is 1 based
                    $fivePrime=$leftMost;

                    #assign to metaplots
                    if (exists ($upstreamFwd{$chr}{$threePrime})){
                        my @over4=split(",",$upstreamFwd{$chr}{$threePrime});
                        for (@over4){
                            my ($motif,$meta_pos)=$_=~/(^[^;]+);(-?\w+$)/;
                            $upstreamM_3{$motif}{$meta_pos}+=1;    
                            $upstream_lengths_scale_3{$motif}{$meta_pos}{length($seq)}+=1;
                        }
                    }

                    if (exists ($downstreamFwd{$chr}{$threePrime})){
                        my @over4=split(",",$downstreamFwd{$chr}{$threePrime});
                        for (@over4){
                            my ($motif,$meta_pos)=$_=~/(^[^;]+);(-?\w+$)/;
                            $downstreamM_3{$motif}{$meta_pos}+=1;
                            $downstream_lengths_scale_3{$motif}{$meta_pos}{length($seq)}+=1;
                        }
                    }
 
                    if (exists ($cdsFwd{$chr}{$threePrime})){
                        my @over6=split(",",$cdsFwd{$chr}{$threePrime}); 
                        for (@over6){
                            $cdsSum_3{$_}++;
                        }
                    }
 
                    #do that again for five prime
                    if (exists ($upstreamFwd{$chr}{$fivePrime})){
                        my @over4=split(",",$upstreamFwd{$chr}{$fivePrime});
                        for (@over4){
                            my ($motif,$meta_pos)=$_=~/(^[^;]+);(-?\w+$)/;
                            $upstreamM_5{$motif}{$meta_pos}+=1;
                            $upstream_lengths_scale_5{$motif}{$meta_pos}{length($seq)}+=1;
                        }
                    }

                    if (exists ($downstreamFwd{$chr}{$fivePrime})){

           #            print "hit\n";

                        my @over4=split(",",$downstreamFwd{$chr}{$fivePrime});
                        for (@over4){
                            my ($motif,$meta_pos)=$_=~/(^[^;]+);(-?\w+$)/;

          #                  print "coord=$fivePrime,motif=$motif,pos=$meta_pos\n";

                            $downstreamM_5{$motif}{$meta_pos}+=1;
                            $downstream_lengths_scale_5{$motif}{$meta_pos}{length($seq)}+=1;
                        }
                    }
 
                    if (exists ($cdsFwd{$chr}{$fivePrime})){
                        my @over6=split(",",$cdsFwd{$chr}{$fivePrime});
                        for (@over6){
                            $cdsSum_5{$_}++;
                        }
                    }
                }
            }
        }
    }                    
}
close (SAM);

print "sam parsed'\n";

#for my $SD (keys %downstreamM_5){
#    for my $position (sort { $a <=> $b } keys %{$downstreamM_5{$SD} } ){
#        print "$motif2gene{$SD},$motif2location{$SD},$SD,$position,$downstreamM_5{$SD}{$position}\n";
#    }
#}

#exit;

###################################################################################################
#scale lengths
my %scaled_upstream_3;
my %scaled_upstream_5;
my %scaled_downstream_3;
my %scaled_downstream_5;

#CDSsum is is a per gene basis...
#the other hashes are on a per motif basis...

for my $motif (sort keys %upstream_lengths_scale_3){        
    for my $pos (sort {$a <=> $b} keys %{$upstream_lengths_scale_3{$motif}}){
        for my $length (sort {$a <=> $b} keys %{$upstream_lengths_scale_3{$motif}{$pos}}){
            #devide count by the total cds count for this gene;
            my $gene=$motif2gene{$motif};
            my $scaled_count=eval { $upstream_lengths_scale_3{$motif}{$pos}{$length}/$cdsSum_3{$gene}} || 0 ;
            $scaled_upstream_3{$pos}{$length}+=$scaled_count; 
        }
    }
}

for my $motif (sort keys %upstream_lengths_scale_5){
    for my $pos (sort {$a <=> $b} keys %{$upstream_lengths_scale_5{$motif}}){
        for my $length (sort {$a <=> $b} keys %{$upstream_lengths_scale_5{$motif}{$pos}}){
            #devide count by the total cds count for this gene;
            my $gene=$motif2gene{$motif}; 
            my $scaled_count=eval { $upstream_lengths_scale_5{$motif}{$pos}{$length}/$cdsSum_5{$gene}} || 0 ;
            $scaled_upstream_5{$pos}{$length}+=$scaled_count;
        }
    }
}

for my $motif (sort keys %downstream_lengths_scale_3){
    for my $pos (sort {$a <=> $b} keys %{$downstream_lengths_scale_3{$motif}}){
        for my $length (sort {$a <=> $b} keys %{$downstream_lengths_scale_3{$motif}{$pos}}){
            #devide count by the total cds count for this gene;
            my $gene=$motif2gene{$motif};
            my $scaled_count=eval { $downstream_lengths_scale_3{$motif}{$pos}{$length}/$cdsSum_3{$gene}} || 0 ;
            $scaled_downstream_3{$pos}{$length}+=$scaled_count;
        }
    }
}

for my $motif (sort keys %downstream_lengths_scale_5){
    for my $pos (sort {$a <=> $b} keys %{$downstream_lengths_scale_5{$motif}}){
        for my $length (sort {$a <=> $b} keys %{$downstream_lengths_scale_5{$motif}{$pos}}){
            #devide count by the total cds count for this gene;
            my $gene=$motif2gene{$motif};
            my $scaled_count=eval { $downstream_lengths_scale_5{$motif}{$pos}{$length}/$cdsSum_5{$gene}} || 0 ;
            $scaled_downstream_5{$pos}{$length}+=$scaled_count;
        }
    }
}

###################################################################################################
my $outStart=$outDir."/".$prefix."_upstream_motif_3prime.csv";
my $outStop=$outDir."/".$prefix."_downstream_motif_3prime.csv";
my $outStartLengthsScale=$outDir."/".$prefix."_upstream_codon_lengths_scale_3prime.csv";
my $outStopLengthsScale=$outDir."/".$prefix."_downstream_codon_lengths_scale_3prime.csv";

open (OUT1,">$outStart") || die "can't open $outStart\n";
open (OUT2,">$outStop")  || die "can't open $outStop\n";
open (OUT3,">$outStartLengthsScale") || die "can't open $outStartLengthsScale\n";
open (OUT4,">$outStopLengthsScale")  || die "can't open $outStopLengthsScale\n";

#####
## meta plots start
#####
print OUT1 "motif_id"; for (-100 .. 500){     print OUT1 ",$_"; } print OUT1 ",sum\n"; #header
for my $motif (sort keys %upstreamM_3){
    print OUT1 "$motif";
    for my $pos (sort {$a <=> $b} keys %{$upstreamM_3{$motif}}){
        print OUT1 ",$upstreamM_3{$motif}{$pos}";
    }
    my $gene=$motif2gene{$motif};
    print OUT1 ",$cdsSum_3{$gene}\n";
}

#####
## meta_plots stop
#####
print OUT2 "motif_id"; for (-500 .. 100){     print OUT2 ",$_"; } print OUT2 ",sum\n"; #header
for my $motif (sort keys %downstreamM_3){
    print OUT2 "$motif";
    for my $pos (sort {$a <=> $b} keys %{$downstreamM_3{$motif}}){
        print OUT2 ",$downstreamM_3{$motif}{$pos}";
    }
    my $gene=$motif2gene{$motif};
    print OUT2 ",$cdsSum_3{$gene}\n";
}

#####
## output scaled length values
#####
for my $pos (sort {$a <=> $b} keys %scaled_upstream_3){ for my $length (sort {$a <=> $b} keys %{$scaled_upstream_3{$pos}}){ print OUT3 "$pos\t$length\t$scaled_upstream_3{$pos}{$length}\n"; } }
for my $pos (sort {$a <=> $b} keys %scaled_downstream_3){ for my $length (sort {$a <=> $b} keys %{$scaled_downstream_3{$pos}}){  print OUT4 "$pos\t$length\t$scaled_downstream_3{$pos}{$length}\n"; } }


###################################################################################################
$outStart=$outDir."/".$prefix."_upstream_motif_5prime.csv";
$outStop=$outDir."/".$prefix."_downstream_motif_5prime.csv";
$outStartLengthsScale=$outDir."/".$prefix."_upstream_codon_lengths_scale_5prime.csv";
$outStopLengthsScale=$outDir."/".$prefix."_downstream_codon_lengths_scale_5prime.csv";

open (OUT5,">$outStart") || die "can't open $outStart\n";
open (OUT6,">$outStop")  || die "can't open $outStop\n";
open (OUT7,">$outStartLengthsScale") || die "can't open $outStartLengthsScale\n";
open (OUT8,">$outStopLengthsScale")  || die "can't open $outStopLengthsScale\n";

#####
## meta plots start
#####
print OUT5 "motif_id"; for (-100 .. 500){     print OUT5 ",$_"; } print OUT5 ",sum\n"; #header
for my $motif (sort keys %upstreamM_5){
    print OUT5 "$motif";
    for my $pos (sort {$a <=> $b} keys %{$upstreamM_5{$motif}}){
        print OUT5 ",$upstreamM_5{$motif}{$pos}";
    }
    my $gene=$motif2gene{$motif};
    print OUT5 ",$cdsSum_5{$gene}\n";
}

#####
## meta_plots stop
#####
print OUT6 "motif_id"; for (-500 .. 100){     print OUT6 ",$_"; } print OUT6 ",sum\n"; #header
for my $motif (sort keys %downstreamM_5){
    print OUT6 "$motif";
    for my $pos (sort {$a <=> $b} keys %{$downstreamM_5{$motif}}){
        print OUT6 ",$downstreamM_5{$motif}{$pos}";
    }
    my $gene=$motif2gene{$motif};
    print OUT6 ",$cdsSum_5{$gene}\n";
}

#####
## output scaled length values
#####
for my $pos (sort {$a <=> $b} keys %scaled_upstream_5){ for my $length (sort {$a <=> $b} keys %{$scaled_upstream_5{$pos}}){ print OUT7 "$pos\t$length\t$scaled_upstream_5{$pos}{$length}\n"; } }

for my $pos (sort {$a <=> $b} keys %scaled_downstream_5){  for my $length (sort {$a <=> $b} keys %{$scaled_downstream_5{$pos}}){  print OUT8 "$pos\t$length\t$scaled_downstream_5{$pos}{$length}\n"; } }

exit
