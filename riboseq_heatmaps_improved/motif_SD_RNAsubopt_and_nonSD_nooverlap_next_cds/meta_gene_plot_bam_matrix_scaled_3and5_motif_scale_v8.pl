#!/usr/bin/perl -w
use strict;

#to do 10/04/2017
#script to take gtf get coords around start/stop codons, and assign 5' + 3' regions to them from sam
#scale each gene, so that the reads coming from it sum to 1
#where a read overlaps multiple in strand genes counts it towards all those genes

#input files:
#gtf exons, start, stop codons
#sam = aligned 
my $gtf=$ARGV[0];
my $sam=$ARGV[1];
my $fasta=$ARGV[2];
my $outDir=$ARGV[3];
my $SD_list=$ARGV[4];
my $predictions=$ARGV[5];
my ($prefix)=$sam=~/([^\/]+).sam$/;


#my $MOTIF="CGGAGGTG";          
#my $MOTIF="AGGAGG";
#my $MOTIF="AGGAGGTG";
#my $MOTIF="GTG";
#my $MOTIF="ATG";

##################################################################################################
#open predictions and store the locations of predicited TISs to exlude from intenral positions later
my %predicted_TIS_fwd;
my %predicted_TIS_rev;
open (PRED, $predictions) || die;
while (<PRED>){

    unless (/^track/){  #skip header
        my @b=split("\t");
        my $chr=$b[0];
        my $start=$b[1]+1;  #bed is 0 based at start#        
        my $end=$b[2];
        my $type=$b[3];
        my $dir=$b[5];

        if ($dir eq "+"){
            $predicted_TIS_fwd{$chr}{$start}=1;   
        }else{
            $predicted_TIS_rev{$chr}{$end}=1;   #should this be end
        }
    }
}
close(PRED);

#################################################################################################
#open and store the list of high confidence SD positions
my %SD_fwd;
my %SD_rev; #chr, pos, = 1

open (SD, $SD_list) || die;
while (<SD>){

    chomp();
    my @s=split(",");
    my $chr=$s[0];
    my $pos=$s[1];
    my $dir=$s[4];    
    my $score=$s[2];
    my $type=$s[3];

    if ($dir eq "fwd"){
        $SD_fwd{$chr}{$pos}=1;
    }else{
        $SD_rev{$chr}{$pos}=1;
    }
}
close(SD);

##################################################################################################
#open fasta and get chr limits
my %fasta_sequences; #key = sequence_name, value=sequence
my $name="";
open (FA, $fasta) || die "can't open $fasta";
while (<FA>){
    chomp;
    if (/^>([^\s]+)/){ #take header up to the first space
        $name=$1;
    }else{
        $fasta_sequences{$name}.=$_;
    }
}
close(FA);

print "fasta parsed\n";

###################################################################################################
#open gtf and get inton positions
my %introns; #key=gene_id, #key2=position, #value = 1;
my %previous_exon; #key=gene_id. value=exon_position

#setup a hash of intron positions
open(GENES2,$gtf) || die "can't open $gtf";      #gft is 1 based
while (<GENES2>){
    unless(/^#/){
        my @b=split("\t");
        my $class=$b[2];
        my $start=$b[3];
        my $end=$b[4];
        my $dir=$b[6];
        if ($class eq "exon"){
  
            my ($gene_id) = $b[8] =~ /gene_id\s"([^\"]+)";/;

            if ($gene_id){
 
                #fwd exons are sorted in ascending order 
                #             100958 100975 <-end(+1)
                # satrt(-1)-> 101077 101715 
                if ($dir eq "+"){
                     if (exists $previous_exon{$gene_id}){   #if there is a previous exon, there is an intron between this one and the last one
                         for my $pos ($previous_exon{$gene_id} .. ($start-1)){   #end of previous exon to start of this one = sorted ascending
                             $introns{$gene_id}{$pos}=1;
                         }
                         $previous_exon{$gene_id}=$end+1;
                     }else{
                         $previous_exon{$gene_id}=$end+1;
                     }

                }else{
                #rev exons are sorted in decending order  
                # end(-1)-> 447087 447794
                #           446060 446254 <-start(+1)
                     if (exists $previous_exon{$gene_id}){   #if there is a previous exon, there is an intron between this one and the last one
                         for my $pos (($end+1) .. $previous_exon{$gene_id}){   #end of this exon to start of the pervious one = sorted decending
                             $introns{$gene_id}{$pos}=1;
                         }
                         $previous_exon{$gene_id}=$start-1;
                    }else{
                         $previous_exon{$gene_id}=$start-1;
                    }
                }
            }
        }
    }
}
close (GENES2);

###################################################################################################
#open gtf file setup genomic cds positions
my %upstreamFwd; #key1 = chr, key2 = position, value = gene_id(s)
my %upstreamRev; #key1 = chr, key2 = position, value = gene_id(s)
my %downstreamFwd; #key1 = chr, key2 = position, value = gene_id(s)
my %downstreamRev; #key1 = chr, key2 = position, value = gene_id(s)
my %cdsFwd; #key1 = chr, key2 = position, value = gene_ids(s)
my %cdsRev; #key1 = chr, key2 = position, value = gene_ids(s)
my %cdsSum_3; #key = gene_id, value=total CDS signal
my %cdsSum_5;
my %upstreamM_3; #key1 = gene_id, key2=metapos, key3=value
my %downstreamM_3; #key1 = gene_id, key2=metapos, key3=value
my %upstreamM_5; #key1 = gene_id, key2=metapos, key3=value
my %downstreamM_5; #key1 = gene_id, key2=metapos, key3=value

my $feature_count=0;
my %motif2gene;
my %motif2chr;
my %motif2dir;
my %combined_CDS;

my %upstreamFwd_TIS;
my %upstreamRev_TIS;
my %upstreamM_TIS_3;
my %upstreamM_TIS_5;
my %TIS_withSD;

my %TIS_fwd;
my %TIS_rev;

open(GENES,$gtf) || die "can't open $gtf";
while (<GENES>){
    unless(/^#/){
        my @b=split("\t");
        my $class=$b[2];
        my $chr=$b[0];
        my $start=$b[3]; 
        my $end=$b[4]; #salmonella CDS does not includee the stop codon. Ecoli CDS does include the stop codon.:
        my $dir=$b[6];
        my ($gene_id) = $b[8] =~ /gene_id\s"([^\"]+)";/;

        if ($gene_id){

            if ($class eq "CDS"){

                $combined_CDS{$gene_id}=0;

                #use the start of the cds for start codons
                #This will need updating for eukartyotic genomes (which hopefully have annotated start codons)

                if ($dir eq "+"){

                    $cdsSum_3{$gene_id}=0;
                    $cdsSum_5{$gene_id}=0;

                    #TIS
                    $TIS_fwd{$chr}{$start}=1;
                    my $posCount=$start-100;
                    my $cdsLength=-100;
                    while ($cdsLength <= 500){
                        if (exists ($upstreamFwd_TIS{$chr}{$posCount})){
                             $upstreamFwd_TIS{$chr}{$posCount}.=",".$gene_id.";".$cdsLength;  #change linker from "_" to ";"
                        }else{
                             $upstreamFwd_TIS{$chr}{$posCount}=$gene_id.";".$cdsLength;
                        }
                        $upstreamM_TIS_3{$gene_id}{$cdsLength}=0;
                        $upstreamM_TIS_5{$gene_id}{$cdsLength}=0;
                        $cdsLength++;
                        $posCount++;
                    }

                    #check for upstream motif
                    for my $match_start (($start-30) .. ($start -1 )){
                        if (exists($SD_fwd{$chr}{$match_start})){

                            #count distance from start codon here:

                            $feature_count++;
                            $motif2gene{$feature_count}=$gene_id;
                            $TIS_withSD{$gene_id}=1;

                            my $posCount=$match_start-100;
                            my $cdsLength=-100;
                            while ($cdsLength <= 500){
                                if (exists ($upstreamFwd{$chr}{$posCount})){
                                    $upstreamFwd{$chr}{$posCount}.=",".$feature_count.";".$cdsLength;  #change linker from "_" to ";"
                                }else{
                                    $upstreamFwd{$chr}{$posCount}=$feature_count.";".$cdsLength;
                                }
                                $upstreamM_3{$feature_count}{$cdsLength}=0;
                                $upstreamM_5{$feature_count}{$cdsLength}=0;
                                $cdsLength++;
                                $posCount++;
                            }
                        }
                    }
                 
                    #check for motif in CDS
                    for my $match_start ($start .. $end){
             
                        if (exists ($cdsFwd{$chr}{$match_start})){
                             $cdsFwd{$chr}{$match_start}.=",".$gene_id;
                        }else{
                             $cdsFwd{$chr}{$match_start}=$gene_id;
                        }

                        if (exists($SD_fwd{$chr}{$match_start})){

                            $feature_count++;                           
                            $motif2gene{$feature_count}=$gene_id;
                            $motif2dir{$feature_count}="fwd";
                            $motif2chr{$feature_count}=$chr;

                            my $posCount=$match_start-100;
                            my $cdsLength=-100;
                            while ($cdsLength <= 500){
                                if (exists ($downstreamFwd{$chr}{$posCount})){
                                    $downstreamFwd{$chr}{$posCount}.=",".$feature_count.";".$cdsLength;  #change linker from "_" to ";"
                                }else{
                                    $downstreamFwd{$chr}{$posCount}=$feature_count.";".$cdsLength;
                                }
                                $downstreamM_3{$feature_count}{$cdsLength}=0;
                                $downstreamM_5{$feature_count}{$cdsLength}=0;
                                $cdsLength++;
                                $posCount++;
                            }
                        }
                    }

                }else{ #reverse cases

                    $cdsSum_3{$gene_id}=0;  #change to relate to features => #feature to gene hash?
                    $cdsSum_5{$gene_id}=0;

                    #TIS
                    $TIS_rev{$chr}{$end}=1;
                    my $revPos=$end+100;
                    my $cdsLength=-100;
                    while ($cdsLength <= 500){
                        if (exists ($upstreamRev_TIS{$chr}{$revPos})){
                             $upstreamRev_TIS{$chr}{$revPos}.=",".$gene_id.";".$cdsLength;  #change linker from "_" to ";"
                        }else{
                             $upstreamRev_TIS{$chr}{$revPos}=$gene_id.";".$cdsLength;
                        }
                        $upstreamM_TIS_3{$gene_id}{$cdsLength}=0;
                        $upstreamM_TIS_5{$gene_id}{$cdsLength}=0;
                        $cdsLength++;
                        $revPos--;
                    }

                    #check for upstream motif
                    for my $match_start (($end+1) .. ($end +30)){
                        if (exists($SD_rev{$chr}{$match_start})){

                            #count distance from start codon here

                            $feature_count++;
                            $motif2gene{$feature_count}=$gene_id;
                            $TIS_withSD{$gene_id}=1;

                            #setup the window around the start position of each match
                            my $revPos=$match_start+100;
                            my $cdsLength=-100;
                            while ($cdsLength <= 500){
                                if (exists ($upstreamRev{$chr}{$revPos})){
                                    $upstreamRev{$chr}{$revPos}.=",".$feature_count.";".$cdsLength;
                                }else{
                                     $upstreamRev{$chr}{$revPos}=$feature_count.";".$cdsLength;
                                }
                                $upstreamM_3{$feature_count}{$cdsLength}=0;
                                $upstreamM_5{$feature_count}{$cdsLength}=0;
                                $cdsLength++;
                                $revPos--;
                            }
                        }
                    }                 

                    #check for downstream Motif (In CDS)
                    for my $match_start ($start .. $end){

                        if (exists ($cdsRev{$chr}{$match_start})){
                            $cdsRev{$chr}{$match_start}.=",".$gene_id;
                        }else{
                            $cdsRev{$chr}{$match_start}=$gene_id;
                        }
                        
                        if (exists($SD_rev{$chr}{$match_start})){

                            $feature_count++;
                            $motif2gene{$feature_count}=$gene_id;
                            $motif2dir{$feature_count}="rev";
                            $motif2chr{$feature_count}=$chr;

                            my $revPos=$match_start+100;
                            my $cdsLength=-100;
                            while ($cdsLength <= 500){
                                if (exists ($downstreamRev{$chr}{$revPos})){
                                    $downstreamRev{$chr}{$revPos}.=",".$feature_count.";".$cdsLength;
                                }else{
                                    $downstreamRev{$chr}{$revPos}=$feature_count.";".$cdsLength;
                                }
                                $downstreamM_3{$feature_count}{$cdsLength}=0;
                                $downstreamM_5{$feature_count}{$cdsLength}=0;
                                $cdsLength++;
                                $revPos--;
                            }
                        }
                    }
                }
            }
        } 
    }
}
close(GENES);

print "GTF parsed\n";

###################################################################################################
#open sam file
my %upstream_lengths_scale_3;     #gene, meta position (-50 to 500), length = count
my %upstream_lengths_scale_5;     #gene, meta position (-50 to 500), length = count
my %downstream_lengths_scale_3;     #gene, meta position (-50 to 500), length = count
my %downstream_lengths_scale_5;     #gene, meta position (-50 to 500), length = count
my %upstream_nonSD_lengths_scale_3;     #gene, meta position (-50 to 500), length = count
my %upstream_nonSD_lengths_scale_5;     #gene, meta position (-50 to 500), length = count
my %upstreamM_nonSD_3;
my %upstreamM_nonSD_5;

open (SAM, $sam) || die "can't open $sam";
while (<SAM>){
    #skip headers
    unless(/^#/){
        my @b=split("\t");
        my $leftMost=$b[3]; #leftmost position of match 5' for fwd, 3' for rev
        my $flag=$b[1];
        my $chr=$b[2];
        my $mapq=$b[4];
        my $cig=$b[5];
        my $seq=$b[9];
        my $threePrime;
        my $fivePrime;
        #mapping uniqness filter
        if ($mapq >= 10){
            unless ($flag & 0x4){   #if aligned
                if ($flag & 0x10){  #if rev calculate 3' == sam coordinate (leftmost)

                   $threePrime=$leftMost;

                   #parse cigar for indels and adjust the length of the alignment             
                   my $length=length($seq);
                   while ($cig =~/(\d+)I/g){   #add to length for insertions
                      $length+=$1;
                   }
                   while ($cig =~/(\d+)D/g){   #substact from length for deletions
                       $length-=$1;
                   }
                   $fivePrime=$leftMost+($length-1);              #SAM is 1 based

                   #assign to metaplots
                   if (exists ($upstreamRev{$chr}{$threePrime})){
                       my @over1=split(",",$upstreamRev{$chr}{$threePrime});
                       for (@over1){
                           my ($motif,$meta_pos)=$_=~/(^[^;]+);(-?\w+)$/;  #/(\w+)_(-?\w+)/;
                           $upstreamM_3{$motif}{$meta_pos}+=1;            
                           $upstream_lengths_scale_3{$motif}{$meta_pos}{length($seq)}+=1;   
                       }    
                   }

                   if (exists ($downstreamRev{$chr}{$threePrime})){
                       my @over1=split(",",$downstreamRev{$chr}{$threePrime});
                       for (@over1){
                           my ($motif,$meta_pos)=$_=~/(^[^;]+);(-?\w+)$/;  #/(\w+)_(-?\w+)/;
                           $downstreamM_3{$motif}{$meta_pos}+=1;
                           $downstream_lengths_scale_3{$motif}{$meta_pos}{length($seq)}+=1;
                       }
                   }

                   if (exists ($upstreamRev_TIS{$chr}{$threePrime})){
                       my @over1=split(",",$upstreamRev_TIS{$chr}{$threePrime});
                       for (@over1){
                           my ($gene,$meta_pos)=$_=~/(^[^;]+);(-?\w+)$/;  #/(\w+)_(-?\w+)/;
                           unless (exists ( $TIS_withSD{$gene})){
                               $upstreamM_nonSD_3{$gene}{$meta_pos}+=1;
                               $upstream_nonSD_lengths_scale_3{$gene}{$meta_pos}{length($seq)}+=1;
                           }
                       }
                   }

                   if (exists ($cdsRev{$chr}{$threePrime})){
                       my @over3=split(",",$cdsRev{$chr}{$threePrime});
                       for (@over3){
                           $cdsSum_3{$_}++;
                       }    
                   }

                   #same again for the 5 prime
                   if (exists ($upstreamRev{$chr}{$fivePrime})){
                       my @over1=split(",",$upstreamRev{$chr}{$fivePrime});
                       for (@over1){
                           my ($motif,$meta_pos)=$_=~/(^[^;]+);(-?\w+)$/;  #/(\w+)_(-?\w+)/;
                           $upstreamM_5{$motif}{$meta_pos}+=1;
                           $upstream_lengths_scale_5{$motif}{$meta_pos}{length($seq)}+=1;
                       }
                   }

                   if (exists ($downstreamRev{$chr}{$fivePrime})){
                       my @over1=split(",",$downstreamRev{$chr}{$fivePrime});
                       for (@over1){
                           my ($motif,$meta_pos)=$_=~/(^[^;]+);(-?\w+)$/;  #/(\w+)_(-?\w+)/;
                           $downstreamM_5{$motif}{$meta_pos}+=1;
                           $downstream_lengths_scale_5{$motif}{$meta_pos}{length($seq)}+=1;
                       }
                   }


                   if (exists ($upstreamRev_TIS{$chr}{$fivePrime})){
                       my @over1=split(",",$upstreamRev_TIS{$chr}{$fivePrime});
                       for (@over1){
                           my ($gene,$meta_pos)=$_=~/(^[^;]+);(-?\w+)$/;  #/(\w+)_(-?\w+)/;
                           unless (exists ( $TIS_withSD{$gene})){
 :                              $upstreamM_nonSD_5{$gene}{$meta_pos}+=1;
                               $upstream_nonSD_lengths_scale_5{$gene}{$meta_pos}{length($seq)}+=1;
                           }
                       }
                   }

                   if (exists ($cdsRev{$chr}{$fivePrime})){
                       my @over3=split(",",$cdsRev{$chr}{$fivePrime});
                       for (@over3){
                           $cdsSum_5{$_}++;
                       }
                   }

               }else{ #if fwd 3' == sam coordinate (leftmost) + read length 

                   #parse cigar for indels and adjust the length of the alignment             
                   my $length=length($seq);
                   while ($cig =~/(\d+)I/g){   #add to length for insertions
                       $length+=$1;
                   }
                   while ($cig =~/(\d+)D/g){   #substact from length for deletions
                       $length-=$1;
                   }

                   $threePrime=$leftMost+($length-1);              #SAM is 1 based
                   $fivePrime=$leftMost;

                   #assign to metaplots
                   if (exists ($upstreamFwd{$chr}{$threePrime})){
                       my @over4=split(",",$upstreamFwd{$chr}{$threePrime});
                       for (@over4){
                           my ($motif,$meta_pos)=$_=~/(^[^;]+);(-?\w+$)/;
                           $upstreamM_3{$motif}{$meta_pos}+=1;    
                           $upstream_lengths_scale_3{$motif}{$meta_pos}{length($seq)}+=1;
                       }
                   }

                   if (exists ($downstreamFwd{$chr}{$threePrime})){
                       my @over4=split(",",$downstreamFwd{$chr}{$threePrime});
                       for (@over4){
                           my ($motif,$meta_pos)=$_=~/(^[^;]+);(-?\w+$)/;
                           $downstreamM_3{$motif}{$meta_pos}+=1;
                           $downstream_lengths_scale_3{$motif}{$meta_pos}{length($seq)}+=1;
                       }
                   }

                   if (exists ($upstreamFwd_TIS{$chr}{$threePrime})){
                       my @over1=split(",",$upstreamFwd_TIS{$chr}{$threePrime});
                      for (@over1){
                           my ($gene,$meta_pos)=$_=~/(^[^;]+);(-?\w+)$/;  #/(\w+)_(-?\w+)/;
                           unless (exists ( $TIS_withSD{$gene})){
                               $upstreamM_nonSD_3{$gene}{$meta_pos}+=1;
                               $upstream_nonSD_lengths_scale_3{$gene}{$meta_pos}{length($seq)}+=1;
                           }
                       }
                   }

                   if (exists ($cdsFwd{$chr}{$threePrime})){
                       my @over6=split(",",$cdsFwd{$chr}{$threePrime}); 
                       for (@over6){
                           $cdsSum_3{$_}++;
                       }
                   }
 
                   #do that again for five prime
                   if (exists ($upstreamFwd{$chr}{$fivePrime})){
                       my @over4=split(",",$upstreamFwd{$chr}{$fivePrime});
                       for (@over4){
                           my ($motif,$meta_pos)=$_=~/(^[^;]+);(-?\w+$)/;
                           $upstreamM_5{$motif}{$meta_pos}+=1;
                           $upstream_lengths_scale_5{$motif}{$meta_pos}{length($seq)}+=1;
                       }
                   }

                   if (exists ($upstreamFwd_TIS{$chr}{$fivePrime})){
                       my @over1=split(",",$upstreamFwd_TIS{$chr}{$fivePrime});
                       for (@over1){
                           my ($gene,$meta_pos)=$_=~/(^[^;]+);(-?\w+)$/;  #/(\w+)_(-?\w+)/;
                           unless (exists ( $TIS_withSD{$gene})){
                               $upstreamM_nonSD_5{$gene}{$meta_pos}+=1;
                               $upstream_nonSD_lengths_scale_5{$gene}{$meta_pos}{length($seq)}+=1;
                           }
                       }
                   }
                  
                   if (exists ($downstreamFwd{$chr}{$fivePrime})){
                       my @over4=split(",",$downstreamFwd{$chr}{$fivePrime});
                       for (@over4){
                           my ($motif,$meta_pos)=$_=~/(^[^;]+);(-?\w+$)/;
                           $downstreamM_5{$motif}{$meta_pos}+=1;
                           $downstream_lengths_scale_5{$motif}{$meta_pos}{length($seq)}+=1;
                       }
                   }
 
                   if (exists ($cdsFwd{$chr}{$fivePrime})){
                       my @over6=split(",",$cdsFwd{$chr}{$fivePrime});
                       for (@over6){
                           $cdsSum_5{$_}++;
                       }
                   }
               }
            }
        }
    }                    
}
close (SAM);

print "sam parsed\n";

#for my $SD (keys %downstreamM_5){
#    for my $position (sort { $a <=> $b } keys %{$downstreamM_5{$SD} } ){
#        print "$motif2gene{$SD},$motif2location{$SD},$SD,$position,$downstreamM_5{$SD}{$position}\n";
#    }
#}

#my $SD_count_3=0; for(keys %upstreamM_3){        $SD_count_3++; }
#my $SD_count_5=0; for(keys %upstreamM_5){        $SD_count_5++; }
#my $tisCount_3=0; for(keys %upstreamM_TIS_3){    $tisCount_3++; }
#my $tisCount_5=0; for(keys %upstreamM_TIS_5){    $tisCount_5++; }
#my $tisAfter_3=0; for(keys %upstreamM_nonSD_3){  $tisAfter_3++; }
#my $tisAfter_5=0; for(keys %upstreamM_nonSD_5){  $tisAfter_5++; }

print "there were $tisCount_3 and $tisCount_5 before\n";
print "there were $SD_count_3 and $SD_count_5 upstream SD motifs\n";
print "there were $tisAfter_3 and $tisAfter_3 after\n";

#exit;

###################################################################################################
# rank the genes by cds expression, to exclude the bottom 10%
#need a list of fwd_genes, and rev_gene

for my $gene (keys %cdsSum_5 ){
    $combined_CDS{$gene}=$cdsSum_5{$gene};
}
for my $gene (keys %cdsSum_3 ){
    $combined_CDS{$gene}=$cdsSum_3{$gene};
}

my %black_list;
my $count=0;
my $count_exclude=0;
my $number_of_genes= keys %cdsSum_5;
my $ten_percent=$number_of_genes*0.2;
for my $gene ( sort { $combined_CDS{$a} <=> $combined_CDS{$b} } keys(%combined_CDS) ){
    if ($count <= $ten_percent){
        $black_list{$gene}=1;
        $count_exclude++;
    }
    $count++;
}
#print "there were $number_of_genes genes\n";
#print "there were $count_exclude genes filtered out\n";

#need to exclude motifs that are close to the next downstream genes
my %overlapping_motif;
for my $motif_id (keys %downstreamM_5){
    for my $pos (sort { $a <=> $b } keys %{ $downstreamM_5{$motif_id}} ){

        my $chr=$motif2chr{$motif_id};

        if ($motif2dir{$motif_id} eq "fwd"){ #fwd cases
            for my $loc ($pos .. ($pos+50)){
                if (exists ($TIS_fwd{$chr}{$loc})){
                    $overlapping_motif{$motif_id}=1;
                }
                if (exists ( $predicted_TIS_fwd{$chr}{$loc})){
                    $overlapping_motif{$motif_id}=1;
                }
            }
        }elsif($motif2dir{$motif_id} eq "rev"){   #reverse
            for my $loc (($pos-50) .. $pos){ 
                if (exists ($TIS_rev{$chr}{$loc})){
                    $overlapping_motif{$motif_id}=1;                   
                }
                if (exists ( $predicted_TIS_rev{$chr}{$loc})){
                    $overlapping_motif{$motif_id}=1;
                }                
            }
        }
    }
}

for my $motif_id (keys %downstreamM_3){
    for my $pos (sort { $a <=> $b } keys %{ $downstreamM_3{$motif_id}} ){

        my $chr=$motif2chr{$motif_id};

        if ($motif2dir{$motif_id} eq "fwd"){ #fwd cases
            for my $loc ($pos .. ($pos+50)){
                if (exists ($TIS_fwd{$chr}{$loc})){
                    $overlapping_motif{$motif_id}=1;
                }
                if (exists ( $predicted_TIS_fwd{$chr}{$loc})){
                    $overlapping_motif{$motif_id}=1;
                }
            }
        }elsif($motif2dir{$motif_id} eq "rev"){   #reverse
            for my $loc (($pos-50) .. $pos){
                if (exists ($TIS_rev{$chr}{$loc})){
                    $overlapping_motif{$motif_id}=1;
                }
                if (exists ( $predicted_TIS_rev{$chr}{$loc})){
                    $overlapping_motif{$motif_id}=1;
                }
            }
        }
    }
}

my %barchart_upstream_5;
my %barchart_upstream_3;
my %barchart_downstream_5;
my %barchart_downstream_3;
my %barchart_upstream_nonSD_5;   #on a per gene 
my %barchart_upstream_nonSD_3;   #on a per gene

my $downstreamCount=0;
my $upstreamCount=0;

#for my $motif_id (keys %upstreamM_5){ unless (exists ($black_list{ $motif2gene{$motif_id} })){ $upstreamCount++; } }
#for my $motif_id (keys %upstreamM_3){ unless (exists ($black_list{ $motif2gene{$motif_id} })){ $upstreamCount++; } }
#for my $motif_id (keys %downstreamM_5){ unless (exists ($black_list{ $motif2gene{$motif_id} })){ unless (exists ($overlapping_motif{$motif_id})){ $downstreamCount++; } } }
#for my $motif_id (keys %downstreamM_3){ unless (exists ($black_list{ $motif2gene{$motif_id} })){ unless (exists ($overlapping_motif{$motif_id})){ $downstreamCount++; } } }

#print "there are $upstreamCount filtered motifs upstream\n";
#print "there are $downstreamCount filtered motifs downstream\n";

#exit;

for my $motif_id (keys %upstreamM_5){
    for my $pos (sort { $a <=> $b } keys %{ $upstreamM_5{$motif_id}} ){
        my $gene_sum=$combined_CDS { $motif2gene{$motif_id} };
        unless (exists ($black_list{ $motif2gene{$motif_id} })){  #blacklisted then remove
            $barchart_upstream_5{$pos}+=eval{ $upstreamM_5{$motif_id}{$pos}/$gene_sum } || 0;
        }
    }
}

for my $motif_id (keys %upstreamM_3){
    for my $pos (sort { $a <=> $b } keys %{ $upstreamM_3{$motif_id}} ){
        my $gene_sum=$combined_CDS { $motif2gene{$motif_id} };
        unless (exists ($black_list{ $motif2gene{$motif_id} })){  #blacklisted then remove     
            $barchart_upstream_3{$pos}+=eval{ $upstreamM_3{$motif_id}{$pos}/$gene_sum } || 0;
        }
    }
}

#need to exclude motifs that are close to the next downstream genes
for my $motif_id (keys %downstreamM_5){
   for my $pos (sort { $a <=> $b } keys %{ $downstreamM_5{$motif_id}} ){
        my $gene_sum=$combined_CDS { $motif2gene{$motif_id} };
        unless (exists ($black_list{ $motif2gene{$motif_id}})){  #blacklisted then remove
            unless (exists ($overlapping_motif{$motif_id})){
                $barchart_downstream_5{$pos}+=eval{ $downstreamM_5{$motif_id}{$pos}/$gene_sum } || 0;
            }
        }
    }
}

for my $motif_id (keys %downstreamM_3){
    for my $pos (sort { $a <=> $b } keys %{ $downstreamM_3{$motif_id}} ){
        my $gene_sum=$combined_CDS { $motif2gene{$motif_id} };
        unless (exists ($black_list{ $motif2gene{$motif_id} })){  #blacklisted then remove     
            unless (exists ($overlapping_motif{$motif_id})){
                $barchart_downstream_3{$pos}+=eval{ $downstreamM_3{$motif_id}{$pos}/$gene_sum } || 0;
            }
        }
    }
}

for my $gene_id (keys %upstreamM_nonSD_5){
    for my $pos (sort { $a <=> $b } keys %{ $upstreamM_nonSD_5{$gene_id}}){
        my $gene_sum=$combined_CDS{$gene_id};
        unless (exists ($black_list{$gene_id})){  #blacklisted then remove
            $barchart_upstream_nonSD_5{$pos}+=eval{ $upstreamM_nonSD_5{$gene_id}{$pos}/$gene_sum } || 0;
        }
    }
}

for my $gene_id (keys %upstreamM_nonSD_3){
    for my $pos (sort { $a <=> $b } keys %{ $upstreamM_nonSD_3{$gene_id}}){
        my $gene_sum=$combined_CDS{$gene_id};
        unless (exists ($black_list{$gene_id})){  #blacklisted then remove
            $barchart_upstream_nonSD_3{$pos}+=eval{ $upstreamM_nonSD_3{$gene_id}{$pos}/$gene_sum } || 0;
        }
    }
}

#scale lengths
my %scaled_upstream_3;
my %scaled_upstream_5;
my %scaled_downstream_3;
my %scaled_downstream_5;
my %scaled_upstream_nonSD_3;   #on a per gene basis
my %scaled_upstream_nonSD_5;   #on a per gene basis

for my $motif (sort keys %upstream_lengths_scale_3){        
    for my $pos (sort {$a <=> $b} keys %{$upstream_lengths_scale_3{$motif}}){
        for my $length (sort {$a <=> $b} keys %{$upstream_lengths_scale_3{$motif}{$pos}}){
            #devide count by the total cds count for this gene;
            my $gene=$motif2gene{$motif};
            unless (exists ($black_list{$gene})){  #blacklisted then remove
                my $scaled_count=eval { $upstream_lengths_scale_3{$motif}{$pos}{$length}/$combined_CDS{$gene}} || 0 ;
                $scaled_upstream_3{$pos}{$length}+=$scaled_count; 
            }
        }
    }
}

for my $motif (sort keys %upstream_lengths_scale_5){
    for my $pos (sort {$a <=> $b} keys %{$upstream_lengths_scale_5{$motif}}){
        for my $length (sort {$a <=> $b} keys %{$upstream_lengths_scale_5{$motif}{$pos}}){
            #devide count by the total cds count for this gene;
            my $gene=$motif2gene{$motif}; 
            unless (exists ($black_list{$gene})){  #blacklisted then remove
                my $scaled_count=eval { $upstream_lengths_scale_5{$motif}{$pos}{$length}/$combined_CDS{$gene}} || 0 ;
                $scaled_upstream_5{$pos}{$length}+=$scaled_count;
            }
        }
    }
}

for my $gene (sort keys %upstream_nonSD_lengths_scale_3){
    for my $pos (sort {$a <=> $b} keys %{$upstream_nonSD_lengths_scale_3{$gene}}){
        for my $length (sort {$a <=> $b} keys %{$upstream_nonSD_lengths_scale_3{$gene}{$pos}}){
            #devide count by the total cds count for this gene;
            unless (exists ($black_list{$gene})){  #blacklisted then remove
                my $scaled_count=eval { $upstream_nonSD_lengths_scale_3{$gene}{$pos}{$length}/$combined_CDS{$gene}} || 0 ;
                $scaled_upstream_nonSD_3{$pos}{$length}+=$scaled_count;
            }
        }
    }
}

for my $gene (sort keys %upstream_nonSD_lengths_scale_5){
    for my $pos (sort {$a <=> $b} keys %{$upstream_nonSD_lengths_scale_5{$gene}}){
        for my $length (sort {$a <=> $b} keys %{$upstream_nonSD_lengths_scale_5{$gene}{$pos}}){
            #devide count by the total cds count for this gene;
            unless (exists ($black_list{$gene})){  #blacklisted then remove
                my $scaled_count=eval { $upstream_nonSD_lengths_scale_5{$gene}{$pos}{$length}/$combined_CDS{$gene}} || 0 ;
                $scaled_upstream_nonSD_5{$pos}{$length}+=$scaled_count;
            }
        }
    }
}

for my $motif (sort keys %downstream_lengths_scale_3){
    for my $pos (sort {$a <=> $b} keys %{$downstream_lengths_scale_3{$motif}}){
        for my $length (sort {$a <=> $b} keys %{$downstream_lengths_scale_3{$motif}{$pos}}){
            #devide count by the total cds count for this gene;
            my $gene=$motif2gene{$motif};
            unless (exists ($black_list{$gene})){  #blacklisted then remove
                unless (exists ($overlapping_motif{$motif})){
                    my $scaled_count=eval { $downstream_lengths_scale_3{$motif}{$pos}{$length}/$combined_CDS{$gene}} || 0 ;
                    $scaled_downstream_3{$pos}{$length}+=$scaled_count;
                }
            }
        }
    }
}

for my $motif (sort keys %downstream_lengths_scale_5){
    for my $pos (sort {$a <=> $b} keys %{$downstream_lengths_scale_5{$motif}}){
        for my $length (sort {$a <=> $b} keys %{$downstream_lengths_scale_5{$motif}{$pos}}){
            #devide count by the total cds count for this gene;
            my $gene=$motif2gene{$motif};
            unless (exists ($black_list{$gene})){  #blacklisted then remove
                unless (exists ($overlapping_motif{$motif})){
                    my $scaled_count=eval { $downstream_lengths_scale_5{$motif}{$pos}{$length}/$combined_CDS{$gene}} || 0 ;
                    $scaled_downstream_5{$pos}{$length}+=$scaled_count;
                }
            }
        }
    }
}

my $ucount=0;
for my $feature (keys %upstreamM_5){
    $ucount++;
}

my $dcount=0;
for my $feature (keys %downstreamM_5){
    $dcount++;
}

print "there were $ucount upstream examples\n";
print "there were $dcount downstream examples\n";



my $oCount=0;
for (keys %overlapping_motif){
    $oCount++;
}
print "here were $oCount overlapping motifs\n";

###################################################################################################
my $outStart=$outDir."/".$prefix."_upstream_motif_3prime_scale.csv";
my $outStartNonSD=$outDir."/".$prefix."_upstream_nonSD_3prime_scale.csv";
my $outStop=$outDir."/".$prefix."_downstream_motif_3prime_scale.csv";
my $outStartLengthsScale=$outDir."/".$prefix."_upstream_codon_lengths_scale_3prime.csv";
my $outStartNonSDLengthsScale=$outDir."/".$prefix."_upstream_nonSD_codon_lengths_scale_3prime.csv";
my $outStopLengthsScale=$outDir."/".$prefix."_downstream_codon_lengths_scale_3prime.csv";

open (OUT1, ">$outStart") || die "can't open $outStart\n";
open (OUT2, ">$outStop")  || die "can't open $outStop\n";
open (OUT3, ">$outStartLengthsScale") || die "can't open $outStartLengthsScale\n";
open (OUT4, ">$outStopLengthsScale")  || die "can't open $outStopLengthsScale\n";
open (OUT9, ">$outStartNonSD") || die "can't open $outStartNonSD\n";
open (OUT10,">$outStartNonSDLengthsScale") || die "can't open $outStartNonSDLengthsScale\n";

#####
## meta plots
#####
for my $pos (sort {$a <=> $b} keys %barchart_upstream_3){
    print OUT1 "$pos,$barchart_upstream_3{$pos}\n";
}

for my $pos (sort {$a <=> $b} keys %barchart_downstream_3){
    print OUT2 "$pos,$barchart_downstream_3{$pos}\n";
}

for my $pos (sort {$a <=> $b} keys %barchart_upstream_nonSD_3){
    print OUT9 "$pos,$barchart_upstream_nonSD_3{$pos}\n";
}

#####
## output scaled length values
#####
for my $pos (sort {$a <=> $b} keys %scaled_upstream_3){ for my $length (sort {$a <=> $b} keys %{$scaled_upstream_3{$pos}}){ print OUT3 "$pos\t$length\t$scaled_upstream_3{$pos}{$length}\n"; } }

for my $pos (sort {$a <=> $b} keys %scaled_downstream_3){ for my $length (sort {$a <=> $b} keys %{$scaled_downstream_3{$pos}}){  print OUT4 "$pos\t$length\t$scaled_downstream_3{$pos}{$length}\n"; } }

for my $pos (sort {$a <=> $b} keys %scaled_upstream_nonSD_3){ for my $length (sort {$a <=> $b} keys %{$scaled_upstream_nonSD_3{$pos}}){ print OUT10 "$pos\t$length\t$scaled_upstream_nonSD_3{$pos}{$length}\n"; } }

###################################################################################################
$outStart=$outDir."/".$prefix."_upstream_motif_5prime_scale.csv";
$outStartNonSD=$outDir."/".$prefix."_upstream_nonSD_5prime_scale.csv";
$outStop=$outDir."/".$prefix."_downstream_motif_5prime_scale.csv";
$outStartLengthsScale=$outDir."/".$prefix."_upstream_codon_lengths_scale_5prime.csv";
$outStartNonSDLengthsScale=$outDir."/".$prefix."_upstream_nonSD_codon_lengths_scale_5prime.csv";
$outStopLengthsScale=$outDir."/".$prefix."_downstream_codon_lengths_scale_5prime.csv";

open (OUT5, ">$outStart") || die "can't open $outStart\n";
open (OUT6, ">$outStop")  || die "can't open $outStop\n";
open (OUT7, ">$outStartLengthsScale") || die "can't open $outStartLengthsScale\n";
open (OUT8, ">$outStopLengthsScale")  || die "can't open $outStopLengthsScale\n";
open (OUT11,">$outStartNonSD") || die "can't open $outStartNonSD\n";
open (OUT12,">$outStartNonSDLengthsScale") || die "can't open $outStartNonSDLengthsScale\n";

#####
## meta plots
#####
for my $pos (sort {$a <=> $b} keys %barchart_upstream_5){
    print OUT5 "$pos,$barchart_upstream_5{$pos}\n";
}

for my $pos (sort {$a <=> $b} keys %barchart_downstream_5){
    print OUT6 "$pos,$barchart_downstream_5{$pos}\n";
}

for my $pos (sort {$a <=> $b} keys %barchart_upstream_nonSD_5){
    print OUT11 "$pos,$barchart_upstream_nonSD_5{$pos}\n";
}

#####
## output scaled length values
#####
for my $pos (sort {$a <=> $b} keys %scaled_upstream_5){ for my $length (sort {$a <=> $b} keys %{$scaled_upstream_5{$pos}}){ print OUT7 "$pos\t$length\t$scaled_upstream_5{$pos}{$length}\n"; } }

for my $pos (sort {$a <=> $b} keys %scaled_downstream_5){  for my $length (sort {$a <=> $b} keys %{$scaled_downstream_5{$pos}}){  print OUT8 "$pos\t$length\t$scaled_downstream_5{$pos}{$length}\n"; } }

for my $pos (sort {$a <=> $b} keys %scaled_upstream_nonSD_5){ for my $length (sort {$a <=> $b} keys %{$scaled_upstream_nonSD_5{$pos}}){ print OUT12 "$pos\t$length\t$scaled_upstream_nonSD_5{$pos}{$length}\n"; } }

exit;
