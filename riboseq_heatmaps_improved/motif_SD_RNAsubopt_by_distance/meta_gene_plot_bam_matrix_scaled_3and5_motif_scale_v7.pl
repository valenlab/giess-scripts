#!/usr/bin/perl -w
use strict;

#to do 10/04/2017
#script to take gtf get coords around start/stop codons, and assign 5' + 3' regions to them from sam
#scale each gene, so that the reads coming from it sum to 1
#where a read overlaps multiple in strand genes counts it towards all those genes

#input files:
#gtf exons, start, stop codons
#sam = aligned 
my $gtf=$ARGV[0];
my $sam=$ARGV[1];
my $fasta=$ARGV[2];
my $outDir=$ARGV[3];
#my $predictions=$ARGV[4];
my $SD_list=$ARGV[4];
my ($prefix)=$sam=~/([^\/]+).sam$/;


#my $MOTIF="CGGAGGTG";          
#my $MOTIF="AGGAGG";
#my $MOTIF="AGGAGGTG";
#my $MOTIF="GTG";
#my $MOTIF="ATG";

##################################################################################################
#open predictions and store the locations of predicited TISs to exlude from intenral positions later
#my %predicted_TIS_fwd;
#my %predicted_TIS_rev;
#open (PRED, $predictions) || die;
#while (<PRED>){
#
#    unless (/^track/){  #skip header
#        my @b=split("\t");
#        my $chr=$b[0];
#        my $start=$b[1]+1;  #bed is 0 based at start
#        my $end=$b[2];
#        my $type=$b[3];
#        my $dir=$b[5];
#
#        if ($dir eq "+"){
#            $predicted_TIS_fwd{$chr}{$start}=1;   
#        }else{
#            $predicted_TIS_rev{$chr}{$end}=1;   #should this be end
#        }
#    }
#}
#close(PRED);

#################################################################################################
#open and store the list of high confidence SD positions
my %SD_fwd;
my %SD_rev; #chr, pos, = 1

open (SD, $SD_list) || die;
while (<SD>){

    chomp();
    my @s=split(",");
    my $chr=$s[0];
    my $pos=$s[1];
    my $dir=$s[4];    
    my $score=$s[2];
    my $type=$s[3];

    if ($dir eq "fwd"){
        $SD_fwd{$chr}{$pos}=1;
    }else{
        $SD_rev{$chr}{$pos}=1;
    }
}
close(SD);

##################################################################################################
#open fasta and get chr limits
my %fasta_sequences; #key = sequence_name, value=sequence
my $name="";
open (FA, $fasta) || die "can't open $fasta";
while (<FA>){
    chomp;
    if (/^>([^\s]+)/){ #take header up to the first space
        $name=$1;
    }else{
        $fasta_sequences{$name}.=$_;
    }
}
close(FA);

print "fasta parsed\n";

###################################################################################################
#open gtf and get inton positions
my %introns; #key=gene_id, #key2=position, #value = 1;
my %previous_exon; #key=gene_id. value=exon_position

#setup a hash of intron positions
open(GENES2,$gtf) || die "can't open $gtf";      #gft is 1 based
while (<GENES2>){
    unless(/^#/){
        my @b=split("\t");
        my $class=$b[2];
        my $start=$b[3];
        my $end=$b[4];
        my $dir=$b[6];
        if ($class eq "exon"){
  
            my ($gene_id) = $b[8] =~ /gene_id\s"([^\"]+)";/;

            if ($gene_id){
 
                #fwd exons are sorted in ascending order 
                #             100958 100975 <-end(+1)
                # satrt(-1)-> 101077 101715 
                if ($dir eq "+"){
                     if (exists $previous_exon{$gene_id}){   #if there is a previous exon, there is an intron between this one and the last one
                         for my $pos ($previous_exon{$gene_id} .. ($start-1)){   #end of previous exon to start of this one = sorted ascending
                             $introns{$gene_id}{$pos}=1;
                         }
                         $previous_exon{$gene_id}=$end+1;
                     }else{
                         $previous_exon{$gene_id}=$end+1;
                     }

                }else{
                #rev exons are sorted in decending order  
                # end(-1)-> 447087 447794
                #           446060 446254 <-start(+1)
                     if (exists $previous_exon{$gene_id}){   #if there is a previous exon, there is an intron between this one and the last one
                         for my $pos (($end+1) .. $previous_exon{$gene_id}){   #end of this exon to start of the pervious one = sorted decending
                             $introns{$gene_id}{$pos}=1;
                         }
                         $previous_exon{$gene_id}=$start-1;
                    }else{
                         $previous_exon{$gene_id}=$start-1;
                    }
                }
            }
        }
    }
}
close (GENES2);

###################################################################################################
#open gtf file setup genomic cds positions
my %upstreamFwd; #key1 = chr, key2 = position, value = gene_id(s)
my %upstreamRev; #key1 = chr, key2 = position, value = gene_id(s)
my %cdsFwd; #key1 = chr, key2 = position, value = gene_ids(s)
my %cdsRev; #key1 = chr, key2 = position, value = gene_ids(s)
my %cdsSum_3; #key = gene_id, value=total CDS signal
my %cdsSum_5;
my %upstreamM_3; #key1 = gene_id, key2=metapos, key3=value
my %upstreamM_5; #key1 = gene_id, key2=metapos, key3=value

my $feature_count=0;
my %motif2gene;
my %motif2location;
my %combined_CDS;
my %motif2distance;

open(GENES,$gtf) || die "can't open $gtf";
while (<GENES>){
    unless(/^#/){
        my @b=split("\t");
        my $class=$b[2];
        my $chr=$b[0];
        my $start=$b[3];
        my $end=$b[4];
        my $dir=$b[6];
        my ($gene_id) = $b[8] =~ /gene_id\s"([^\"]+)";/;

        if ($gene_id){

            if ($class eq "CDS"){

                $combined_CDS{$gene_id}=0;

                #use the start of the cds for start codons
                #This will need updating for eukartyotic genomes (which hopefully have annotated start codons)

                if ($dir eq "+"){

                    $cdsSum_3{$gene_id}=0;
                    $cdsSum_5{$gene_id}=0;

                    for ($start .. $end){
                        if (exists ($cdsFwd{$chr}{$_})){
                             $cdsFwd{$chr}{$_}.=",".$gene_id;
                        }else{
                             $cdsFwd{$chr}{$_}=$gene_id;
                        }
                    }

                    #check for upstream motif
                    for my $match_start (($start-30) .. ($start -1 )){
                        if (exists($SD_fwd{$chr}{$match_start})){

                            #count distance from start codon here:
                            my $TIS_distance=$start-$match_start;
#                            print "FWD,$TIS_distance,$chr,$match_start\n"; 

                            $feature_count++;
                            $motif2gene{$feature_count}=$gene_id;
                            $motif2distance{$feature_count}=$TIS_distance;       #assign a motif to distance hash

                            my $posCount=$match_start-100;
                            my $cdsLength=-100;
                            while ($cdsLength <= 500){
                                if (exists ($upstreamFwd{$chr}{$posCount})){
                                    $upstreamFwd{$chr}{$posCount}.=",".$feature_count.";".$cdsLength;  #change linker from "_" to ";"
                                }else{
                                    $upstreamFwd{$chr}{$posCount}=$feature_count.";".$cdsLength;
                                }
                                $upstreamM_3{$feature_count}{$cdsLength}=0;
                                $upstreamM_5{$feature_count}{$cdsLength}=0;
                                $cdsLength++;
                                $posCount++;
                            }
                        }
                    }
                }else{ #reverse cases

                    $cdsSum_3{$gene_id}=0;  #change to relate to features => #feature to gene hash?
                    $cdsSum_5{$gene_id}=0;

                    for ($start .. $end){
                         if (exists ($cdsRev{$chr})){
                            $cdsRev{$chr}{$_}.=",".$gene_id;
                         }else{
                            $cdsRev{$chr}{$_}=$gene_id;
                         }
                    }

                    #check for upstream motif
                    for my $match_start (($end+1) .. ($end +30)){
                        if (exists($SD_rev{$chr}{$match_start})){

                            #count distance from start codon here
                            my $TIS_distance=$match_start-$end;
#                            print "REV,$TIS_distance,$chr,$match_start\n";

                            $feature_count++;
                            $motif2gene{$feature_count}=$gene_id;
                            $motif2distance{$feature_count}=$TIS_distance;        #assign a motif to distance hash

                            #setup the window around the start position of each match
                            my $revPos=$match_start+100;
                            my $cdsLength=-100;
                            while ($cdsLength <= 500){
                                if (exists ($upstreamRev{$chr}{$revPos})){
                                    $upstreamRev{$chr}{$revPos}.=",".$feature_count.";".$cdsLength;
                                }else{
                                     $upstreamRev{$chr}{$revPos}=$feature_count.";".$cdsLength;
                                }
                                $upstreamM_3{$feature_count}{$cdsLength}=0;
                                $upstreamM_5{$feature_count}{$cdsLength}=0;
                                $cdsLength++;
                                $revPos--;
                            }
                        }
                    }                 
                }
            }
        } 
    }
}
close(GENES);

print "gtf parsed\n";

#my $ucount=0;
#for my $feature (keys %upstreamM_5){
#    $ucount++;
#}

#my $dcount=0;
#for my $feature (keys %downstreamM_5){
#    $dcount++;
#}

#print "there were $ucount upstream examples\n";
#print "there were $dcount downstream examples\n";

###################################################################################################
#open sam file
my %upstream_lengths_scale_3;     #gene, meta position (-50 to 500), length = count
my %upstream_lengths_scale_5;     #gene, meta position (-50 to 500), length = count

open (SAM, $sam) || die "can't open $sam";
while (<SAM>){
    #skip headers
    unless(/^#/){
        my @b=split("\t");
        my $leftMost=$b[3]; #leftmost position of match 5' for fwd, 3' for rev
        my $flag=$b[1];
        my $chr=$b[2];
        my $mapq=$b[4];
        my $cig=$b[5];
        my $seq=$b[9];
        my $threePrime;
        my $fivePrime;
        #mapping uniqness filter
        if ($mapq >= 10){
            unless ($flag & 0x4){   #if aligned
                if ($flag & 0x10){  #if rev calculate 3' == sam coordinate (leftmost)

                   $threePrime=$leftMost;

                   #parse cigar for indels and adjust the length of the alignment             
                   my $length=length($seq);
                   while ($cig =~/(\d+)I/g){   #add to length for insertions
                      $length+=$1;
                   }
                   while ($cig =~/(\d+)D/g){   #substact from length for deletions
                       $length-=$1;
                   }
                   $fivePrime=$leftMost+($length-1);              #SAM is 1 based

                   #assign to metaplots
                   if (exists ($upstreamRev{$chr}{$threePrime})){
                       my @over1=split(",",$upstreamRev{$chr}{$threePrime});
                       for (@over1){

                           my ($motif,$meta_pos)=$_=~/(^[^;]+);(-?\w+)$/;  #/(\w+)_(-?\w+)/;
                           $upstreamM_3{$motif}{$meta_pos}+=1;            
                           $upstream_lengths_scale_3{$motif}{$meta_pos}{length($seq)}+=1;   
                       }    
                   }

                   if (exists ($cdsRev{$chr}{$threePrime})){
                       my @over3=split(",",$cdsRev{$chr}{$threePrime});
                       for (@over3){
                           $cdsSum_3{$_}++;
                       }    
                   }

                   #same again for the 5 prime
                   if (exists ($upstreamRev{$chr}{$fivePrime})){
                       my @over1=split(",",$upstreamRev{$chr}{$fivePrime});
                       for (@over1){
                           my ($motif,$meta_pos)=$_=~/(^[^;]+);(-?\w+)$/;  #/(\w+)_(-?\w+)/;
                           $upstreamM_5{$motif}{$meta_pos}+=1;
                           $upstream_lengths_scale_5{$motif}{$meta_pos}{length($seq)}+=1;
                       }
                   }

                   if (exists ($cdsRev{$chr}{$fivePrime})){
                       my @over3=split(",",$cdsRev{$chr}{$fivePrime});
                       for (@over3){
                           $cdsSum_5{$_}++;
                       }
                   }

                }else{ #if fwd 3' == sam coordinate (leftmost) + read length 

                    #parse cigar for indels and adjust the length of the alignment             
                    my $length=length($seq);
                    while ($cig =~/(\d+)I/g){   #add to length for insertions
                        $length+=$1;
                    }
                    while ($cig =~/(\d+)D/g){   #substact from length for deletions
                        $length-=$1;
                    }

                    $threePrime=$leftMost+($length-1);              #SAM is 1 based
                    $fivePrime=$leftMost;

                    #assign to metaplots
                    if (exists ($upstreamFwd{$chr}{$threePrime})){
                        my @over4=split(",",$upstreamFwd{$chr}{$threePrime});
                        for (@over4){
                            my ($motif,$meta_pos)=$_=~/(^[^;]+);(-?\w+$)/;
                            $upstreamM_3{$motif}{$meta_pos}+=1;    
                            $upstream_lengths_scale_3{$motif}{$meta_pos}{length($seq)}+=1;
                        }
                    }
 
                    if (exists ($cdsFwd{$chr}{$threePrime})){
                        my @over6=split(",",$cdsFwd{$chr}{$threePrime}); 
                        for (@over6){
                            $cdsSum_3{$_}++;
                        }
                    }
 
                    #do that again for five prime
                    if (exists ($upstreamFwd{$chr}{$fivePrime})){
                        my @over4=split(",",$upstreamFwd{$chr}{$fivePrime});
                        for (@over4){
                            my ($motif,$meta_pos)=$_=~/(^[^;]+);(-?\w+$)/;
                            $upstreamM_5{$motif}{$meta_pos}+=1;
                            $upstream_lengths_scale_5{$motif}{$meta_pos}{length($seq)}+=1;
                        }
                    }
 
                    if (exists ($cdsFwd{$chr}{$fivePrime})){
                        my @over6=split(",",$cdsFwd{$chr}{$fivePrime});
                        for (@over6){
                            $cdsSum_5{$_}++;
                        }
                    }
                }
            }
        }
    }                    
}
close (SAM);

print "sam parsed\n";

#for my $SD (keys %downstreamM_5){
#    for my $position (sort { $a <=> $b } keys %{$downstreamM_5{$SD} } ){
#        print "$motif2gene{$SD},$motif2location{$SD},$SD,$position,$downstreamM_5{$SD}{$position}\n";
#    }
#}


###################################################################################################
# rank the genes by cds expression, to exclude the bottom 10%
#need a list of fwd_genes, and rev_gene

for my $gene (keys %cdsSum_5 ){
    $combined_CDS{$gene}=$cdsSum_5{$gene};
}

for my $gene (keys %cdsSum_3 ){
    $combined_CDS{$gene}=$cdsSum_3{$gene};
}

my %black_list;

my $count=0;
my $count_exclude=0;
my $number_of_genes= keys %cdsSum_5;
my $ten_percent=$number_of_genes*0.2;
for my $gene ( sort { $combined_CDS{$a} <=> $combined_CDS{$b} } keys(%combined_CDS) ){
    if ($count <= $ten_percent){
        $black_list{$gene}=1;
        $count_exclude++;
    }
    $count++;
}

#print "there were $number_of_genes genes\n";
#print "there were $count_exclude genes filtered out\n";

my %barchart_upstream_5;
my %barchart_upstream_3;

#further subset by distance to TIS
for my $motif_id (keys %upstreamM_5){
    for my $pos (sort { $a <=> $b } keys %{ $upstreamM_5{$motif_id}} ){
        my $gene_sum=$combined_CDS { $motif2gene{$motif_id} };
        my $TISdist=$motif2distance{$motif_id};
        unless (exists ($black_list{ $motif2gene{$motif_id} })){  #blacklisted then remove

            $barchart_upstream_5{$pos}{$TISdist}+=eval{ $upstreamM_5{$motif_id}{$pos}/$gene_sum } || 0;
            #needs to be further subdevided by TIS_distance
 
        }
    }
}


#further subset by distance to TIS
for my $motif_id (keys %upstreamM_3){
    for my $pos (sort { $a <=> $b } keys %{ $upstreamM_3{$motif_id}} ){
        my $gene_sum=$combined_CDS { $motif2gene{$motif_id} };
        my $TISdist=$motif2distance{$motif_id};
        unless (exists ($black_list{ $motif2gene{$motif_id} })){  #blacklisted then remove     
 
           $barchart_upstream_3{$pos}{$TISdist}+=eval{ $upstreamM_3{$motif_id}{$pos}/$gene_sum } || 0;
           #needs to be further subdevided by TIS_distance

        }
    }
}

#scale lengths
my %scaled_upstream_3;
my %scaled_upstream_5;

for my $motif (sort keys %upstream_lengths_scale_3){        
    for my $pos (sort {$a <=> $b} keys %{$upstream_lengths_scale_3{$motif}}){
        for my $length (sort {$a <=> $b} keys %{$upstream_lengths_scale_3{$motif}{$pos}}){
            #devide count by the total cds count for this gene;
            my $gene=$motif2gene{$motif};
            my $TISdist=$motif2distance{$motif};
 #          print "3prime,$TISdist,$upstream_lengths_scale_3{$motif}{$pos}{$length},$combined_CDS{$gene}\n";

            unless (exists ($black_list{$gene})){  #blacklisted then remove
 
               my $scaled_count=eval { $upstream_lengths_scale_3{$motif}{$pos}{$length}/$combined_CDS{$gene}} || 0 ;

                $scaled_upstream_3{$pos}{$length}{$TISdist}+=$scaled_count; 
                #needs to be further subdevided by TIS_distance
 
            }
        }
    }
}

for my $motif (sort keys %upstream_lengths_scale_5){
    for my $pos (sort {$a <=> $b} keys %{$upstream_lengths_scale_5{$motif}}){
        for my $length (sort {$a <=> $b} keys %{$upstream_lengths_scale_5{$motif}{$pos}}){
            #devide count by the total cds count for this gene;
            my $gene=$motif2gene{$motif}; 
            my $TISdist=$motif2distance{$motif};

 #           print "5prime,$TISdist,$upstream_lengths_scale_5{$motif}{$pos}{$length},$combined_CDS{$gene}\n";
                                             #this is always zero

            unless (exists ($black_list{$gene})){  #blacklisted then remove
 
                my $scaled_count=eval { $upstream_lengths_scale_5{$motif}{$pos}{$length}/$combined_CDS{$gene}} || 0 ;

                $scaled_upstream_5{$pos}{$length}{$TISdist}+=$scaled_count;
                #needs to be further subdevided by TIS_distance

            }
        }
    }
}


#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
my $outStart=$outDir."/".$prefix."_upstream_motif_3prime_scale.csv";
my $outStartLengthsScale=$outDir."/".$prefix."_upstream_codon_lengths_scale_3prime.csv";
open (OUT1,">$outStart") || die "can't open $outStart\n";
open (OUT3,">$outStartLengthsScale") || die "can't open $outStartLengthsScale\n";

#####
## meta plots up
#####
for my $pos (sort {$a <=> $b} keys %barchart_upstream_3){
    for my $dist (sort {$a <=> $b} keys %{ $barchart_upstream_3{$pos} } ){
        print OUT1 "$pos,$dist,$barchart_upstream_3{$pos}{$dist}\n";
    }
}

#####
## output scaled length values
#####
for my $pos (sort {$a <=> $b} keys %scaled_upstream_3){ 
    for my $length (sort {$a <=> $b} keys %{$scaled_upstream_3{$pos}}){ 
        for my $dist (sort {$a <=> $b} keys %{ $scaled_upstream_3{$pos}{$length} } ){
            print OUT3 "$pos\t$length\t$dist\t$scaled_upstream_3{$pos}{$length}{$dist}\n"; 
        }
    } 
}


###################################################################################################
$outStart=$outDir."/".$prefix."_upstream_motif_5prime_scale.csv";
$outStartLengthsScale=$outDir."/".$prefix."_upstream_codon_lengths_scale_5prime.csv";

open (OUT5,">$outStart") || die "can't open $outStart\n";
open (OUT7,">$outStartLengthsScale") || die "can't open $outStartLengthsScale\n";

#####
## meta plots up
#####
for my $pos (sort {$a <=> $b} keys %barchart_upstream_5){
    for my $dist (sort {$a <=> $b} keys %{ $barchart_upstream_5{$pos} } ){
        print OUT5 "$pos,$dist,$barchart_upstream_5{$pos}{$dist}\n";
                               #this is always zero
    }
}

#####
## output scaled length values
#####
for my $pos (sort {$a <=> $b} keys %scaled_upstream_5){ 
    for my $length (sort {$a <=> $b} keys %{$scaled_upstream_5{$pos}}){ 
        for my $dist (sort {$a <=> $b} keys %{ $scaled_upstream_5{$pos}{$length} } ){
            print OUT7 "$pos\t$length\t$dist\t$scaled_upstream_5{$pos}{$length}{$dist}\n";
                                              #this is always zero 
        } 
    }
}


exit;
