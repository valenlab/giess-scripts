#!/usr/bin/env Rscript
args = commandArgs(trailingOnly=TRUE)

library(ggplot2)
library(dplyr)
library(gridExtra)

##
# 1 unshifted heatmaps
##

start_m5<-read.table(args[1])
start_m3<-read.table(args[2])

mono.start5.raw<-read.csv(args[3],header=FALSE ,check.names=FALSE,sep=",")
mono.start3.raw<-read.csv(args[4],header=FALSE ,check.names=FALSE,sep=",")

start_m5_50 <- subset(start_m5, V1<=100 & V1 >= -100);
start_m5_50 <- start_m5_50 %>% group_by(V2) %>% mutate(LenSum=sum(V3), LenSD=sd(V3), LenMean=mean(V3)) %>% mutate(LenZ=(V3-LenMean)/LenSD)
start_m5_50 <- start_m5_50 %>% group_by(V1) %>% mutate(PosSum=sum(V3), PosSD=sd(V3), PosMean=mean(V3)) %>% mutate(PosZ=(V3-PosMean)/PosSD)

start_m3_50 <- subset(start_m3, V1<=100 & V1 >= -100);
start_m3_50 <- start_m3_50 %>% group_by(V2) %>% mutate(LenSum=sum(V3), LenSD=sd(V3), LenMean=mean(V3)) %>% mutate(LenZ=(V3-LenMean)/LenSD)
start_m3_50 <- start_m3_50 %>% group_by(V1) %>% mutate(PosSum=sum(V3), PosSD=sd(V3), PosMean=mean(V3)) %>% mutate(PosZ=(V3-PosMean)/PosSD)

#Just take the mean value from both Z-scores
start_m5_50$meanZ <- (start_m5_50$PosZ + start_m5_50$LenZ ) / 2 
start_m3_50$meanZ <- (start_m3_50$PosZ + start_m3_50$LenZ ) / 2 

mean5<-ggplot(subset(start_m5_50, V1 <= 100 & V1 >= -100) , aes(x=V1, y=V2, fill=meanZ)) + geom_tile()  +
               scale_fill_gradientn(colours=c("orange", "yellow", "lightblue","blue", "navy"), oob = identity, name="Z-score") +
               xlab("Position relative to start codon") + ylab("Protected fragment length") +
               scale_x_continuous(limits = c(-100,100), expand = c(0, 0), breaks=c(-100,-80,-60,-40,-20,0,20,40,60,80,100)) +
               theme_bw() + theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank()) +
               theme(text = element_text(size = 12))

mean3<-ggplot(subset(start_m3_50, V1 <= 100 & V1 >= -100) , aes(x=V1, y=V2, fill=meanZ)) + geom_tile()  +
               scale_fill_gradientn(colours=c("orange", "yellow", "lightblue", "blue", "navy"), oob = identity, name="Z-score") +
               xlab("Position relative to start codon") + ylab("Protected fragment length") +
               scale_x_continuous(limits = c(-100,100), expand = c(0, 0), breaks=c(-100,-80,-60,-40,-20,0,20,40,60,80,100)) +
               theme_bw() + theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank()) +
               theme(text = element_text(size = 12))

##
# 2 unshifted barplots
##

#scale the data by the sum of the window
subset_mono5.raw<-filter(mono.start5.raw, V1 >= -100, V1 <= 100)
subset_mono5.raw <- mutate(subset_mono5.raw, proportion = V2/sum(V2))

subset_mono3.raw<-filter(mono.start3.raw, V1 >= -100, V1 <= 100)
subset_mono3.raw <- mutate(subset_mono3.raw, proportion = V2/sum(V2))

max_y <- max(subset_mono3.raw$proportion,subset_mono5.raw$proportion)

mb5 <- ggplot(data=subset_mono5.raw, aes(x=V1, y=proportion, fill=as.factor((V1 %% 3)+1))) +
             geom_bar(stat="identity",width=1, colour="black", size=0.3) +
             theme_bw() +
             xlab("Position relative to start codon") +
             ylab("Proportion of reads") +
             theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank()) +
             scale_x_continuous(limits = c(-100,100), expand = c(0, 0), breaks=c(-100,-80,-60,-40,-20,0,20,40,60,80,100)) +
             scale_y_continuous(limits = c(0, max_y), expand= c(0, 0)) +
             scale_fill_manual(values=c("#db811a", "#9a9a99", "#6d6e63"), name = "Frame") +
             theme(text = element_text(size = 12))

mb3 <- ggplot(data=subset_mono3.raw, aes(x=V1, y=proportion, fill=as.factor((V1 %% 3)+1))) +
             geom_bar(stat="identity",width=1, colour="black", size=0.3) +
             theme_bw() +
             xlab("Position relative to start codon") +
             ylab("Proportion of reads") +
             theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank()) +
             scale_x_continuous(limits = c(-100,100), expand = c(0, 0), breaks=c(-100,-80,-60,-40,-20,0,20,40,60,80,100)) +
             scale_y_continuous(limits = c(0, max_y), expand= c(0, 0)) +
             scale_fill_manual(values=c("#db811a", "#9a9a99", "#6d6e63"), name = "Frame") +
             theme(text = element_text(size = 12))

#grid.arrange(mb5,mb3)

###
# Pretify plots
###

# Function to save legend
get_legend<-function(myggplot){
  tmp <- ggplot_gtable(ggplot_build(myggplot))
  leg <- which(sapply(tmp$grobs, function(x) x$name) == "guide-box")
  legend <- tmp$grobs[[leg]]
  return(legend)
}

# Save the legend
#+++++++++++++++++++++++
legend_heatmapM5<- get_legend(mean5)
legend_heatmapM3<- get_legend(mean3)
legend_barchart5 <- get_legend(mb5)
legend_barchart3 <- get_legend(mb3)

mean5 <- mean5 + theme(legend.position="none")
mean3 <- mean3 + theme(legend.position="none")
mb5 <- mb5 + theme(legend.position="none")
mb3 <- mb3 + theme(legend.position="none")

##
# Output
##

lay <- rbind(c(1,1,1,1,1,1,1,1,2,3,3,3,3,3,3,3,3,4),
             c(5,5,5,5,5,5,5,5,6,7,7,7,7,7,7,7,7,8))

out1<-arrangeGrob(mb5, legend_barchart5, mb3, legend_barchart3, mean5, legend_heatmapM5, mean3, legend_heatmapM3, layout_matrix = lay)
ggsave(file=,args[5], out1, width=500, height=300, unit="mm", dpi=1200) 
