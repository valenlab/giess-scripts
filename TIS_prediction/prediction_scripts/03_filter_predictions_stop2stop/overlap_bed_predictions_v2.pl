#!/usr/bin/perl -w
use strict;

#to do 10/05/2016
#script to take 2 beds files, and output regions common to both files

my $bed1=$ARGV[0]; 
my $bed2=$ARGV[1]; 

my %regions_fwd; 
my %regions_rev; 

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
open (BED1, $bed1) || die "can't open $bed1";     
while (<BED1>){

    unless (/^track/){  #skip header
        my @b=split("\t");
        my $chr=$b[0];
        my $start=$b[1]; 
        my $end=$b[2];
        my $type=$b[3];
        my $dir=$b[5];

        if ($dir eq "+"){
            $regions_fwd{$chr}{$start}{$end}=$type;
        }else{
            $regions_rev{$chr}{$start}{$end}=$type;
        }
    }
}
close (BED1);

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
my %match;
open (BED2, $bed2) || die "can't open $bed2";      #bed is O based at start, 1 based at end
while (<BED2>){
    unless (/^track/){  #skip header
        my @b=split("\t");
        my $chr=$b[0];
        my $start=$b[1];       
        my $end=$b[2];
        my $type=$b[3];
        my $dir=$b[5];
        
        if ($dir eq "+"){
            if ( exists ( $regions_fwd{$chr}{$start}{$end} ) ) {
                print $_;
#               $match{$regions_fwd{$chr}{$start}{$end}}++;
            }           
        }else{
            if ( exists ( $regions_rev{$chr}{$start}{$end} ) ) {
                 print $_;
#                $match{$regions_rev{$chr}{$start}{$end}}++;
            }
        }
    }
}
close(BED2);

for (keys %match){
#    print "$_,$match{$_}\n";
}
exit;
