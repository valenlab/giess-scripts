#!/usr/bin/perl -w
use strict;

#to do 18/08/2016
#script to take a bed file of orf predictions and to report n-termini suppoprt per prediction catagory

my $gtf=$ARGV[0];
my $fasta=$ARGV[1]; 
my $nterm=$ARGV[2]; #bed of N-termini predictions
my $bed=$ARGV[3];   #of predictions

#for all matching, elongated or predicted
#   count the number that have n-terminal support as matching, elongated or predicted

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#open gtf file setup genomic hashes, to store start and stop coords per gene, per direction

my %gene_regions_fwd_start; #old way of finding starts and ends
my %gene_regions_fwd_stop;
my %gene_regions_rev_start;
my %gene_regions_rev_stop;

#get gene coords per direction
my %start_codon; #key=gene_id, value=pos
my %gene_regions_fwd; # chr, start, stop = gene 
my %gene_regions_rev; # chr, start, stop = gene 

#store gene ids
my %gene_chr;  #gene = chr
my %gene_fwd_start; #gene = start
my %gene_fwd_stop ; #gene = end 
my %gene_rev_start; #gene = start 
my %gene_rev_stop ; #gene = end

#for subroutine
my %gene_info_fwd; #key=gene_id, key2=start, value=stop
my %gene_info_rev; #key=gene_id, key2=start, value=stop

open(GENES,$gtf) || die "can't open $gtf";        #gtf is 1 based
while (<GENES>){
    unless(/^#/){
        my @b=split("\t");
        my $class=$b[2];
        my $chr=$b[0];
        my $prim5=$b[3];
        my $prim3=$b[4];
        my $dir=$b[6];
        my $gene_id="unknown";
        ($gene_id) = $b[8] =~ /gene_id\s"([^\"]+)";/;

        #update to use cds rather than start / stop
        if ($class eq "CDS"){

            if ($dir eq "+"){ #use start positions as begining of feature
                $gene_regions_fwd_start{$gene_id}=$prim5;
                $gene_regions_fwd_stop{$chr}{$prim3}=$gene_id; #ecoli cds does include the stop codon
                $gene_regions_fwd{$chr}{$prim5}{$prim3}=$gene_id;
                $gene_fwd_start{$gene_id}=$prim5;
                $gene_fwd_stop{$gene_id}=$prim3;
                $gene_chr{$gene_id}=$chr;
                $gene_info_fwd{$gene_id}{$prim5}=$prim3;
            }else{
                $gene_regions_rev_start{$gene_id}=$prim3;
                $gene_regions_rev_stop{$chr}{$prim5}=$gene_id; #ecoli cds does include the stop codon
                $gene_regions_rev{$chr}{$prim5}{$prim3}=$gene_id;
                $gene_rev_start{$gene_id}=$prim5;
                $gene_rev_stop{$gene_id}=$prim3;
                $gene_chr{$gene_id}=$chr;
                $gene_info_rev{$gene_id}{$prim3}=$prim5-1; #this was previously coded as $prim5-1
            }
        }
    }
}
close(GENES);

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#open n-termini bed
my %n_term_fwd; #key=chr, key=start_position, value=count
my %n_term_rev;
my $countN=0;

open (PEP, $nterm) || die "can't open $nterm";      #bed is O based at start, 1 based at end
while (<PEP>){

    unless (/^track/){  #skip header
        my @b=split("\t");
        my $chr=$b[0];
        my $start=$b[1]+1;     #start is zero bases
        my $stop=$b[2];
        my $count=$b[4];
        my $dir=$b[5];

        #assign to metaplots 
        if ($dir eq "+"){                                      #fwd cases
            #five_prime=$start;
            $n_term_fwd{$chr}{$start}=$stop;
            $countN++;
        }else{                                                 #reverse cases
            #five_prime=$stop;
            $n_term_rev{$chr}{$stop}=$start;
            $countN++;
        }
    }
}
close (PEP);

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#open fasta 
my %fasta_sequences; #key = sequence_name, value=sequence
my $name;
open (FA, $fasta) || die "can't open $fasta";
while (<FA>){
    chomp;
    if (/^>([^\s]+)/){ #take header up to the first space
        $name=$1;
    }else{
        $fasta_sequences{$name}.=$_;
    }
}
close(FA);

#pass hash reference to subroutine
my ($inframe_fwd_ref, $inframe_rev_ref)=&stopToStopFromGTF( \%fasta_sequences, \%gene_info_fwd, \%gene_info_rev, \%gene_chr);

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#assign N-termini here
#get a hash of genes and how they are supported

#to dereferece (makes a new copy og the hash)
my %inframe_fwd=%{$inframe_fwd_ref};  #gene,chr,pos=1
my %inframe_rev=%{$inframe_rev_ref};  #gene,chr,pos=1

my %nterm_assignment_fwd;  #gene = type
my %nterm_assignment_rev;  #gene = type

my $n_sum_ano=0;
my $n_sum_tru=0;
my $n_sum_elo=0;
my $n_sum_new=0;

for my $c (sort keys %n_term_fwd ){
    for my $five(sort keys %{$n_term_fwd{$c}}){

        my $three=$n_term_fwd{$c}{$five};

        #check if they overlap a gene region and are in frame
        if (exists ($inframe_fwd{$c}{$five})){
            for my $gene (keys %{$inframe_fwd{$c}{$five}}){
                if ($five == $gene_fwd_start{$gene}){              #annotated
                    $nterm_assignment_fwd{$c}{$three}{$five}="ann";
                    $n_sum_ano++;
                }elsif($five > $gene_fwd_start{$gene} ){           #trunaction
                    $nterm_assignment_fwd{$c}{$three}{$five}="tru";
                    $n_sum_tru++;
                }elsif($five < $gene_fwd_start{$gene} ){           #elongation (999bp)
                    $nterm_assignment_fwd{$c}{$three}{$five}="elo";
                    $n_sum_elo++;
                }
            }
        }else{
            $n_sum_new++;
        }
    }
}

for my $c (sort keys %n_term_rev ){
    for my $five (sort keys %{$n_term_rev{$c}}){
  
        my $three=$n_term_rev{$c}{$five};

        #check if they overlap a gene region and are in frame
        if (exists ($inframe_rev{$c}{$five})){
            for my $gene (keys %{$inframe_rev{$c}{$five}}){
                if ($five == $gene_rev_stop{$gene}){           #annotated
                    $nterm_assignment_rev{$c}{$three}{$five}="ann";
                    $n_sum_ano++;
                }elsif($five < $gene_rev_stop{$gene} ){        #truncation
                    $nterm_assignment_rev{$c}{$three}{$five}="tru";
                    $n_sum_tru++;
                }elsif($five > $gene_rev_stop{$gene} ){        #elongation (999bp)                        
                    $nterm_assignment_rev{$c}{$three}{$five}="elo";
                    $n_sum_elo++;
                }
            }
        }else{
            $n_sum_new++;
        }
    }
}

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#open bed of predictions

#predictions sums
my $pred_sum_ano=0;
my $pred_sum_tru=0;
my $pred_sum_elo=0;

#martix counts
my $ano_ano=0;
my $ano_tru=0;
my $ano_elo=0;
my $tru_ano=0;
my $tru_tru=0;
my $tru_elo=0;
my $elo_ano=0;
my $elo_tru=0;
my $elo_elo=0;

open (BED, $bed) || die;
while (<BED>){

    unless (/^track/){  #skip header
        my @b=split("\t");
        my $chr=$b[0];
        my $start=$b[1]+1;     #start is zero based
        my $stop=$b[2];
        my $count=$b[4];
        my $dir=$b[5];
        my $type=$b[3]; #Annotated Truncation Extension

        if ($type eq "Annotated"){ $pred_sum_ano++; }
        if ($type eq "Extension"){ $pred_sum_elo++; }
        if ($type eq "Truncation"){ $pred_sum_tru++; }

        if ($dir eq "+"){

            #check if the region shares a stop codon with a gene
            if (exists ($nterm_assignment_fwd{$chr}{$stop})){    #might need a -3 (at stop)

                for my $nstart (keys %{ $nterm_assignment_fwd{$chr}{$stop} } ){    #might need -3

                    if ($type eq "Annotated"){
                        if ($nterm_assignment_fwd{$chr}{$stop}{$nstart} eq "ann"){     #might need -3
                            if ($start == $nstart){ #check if the start matches 
                                $ano_ano++;
                            }else{
                                print "non exact match ann fwd\n";
                            }
                        }elsif ($nterm_assignment_fwd{$chr}{$stop}{$nstart} eq "tru"){ $ano_tru++;          #might need -3
                        }elsif ($nterm_assignment_fwd{$chr}{$stop}{$nstart} eq "elo"){ $ano_elo++;          #might need -3
                        }

                    #check for extensions
                    }elsif ( $type eq "Extension"){
                        if     ($nterm_assignment_fwd{$chr}{$stop}{$nstart} eq "ann"){ $elo_ano++;
                        }elsif ($nterm_assignment_fwd{$chr}{$stop}{$nstart} eq "tru"){ $elo_tru++;
                        }elsif ($nterm_assignment_fwd{$chr}{$stop}{$nstart} eq "elo"){
                            if ($start == $nstart){
                                $elo_elo++;
                            }else{
                                print "non exact match elo fwd\n";
                            }
                        }

                    #otherwise truncation
                    }elsif ( $type eq "Truncation"){
                        if     ($nterm_assignment_fwd{$chr}{$stop}{$nstart} eq "ann"){ $tru_ano++;
                        }elsif ($nterm_assignment_fwd{$chr}{$stop}{$nstart} eq "tru"){ 
                            if ($start == $nstart){
                                $tru_tru++;
                            }else{
                                print "non exact match tru fwd\n";
                            }
                        }elsif ($nterm_assignment_fwd{$chr}{$stop}{$nstart} eq "elo"){ $tru_elo++;
                        }
                    }
                }
            }

        #rev
        }else{

            #for rev, start is 3' (stop)
            #stop is 5' (start)

            #if the region shares a stop codon with a supported region
            if (exists ($nterm_assignment_rev{$chr}{$start} ) ){  #we might need start +3

                for my $nstop (keys %{ $nterm_assignment_rev{$chr}{$start} } ){

                    if ($type eq "Annotated"){
                        if ($nterm_assignment_rev{$chr}{$start}{$nstop} eq "ann"){
                            if ($stop == $nstop){
                                $ano_ano++;
                            }else{
                                 print "non exact match ann rev\n";
                            }
                        }elsif ($nterm_assignment_rev{$chr}{$start}{$nstop} eq "tru"){ $ano_tru++;
                        }elsif ($nterm_assignment_rev{$chr}{$start}{$nstop} eq "elo"){ $ano_elo++;
                        }

                    }elsif ($type eq "Extension"){
                        if ($nterm_assignment_rev{$chr}{$start}{$nstop} eq "elo"){
                            if ($stop == $nstop){
                                $elo_elo++;
                            }else{
                                 print "non exact match elo rev\n";
                            }
                        }elsif ($nterm_assignment_rev{$chr}{$start}{$nstop} eq "tru"){ $elo_tru++;
                        }elsif ($nterm_assignment_rev{$chr}{$start}{$nstop} eq "ano"){ $elo_ano++;
                        }

                    }elsif ($type eq "Truncation"){
                        if ($nterm_assignment_rev{$chr}{$start}{$nstop} eq "tru"){
                            if ($stop == $nstop){
                                $tru_tru++;
                            }else{
                                 print "non exact match tru rev\n";
                            }
                        }elsif ($nterm_assignment_rev{$chr}{$start}{$nstop} eq "ann"){ $tru_ano++;
                        }elsif ($nterm_assignment_rev{$chr}{$start}{$nstop} eq "elo"){ $tru_elo++;
                        }
                    }
                }
            }   
        }
    }
}
close(BED);

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
print ",,nterminal_support,,\n";
print "predictions,,ann,tru,elo\n";
print ",sum,$n_sum_ano,$n_sum_tru,$n_sum_elo\n";
print "ann,$pred_sum_ano,$ano_ano,$ano_tru,$ano_elo\n";
print "tru,$pred_sum_tru,$tru_ano,$tru_tru,$tru_elo\n";
print "ext,$pred_sum_elo,$elo_ano,$elo_tru,$elo_elo\n";

exit;

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
sub stopToStopFromGTF {

    #regerences to gene and fasta hashes
    my $fasta_ref=$_[0];
    my $gene_info_fwd_ref=$_[1];
    my $gene_info_rev_ref=$_[2];
    my $gene_2_chr_ref=$_[3];

    #to dereferece (makes a new copy og the hash)
    my %gene_info_fwd=%{$gene_info_fwd_ref};
    my %gene_info_rev=%{$gene_info_rev_ref};
    my %gene_2_chr=%{$gene_2_chr_ref};

    my %in_frame_fwd;     #chr,pos,gene,1
    my %in_frame_rev;     #key=chr, pos, gene, value=1;

    for my $gene ( keys %gene_info_fwd ) {
        for my $annotated_start (keys %{$gene_info_fwd{$gene}}){

            #get first nucleotide of start codon then procees upstream 3nt at a time until we find a stop codon (pattern match)     
            my $search=1;
            my $count=0;  #set upper limit to 1000 (100)
            my $pos=$annotated_start-1;
            while ($search){
                $pos=$pos-3;

                #check that the substing is not smaller than the chr!
                if ($pos<0){ last; }

                my $seq=substr($fasta_sequences{$gene_2_chr{$gene}},$pos,3);
                #check for stop codon
                if ($seq=~/TAG/ || $seq=~/TAA/ || $seq=~/TGA/ ){
                    $search=0;
                }

                if ($count>=999){ $search=0; }
                $count++;
            }

            #also loop for cds and in frame positions
            my $frameCount=0;
            for ($pos+4 .. $gene_info_fwd{$gene}{$annotated_start}){
                $frameCount++;
                if ($frameCount%3 == 1){
                    $in_frame_fwd{$gene_2_chr{$gene}}{$_}{$gene}=1;
                }
            }
        }
    }

    for my $gene (keys %gene_info_rev){
        for my $annotated_start (keys %{$gene_info_rev{$gene}}){

            #get first nucleotide of start codon then procees upstream 3nt at a time until we find a stop codon (pattern match)     
            my $search=1;
            my $count=0;  #set upper limit to 1000     
            my $pos=$annotated_start-1;
            while ($search){
                $pos=$pos+3;

                #check that the substing is not bigger or smaller than the chr!
                if ($pos+1>length($fasta_sequences{$gene_2_chr{$gene}})){ last; }

                my $seq=reverse(substr($fasta_sequences{$gene_2_chr{$gene}},$pos-2,3));
                $seq=~tr/ACGTacgt/TGCAtgca/;
                #check for start codon
                if ($seq=~/TAG/ || $seq=~/TAA/ || $seq=~/TGA/ ){
                    $search=0;
                }

               if ($count>=999){ $search=0; }
               $count++;
            }

            #also loop for cds and in frame positions
            my $frameCount=0;
            #start to end
            for ($gene_info_rev{$gene}{$annotated_start} .. $pos-2) {
                $frameCount++;
                if ($frameCount%3 == 1){
                    $in_frame_rev{$gene_2_chr{$gene}}{$_}{$gene}=1;
                }
            }
        }
    }
    return (\%in_frame_fwd, \%in_frame_rev);
}

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
