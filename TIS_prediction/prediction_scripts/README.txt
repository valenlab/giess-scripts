#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
# 1 Make heatmaps
#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#

##1 heatmaps/barplots from aligned bam files
01_heatmaps/counts_from_bam_scaled/run_heatmaps.sh -b <IN_BAM_FOLDER> -g <GTF> -o <OUT_FOLDER> 

##2 line plots of shifted wig files 
01_heatmaps/counts_from_wig_scaled/run_lenghts.sh -w <IN_WIG_FOLDER> -g <GTF> -o <OUT_FOLDER>

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
# 2 Run models
#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#

##1 Generate matrices S. typhimurium
02_prediction/big_matrix_5prime_wholeGenome_20to20_fpkm_codon_rank_all_positions_v14.pl <GTF> <SAM> <FASTA> <VERIFIED_STARTS_BED> <MATRIX_CSV>

##1b: Generate matrices E. coli
02_prediction/big_matrix_5prime_wholeGenome_20to20_fpkm_codon_rank_all_positions_ecoli.v17.pl <GTF> <SAM> <FASTA> <VERIFIED_STARTS_BED> <MATRIX_CSV>

##2: Generate negative/positive sets
02_prediction/positive_set_from_non_nterminal_peptides_v10.pl <MATRIX_CSV> <VERIFIED_STARTS_BED> <POSITIVE_SET> <NEGATIVE_SET> 

##3: Subset matrices, to canonical and non canonical start codons in stop2stop region
awk -F, '$6=="TRUE"' <MATRIX_CSV> | awk -F, '$7=="TRUE"' > All.CANONICAL.STOP2STOP.CSV

##3b: Subset matrices, to all canonical and non canonical start codons (for full genome predictions)  
awk -F, '$6=="TRUE"' <MATRIX_CSV> > ALL.CANONICAL.CSV

##4: Run predictions in R
02_prediction/r_scripts/
    mono.5prime.combined.stopToStop.wholeGenome.R
    poly.5prime.combined.stopToStop.wholeGenome.R        
    TET2.5prime.stopToStop.ORF.regions.R
    TET3.5prime.stopToStop.ORF.regions.R
    li_rich1.5prime.stopToStop.R
    li_rich3.5prime.stopToStop.R
    mohammed_wt1.5prime.stopToStop.R
    mohammed_wt2.5prime.stopToStop.R

    #example of automated parameter selection for number of trees
	#mono.5prime.automated.parameter.selection.R

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
# 3 Filter predictions stop2stop
#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#

##1: Select best predictions S. typhimurium
03_filter_predictions_stop2stop/filter_predictions_Styphi_full_genome_v12.pl <GTF> <FASTA> <OUT_FOLDER> <MATRIX_OF_PREDICTIONS>

##1b: Select best predictions E. coli
03_filter_predictions_stop2stop/filter_predictions_Ecoli_full_genome_v13.pl <GTF> <FASTA> <OUT_FOLDER> <MATRIX_OF_PREDICTIONS>

##2: Overlap Predictions
03_filter_predictions_stop2stop/overlap_bed_predictions_v2.pl <PREDICTIONS_1_BED> <PREDICTIONS_2_BED> > <PREDICTIONS_COMBINED_BED>

##3: Compare predictions to bed of verified starts (S. typhimurium N-termini)
03_filter_predictions_stop2stop/nterm_support_from_bed_confusion_v8_styphi.pl <GTF> <FASTA> <PREDICTIONS_BED>

##3b: Compare predictions to bed of verified starts (E. coli ecogene)
03_filter_predictions_stop2stop/nterm_support_from_bed_confusion_v6_ecogene.pl <GTF> <FASTA> <PREDICTIONS_BED>


#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
# 4 Filter predictions whole genome
#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#

##1 Output all predictions as bed
04_filter_predictions_full_genome/full_predictions_Styphi_full_genome_v13.pl <GTF> <FASTA> <PREDICTIONS_BED> <PREDICTIONS_CSV> 

##2 Calculate ribo-seq coverage for each ORF
04_filter_predictions_full_genome/bed_coverage_from_sam.pl <SAM> <WHOLE_GENOME_PREDICTIONS_BED> > <WHOLE_GENOME_PREDICTIONS_COVERAGE>

##3 Filter predictions on ribo-seq coverage and ORF length
04_filter_predictions_full_genome/filter_wholeGenome_bed_cdss_coverage.pl <WHOLE_GENOME_PREDICTIONS_COVERAGE> <GTF> > <WHOLE_GENOME_PREDICTIONS_COVERAGE.FILTERED_LENGTH300_COVERAGE75>

##4: Overlap Predictions
04_filter_predictions_full_genome/overlap_bed_predictions_v2.pl <WHOLE_GENOME_PREDICTIONS_1_BED> <WHOLE_GENOME_PREDICTIONS_2_BED> > <WHOLE_GENOME_PREDICTIONS_COMBINED_BED>

##5: Compare predictions to bed of verifired starts (salmonella N-termini)
04_filter_predictions_full_genome/nterm_support_from_bed_confusion_v8_styphi.pl <GTF> <FASTA> <PREDICTIONS_BED>


#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
# 5 Plots of predictions
#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#

##1 predictions plots with shifted ribo-seq counts
05_prediction_plots/plots_shifted/run_shifted_plots.sh -p <PREDICTIONS_BED> -g <GTF> -o <OUT_FOLDER> -f <SHIFTED_WIG_FWD> -r <SHIFTED_WIG_REV>

##2 prediction plots with 5' and 3' ribo-seq counts (3 and 5' input files are the *start_codon.csv, from step 1:1)
05_prediction_plots/plots_novel/bash run_predictions_novel3.sh -p <PREDICTIONS_BED> -n <WHOLE_GENOME_PREDICTIONS_BED> -f <FASTA> -g <GTF> -o <OUT_FOLDER> -r <3_PRIME_COUNTS_CSV> -i <5_PRIME_COUNTS_CSV> -s <SAM>

