#!/usr/bin/perl -w
use strict;

#to do 21/04/2017
#script to take gtf get coords around start/stop codons, and assign 5' + 3' regions to them from sam
#scale each gene, so that the reads coming from it sum to 1
#where a read overlaps multiple in strand genes counts it towards all those genes

#input files:
#gtf exons, start, stop codons
#sam = aligned 
my $gtf=$ARGV[0];
my $sam=$ARGV[1];
my $fasta=$ARGV[2];
my $outDir=$ARGV[3];
my ($prefix)=$sam=~/([^\/]+).sam$/;

##################################################################################################
#open fasta and get chr limits
my %fasta_sequences; #key = sequence_name, value=sequence
my $name="";
open (FA, $fasta) || die "can't open $fasta";
while (<FA>){
    chomp;
    if (/^>([^\s]+)/){ #take header up to the first space
        $name=$1;
    }else{
        $fasta_sequences{$name}.=$_;
    }
}
close(FA);

print "fasta parsed\n";

###################################################################################################
#open gtf and get inton positions
my %introns; #key=gene_id, #key2=position, #value = 1;
my %previous_exon; #key=gene_id. value=exon_position

#setup a hash of intron positions
open(GENES2,$gtf) || die "can't open $gtf";      #gft is 1 based
while (<GENES2>){
    unless(/^#/){
        my @b=split("\t");
        my $class=$b[2];
        my $start=$b[3];
        my $end=$b[4];
        my $dir=$b[6];
        if ($class eq "exon"){
  
            my ($gene_id) = $b[8] =~ /gene_id\s"([^\"]+)";/;

            if ($gene_id){
 
                #fwd exons are sorted in ascending order 
                #             100958 100975 <-end(+1)
                # satrt(-1)-> 101077 101715 
                if ($dir eq "+"){
                     if (exists $previous_exon{$gene_id}){   #if there is a previous exon, there is an intron between this one and the last one
                         for my $pos ($previous_exon{$gene_id} .. ($start-1)){   #end of previous exon to start of this one = sorted ascending
                             $introns{$gene_id}{$pos}=1;
                         }
                         $previous_exon{$gene_id}=$end+1;
                     }else{
                         $previous_exon{$gene_id}=$end+1;
                     }

                }else{
                #rev exons are sorted in decending order  
                # end(-1)-> 447087 447794
                #           446060 446254 <-start(+1)
                     if (exists $previous_exon{$gene_id}){   #if there is a previous exon, there is an intron between this one and the last one
                         for my $pos (($end+1) .. $previous_exon{$gene_id}){   #end of this exon to start of the pervious one = sorted decending
                             $introns{$gene_id}{$pos}=1;
                         }
                         $previous_exon{$gene_id}=$start-1;
                    }else{
                         $previous_exon{$gene_id}=$start-1;
                    }
                }
            }
        }
    }
}
close (GENES2);

###################################################################################################
#open gtf file setup genomic cds positions
my %upstreamFwd; #key1 = chr, key2 = position, value = gene_id(s)
my %upstreamRev; #key1 = chr, key2 = position, value = gene_id(s)
my %downstreamFwd; #key1 = chr, key2 = position, value = gene_id(s)
my %downstreamRev; #key1 = chr, key2 = position, value = gene_id(s)
my %cdsFwd; #key1 = chr, key2 = position, value = gene_ids(s)
my %cdsRev; #key1 = chr, key2 = position, value = gene_ids(s)
my %cdsSum_3; #key = gene_id, value=total CDS signal
my %cdsSum_5;
my %upstreamM_3; #key1 = gene_id, key2=metapos, key3=value
my %downstreamM_3; #key1 = gene_id, key2=metapos, key3=value
my %upstreamM_5; #key1 = gene_id, key2=metapos, key3=value
my %downstreamM_5; #key1 = gene_id, key2=metapos, key3=value
my %combined_CDS;
my %genes_fwd;
my %genes_rev;

open(GENES,$gtf) || die "can't open $gtf";
while (<GENES>){
    unless(/^#/){
        my @b=split("\t");
        my $class=$b[2];
        my $chr=$b[0];
        my $start=$b[3];
        my $end=$b[4];
        my $dir=$b[6];
        my ($gene_id) = $b[8] =~ /gene_id\s"([^\"]+)";/;

        if ($gene_id){

            if ($class eq "CDS"){

                $combined_CDS{$gene_id}=0;

                #use the start of the cds for start codons

                if ($dir eq "+"){
                    $genes_fwd{$gene_id}=1;
                    $cdsSum_3{$gene_id}=0;
                    $cdsSum_5{$gene_id}=0;
                    for ($start .. $end){
                        if (exists ($cdsFwd{$chr}{$_})){
                             $cdsFwd{$chr}{$_}.=",".$gene_id;
                        }else{
                             $cdsFwd{$chr}{$_}=$gene_id;
                        }
                    }

                    #start codons
                    my $match_start=$start;
                    my $posCount=$match_start-100;
                    my $cdsLength=-100;

       #             print "$gene_id,Start,$chr,$match_start\n";

                    while ($cdsLength <= 500){
                        if (exists ($upstreamFwd{$chr}{$posCount})){
                            $upstreamFwd{$chr}{$posCount}.=",".$gene_id.";".$cdsLength;  #change linker from "_" to ";"
                        }else{
                            $upstreamFwd{$chr}{$posCount}=$gene_id.";".$cdsLength;
                        }
                        $upstreamM_3{$gene_id}{$cdsLength}=0;
                        $upstreamM_5{$gene_id}{$cdsLength}=0;
                        $cdsLength++;
                        $posCount++;
                    }
                 
                    #stop codon
                    $match_start=$end+1;     #in stphimurium the stop codon is 3nt after the end fo the CDS
    #                $match_start=$end-2;     #in ecoli the stop codon is the last codon of the CDS
                    $posCount=$match_start-100;
                    $cdsLength=-100;

    #                print "$gene_id,Stop,$chr,$match_start\n";

                    while ($cdsLength <= 500){
                        if (exists ($downstreamFwd{$chr}{$posCount})){
                            $downstreamFwd{$chr}{$posCount}.=",".$gene_id.";".$cdsLength;  #change linker from "_" to ";"
                        }else{
                            $downstreamFwd{$chr}{$posCount}=$gene_id.";".$cdsLength;
                        }
                        $downstreamM_3{$gene_id}{$cdsLength}=0;
                        $downstreamM_5{$gene_id}{$cdsLength}=0;
                        $cdsLength++;
                        $posCount++;
                    }

                }else{ #reverse cases
                     $genes_rev{$gene_id}=1;
                     $cdsSum_3{$gene_id}=0;  #change to relate to features => #feature to gene hash?
                     $cdsSum_5{$gene_id}=0;
                     for ($start .. $end){
                         if (exists ($cdsRev{$chr})){
                            $cdsRev{$chr}{$_}.=",".$gene_id;
                         }else{
                            $cdsRev{$chr}{$_}=$gene_id;
                         }
                     }

                    #start codons
                    my $match_start=$end;
                    my $revPos=$match_start+100;
                    my $cdsLength=-100;

     #              print "$gene_id,Start,$chr,$match_start\n";

                    while ($cdsLength <= 500){
                        if (exists ($upstreamRev{$chr}{$revPos})){
                             $upstreamRev{$chr}{$revPos}.=",".$gene_id.";".$cdsLength;
                        }else{
                             $upstreamRev{$chr}{$revPos}=$gene_id.";".$cdsLength;
                        }
                        $upstreamM_3{$gene_id}{$cdsLength}=0;
                        $upstreamM_5{$gene_id}{$cdsLength}=0;
                        $cdsLength++;
                        $revPos--;
                    }
                    
                    #stop codons
                    $match_start=$start-1;          #in stphimurium the stop codon is 3nt after the end fo the CDS
     #               $match_start=$end+2;            #in ecoli the stop codon is the last codon of the CDS

                    $revPos=$match_start+100;
                    $cdsLength=-100;
 
        #            print "$gene_id,Stop,$chr,$match_start\n"; 

                    while ($cdsLength <= 500){
                        if (exists ($downstreamRev{$chr}{$revPos})){
                            $downstreamRev{$chr}{$revPos}.=",".$gene_id.";".$cdsLength;
                        }else{
                            $downstreamRev{$chr}{$revPos}=$gene_id.";".$cdsLength;
                        }
                        $downstreamM_3{$gene_id}{$cdsLength}=0;
                        $downstreamM_5{$gene_id}{$cdsLength}=0;
                        $cdsLength++;
                        $revPos--;
                    }
                }
            }
        } 
    }
}
close(GENES);

###################################################################################################
#open sam file
my %upstream_lengths_scale_3;     #gene, meta position (-50 to 500), length = count
my %upstream_lengths_scale_5;     #gene, meta position (-50 to 500), length = count

my %downstream_lengths_scale_3;     #gene, meta position (-50 to 500), length = count
my %downstream_lengths_scale_5;     #gene, meta position (-50 to 500), length = count

open (SAM, $sam) || die "can't open $sam";
while (<SAM>){
    #skip headers
    unless(/^#/){
        my @b=split("\t");
        my $leftMost=$b[3]; #leftmost position of match 5' for fwd, 3' for rev
        my $flag=$b[1];
        my $chr=$b[2];
        my $mapq=$b[4];
        my $cig=$b[5];
        my $seq=$b[9];
        my $threePrime;
        my $fivePrime;
        #mapping uniqness filter
        if ($mapq >= 10){
            unless ($flag & 0x4){   #if aligned
                if ($flag & 0x10){  #if rev calculate 3' == sam coordinate (leftmost)

                   $threePrime=$leftMost;

                   #parse cigar for indels and adjust the length of the alignment             
                   my $length=length($seq);
                   while ($cig =~/(\d+)I/g){   #add to length for insertions
                      $length+=$1;
                   }
                   while ($cig =~/(\d+)D/g){   #substact from length for deletions
                       $length-=$1;
                   }
                   $fivePrime=$leftMost+($length-1);              #SAM is 1 based

                   #assign to metaplots
                   if (exists ($upstreamRev{$chr}{$threePrime})){
                       my @over1=split(",",$upstreamRev{$chr}{$threePrime});
                       for (@over1){
                           my ($motif,$meta_pos)=$_=~/(^[^;]+);(-?\w+)$/;  #/(\w+)_(-?\w+)/;
                           $upstreamM_3{$motif}{$meta_pos}+=1;            
                           $upstream_lengths_scale_3{$motif}{$meta_pos}{length($seq)}+=1;   
                       }    
                   }

                   if (exists ($downstreamRev{$chr}{$threePrime})){
                       my @over1=split(",",$downstreamRev{$chr}{$threePrime});
                       for (@over1){
                           my ($motif,$meta_pos)=$_=~/(^[^;]+);(-?\w+)$/;  #/(\w+)_(-?\w+)/;
                           $downstreamM_3{$motif}{$meta_pos}+=1;
                           $downstream_lengths_scale_3{$motif}{$meta_pos}{length($seq)}+=1;
                       }
                   }

                   if (exists ($cdsRev{$chr}{$threePrime})){
                       my @over3=split(",",$cdsRev{$chr}{$threePrime});
                       for (@over3){
                           $cdsSum_3{$_}++;
                       }    
                   }

                   #same again for the 5 prime
                   if (exists ($upstreamRev{$chr}{$fivePrime})){
                       my @over1=split(",",$upstreamRev{$chr}{$fivePrime});
                       for (@over1){
                           my ($motif,$meta_pos)=$_=~/(^[^;]+);(-?\w+)$/;  #/(\w+)_(-?\w+)/;
                           $upstreamM_5{$motif}{$meta_pos}+=1;
                           $upstream_lengths_scale_5{$motif}{$meta_pos}{length($seq)}+=1;
                       }
                   }

                   if (exists ($downstreamRev{$chr}{$fivePrime})){
                       my @over1=split(",",$downstreamRev{$chr}{$fivePrime});
                       for (@over1){
                           my ($motif,$meta_pos)=$_=~/(^[^;]+);(-?\w+)$/;  #/(\w+)_(-?\w+)/;
                           $downstreamM_5{$motif}{$meta_pos}+=1;
                           $downstream_lengths_scale_5{$motif}{$meta_pos}{length($seq)}+=1;
                       }
                   }

                   if (exists ($cdsRev{$chr}{$fivePrime})){
                       my @over3=split(",",$cdsRev{$chr}{$fivePrime});
                       for (@over3){
                           $cdsSum_5{$_}++;
                       }
                   }

                }else{ #if fwd 3' == sam coordinate (leftmost) + read length 

                    #parse cigar for indels and adjust the length of the alignment             
                    my $length=length($seq);
                    while ($cig =~/(\d+)I/g){   #add to length for insertions
                        $length+=$1;
                    }
                    while ($cig =~/(\d+)D/g){   #substact from length for deletions
                        $length-=$1;
                    }

                    $threePrime=$leftMost+($length-1);              #SAM is 1 based
                    $fivePrime=$leftMost;

                    #assign to metaplots
                    if (exists ($upstreamFwd{$chr}{$threePrime})){
                        my @over4=split(",",$upstreamFwd{$chr}{$threePrime});
                        for (@over4){
                            my ($motif,$meta_pos)=$_=~/(^[^;]+);(-?\w+$)/;
                            $upstreamM_3{$motif}{$meta_pos}+=1;    
                            $upstream_lengths_scale_3{$motif}{$meta_pos}{length($seq)}+=1;
                        }
                    }

                    if (exists ($downstreamFwd{$chr}{$threePrime})){
                        my @over4=split(",",$downstreamFwd{$chr}{$threePrime});
                        for (@over4){
                            my ($motif,$meta_pos)=$_=~/(^[^;]+);(-?\w+$)/;
                            $downstreamM_3{$motif}{$meta_pos}+=1;
                            $downstream_lengths_scale_3{$motif}{$meta_pos}{length($seq)}+=1;
                        }
                    }
 
                    if (exists ($cdsFwd{$chr}{$threePrime})){
                        my @over6=split(",",$cdsFwd{$chr}{$threePrime}); 
                        for (@over6){
                            $cdsSum_3{$_}++;
                        }
                    }
 
                    #do that again for five prime
                    if (exists ($upstreamFwd{$chr}{$fivePrime})){
                        my @over4=split(",",$upstreamFwd{$chr}{$fivePrime});
                        for (@over4){
                            my ($motif,$meta_pos)=$_=~/(^[^;]+);(-?\w+$)/;
                            $upstreamM_5{$motif}{$meta_pos}+=1;
                            $upstream_lengths_scale_5{$motif}{$meta_pos}{length($seq)}+=1;
                        }
                    }

                    if (exists ($downstreamFwd{$chr}{$fivePrime})){
                        my @over4=split(",",$downstreamFwd{$chr}{$fivePrime});
                        for (@over4){
                            my ($motif,$meta_pos)=$_=~/(^[^;]+);(-?\w+$)/;
                            $downstreamM_5{$motif}{$meta_pos}+=1;
                            $downstream_lengths_scale_5{$motif}{$meta_pos}{length($seq)}+=1;
                        }
                    }
 
                    if (exists ($cdsFwd{$chr}{$fivePrime})){
                        my @over6=split(",",$cdsFwd{$chr}{$fivePrime});
                        for (@over6){
                            $cdsSum_5{$_}++;
                        }
                    }
                }
            }
        }
    }                    
}
close (SAM);

print "sam parsed\n";

###################################################################################################
# rank the genes by cds expression, to exclude the bottom 10%
#need a list of fwd_genes, and rev_gene

for my $gene (keys %genes_fwd){
    $combined_CDS{$gene}=$cdsSum_5{$gene};
}

for my $gene (keys %genes_rev){
    $combined_CDS{$gene}=$cdsSum_3{$gene};
}

my %black_list;

my $count=0;
my $count_exclude=0;
my $number_of_genes= keys %cdsSum_5;
my $ten_percent=$number_of_genes*0.1;
#my $ten_percent=$number_of_genes*0.2;
for my $gene ( sort { $combined_CDS{$a} <=> $combined_CDS{$b} } keys(%combined_CDS) ){
    if ($count <= $ten_percent){
        $black_list{$gene}=1;
        $count_exclude++;
    }
    $count++;
#    print "$gene,$combined_CDS{$gene}\n";
}

#print "there were $number_of_genes genes\n";
#print "there were $count_exclude genes filtered out\n";

#exit;

####

my %barchart_upstream_5;
my %barchart_upstream_3;
my %barchart_downstream_5;
my %barchart_downstream_3;

for my $gene_id (keys %upstreamM_5){
    for my $pos (sort { $a <=> $b } keys %{ $upstreamM_5{$gene_id}} ){
        my $gene_sum=$combined_CDS{$gene_id};
        unless (exists ($black_list{$gene_id})){  #blacklisted then remove
            $barchart_upstream_5{$pos}+=eval{ $upstreamM_5{$gene_id}{$pos}/$gene_sum } || 0;
        }
    }
}

for my $gene_id (keys %upstreamM_3){
    for my $pos (sort { $a <=> $b } keys %{ $upstreamM_3{$gene_id}} ){
        my $gene_sum=$combined_CDS{$gene_id};
        unless (exists ($black_list{$gene_id})){  #blacklisted then remove
            $barchart_upstream_3{$pos}+=eval{ $upstreamM_3{$gene_id}{$pos}/$gene_sum } || 0;
        }
    }
}

for my $gene_id (keys %downstreamM_5){
    for my $pos (sort { $a <=> $b } keys %{ $downstreamM_5{$gene_id}} ){
        my $gene_sum=$combined_CDS{$gene_id};
        unless (exists ($black_list{$gene_id})){  #blacklisted then remove
            $barchart_downstream_5{$pos}+=eval{ $downstreamM_5{$gene_id}{$pos}/$gene_sum } || 0;
        }
    }
}

for my $gene_id (keys %downstreamM_3){
    for my $pos (sort { $a <=> $b } keys %{ $downstreamM_3{$gene_id}} ){
        my $gene_sum=$combined_CDS{$gene_id};
        unless (exists ($black_list{$gene_id})){  #blacklisted then remove     
            $barchart_downstream_3{$pos}+=eval{ $downstreamM_3{$gene_id}{$pos}/$gene_sum } || 0;
        }
    }
}

#scale lengths
my %scaled_upstream_3;
my %scaled_upstream_5;
my %scaled_downstream_3;
my %scaled_downstream_5;

for my $gene (sort keys %upstream_lengths_scale_3){        
    for my $pos (sort {$a <=> $b} keys %{$upstream_lengths_scale_3{$gene}}){
        for my $length (sort {$a <=> $b} keys %{$upstream_lengths_scale_3{$gene}{$pos}}){
            #devide count by the total cds count for this gene
            unless (exists ($black_list{$gene})){  #blacklisted then remove
                my $scaled_count=eval { $upstream_lengths_scale_3{$gene}{$pos}{$length}/$combined_CDS{$gene}} || 0 ;
                $scaled_upstream_3{$pos}{$length}+=$scaled_count; 
            }
        }
    }
}

for my $gene (sort keys %upstream_lengths_scale_5){
    for my $pos (sort {$a <=> $b} keys %{$upstream_lengths_scale_5{$gene}}){
        for my $length (sort {$a <=> $b} keys %{$upstream_lengths_scale_5{$gene}{$pos}}){
            #devide count by the total cds count for this gene;
            unless (exists ($black_list{$gene})){  #blacklisted then remove
                my $scaled_count=eval { $upstream_lengths_scale_5{$gene}{$pos}{$length}/$combined_CDS{$gene}} || 0 ;
                $scaled_upstream_5{$pos}{$length}+=$scaled_count;
            }
        }
    }
}

for my $gene (sort keys %downstream_lengths_scale_3){
    for my $pos (sort {$a <=> $b} keys %{$downstream_lengths_scale_3{$gene}}){
        for my $length (sort {$a <=> $b} keys %{$downstream_lengths_scale_3{$gene}{$pos}}){
            #devide count by the total cds count for this gene;
            unless (exists ($black_list{$gene})){  #blacklisted then remove
                my $scaled_count=eval { $downstream_lengths_scale_3{$gene}{$pos}{$length}/$combined_CDS{$gene}} || 0 ;
                $scaled_downstream_3{$pos}{$length}+=$scaled_count;
            }
        }
    }
}

for my $gene (sort keys %downstream_lengths_scale_5){
    for my $pos (sort {$a <=> $b} keys %{$downstream_lengths_scale_5{$gene}}){
        for my $length (sort {$a <=> $b} keys %{$downstream_lengths_scale_5{$gene}{$pos}}){
            #devide count by the total cds count for this gene;
            unless (exists ($black_list{$gene})){  #blacklisted then remove
                my $scaled_count=eval { $downstream_lengths_scale_5{$gene}{$pos}{$length}/$combined_CDS{$gene}} || 0 ;
                $scaled_downstream_5{$pos}{$length}+=$scaled_count;
            }
        }
    }
}

###################################################################################################
my $outStart=$outDir."/".$prefix."_upstream_motif_3prime_scale.csv";
my $outStop=$outDir."/".$prefix."_downstream_motif_3prime_scale.csv";
my $outStartLengthsScale=$outDir."/".$prefix."_upstream_codon_lengths_scale_3prime.csv";
my $outStopLengthsScale=$outDir."/".$prefix."_downstream_codon_lengths_scale_3prime.csv";

open (OUT1,">$outStart") || die "can't open $outStart\n";
open (OUT2,">$outStop")  || die "can't open $outStop\n";
open (OUT3,">$outStartLengthsScale") || die "can't open $outStartLengthsScale\n";
open (OUT4,">$outStopLengthsScale")  || die "can't open $outStopLengthsScale\n";

#####
## meta plots up
#####
for my $pos (sort {$a <=> $b} keys %barchart_upstream_3){
    print OUT1 "$pos,$barchart_upstream_3{$pos}\n";
}

#####
## meta_plots down
#####
for my $pos (sort {$a <=> $b} keys %barchart_downstream_3){
    print OUT2 "$pos,$barchart_downstream_3{$pos}\n";
}

#####
## output scaled length values
#####
for my $pos (sort {$a <=> $b} keys %scaled_upstream_3){ for my $length (sort {$a <=> $b} keys %{$scaled_upstream_3{$pos}}){ print OUT3 "$pos\t$length\t$scaled_upstream_3{$pos}{$length}\n"; } }
    #this is empty

for my $pos (sort {$a <=> $b} keys %scaled_downstream_3){ for my $length (sort {$a <=> $b} keys %{$scaled_downstream_3{$pos}}){  print OUT4 "$pos\t$length\t$scaled_downstream_3{$pos}{$length}\n"; } }


###################################################################################################
$outStart=$outDir."/".$prefix."_upstream_motif_5prime_scale.csv";
$outStop=$outDir."/".$prefix."_downstream_motif_5prime_scale.csv";
$outStartLengthsScale=$outDir."/".$prefix."_upstream_codon_lengths_scale_5prime.csv";
$outStopLengthsScale=$outDir."/".$prefix."_downstream_codon_lengths_scale_5prime.csv";

open (OUT5,">$outStart") || die "can't open $outStart\n";
open (OUT6,">$outStop")  || die "can't open $outStop\n";
open (OUT7,">$outStartLengthsScale") || die "can't open $outStartLengthsScale\n";
open (OUT8,">$outStopLengthsScale")  || die "can't open $outStopLengthsScale\n";

#####
## meta plots up
#####
for my $pos (sort {$a <=> $b} keys %barchart_upstream_5){
    print OUT5 "$pos,$barchart_upstream_5{$pos}\n";
}

#####
## meta_plots down
#####
for my $pos (sort {$a <=> $b} keys %barchart_downstream_5){
    print OUT6 "$pos,$barchart_downstream_5{$pos}\n";
}

#####
## output scaled length values
#####
for my $pos (sort {$a <=> $b} keys %scaled_upstream_5){ for my $length (sort {$a <=> $b} keys %{$scaled_upstream_5{$pos}}){ print OUT7 "$pos\t$length\t$scaled_upstream_5{$pos}{$length}\n"; } }

for my $pos (sort {$a <=> $b} keys %scaled_downstream_5){  for my $length (sort {$a <=> $b} keys %{$scaled_downstream_5{$pos}}){  print OUT8 "$pos\t$length\t$scaled_downstream_5{$pos}{$length}\n"; } }

exit;
