#!/bin/bash

#AG 21/01/16
#script to produce a plot of 5' riboseq length distributions from a bam file
#1 Bam to sam
#2 Get 5' reads
#3 count scaled lenghts relative to start and stop codons

#dependanices (must be in path)
#samtools

usage(){
cat << EOF
usage: $0 options

script to trim and align riboseq fastq datasets 

OPTIONS:
    -b    path to the input bam files
    -o    path to output dir
    -g    gtf file (matched to the bam gemone)
    -h    this help message

example usage: run_lenghts.sh -b <in.bam> -g in.gtf -o <out_dir>

EOF
}

while getopts ":b:o:g:h" opt; do
    case $opt in 
        b)
            in_bam=$OPTARG
              echo "-b input bam file $OPTARG"
              ;;
        o)
            out_dir=$OPTARG
            echo "-o output folder $OPTARG"
            ;;
        g)
            in_gtf=$OPTARG
            echo "-g gtf file $OPTARG"
            ;;
        h)
            usage
            exit
            ;;
        ?) 
            echo "Invalid option: -$OPTARG"
            usage
            exit 1
              ;;
    esac
done

if [ ! -e $in_bam ]  || [ ! $out_dir ] || [ ! $in_gtf ]; then
        echo "something's missing - check your command lines args"
        usage
        exit 1
fi

if [ ! -d $out_dir ]; then
    mkdir $out_dir
fi

if [ ! -d ${out_dir}/tmp ]; then
    mkdir ${out_dir}/tmp
fi

for file in $in_bam/*.bam; do

    name=$(basename $file)
    prefix=${name%.bam}
    echo starting $prefix

    #------------------------------------------------------------------------------------------
    #1 Bam to sam
    #------------------------------------------------------------------------------------------

    samtools view $file > ${out_dir}/tmp/${prefix}.sam
    
    #------------------------------------------------------------------------------------------
    #2 Count 5' ends of reads
    #------------------------------------------------------------------------------------------

    nice -n 10 perl meta_gene_plot_bam_matrix_scaled_5prime.pl $in_gtf ${out_dir}/tmp/${prefix}.sam $out_dir

    nice -n 10 perl meta_gene_plot_bam_matrix_scaled_3prime.pl $in_gtf ${out_dir}/tmp/${prefix}.sam $out_dir

    #------------------------------------------------------------------------------------------ 
    #3 Make metagene plots
    #------------------------------------------------------------------------------------------

    nice -n 10 perl meta_plot_matrix_scale.pl ${out_dir}/${prefix}_start_codon_5prime.csv > ${out_dir}/${prefix}_start_meta_scaled_5prime.csv
    nice -n 10 perl meta_plot_matrix_scale.pl ${out_dir}/${prefix}_stop_codon_5prime.csv > ${out_dir}/${prefix}_stop_meta_scaled_5prime.csv

    nice -n 10 perl meta_plot_matrix_scale.pl ${out_dir}/${prefix}_start_codon_3prime.csv > ${out_dir}/${prefix}_start_meta_scaled_3prime.csv
    nice -n 10 perl meta_plot_matrix_scale.pl ${out_dir}/${prefix}_stop_codon_3prime.csv > ${out_dir}/${prefix}_stop_meta_scaled_3prime.csv

    #------------------------------------------------------------------------------------------ 
    #4 Plot heatmaps
    #------------------------------------------------------------------------------------------

    Rscript plot_heatmaps.R ${out_dir}/${prefix}_start_codon_lengths_scale_5prime.csv  ${out_dir}/${prefix}_start_meta_scaled_5prime.csv ${out_dir}/${prefix}_unshifted_plots.pdf

    #------------------------------------------------------------------------------------------ 
    #4 Tidy up
    #------------------------------------------------------------------------------------------

    rm ${out_dir}/tmp/${prefix}.sam

done
