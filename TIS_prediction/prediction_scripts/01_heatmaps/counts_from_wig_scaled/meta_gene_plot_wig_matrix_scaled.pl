#!/usr/bin/perl -w
use strict;

#to do 12/02/2016
#script to take gtf get coords around start/stop codons, and assign counts to them from bam
#scale each gene, so that the reads coming from it sum to 1 (disparding the drop bottom 10% of genes on CDS expression)
#where a read overlaps multiple in strand genes counts it towards all those genes

#input files:
#gtf exons, start, stop codons
my $gtf=$ARGV[0];
my $wig_fwd=$ARGV[1];
my $wig_rev=$ARGV[2];
my $outDir=$ARGV[3];
my ($prefix)=$wig_fwd=~/([^\/]+)-forward.wig$/;

###################################################################################################
#open gtf and get inton positions
my %introns; #key=gene_id, #key2=position, #value = 1;
my %previous_exon; #key=gene_id. value=exon_position

#setup a hash of intron positions
open(GENES2,$gtf) || die "can't open $gtf";      #gft is 1 based
while (<GENES2>){
    unless(/^#/){
        my @b=split("\t");
        my $class=$b[2];
        my $start=$b[3];
        my $end=$b[4];
        my $dir=$b[6];
        if ($class eq "exon"){
            my ($gene_id) = $b[8] =~ /gene_id\s"(\w+)"/;

            #fwd exons are sorted in ascending order 
            #             100958 100975 <-end(+1)
            # satrt(-1)-> 101077 101715 
            if ($dir eq "+"){
                if (exists $previous_exon{$gene_id}){   #if there is a previous exon, there is an intron between this one and the last one
                     for my $pos ($previous_exon{$gene_id} .. ($start-1)){   #end of previous exon to start of this one = sorted ascending
                         $introns{$gene_id}{$pos}=1;
                     }
                     $previous_exon{$gene_id}=$end+1;
                }else{
                     $previous_exon{$gene_id}=$end+1;
                }

            }else{
            #rev exons are sorted in decending order  
            # end(-1)-> 447087 447794
            #           446060 446254 <-start(+1)
                if (exists $previous_exon{$gene_id}){   #if there is a previous exon, there is an intron between this one and the last one
                     for my $pos (($end+1) .. $previous_exon{$gene_id}){   #end of this exon to start of the pervious one = sorted decending
                         $introns{$gene_id}{$pos}=1;
                     }
                     $previous_exon{$gene_id}=$start-1;
                }else{
                     $previous_exon{$gene_id}=$start-1;
                }
            }
        }
    }
}
close (GENES2);

###################################################################################################
#open gtf file setup genomic cds positions
my %startPosFwd; #key1 = chr, key2 = position, value = gene_id(s)
my %startPosRev; #key1 = chr, key2 = position, value = gene_id(s)
my %stopPosFwd; #key1 = chr, key2 = position, value = gene_id(s)
my %stopPosRev; #key1 = chr, key2 = position, value = gene_id(s)
my %cdsFwd; #key1 = chr, key2 = position, value = gene_ids(s)
my %cdsRev; #key1 = chr, key2 = position, value = gene_ids(s)
my %startM; #key1 = gene_id, key2=metapos, key3=value
my %stopM; #key1 = gene_id, key2=metapos, key3=value
my %cdsSum; #key = gene_id, value=total CDS signal

open(GENES,$gtf) || die "can't open $gtf";        #gtf is 1 based
while (<GENES>){
    unless(/^#/){
        my @b=split("\t");
        my $class=$b[2];
        my $chr=$b[0];
        my $start=$b[3];
        my $end=$b[4];
        my $dir=$b[6];

        my ($gene_id) = $b[8] =~ /gene_id\s"([^\"]+)";/;

        if ($class eq "CDS"){
            if ($gene_id){ #ecoli gtf does not have start and stop codos
                if ($dir eq "+"){

                    my $cdsLength=-51;    
                    for (($start-50) .. ($start+500)){ #50bp upstream, 100bp downstream
                        unless (exists $introns{$gene_id}{$_}){ #check if this position is intronic
                            $cdsLength+=1;    
                            if (exists ($startPosFwd{$chr}{$_})){
                                $startPosFwd{$chr}{$_}.=",".$gene_id.";".$cdsLength;  #change linker from "_" to ";"
                            }else{
                                $startPosFwd{$chr}{$_}=$gene_id.";".$cdsLength;
                            }
                            $startM{$gene_id}{$cdsLength}=0;
                        }
                    }    

                    $cdsLength=-501;
                    for (($end-500) .. ($end+50)){ #stop codons 50bp upstream, 100bp downstream
                        unless (exists $introns{$gene_id}{$_}){ #check if this position is intronic
                            $cdsLength+=1;
                            if (exists ($stopPosFwd{$chr}{$_})){
                                $stopPosFwd{$chr}{$_}.=",".$gene_id.";".$cdsLength;
                            }else{
                                $stopPosFwd{$chr}{$_}=$gene_id.";".$cdsLength;
                            }
                            $stopM{$gene_id}{$cdsLength}=0;
                        }
                    }

                    $cdsSum{$gene_id}=0;
                    for ($start .. $end){
                        if (exists ($cdsFwd{$chr}{$_})){
                             $cdsFwd{$chr}{$_}.=",".$gene_id;
                        }else{
                             $cdsFwd{$chr}{$_}=$gene_id;
                        }
                    }


                }else{ #reverse cases

                    my $upstream=$end+50;
                    my $downstream=$end-500;
                    my $cdsLength=-51;
                    while ($upstream >= $downstream){ #50bp upstream, 100bp downstream
                        unless (exists $introns{$gene_id}{$_}){ #check if this position is intronic
                            $cdsLength+=1;
                            if (exists ($startPosRev{$chr}{$_})){
                                 $startPosRev{$chr}{$_}.=",".$gene_id.";".$cdsLength;
                            }else{
                                 $startPosRev{$chr}{$_}=$gene_id.";".$cdsLength;
                            }
                            $startM{$gene_id}{$cdsLength}=0;
                            $upstream--;
                        }
                    }

                    $upstream=$start+500;
                    $downstream=$start-50;
                    $cdsLength=-501;
                    while ($upstream >= $downstream){ #50bp upstream, 100bp downstream
                        unless (exists $introns{$gene_id}{$_}){ #check if this position is intronic
                            $cdsLength+=1;
                            if (exists ($stopPosRev{$chr}{$_})){
                                $stopPosRev{$chr}{$_}.=",".$gene_id.";".$cdsLength;
                            }else{
                                $stopPosRev{$chr}{$_}=$gene_id.";".$cdsLength;
                            }
                            $stopM{$gene_id}{$cdsLength}=0;
                            $upstream--;
                        }
                    }

                    $cdsSum{$gene_id}=0;
                    for ($start .. $end){
                        if (exists ($cdsRev{$chr})){
                            $cdsRev{$chr}{$_}.=",".$gene_id;
                        }else{
                            $cdsRev{$chr}{$_}=$gene_id;
                        }
                    }
                }
            }else{
                print "wierd gene $b[8]\n$_";
            }
        }
    }
}
close(GENES);

print "gtf parsed\n";

###################################################################################################
#open forward wig file
my $chr;

open (FWD, $wig_fwd) || die "can't open $wig_fwd";      #wig is 1 based
while (<FWD>){

    chomp;
    if (/^variableStep\schrom=(.*)$/){
        $chr=$1
    }else{
        my @w=split("\t");
        my $pos=$w[0];
        my $count=$w[1];
                    
        #assign to metaplots 

        if (exists ($startPosFwd{$chr}{$pos})){
            my @over1=split(",",$startPosFwd{$chr}{$pos});
            for (@over1){
                my ($gene,$meta_pos)=$_=~/(^[^;]+);(-?\w+)$/;  #/(\w+)_(-?\w+)/;
                $startM{$gene}{$meta_pos}+=$count;            
            }    
        }
 
        if (exists ($stopPosFwd{$chr}{$pos})){
            my @over2=split(",",$stopPosFwd{$chr}{$pos});
            for (@over2){
                my ($gene,$meta_pos)=$_=~/(^[^;]+);(-?\w+)$/;
                $stopM{$gene}{$meta_pos}+=$count;    
            }
        }
            
        if (exists ($cdsFwd{$chr}{$pos})){
            my @over3=split(",",$cdsFwd{$chr}{$pos});
            for (@over3){
                $cdsSum{$_}+=$count;
            }    
        }
    }                    
}
close (FWD);

print "Forward wig parsed\n";

###################################################################################################
#open rev wig file

open (REV, $wig_rev) || die "can't open $wig_rev";      #wig is 1 based
while (<REV>){

    chomp;
    if (/^variableStep\schrom=(.*)$/){
        $chr=$1
    }else{
        my @w=split("\t");
        my $pos=$w[0];
        my $count=$w[1];

        #assign to metaplots 

        if (exists ($startPosRev{$chr}{$pos})){
            my @over1=split(",",$startPosRev{$chr}{$pos});
            for (@over1){
                my ($gene,$meta_pos)=$_=~/(^[^;]+);(-?\w+)$/;  #/(\w+)_(-?\w+)/;
                $startM{$gene}{$meta_pos}+=$count;      #+=1;   #+= count
            }
        }

        if (exists ($stopPosRev{$chr}{$pos})){
            my @over2=split(",",$stopPosRev{$chr}{$pos});
            for (@over2){
                my ($gene,$meta_pos)=$_=~/(^[^;]+);(-?\w+)$/;
                $stopM{$gene}{$meta_pos}+=$count;   #+=1;
            }
        }

        if (exists ($cdsRev{$chr}{$pos})){
            my @over3=split(",",$cdsRev{$chr}{$pos});
            for (@over3){
                $cdsSum{$_}+=$count;
            }
        }
    }
}
close (REV);

print "Reverse wig parsed\n";

###################################################################################################
#drop the lowest 10% of genes
my %black_list;
my $count=0;
my $number_of_genes=keys %cdsSum;
my $ten_percent=$number_of_genes*0.1;

#sort the gene list by total cds TE, lowest to highest
for my $gene ( sort { $cdsSum{$a} <=> $cdsSum{$b} } keys(%cdsSum) ){
    if ($count <= $ten_percent){ $black_list{$gene}=1; }
    $count++;
}

###################################################################################################
my $outStart=$outDir."/".$prefix."_start_codon.csv";
my $outStop=$outDir."/".$prefix."_stop_codon.csv";

open (OUT1,">$outStart") || die "can't open $outStart\n";
open (OUT2,">$outStop")  || die "can't open $outStop\n";

#meta plots output per gene
print OUT1 "gene_id"; for (-50 .. 500){     print OUT1 ",$_"; } print OUT1 ",sum\n"; #header

for my $gene (sort keys %startM){
    print OUT1 "$gene";
    for my $pos (sort {$a <=> $b} keys %{$startM{$gene}}){
        print OUT1 ",$startM{$gene}{$pos}";
    }
    print OUT1 ",$cdsSum{$gene}\n";
}

print OUT2 "gene_id"; for (-500 .. 50){     print OUT2 ",$_"; } print OUT2 "sum\n"; #header
for my $gene (sort keys %stopM){
    print OUT2 "$gene";
    for my $pos (sort {$a <=> $b} keys %{$stopM{$gene}}){
        print OUT2 ",$stopM{$gene}{$pos}";
    }
    print OUT2 ",$cdsSum{$gene}\n";
}

exit;
