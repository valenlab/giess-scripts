#!/bin/bash

#AG 12/02/15
#script to produce a plot of 5' riboseq length distributions from a shoelaces wig
#plot scaled and unscaled lenghts relative to start and stop codons

#dependanices (must be in path)
#samtools
#r
#Rscript

usage(){
cat << EOF
usage: $0 options

script to trim and align riboseq fastq datasets 

OPTIONS:
    -w    path to the folder containing the input wig files
    -o    path to output dir
    -g    gtf file (matched to the bam gemone)
    -h    this help message

example usage: run_lenghts.sh -w <in.wig> -g in.gtf -o <out_dir>

EOF
}

while getopts ":w:o:g:h" opt; do
    case $opt in 
        w)
            in_wig=$OPTARG
            echo "-w input bam file $OPTARG"
            ;;
        o)
            out_dir=$OPTARG
            echo "-o output folder $OPTARG"
            ;;
        g)
            in_gtf=$OPTARG
            echo "-g gtf file $OPTARG"
            ;;
        h)
            usage
            exit
            ;;
        ?) 
            echo "Invalid option: -$OPTARG"
            usage
            exit 1
              ;;
    esac
done

if [ ! -e $in_wig ]  || [ ! $out_dir ] || [ ! $in_gtf ]; then
        echo "something's missing - check your command lines args"
        usage
        exit 1
fi

if [ ! -d $out_dir ]; then
    mkdir $out_dir
fi

for file in $in_wig/*-forward.wig; do

    prefix=${file%-forward.wig}
	name=$(basename $prefix)
    echo starting $name
	echo prefix = $prefix
	echo base = $name
  
    #------------------------------------------------------------------------------------------
    #1 get counds
    #------------------------------------------------------------------------------------------

    nice -n 10 perl meta_gene_plot_wig_matrix_scaled.pl $in_gtf $file ${in_wig}/${name}-reverse.wig $in_ref $out_dir

    #------------------------------------------------------------------------------------------ 
    #2 Make metagene plots
    #------------------------------------------------------------------------------------------

    perl meta_plot_matrix_scale.pl ${out_dir}/${name}_start_codon.csv > ${out_dir}/${name}_start_meta_scaled.csv
    perl meta_plot_matrix_scale.pl ${out_dir}/${name}_stop_codon.csv > ${out_dir}/${name}_stop_meta_scaled.csv

    Rscript plot.R ${out_dir}/${name}_start_meta_scaled.csv ${out_dir}/${name}_plots.pdf

    echo meta plots done

done

