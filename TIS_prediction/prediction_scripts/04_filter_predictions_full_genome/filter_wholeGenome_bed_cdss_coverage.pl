#!/usr/bin/perl -w
use strict;

#to do 06/11/2016
#script to take a bed file of orf predictions and produce output stats

my $bed1=$ARGV[0]; 
my $genes=$ARGV[1];

my $LENGTH_CUTOFF=300;    
my $COVERAGE_CUTOFF=0.75;

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#

my %cds_regions_fwd;
my %cds_regions_rev;

open(GENES,$genes) || die "can't open $genes";        #gtf is 1 based
while (<GENES>){
    unless(/^#/){
        my @b=split("\t");
        my $class=$b[2];
        my $chr=$b[0];
        my $prim5=$b[3];
        my $prim3=$b[4];
        my $dir=$b[6];

       if ($class eq "exon"){
#      if ($class eq "CDS"){
            if ($dir eq "+"){
                for ($prim5 .. $prim3){
                   $cds_regions_fwd{$chr}{$_}=1;
                }
            }else{
                for ($prim5 .. $prim3){
                    $cds_regions_rev{$chr}{$_}=1;
                }
            }
        }
    }
}
close(GENES);


#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#

open (BED1, $bed1) || die "can't open $bed1";     
while (<BED1>){

    unless (/^track/){  #skip header
        my @b=split("\t");
        my $chr=$b[0];
        my $start=$b[1]; #this is zero based 
        my $end=$b[2];
        my $type=$b[3];
        my $score=$b[4];
        my $dir=$b[5];
        my $cov=$b[9];
        my $length=$end-$start;

#        print "$length,$score\n";

        if ($cov >= $COVERAGE_CUTOFF){
            #remove coverage
            pop(@b);
            my $outLine=join("\t",@b);

            #print "$cov,$outLine\n";
 
            if ($dir eq "+"){
                my $keep=1;
                for ($start .. $end){
                    if (exists ($cds_regions_fwd{$chr}{$_})){
                        $keep=0;
                    }
                }
                if ($keep){
                    if ($length >= $LENGTH_CUTOFF){
                        print "$outLine\n";
                    }
                }
            }else{
                my $keep=1;
                for ($start .. $end){
                    if (exists ($cds_regions_rev{$chr}{$_})){
                        $keep=0;
                    }
                }
                if ($keep){ 
                    if ($length >= $LENGTH_CUTOFF){
                        print "$outLine\n"; 
                    }
                }
            }
        }
    }
}
close (BED1);

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#

exit;
