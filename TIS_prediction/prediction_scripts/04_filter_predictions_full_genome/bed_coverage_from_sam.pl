#!/usr/bin/perl -w
use strict;

#to do 04/11/2016
#script to get the coverage stats from bed predictions;
#N terminal counts do not take into account peptides missing their first methionine

#input files:
my $sam=$ARGV[0];
my $bed_file=$ARGV[1];

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#open sam file, store riboseq counts per position
my %counts_fwd; #key = chr, key2 = position, key3 = lenghts, value = counts.
my %counts_rev; #key = chr, key2 = position, key3 = lenghts, value = counts.

open (SAM, $sam) || die "can't open $sam";      #sam is 1 based
while (<SAM>){
    #skip headers
    unless(/^#/){
        my @b=split("\t");
        my $leftMost=$b[3]; #leftmost position of match 5' for fwd, 3' for rev
        my $flag=$b[1];
        my $chr=$b[2];
        my $mapq=$b[4];
        my $cig=$b[5];
        my $seq=$b[9];
        my $length=length($seq);                        
        
        if ($mapq >= 10){ #mapping uniqness filter
            unless ($flag & 0x4){   #if aligned

                if ($flag & 0x10){  #if rev calculate 5' from the rightmost position

                    #parse cigar for indels and adjust the length of the alignment             
                    my $length5=length($seq);
                    while ($cig =~/(\d+)I/g){   #add to length for insertions
                        $length5+=$1;    
                    }
                    while ($cig =~/(\d+)D/g){   #substact from length for deletions
                        $length5-=$1;
                    }

                    my $fivePrime=$leftMost+$length5-1;   #sam is 1 based
                    for my $pos ($leftMost .. $fivePrime){
                        $counts_rev{$chr}{$pos}=1;
                    }

                }else{ #if fwd this is easy

                    my $length5=length($seq);
                    while ($cig =~/(\d+)I/g){   #add to length for insertions
                        $length5+=$1;
                    }
                    while ($cig =~/(\d+)D/g){   #substact from length for deletions
                        $length5-=$1;
                    }

                    my $threePrime=$leftMost+$length5-1;   #sam is 1 based
                    for my $pos ($leftMost .. $threePrime){
                        $counts_fwd{$chr}{$pos}=1;
                    }
                }
            }
        }
    }                    
}
close (SAM);


#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#open bed of predictions

open (BED, $bed_file) || die "can't open $bed_file";      #bed is O based at start, 1 based at end
while (<BED>){

    chomp;

    unless (/^track/){  #skip header
        my @b=split("\t");
        my $chr=$b[0];
        my $start=$b[1]+1;     #start is zero based
        my $stop=$b[2];
        my $score=$b[4];
        my $dir=$b[5];

        my $cov=0;
        my $length=0;

        #assign to metaplots 
        if ($dir eq "+"){                 #fwd cases    
            for my $pos ($start .. $stop){
                $length++;
                if (exists ($counts_fwd{$chr}{$pos})){
                    $cov++;
                }
            }
            my $coverage=$cov/$length;
            #print "$coverage,$score\n";
            print "$_\t$coverage\n";

        }else{    #reverse cases
            for my $pos ($start .. $stop){
                $length++;
                if (exists ($counts_rev{$chr}{$pos})){
                    $cov++;
                }
            }
            my $coverage=$cov/$length;
            #print "$coverage,$score\n";
            print "$_\t$coverage\n";
        }
    }
}
close (BED);

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#

exit;
