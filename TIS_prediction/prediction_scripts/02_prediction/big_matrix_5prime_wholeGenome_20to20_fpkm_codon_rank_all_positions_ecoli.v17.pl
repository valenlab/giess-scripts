#!/usr/bin/perl -w
use strict;

#to do 15/04/2016
#script to produce the matrix for pca, start codon classifiaction
#output is split into lengths.
#for each gene look upstream of the start codon to find the next in frame stop codon

#with fpkm 
#whole genome

#input files:
#gtf exons, start, stop codons
#sam = aligned 
my $gtf=$ARGV[0];
my $sam=$ARGV[1];
my $fasta=$ARGV[2];
my $bed_file=$ARGV[3];  #nterminal bed
my $out_file=$ARGV[4];

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#

my %start_codon; #key=gene_id, value=pos
my %gene_info_fwd; #key=gene_id, key2=start, value=stop
my %gene_info_rev; #key=gene_id, key2=start, value=stop
my %gene_lengths; #for FPKM
my %gene_2_chr; #key = gene_id; value = chr

my %ann_start_codon_fwd; #key=chr, key2=pos, value=1   #used for flags only
my %ann_start_codon_rev; #key=chr, key2=pos, value=1   
my %multi_exon;

open(GENES,$gtf) || die "can't open $gtf";        #gtf is 1 based
while (<GENES>){
    unless(/^#/){
        my @b=split("\t");
        my $class=$b[2];
        my $chr=$b[0];
        my $start=$b[3];
        my $end=$b[4];
        my $dir=$b[6];
        my $gene_id="unknown";
        ($gene_id) = $b[8] =~ /gene_id\s"([^\"]+)";/;

        if ($class eq "CDS"){ #reworked to account for gtf's without start/stop codons

            #using CDS can be problematic because a few genes are multiexonic

            if ($dir eq "+"){ #fwd cases. Use start positions as 5'

     #           #fwd genes take start stop from first exons.

                if (exists( $multi_exon{$gene_id}) ){
                    $gene_lengths{$gene_id}+=($end+1)-$start;  #add on this start to end
                    my $previous_start=$start_codon{$gene_id};
                    $gene_info_fwd{$gene_id}{$previous_start}=($end-3); #stops   #-3 for ecoli to skip the start codon
                }else{
                    $start_codon{$gene_id}=$start; #starts
                    $ann_start_codon_fwd{$chr}{$start}=1;
                    $gene_info_fwd{$gene_id}{$start}=($end-3); #stops           #-3 for ecoli to skip the start codon
                    $gene_lengths{$gene_id}=($end+1)-$start;  #start to end
                    $gene_2_chr{$gene_id}=$chr;
                    $multi_exon{$gene_id}=1
                }

           }else{  #reverse cases. Use stop positions as 5'

    #           #rev genes take start and stop from first exon

                if (exists ( $multi_exon{$gene_id}) ){
                    $gene_lengths{$gene_id}+=($end+1)-$start; #add on this end to start
                    my $previous_start=$start_codon{$gene_id};
                    $gene_info_rev{$gene_id}{$previous_start}=$start;
                }else{
                    $start_codon{$gene_id}=$end;
                    $ann_start_codon_rev{$chr}{$end}=1;
                    $gene_info_rev{$gene_id}{$end}=$start;
                    $gene_lengths{$gene_id}=($end+1)-$start; #end to start
                    $gene_2_chr{$gene_id}=$chr;
                    $multi_exon{$gene_id}=1;
                }
            }
        }
    }
}
close(GENES);

print "5' gtf parsed\n";

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#open fasta for codon sequeces
my %fasta_sequences; #key = sequence_name, value=sequence
my $name;
open (FA, $fasta) || die "can't open $fasta";
while (<FA>){
    chomp;
    if (/^>([^\s]+)/){ #take header up to the first space
    $name=$1;
    }else{
        $fasta_sequences{$name}.=$_;
    }
}
close(FA);

print "fasta parsed\n";

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#assign regions
my %stop_2_stop_fwd;
my %stop_2_stop_rev;

for my $gene (keys %gene_info_fwd){
    for my $annotated_start (keys %{$gene_info_fwd{$gene}}){ 
        my $stop_codon=$gene_info_fwd{$gene}{$annotated_start};

		#get first nucleotide of start codon then procees upstream 3nt at a time until we find a stop codon (pattern match) 	
        my $search=1; 
        my $count=0;  #set upper limit to 999
        my $pos=$stop_codon; #-4 to skip the stop codon

        while ($search){
            $pos=$pos-3;

            #check that the substing is not smaller than the chr!
            if ($pos-3 < 0){ $search=0; }
                         
            $stop_2_stop_fwd{ $gene_2_chr{$gene}}{$pos+1}=1;
                   
            #check for stop codon
            my $seq=substr($fasta_sequences{$gene_2_chr{$gene}},$pos,3);  
            if ($seq=~/TAG/ || $seq=~/TAA/ || $seq=~/TGA/ ){
                $search=0;
            }

            if ($count>=333){ $search=0; }
          
            #extend up to 999nt past the start codon
            if ($pos < $annotated_start){
                $count++;
            }
        }
    }
}

for my $gene (keys %gene_info_rev){
    for my $annotated_start (keys %{$gene_info_rev{$gene}}){
        my $stop_codon=$gene_info_rev{$gene}{$annotated_start};

        #get first nucleotide of start codon then proceed upstream 3nt at a time until we find a stop codon (pattern match)     
        my $search=1;
        my $count=0;  #set upper limit to 999     
        my $pos=$stop_codon+1;

        while ($search){
            $pos=$pos+3;

            #check that the substing is not bigger or smaller than the chr!
            if ($pos+3>length($fasta_sequences{$gene_2_chr{$gene}})){ $search=0 }

            $stop_2_stop_rev{ $gene_2_chr{$gene} } {$pos+1}=1;

            #check for stop codon
            my $seq=reverse(substr($fasta_sequences{$gene_2_chr{$gene}},$pos-2,3));
            $seq=~tr/ACGTacgt/TGCAtgca/;
            if ($seq=~/TAG/ || $seq=~/TAA/ || $seq=~/TGA/ ){
                $search=0;
            }
            if ($count>=333){ $search=0; }

            #exetend up to 999nt past the start codon
            if ($pos > $annotated_start){
                $count++;
            }
        }
    } 
}

###################################################################################################
#open sam file, store riboseq counts per position
my %counts_fwd; #key = chr, key2 = position, key3 = lenghts, value = counts.
my %counts_rev; #key = chr, key2 = position, key3 = lenghts, value = counts.

my $longest_fragment=1;
my $shortest_fragment=1000000;
my $total_counts=0; #for FKPM 

open (SAM, $sam) || die "can't open $sam";      #sam is 1 based
while (<SAM>){
    #skip headers
    unless(/^#/){
        my @b=split("\t");
        my $leftMost=$b[3]; #leftmost position of match 5' for fwd, 3' for rev
        my $flag=$b[1];
        my $chr=$b[2];
        my $mapq=$b[4];
        my $cig=$b[5];
        my $seq=$b[9];
        my $fivePrime;                     
        my $length=length($seq);                        
        
        if ($mapq >= 10){ #mapping uniqness filter
            unless ($flag & 0x4){   #if aligned

                $total_counts++;

                if ($flag & 0x10){  #if rev calculate 5' from the rightmost position

                    #parse cigar for indels and adjust the length of the alignment             
                    my $length5=length($seq);
                    while ($cig =~/(\d+)I/g){   #add to length for insertions
                        $length5+=$1;    
                    }
                    while ($cig =~/(\d+)D/g){   #substact from length for deletions
                        $length5-=$1;
                    }

                    $fivePrime=$leftMost+$length5-1;   #sam is 1 based
                    $counts_rev{$chr}{$fivePrime}{$length}++;

                }else{ #if fwd this is easy
                    $fivePrime=$leftMost;
                    $counts_fwd{$chr}{$fivePrime}{$length}++;
                }

				#find the fragment length range for the output matrix 
                if ($length<$shortest_fragment){
				    $shortest_fragment=$length;
                }elsif ($length>$longest_fragment){
                    $longest_fragment=$length;
				}
            }
        }else{
            unless ($flag & 0x4){   #if aligned
                $total_counts++;
            }   
        }
    }                    
}
close (SAM);

print "sam parsed\n";

###################################################################################################
#open n-terminal bed
my %n_term_rev; #key=chr, key=start_position, value=count
my %n_term_fwd;

open (BED, $bed_file) || die "can't open $bed_file";      #bed is O based at start, 1 based at end
while (<BED>){

    unless (/^track/){  #skip header
        my @b=split("\t");
        my $chr=$b[0];
        my $start=$b[1]+1;     #start is zero based
        my $stop=$b[2];
        my $count=$b[4];
        my $dir=$b[5];

        #assign to metaplots 
        if ($dir eq "+"){                                            #fwd cases
            #five_prime=$start;
            $n_term_fwd{$chr}{$start}+=$count;
        }else{                                                         #reverse cases
            #five_prime=$stop;
            $n_term_rev{$chr}{$stop}+=$count;  #stop is 1 based
        }
    }
}
close (BED);

print "n-termini bed parsed\n";

####################################################################################################
#summarise and output matrix
open (OUT, ">$out_file") || die;

#header
print OUT "#id,codon,dir,canonical_candidate_sites_ORF_region,annotated_start_site,cognate_start_site,stop_to_stop,n_terminal_peptides,reads_at_pos,window_reads_downstream,window_reads_upstream,window_up_down_ratio,proprtion_of_reads_upstream,proportion_of_reads_downstream,ORF_fpkm";

for my $hpos (-20 .. 20){
    print OUT ",seq_".$hpos;    
}

for my $len ($shortest_fragment .. $longest_fragment){
	for my $pos(-20 .. 20){
         print OUT ",".$pos."_".$len;
	}
}
 
print OUT "\n";

#####Rather then gene by gene loop though each chromosome, starting 20nt upstream and ending 20 nt before the end.######
#####Keep gene regions for deturming starts and inframe positions#####

for my $chr (sort keys %fasta_sequences){

    my $end=(length($fasta_sequences{$chr}));  #rightmost  #zero or 1 based?
    my $start=0;                              #leftmost

    #start=    #for rev 3' but then they are switched = 5' in realtion to strand+ -18
    #end=      #for rev 5' but then they are switched = 3' in relation to strand+ +20

    #windows
    for my $codon_pos ($start+20 .. $end-20){
        my $annotated_start_site="FALSE";
        my $cognate="FALSE";
        my $stop2stop="FALSE";
	    my $n_termini_count=0;
        my $reads_at_pos=0;
        my $window_reads_downstream=0;
        my $window_reads_upstream=0;
        my $win_sum=0;

        my $codon=reverse(substr($fasta_sequences{$chr},$codon_pos-3,3));    
        $codon=~tr/ACGTacgt/TGCAtgca/;
 
        #do not restrict to cognate and near cognate start codons#

        #parse all lenghts for count summaries (summmed over all fragment lengths)
        for my $len ($shortest_fragment .. $longest_fragment){
            for my $win_pos (($codon_pos-20) .. ($codon_pos+20)){
                # up and downstream should be opposite for rev reads
                if (exists ($counts_rev{$chr}{$win_pos}{$len})){
                    $win_sum+=$counts_rev{$chr}{$win_pos}{$len};
                    if ($win_pos==$codon_pos){ $reads_at_pos+=$counts_rev{$chr}{$win_pos}{$len}; }
                    if ($win_pos<$codon_pos){ $window_reads_downstream+=$counts_rev{$chr}{$win_pos}{$len}; }
                    if ($win_pos>$codon_pos){ $window_reads_upstream+=$counts_rev{$chr}{$win_pos}{$len}; }
                }
            }
        }

        my $window_up_down_ratio=eval {$window_reads_downstream/$window_reads_upstream} || 0;            
        my $proportion_upstream=eval {$window_reads_upstream/$win_sum} || 0;
        my $proportion_downstream=eval {$window_reads_downstream/$win_sum} || 0;

        my $id=$chr."_".$codon_pos."_rev";
        my $window_seq=reverse(substr($fasta_sequences{$chr},($codon_pos-21),41));  
        $window_seq=~tr/ACGTacgt/TGCAtgca/;
 
        #annotation at this position
        if (exists ($ann_start_codon_rev{$chr}{$codon_pos})){$annotated_start_site="TRUE";}
        if (exists ($stop_2_stop_rev{$chr}{$codon_pos})){ $stop2stop="TRUE";}
        if ($codon =~/[ACGT]TG/ || $codon =~/A[ACGT]G/ || $codon =~/AT[ACGT]/){ $cognate="TRUE";}

        #n termnini here
        if (exists ($n_term_rev{$chr}{$codon_pos})){  
            $n_termini_count=$n_term_rev{$chr}{$codon_pos};
        }

        #fkpm + codon rank here
        my ($ORF_FPKM, $codon_rank)=&stop2stop_rev($chr,$codon_pos);

        print OUT "$id,$codon,rev,$codon_rank,$annotated_start_site,$cognate,$stop2stop,$n_termini_count,$reads_at_pos,$window_reads_downstream,$window_reads_upstream,$window_up_down_ratio,$proportion_downstream,$proportion_upstream,$ORF_FPKM";
                
        #Loop through sequence here
        my @seq_out=split("",$window_seq); #window seq is already reversed
        for my $nuc (@seq_out){
            print OUT ",$nuc";
        }

        #LOOP THROUGH LENGTHS HERE
        for my $len ($shortest_fragment .. $longest_fragment){
            #this needs to be for +30 to 10 for reverse genes (opposite of fwd genes)
            my $win_pos=$codon_pos+20;
            while ($win_pos >= $codon_pos-20){
                my $signal=0;
                #riboseq signal       #%counts; #key = chr, key2 = position, value = counts.
                if (exists ($counts_rev{$chr}{$win_pos}{$len})){
                    $signal=$counts_rev{$chr}{$win_pos}{$len};
                }
                   
                my $fraction_of_window_signal=eval {$signal/$win_sum} || 0;
                print OUT ",$fraction_of_window_signal";
                $win_pos--;
            } 
        }
        print OUT "\n";
    }
}

print "rev genes processed\n";

#loop though chrososomes;
for my $chr (sort keys %fasta_sequences){

    my $end=(length($fasta_sequences{$chr}));  #rightmost  #zero or 1 based?
    my $start=0;                              #leftmost

    #start    #for fwd = 5' -20
    #end      #for rev = 3' +18

    #windows   #not exluding introns!!
    for my $codon_pos ($start+20 .. $end-20){
        my $annotated_start_site="FALSE";
        my $cognate="FALSE";
        my $stop2stop="FALSE";
        my $n_termini_count=0;
        my $reads_at_pos=0;
        my $window_reads_downstream=0;
        my $window_reads_upstream=0;
        my $win_sum=0;

        my $codon=substr($fasta_sequences{$chr},$codon_pos-1,3);

        #do not resrict to canonical and noncanonical start codons#

        #parse all lenghts for count summaries (summmed over all fragment lengths)
        for my $len ($shortest_fragment .. $longest_fragment){
            for my $win_pos (($codon_pos-20) .. ($codon_pos+20)){
                if (exists ($counts_fwd{$chr}{$win_pos}{$len})){
                    $win_sum+=$counts_fwd{$chr}{$win_pos}{$len};
                    if ($win_pos==$codon_pos){ $reads_at_pos+=$counts_fwd{$chr}{$win_pos}{$len}; }
                    if ($win_pos>$codon_pos){ $window_reads_downstream+=$counts_fwd{$chr}{$win_pos}{$len}; }
                    if ($win_pos<$codon_pos){ $window_reads_upstream+=$counts_fwd{$chr}{$win_pos}{$len}; }
                }
            }
        }

        my $window_up_down_ratio=eval {$window_reads_downstream/$window_reads_upstream} || 0;
        my $proportion_upstream=eval {$window_reads_upstream/$win_sum} || 0;
        my $proportion_downstream=eval {$window_reads_downstream/$win_sum} || 0;

        my $id=$chr."_".$codon_pos."_fwd";
        my $window_seq=substr($fasta_sequences{$chr},($codon_pos-21),41);
 
        #annotation at this position
        if (exists ($ann_start_codon_fwd{$chr}{$codon_pos})){$annotated_start_site="TRUE";}
        if (exists ($stop_2_stop_fwd{$chr}{$codon_pos})){ $stop2stop="TRUE"; }
        if ($codon =~/[ACGT]TG/ || $codon =~/A[ACGT]G/ || $codon =~/AT[ACGT]/){ $cognate="TRUE";}

        #n-termini counts
        if (exists ($n_term_fwd{$chr}{$codon_pos})){
            $n_termini_count=$n_term_fwd{$chr}{$codon_pos};
        }

        #fkpm + codon rank here
        my ($ORF_FPKM,$codon_rank)=&stop2stop_fwd($chr,$codon_pos);

        print OUT "$id,$codon,fwd,$codon_rank,$annotated_start_site,$cognate,$stop2stop,$n_termini_count,$reads_at_pos,$window_reads_downstream,$window_reads_upstream,$window_up_down_ratio,$proportion_downstream,$proportion_upstream,$ORF_FPKM";
      
        #Loop through sequence here
        #output sequence
        my @seq_out=split("",$window_seq);
        for my $nuc (@seq_out){
            print OUT ",$nuc";
        }

        #LOOP THROUGH LENGTHS HERE
        for my $len ($shortest_fragment .. $longest_fragment){
            #second loop for output
            for my $win_pos (($codon_pos-20) .. ($codon_pos+20)){                  
                my $signal=0;
                #riboseq signal
                #%counts; #key = chr, key2 = position, value = counts.
                if (exists ($counts_fwd{$chr}{$win_pos}{$len})){ 
                    $signal+=$counts_fwd{$chr}{$win_pos}{$len};
                }

                my $fraction_of_window_signal=eval {$signal/$win_sum} || 0;            
                print OUT ",$fraction_of_window_signal";
            }
        }
        print OUT "\n";
    }
}

close (OUT);

print "fwd gene processed\nshortest: $shortest_fragment\nlongest: $longest_fragment\n";

exit;

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
sub stop2stop_fwd{
    #for a given position (chr + pos)
    #find the nearest downstream in frame stop codon, and upstream in frame stop codon (to a limit of 999nt, without going out of chr limits)
    #calculate fkpm
    #calculate the number of potential start sites upstream of this one + 1

    my $CHR=$_[0];
    my $current_codon_pos=$_[1];
    my $upstream_codons=0;

    ###
    #find upstream stop codon
    ###

    #keep track of the potential start sites     
    my $search=1;
    my $count=0;  #set upper limit to 1000 (100)
    my $pos=$current_codon_pos-1;
    while ($search){
        $pos=$pos-3;

        #check that the substing is not smaller than the chr!
        if ($pos<0){ last; }

        my $seq=substr($fasta_sequences{$CHR},$pos,3);
        if ($seq=~/TAG/ || $seq=~/TAA/ || $seq=~/TGA/ ){
            $search=0;
        }

        if ($seq=~ /[ACGT]TG/ || $seq =~ /A[ACGT]G/ || $seq =~ /AT[ACGT]/ ){
            $upstream_codons++;
        }

        if ($count>=333){ $search=0; } #limit to 999nt
        $count++
    }

    ###
    #find downstream stop codon
    ###

    $search=1;
    my $limit=length($fasta_sequences{$CHR});  #set upper limit to the end fo the chromosome
    $pos=$current_codon_pos-1;
    my $offset=$pos%3;
    my $ORF_length=0;
    my $read_sum=0;
    while ($search){
        $pos++;
        $ORF_length++;

        #sum reads for fpkm
        if (exists ($counts_fwd{$CHR}{$pos})){
            for my $length (keys %{ $counts_fwd{$CHR}{$pos} } ){
                $read_sum+=$counts_fwd{$CHR}{$pos}{$length};
            }
        }

        if ($pos%3 == $offset){

            #check that the substing is not smaller than the chr!
            if ($pos+3>$limit){ $search=0; }

            my $seq=substr($fasta_sequences{$CHR},$pos,3);
            #check for stop codon
            if ($seq=~/TAG/ || $seq=~/TAA/ || $seq=~/TGA/ ){
               $search=0;
            }
       }
   }

   my $ORF_FPKM=eval { (1000000000*$read_sum)/($total_counts*$ORF_length) } || 0;
   return ($ORF_FPKM, $upstream_codons+1);
}

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
sub stop2stop_rev{

    my $CHR=$_[0];
    my $current_codon_pos=$_[1];

    my $upstream_codons=0;
    my $ORF_stop=0;

    ###
    #find upstream stop codon
    ###

    my $search=1;
    my $count=0;  #set upper limit to 999 
    my $pos=$current_codon_pos-1;
    while ($search){
        $pos=$pos+3;

        #check that the substing is not bigger or smaller than the chr!
        if ($pos>length($fasta_sequences{$CHR})){ last; }

        my $seq=reverse(substr($fasta_sequences{$CHR},$pos-2,3));
        $seq=~tr/ACGTacgt/TGCAtgca/;
        #check for start codon
        if ($seq=~/TAG/ || $seq=~/TAA/ || $seq=~/TGA/ ){
            $search=0;
        }

        if ($seq=~ /[AGCT]TG/ || $seq =~ /A[ACTG]G/ || $seq =~ /AT[ACGT]/ ){
            $upstream_codons++;
        }

        if ($count>=333){ $search=0; } #limit to 999nt
        $count++;        
    }

    ###
    #find downstream stop codon
    ###

    #get first nucleotide of start codon then proceed upstream 3nt at a time until we find a stop codon (pattern match)     
    $search=1;
    my $limit=0;  #set upper limit to start of chromosome
    $pos=$current_codon_pos-1;
    my $offset=$pos%3;
    my $ORF_length=0;
    my $read_sum=0;
    while ($search){
        $pos--;
        $ORF_length++;

        #sum reads for fpkm
        if (exists ($counts_rev{$CHR}{$pos+2})){
            for my $length (keys %{ $counts_rev{$CHR}{$pos+2} } ){
                $read_sum+=$counts_rev{$CHR}{$pos+2}{$length};
            }
        }

        if ($pos%3 == $offset){   

            #check that the substring is not bigger or smaller than the chr!
            if ($pos-3<=$limit){ $search=0;}

            my $seq=reverse(substr($fasta_sequences{$CHR},$pos-2,3));
            $seq=~tr/ACGTacgt/TGCAtgca/;
            #check for start codon
            if ($seq=~/TAG/ || $seq=~/TAA/ || $seq=~/TGA/ ){
                $search=0;
            }
        }
    }

    my $ORF_FPKM=eval { (1000000000*$read_sum)/($total_counts*$ORF_length) } || 0;  
    return ($ORF_FPKM, $upstream_codons+1);
}

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
