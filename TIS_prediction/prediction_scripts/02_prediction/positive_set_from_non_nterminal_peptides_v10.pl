#!/usr/bin/perl -w
use strict;

#to do 24/11/2016
#script to take a matrix of genomic positions and a bed file of n-termini fragments 
#assign counts of n-termini peptides from a bed file to start codons
#filter the genes at a sensible cuttoff, ie. take only the 50% highest genes by cds expression

my $matrix=$ARGV[0];
my $bed_file=$ARGV[1];
my $out_positive=$ARGV[2]; #1000 start codons from upper 50% of genes by experssions, without n-termial support
my $out_negative=$ARGV[3]; #8000 non start codons, without n-termini support (inframe)

###################################################################################################
#open bed file

my %n_term_fwd; #key=chr, key=start_position, value=count
my %n_term_rev; 

open (BED, $bed_file) || die "can't open $bed_file";      #bed is O based at start, 1 based at end
while (<BED>){

    unless (/^track/){  #skip header
        my @b=split("\t");
		my $chr=$b[0];
        my $start=$b[1]+1;     #start is zero bases
        my $stop=$b[2];
        my $count=$b[4];
		my $dir=$b[5];
                    
        #assign to metaplots 
		if ($dir eq "+"){                                            #fwd cases
			#five_prime=$start;
            $n_term_fwd{$chr}{$start}+=$count;            
        }else{                                                         #reverse cases
            #five_prime=$stop;
            $n_term_rev{$chr}{$stop}+=$count;
		}
    }                    
}
close (BED);

###################################################################################################
#open matrix

my @start_supported;
my @start_random;
my @start_FPKM; #for median
my @not_start;
my $header="#empty_header\n";

open (MAT, $matrix) || die "can't open $matrix";     
while (<MAT>){

    if (/^#/){
		$header=$_;
	}else{

        my @l=split(",");
        my $dir=$l[2];
        my $start_codon=$l[4];
        my $in_frame=$l[6];
        my $fpkm=$l[14]; 

        my $chr;
        my $pos;    
        if ($l[0] =~/(pRSF1010_SL1344)_(\d+)/){
            $chr=$1;
            $pos=$2;
        }elsif($l[0] =~/(pCol1B9_SL1344)_(\d+)/){
            $chr=$1;
            $pos=$2;
        }elsif($l[0] =~/(pSLT_SL1344)_(\d+)/){
            $chr=$1;
            $pos=$2;
        }elsif($l[0] =~/(Chromosome)_(\d+)/){
            $chr=$1;
            $pos=$2;
        }

        #filter for those inframe
        if($in_frame eq "TRUE"){  

            #start codons
            if ($start_codon eq "TRUE"){ 
    
                if ($dir eq "fwd"){
                    if (exists ($n_term_fwd{$chr}{$pos})){
                        push (@start_supported,$_);
                        push (@start_FPKM,$fpkm); 

                    }else{
                        push (@start_random,$_);
                        push (@start_FPKM,$fpkm);
                    }
                }else{ #rev
                    if (exists ($n_term_rev{$chr}{$pos})){
                        push (@start_supported,$_);
                        push (@start_FPKM,$fpkm);
 
                    }else{
                        push (@start_random,$_);
                        push (@start_FPKM,$fpkm);
                    }            
                }

            #exclude non start codons that have N-termni support!!
            }else{ #non start codons
                if ($dir eq "fwd"){
                    unless (exists ($n_term_fwd{$chr}{$pos})){
                        unless(exists ($n_term_fwd{$chr}{$pos+3})){ #why both of these?
                            push (@not_start,$_);
                        }
                    }
                }else{ #rev
                    unless (exists ($n_term_rev{$chr}{$pos})){
                        unless(exists ($n_term_rev{$chr}{$pos-3})){
                            push (@not_start,$_);
                        }
                    }
                }
            }
    	}
    }
}
close(MAT);

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#find median expression from start codons
my $median_exp=0;
my $size=$#start_FPKM;
my $count=0;
for my $ORF (sort {$a <=> $b} @start_FPKM){
	$count++;
	if ($count > $size/2){   #1.35
        $median_exp=$ORF;
		last;
	}
}

print "size = $size, median = $median_exp\n";    #this is now median expression of the window

my $sizeUn=$#start_random; 
my $sizeSu=$#start_supported; 

print "there are $sizeUn unsupported start codons\n";
print "there are $sizeSu supporterd start codons\n";

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#select all start codons from the upper 50% of genes by expression, without N-termini support
my @positive;
for my $line (@start_random){
    my @position=split(",",$line);
    if ($position[14] >= $median_exp){ #filter on median exp!!!!!
    	push @positive, $line;
    }
}

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#output negative set3
my @negative;
my $size2=0;
while ($size2 < 8000){
 
    #get element
    my $index = rand @not_start;
#    my @position=split(",",$not_start[$index]);

    push (@negative, splice @not_start, $index, 1);
    $size2++;
}

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#output
open (OUT1,">$out_positive")  || die "can't open $out_positive\n";
open (OUT2,">$out_negative")  || die "can't open $out_negative\n";

#headers
print OUT1 $header;
print OUT2 $header;

for (@positive){
    print OUT1 $_;
}

for (@negative){
    print OUT2 $_;
}

exit;
