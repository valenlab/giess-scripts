library (h2o)
library (caret)
h2o.init(nthreads = 1, max_mem_size="100g")
h2o.removeAll()

h2o.shutdown(prompt = TRUE)

###################################################################################################

TET2.neg.raw <- read.csv("/export/valenfs/projects/final_results/prokaryotic_riboseq/ribo_seq/matrix/sets_24_11_2016_ecoli_combined/TET2.all.neg.csv", header=TRUE)
TET2.pos.raw <- read.csv("/export/valenfs/projects/final_results/prokaryotic_riboseq/ribo_seq/matrix/sets_24_11_2016_ecoli_combined/TET2.all.pos.csv", header=TRUE)

rownames(TET2.pos.raw) <- TET2.pos.raw[, 1]
TET2.pos.raw <- TET2.pos.raw[,-1]
TET2.pos.raw$seq_1[TET2.pos.raw$seq_1 == TRUE] <-"T"
TET2.pos.raw[,15:55] <- lapply(TET2.pos.raw[,15:55] , factor)
TET2.pos.raw[,56:1982] <- lapply(TET2.pos.raw[,56:1982] , as.numeric)

rownames(TET2.neg.raw) <- TET2.neg.raw[, 1]
TET2.neg.raw <- TET2.neg.raw[,-1]
TET2.neg.raw$seq_1[TET2.neg.raw$seq_1 == TRUE] <-"T"
TET2.neg.raw[,15:55] <- lapply(TET2.neg.raw[,15:55] , factor)
TET2.neg.raw[56:1982] <- lapply(TET2.neg.raw[56:1982] , as.numeric)

#the set needs to be randomised
set.seed(1111)
TET2.pos.ran <- TET2.pos.raw[sample(nrow(TET2.pos.raw)),]
TET2.neg.ran <- TET2.neg.raw[sample(nrow(TET2.neg.raw)),]

TET2.pos.train.raw <- TET2.pos.ran[1:1100,]  
TET2.neg.train.raw <- TET2.neg.ran[1:4400,]
TET2.train.raw <- rbind(TET2.pos.train.raw,TET2.neg.train.raw)

TET2.pos.cv.raw <- TET2.pos.ran[1101:1300,]
TET2.neg.cv.raw <- TET2.neg.ran[4401:5200,]
TET2.cv.raw <- rbind(TET2.pos.cv.raw,TET2.neg.cv.raw)

TET2.train.raw$y=as.factor(TET2.train.raw$annotated_start_site)
levels(TET2.train.raw$y) =c('neg','pos')
TET2.cv.raw$y=as.factor(TET2.cv.raw$annotated_start_site)
levels(TET2.cv.raw$y) =c('neg','pos')

TET2.train.raw.hex <- as.h2o(TET2.train.raw, destination_frame="TET2.train.raw.hex")
TET2.cv.raw.hex <- as.h2o(TET2.cv.raw, destination_frame = "TET2.cv.raw.hex")

###################################################################################################

#1,     #Codon
#3,		  #Codon rank
#11:13  #read ratios in window
#14     #ORF FPKM
#15:34 		#Seq -20 to -1
#38:45 		#Seq   3 to 10
#61:70    #-25 to -16nt length 20
#102:111 #-25 to -16nt length 21
#..
#635:644  #-25 to -16nt length 34 
#676:685  #-25 to -16nt length 35

TET2.RF.50   <- h2o.randomForest(y = 1983, x = c(1, 3, 11:34, 38:45,61:70,102:111,143:152,184:193,225:234,266:275,307:316,348:357,389:398,430:439,471:480,512:521,553:562,594:603,635:644,676,685), training_frame=TET2.train.raw.hex, validation_frame=TET2.cv.raw.hex, ntrees=50, balance_classes = FALSE, seed=7777777)
TET2.RF.100  <- h2o.randomForest(y = 1983, x = c(1, 3, 11:34, 38:45,61:70,102:111,143:152,184:193,225:234,266:275,307:316,348:357,389:398,430:439,471:480,512:521,553:562,594:603,635:644,676,685), training_frame=TET2.train.raw.hex, validation_frame=TET2.cv.raw.hex, ntrees=100, balance_classes = FALSE, seed=7777777)
TET2.RF.150  <- h2o.randomForest(y = 1983, x = c(1, 3, 11:34, 38:45,61:70,102:111,143:152,184:193,225:234,266:275,307:316,348:357,389:398,430:439,471:480,512:521,553:562,594:603,635:644,676,685), training_frame=TET2.train.raw.hex, validation_frame=TET2.cv.raw.hex, ntrees=150, balance_classes = FALSE, seed=7777777)
TET2.RF.200  <- h2o.randomForest(y = 1983, x = c(1, 3, 11:34, 38:45,61:70,102:111,143:152,184:193,225:234,266:275,307:316,348:357,389:398,430:439,471:480,512:521,553:562,594:603,635:644,676,685), training_frame=TET2.train.raw.hex, validation_frame=TET2.cv.raw.hex, ntrees=200, balance_classes = FALSE, seed=7777777)
TET2.RF.250  <- h2o.randomForest(y = 1983, x = c(1, 3, 11:34, 38:45,61:70,102:111,143:152,184:193,225:234,266:275,307:316,348:357,389:398,430:439,471:480,512:521,553:562,594:603,635:644,676,685), training_frame=TET2.train.raw.hex, validation_frame=TET2.cv.raw.hex, ntrees=250, balance_classes = FALSE, seed=7777777)
TET2.RF.300  <- h2o.randomForest(y = 1983, x = c(1, 3, 11:34, 38:45,61:70,102:111,143:152,184:193,225:234,266:275,307:316,348:357,389:398,430:439,471:480,512:521,553:562,594:603,635:644,676,685), training_frame=TET2.train.raw.hex, validation_frame=TET2.cv.raw.hex, ntrees=300, balance_classes = FALSE, seed=7777777)
TET2.RF.350  <- h2o.randomForest(y = 1983, x = c(1, 3, 11:34, 38:45,61:70,102:111,143:152,184:193,225:234,266:275,307:316,348:357,389:398,430:439,471:480,512:521,553:562,594:603,635:644,676,685), training_frame=TET2.train.raw.hex, validation_frame=TET2.cv.raw.hex, ntrees=350, balance_classes = FALSE, seed=7777777)
TET2.RF.400  <- h2o.randomForest(y = 1983, x = c(1, 3, 11:34, 38:45,61:70,102:111,143:152,184:193,225:234,266:275,307:316,348:357,389:398,430:439,471:480,512:521,553:562,594:603,635:644,676,685), training_frame=TET2.train.raw.hex, validation_frame=TET2.cv.raw.hex, ntrees=400, balance_classes = FALSE, seed=7777777)
TET2.RF.450  <- h2o.randomForest(y = 1983, x = c(1, 3, 11:34, 38:45,61:70,102:111,143:152,184:193,225:234,266:275,307:316,348:357,389:398,430:439,471:480,512:521,553:562,594:603,635:644,676,685), training_frame=TET2.train.raw.hex, validation_frame=TET2.cv.raw.hex, ntrees=450, balance_classes = FALSE, seed=7777777)
TET2.RF.500  <- h2o.randomForest(y = 1983, x = c(1, 3, 11:34, 38:45,61:70,102:111,143:152,184:193,225:234,266:275,307:316,348:357,389:398,430:439,471:480,512:521,553:562,594:603,635:644,676,685), training_frame=TET2.train.raw.hex, validation_frame=TET2.cv.raw.hex, ntrees=500, balance_classes = FALSE, seed=7777777)
TET2.RF.550  <- h2o.randomForest(y = 1983, x = c(1, 3, 11:34, 38:45,61:70,102:111,143:152,184:193,225:234,266:275,307:316,348:357,389:398,430:439,471:480,512:521,553:562,594:603,635:644,676,685), training_frame=TET2.train.raw.hex, validation_frame=TET2.cv.raw.hex, ntrees=550, balance_classes = FALSE, seed=7777777)
TET2.RF.600  <- h2o.randomForest(y = 1983, x = c(1, 3, 11:34, 38:45,61:70,102:111,143:152,184:193,225:234,266:275,307:316,348:357,389:398,430:439,471:480,512:521,553:562,594:603,635:644,676,685), training_frame=TET2.train.raw.hex, validation_frame=TET2.cv.raw.hex, ntrees=600, balance_classes = FALSE, seed=7777777)
TET2.RF.650  <- h2o.randomForest(y = 1983, x = c(1, 3, 11:34, 38:45,61:70,102:111,143:152,184:193,225:234,266:275,307:316,348:357,389:398,430:439,471:480,512:521,553:562,594:603,635:644,676,685), training_frame=TET2.train.raw.hex, validation_frame=TET2.cv.raw.hex, ntrees=650, balance_classes = FALSE, seed=7777777)
TET2.RF.700  <- h2o.randomForest(y = 1983, x = c(1, 3, 11:34, 38:45,61:70,102:111,143:152,184:193,225:234,266:275,307:316,348:357,389:398,430:439,471:480,512:521,553:562,594:603,635:644,676,685), training_frame=TET2.train.raw.hex, validation_frame=TET2.cv.raw.hex, ntrees=700, balance_classes = FALSE, seed=7777777)
TET2.RF.750  <- h2o.randomForest(y = 1983, x = c(1, 3, 11:34, 38:45,61:70,102:111,143:152,184:193,225:234,266:275,307:316,348:357,389:398,430:439,471:480,512:521,553:562,594:603,635:644,676,685), training_frame=TET2.train.raw.hex, validation_frame=TET2.cv.raw.hex, ntrees=750, balance_classes = FALSE, seed=7777777)
TET2.RF.800  <- h2o.randomForest(y = 1983, x = c(1, 3, 11:34, 38:45,61:70,102:111,143:152,184:193,225:234,266:275,307:316,348:357,389:398,430:439,471:480,512:521,553:562,594:603,635:644,676,685), training_frame=TET2.train.raw.hex, validation_frame=TET2.cv.raw.hex, ntrees=800, balance_classes = FALSE, seed=7777777)
TET2.RF.850  <- h2o.randomForest(y = 1983, x = c(1, 3, 11:34, 38:45,61:70,102:111,143:152,184:193,225:234,266:275,307:316,348:357,389:398,430:439,471:480,512:521,553:562,594:603,635:644,676,685), training_frame=TET2.train.raw.hex, validation_frame=TET2.cv.raw.hex, ntrees=850, balance_classes = FALSE, seed=7777777)
TET2.RF.900  <- h2o.randomForest(y = 1983, x = c(1, 3, 11:34, 38:45,61:70,102:111,143:152,184:193,225:234,266:275,307:316,348:357,389:398,430:439,471:480,512:521,553:562,594:603,635:644,676,685), training_frame=TET2.train.raw.hex, validation_frame=TET2.cv.raw.hex, ntrees=900, balance_classes = FALSE, seed=7777777)
TET2.RF.950  <- h2o.randomForest(y = 1983, x = c(1, 3, 11:34, 38:45,61:70,102:111,143:152,184:193,225:234,266:275,307:316,348:357,389:398,430:439,471:480,512:521,553:562,594:603,635:644,676,685), training_frame=TET2.train.raw.hex, validation_frame=TET2.cv.raw.hex, ntrees=950, balance_classes = FALSE, seed=7777777)
TET2.RF.1000 <- h2o.randomForest(y = 1983, x = c(1, 3, 11:34, 38:45,61:70,102:111,143:152,184:193,225:234,266:275,307:316,348:357,389:398,430:439,471:480,512:521,553:562,594:603,635:644,676,685), training_frame=TET2.train.raw.hex, validation_frame=TET2.cv.raw.hex, ntrees=1000, balance_classes = FALSE, seed=7777777)

TET2.RF.50@model$training_metrics@metrics$AUC
TET2.RF.100@model$training_metrics@metrics$AUC
TET2.RF.150@model$training_metrics@metrics$AUC
TET2.RF.200@model$training_metrics@metrics$AUC
TET2.RF.250@model$training_metrics@metrics$AUC
TET2.RF.300@model$training_metrics@metrics$AUC
TET2.RF.350@model$training_metrics@metrics$AUC
TET2.RF.400@model$training_metrics@metrics$AUC
TET2.RF.450@model$training_metrics@metrics$AUC
TET2.RF.500@model$training_metrics@metrics$AUC
TET2.RF.550@model$training_metrics@metrics$AUC
TET2.RF.600@model$training_metrics@metrics$AUC
TET2.RF.650@model$training_metrics@metrics$AUC
TET2.RF.700@model$training_metrics@metrics$AUC
TET2.RF.750@model$training_metrics@metrics$AUC
TET2.RF.800@model$training_metrics@metrics$AUC
TET2.RF.850@model$training_metrics@metrics$AUC
TET2.RF.900@model$training_metrics@metrics$AUC
TET2.RF.950@model$training_metrics@metrics$AUC
TET2.RF.1000@model$training_metrics@metrics$AUC

TET2.RF.50@model$validation_metrics@metrics$AUC
TET2.RF.100@model$validation_metrics@metrics$AUC
TET2.RF.150@model$validation_metrics@metrics$AUC
TET2.RF.200@model$validation_metrics@metrics$AUC
TET2.RF.250@model$validation_metrics@metrics$AUC
TET2.RF.300@model$validation_metrics@metrics$AUC
TET2.RF.350@model$validation_metrics@metrics$AUC
TET2.RF.400@model$validation_metrics@metrics$AUC
TET2.RF.450@model$validation_metrics@metrics$AUC
TET2.RF.500@model$validation_metrics@metrics$AUC
TET2.RF.550@model$validation_metrics@metrics$AUC
TET2.RF.600@model$validation_metrics@metrics$AUC
TET2.RF.650@model$validation_metrics@metrics$AUC
TET2.RF.700@model$validation_metrics@metrics$AUC
TET2.RF.750@model$validation_metrics@metrics$AUC
TET2.RF.800@model$validation_metrics@metrics$AUC
TET2.RF.850@model$validation_metrics@metrics$AUC
TET2.RF.900@model$validation_metrics@metrics$AUC
TET2.RF.950@model$validation_metrics@metrics$AUC
TET2.RF.1000@model$validation_metrics@metrics$AUC

TET2.var.imp.650<-as.data.frame(h2o.varimp(TET2.RF.650))
write.csv(TET2.var.imp.650,file="/export/valenfs/projects/final_results/prokaryotic_riboseq/ribo_seq/matrix/sets_24_11_2016_ecoli_combined/TET2.varimp.650.csv")

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
TET2.stop2stop<-read.csv("/export/valenfs/projects/final_results/prokaryotic_riboseq/ribo_seq/matrix/sets_24_11_2016_ecoli_combined/TET2b.all.stop2.stop.canonical", header=FALSE)

rownames(TET2.stop2stop) <- TET2.stop2stop[, 1]
TET2.stop2stop <- TET2.stop2stop[,-1]
colnames(TET2.stop2stop) <- colnames(TET2.pos.raw)
TET2.stop2stop[,15:55] <- lapply(TET2.stop2stop[,15:55], factor)
TET2.stop2stop[,56:1982] <- lapply(TET2.stop2stop[,56:1982], as.numeric)

TET2.stop2stop.hex <- as.h2o(TET2.stop2stop, destination_frame = "TET2.stop2stop.hex")

TET2.stop2stop.RF.650 <- h2o.predict(TET2.RF.650, TET2.stop2stop.hex)
TET2.stop2stop$pred    <-as.data.frame(TET2.stop2stop.RF.650$predict)[,1]
TET2.stop2stop$pred_pos<-as.data.frame(TET2.stop2stop.RF.650$pos)[,1]
write.csv(TET2.stop2stop,file="/export/valenfs/projects/final_results/prokaryotic_riboseq/ribo_seq/matrix/sets_24_11_2016_ecoli_combined/TET2.stop2stop.predictions.650.csv")

