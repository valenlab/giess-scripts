library (h2o)
library (caret)
h2o.init(nthreads = 1, max_mem_size="100g")
h2o.removeAll()

###################################################################################################

cm2.neg.raw <- read.csv("/export/valenfs/projects/final_results/prokaryotic_riboseq/ribo_seq/matrix/sets_24_11_2016_ecoli_combined/CM2.all.neg.csv", header=TRUE)
cm2.pos.raw <- read.csv("/export/valenfs/projects/final_results/prokaryotic_riboseq/ribo_seq/matrix/sets_24_11_2016_ecoli_combined/CM2.all.pos.csv", header=TRUE)

rownames(cm2.pos.raw) <- cm2.pos.raw[, 1]
cm2.pos.raw <- cm2.pos.raw[,-1]
cm2.pos.raw$seq_1[cm2.pos.raw$seq_1 == TRUE] <-"T"
cm2.pos.raw[,15:55] <- lapply(cm2.pos.raw[,15:55] , factor)
cm2.pos.raw[,56:2392] <- lapply(cm2.pos.raw[,56:2392] , as.numeric)

rownames(cm2.neg.raw) <- cm2.neg.raw[, 1]
cm2.neg.raw <- cm2.neg.raw[,-1]
cm2.neg.raw$seq_1[cm2.neg.raw$seq_1 == TRUE] <-"T"
cm2.neg.raw[,15:55] <- lapply(cm2.neg.raw[,15:55] , factor)
cm2.neg.raw[,56:2392] <- lapply(cm2.neg.raw[,56:2392] , as.numeric)

#the set needs to be randomised
set.seed(4444)
cm2.pos.ran <- cm2.pos.raw[sample(nrow(cm2.pos.raw)),]
cm2.neg.ran <- cm2.neg.raw[sample(nrow(cm2.neg.raw)),]

cm2.pos.train.raw <- cm2.pos.ran[1:1100,]  
cm2.neg.train.raw <- cm2.neg.ran[1:4400,]
cm2.train.raw <- rbind(cm2.pos.train.raw,cm2.neg.train.raw)

cm2.pos.cv.raw <- cm2.pos.ran[1101:1300,]
cm2.neg.cv.raw <- cm2.neg.ran[4401:5200,]
cm2.cv.raw <- rbind(cm2.pos.cv.raw,cm2.neg.cv.raw)

cm2.train.raw$y=as.factor(cm2.train.raw$annotated_start_site)
levels(cm2.train.raw$y) =c('neg','pos')
cm2.cv.raw$y=as.factor(cm2.cv.raw$annotated_start_site)
levels(cm2.cv.raw$y) =c('neg','pos')

cm2.train.raw.hex <- as.h2o(cm2.train.raw, destination_frame="cm2.train.raw.hex")
cm2.cv.raw.hex <- as.h2o(cm2.cv.raw, destination_frame = "cm2.cv.raw.hex")

###################################################################################################

#1,        #Codon
#3,		     #Codon rank
#11:13     #read ratios in window
#14        #ORF FPKM
#15:34  	 #Seq -20 to -1
#38:45     #Seq   3 to 10
#471:486   #-25 to -10 length 30
#495:497   #-1 to +1 length 30
#--
#1291:1306 #-25 to -10 length 50
#1315:1317 #-1 to +1 length 50

cm2.RF.50    <- h2o.randomForest(y = 2393, x =  c(1, 3, 11:34, 38:45, 471:486, 495:497, 512:527, 526:538, 553:568, 577:579, 594:609, 618:620, 635:650, 659:661, 676:691, 700:702, 717:732, 741:743, 758:773, 782:784, 799:814, 823:825, 840:855, 864:866, 881:896, 905:907, 922:937, 946:948, 963:978, 987:989, 1004:1019, 1028:1030, 1045:1060, 1069:1071, 1086:1101, 1110:1112, 1127:1142, 1151:1153, 1168:1183, 1192:1194, 1209:1224, 1233:1235, 1250:1265, 1274:1276, 1291:1306, 1315:1317), training_frame=cm2.train.raw.hex, validation_frame=cm2.cv.raw.hex, ntrees=50, balance_classes = FALSE, seed=7777777)
cm2.RF.100   <- h2o.randomForest(y = 2393, x =  c(1, 3, 11:34, 38:45, 471:486, 495:497, 512:527, 526:538, 553:568, 577:579, 594:609, 618:620, 635:650, 659:661, 676:691, 700:702, 717:732, 741:743, 758:773, 782:784, 799:814, 823:825, 840:855, 864:866, 881:896, 905:907, 922:937, 946:948, 963:978, 987:989, 1004:1019, 1028:1030, 1045:1060, 1069:1071, 1086:1101, 1110:1112, 1127:1142, 1151:1153, 1168:1183, 1192:1194, 1209:1224, 1233:1235, 1250:1265, 1274:1276, 1291:1306, 1315:1317), training_frame=cm2.train.raw.hex, validation_frame=cm2.cv.raw.hex, ntrees=100, balance_classes = FALSE, seed=7777777)
cm2.RF.150   <- h2o.randomForest(y = 2393, x =  c(1, 3, 11:34, 38:45, 471:486, 495:497, 512:527, 526:538, 553:568, 577:579, 594:609, 618:620, 635:650, 659:661, 676:691, 700:702, 717:732, 741:743, 758:773, 782:784, 799:814, 823:825, 840:855, 864:866, 881:896, 905:907, 922:937, 946:948, 963:978, 987:989, 1004:1019, 1028:1030, 1045:1060, 1069:1071, 1086:1101, 1110:1112, 1127:1142, 1151:1153, 1168:1183, 1192:1194, 1209:1224, 1233:1235, 1250:1265, 1274:1276, 1291:1306, 1315:1317), training_frame=cm2.train.raw.hex, validation_frame=cm2.cv.raw.hex, ntrees=150, balance_classes = FALSE, seed=7777777)
cm2.RF.200   <- h2o.randomForest(y = 2393, x =  c(1, 3, 11:34, 38:45, 471:486, 495:497, 512:527, 526:538, 553:568, 577:579, 594:609, 618:620, 635:650, 659:661, 676:691, 700:702, 717:732, 741:743, 758:773, 782:784, 799:814, 823:825, 840:855, 864:866, 881:896, 905:907, 922:937, 946:948, 963:978, 987:989, 1004:1019, 1028:1030, 1045:1060, 1069:1071, 1086:1101, 1110:1112, 1127:1142, 1151:1153, 1168:1183, 1192:1194, 1209:1224, 1233:1235, 1250:1265, 1274:1276, 1291:1306, 1315:1317), training_frame=cm2.train.raw.hex, validation_frame=cm2.cv.raw.hex, ntrees=200, balance_classes = FALSE, seed=7777777)
cm2.RF.250   <- h2o.randomForest(y = 2393, x =  c(1, 3, 11:34, 38:45, 471:486, 495:497, 512:527, 526:538, 553:568, 577:579, 594:609, 618:620, 635:650, 659:661, 676:691, 700:702, 717:732, 741:743, 758:773, 782:784, 799:814, 823:825, 840:855, 864:866, 881:896, 905:907, 922:937, 946:948, 963:978, 987:989, 1004:1019, 1028:1030, 1045:1060, 1069:1071, 1086:1101, 1110:1112, 1127:1142, 1151:1153, 1168:1183, 1192:1194, 1209:1224, 1233:1235, 1250:1265, 1274:1276, 1291:1306, 1315:1317), training_frame=cm2.train.raw.hex, validation_frame=cm2.cv.raw.hex, ntrees=250, balance_classes = FALSE, seed=7777777)
cm2.RF.300   <- h2o.randomForest(y = 2393, x =  c(1, 3, 11:34, 38:45, 471:486, 495:497, 512:527, 526:538, 553:568, 577:579, 594:609, 618:620, 635:650, 659:661, 676:691, 700:702, 717:732, 741:743, 758:773, 782:784, 799:814, 823:825, 840:855, 864:866, 881:896, 905:907, 922:937, 946:948, 963:978, 987:989, 1004:1019, 1028:1030, 1045:1060, 1069:1071, 1086:1101, 1110:1112, 1127:1142, 1151:1153, 1168:1183, 1192:1194, 1209:1224, 1233:1235, 1250:1265, 1274:1276, 1291:1306, 1315:1317), training_frame=cm2.train.raw.hex, validation_frame=cm2.cv.raw.hex, ntrees=300, balance_classes = FALSE, seed=7777777)
cm2.RF.350   <- h2o.randomForest(y = 2393, x =  c(1, 3, 11:34, 38:45, 471:486, 495:497, 512:527, 526:538, 553:568, 577:579, 594:609, 618:620, 635:650, 659:661, 676:691, 700:702, 717:732, 741:743, 758:773, 782:784, 799:814, 823:825, 840:855, 864:866, 881:896, 905:907, 922:937, 946:948, 963:978, 987:989, 1004:1019, 1028:1030, 1045:1060, 1069:1071, 1086:1101, 1110:1112, 1127:1142, 1151:1153, 1168:1183, 1192:1194, 1209:1224, 1233:1235, 1250:1265, 1274:1276, 1291:1306, 1315:1317), training_frame=cm2.train.raw.hex, validation_frame=cm2.cv.raw.hex, ntrees=350, balance_classes = FALSE, seed=7777777)
cm2.RF.400   <- h2o.randomForest(y = 2393, x =  c(1, 3, 11:34, 38:45, 471:486, 495:497, 512:527, 526:538, 553:568, 577:579, 594:609, 618:620, 635:650, 659:661, 676:691, 700:702, 717:732, 741:743, 758:773, 782:784, 799:814, 823:825, 840:855, 864:866, 881:896, 905:907, 922:937, 946:948, 963:978, 987:989, 1004:1019, 1028:1030, 1045:1060, 1069:1071, 1086:1101, 1110:1112, 1127:1142, 1151:1153, 1168:1183, 1192:1194, 1209:1224, 1233:1235, 1250:1265, 1274:1276, 1291:1306, 1315:1317), training_frame=cm2.train.raw.hex, validation_frame=cm2.cv.raw.hex, ntrees=400, balance_classes = FALSE, seed=7777777)
cm2.RF.450   <- h2o.randomForest(y = 2393, x =  c(1, 3, 11:34, 38:45, 471:486, 495:497, 512:527, 526:538, 553:568, 577:579, 594:609, 618:620, 635:650, 659:661, 676:691, 700:702, 717:732, 741:743, 758:773, 782:784, 799:814, 823:825, 840:855, 864:866, 881:896, 905:907, 922:937, 946:948, 963:978, 987:989, 1004:1019, 1028:1030, 1045:1060, 1069:1071, 1086:1101, 1110:1112, 1127:1142, 1151:1153, 1168:1183, 1192:1194, 1209:1224, 1233:1235, 1250:1265, 1274:1276, 1291:1306, 1315:1317), training_frame=cm2.train.raw.hex, validation_frame=cm2.cv.raw.hex, ntrees=450, balance_classes = FALSE, seed=7777777)
cm2.RF.500   <- h2o.randomForest(y = 2393, x =  c(1, 3, 11:34, 38:45, 471:486, 495:497, 512:527, 526:538, 553:568, 577:579, 594:609, 618:620, 635:650, 659:661, 676:691, 700:702, 717:732, 741:743, 758:773, 782:784, 799:814, 823:825, 840:855, 864:866, 881:896, 905:907, 922:937, 946:948, 963:978, 987:989, 1004:1019, 1028:1030, 1045:1060, 1069:1071, 1086:1101, 1110:1112, 1127:1142, 1151:1153, 1168:1183, 1192:1194, 1209:1224, 1233:1235, 1250:1265, 1274:1276, 1291:1306, 1315:1317), training_frame=cm2.train.raw.hex, validation_frame=cm2.cv.raw.hex, ntrees=500, balance_classes = FALSE, seed=7777777)
cm2.RF.550   <- h2o.randomForest(y = 2393, x =  c(1, 3, 11:34, 38:45, 471:486, 495:497, 512:527, 526:538, 553:568, 577:579, 594:609, 618:620, 635:650, 659:661, 676:691, 700:702, 717:732, 741:743, 758:773, 782:784, 799:814, 823:825, 840:855, 864:866, 881:896, 905:907, 922:937, 946:948, 963:978, 987:989, 1004:1019, 1028:1030, 1045:1060, 1069:1071, 1086:1101, 1110:1112, 1127:1142, 1151:1153, 1168:1183, 1192:1194, 1209:1224, 1233:1235, 1250:1265, 1274:1276, 1291:1306, 1315:1317), training_frame=cm2.train.raw.hex, validation_frame=cm2.cv.raw.hex, ntrees=550, balance_classes = FALSE, seed=7777777)
cm2.RF.600   <- h2o.randomForest(y = 2393, x =  c(1, 3, 11:34, 38:45, 471:486, 495:497, 512:527, 526:538, 553:568, 577:579, 594:609, 618:620, 635:650, 659:661, 676:691, 700:702, 717:732, 741:743, 758:773, 782:784, 799:814, 823:825, 840:855, 864:866, 881:896, 905:907, 922:937, 946:948, 963:978, 987:989, 1004:1019, 1028:1030, 1045:1060, 1069:1071, 1086:1101, 1110:1112, 1127:1142, 1151:1153, 1168:1183, 1192:1194, 1209:1224, 1233:1235, 1250:1265, 1274:1276, 1291:1306, 1315:1317), training_frame=cm2.train.raw.hex, validation_frame=cm2.cv.raw.hex, ntrees=600, balance_classes = FALSE, seed=7777777)
cm2.RF.650   <- h2o.randomForest(y = 2393, x =  c(1, 3, 11:34, 38:45, 471:486, 495:497, 512:527, 526:538, 553:568, 577:579, 594:609, 618:620, 635:650, 659:661, 676:691, 700:702, 717:732, 741:743, 758:773, 782:784, 799:814, 823:825, 840:855, 864:866, 881:896, 905:907, 922:937, 946:948, 963:978, 987:989, 1004:1019, 1028:1030, 1045:1060, 1069:1071, 1086:1101, 1110:1112, 1127:1142, 1151:1153, 1168:1183, 1192:1194, 1209:1224, 1233:1235, 1250:1265, 1274:1276, 1291:1306, 1315:1317), training_frame=cm2.train.raw.hex, validation_frame=cm2.cv.raw.hex, ntrees=650, balance_classes = FALSE, seed=7777777)
cm2.RF.700   <- h2o.randomForest(y = 2393, x =  c(1, 3, 11:34, 38:45, 471:486, 495:497, 512:527, 526:538, 553:568, 577:579, 594:609, 618:620, 635:650, 659:661, 676:691, 700:702, 717:732, 741:743, 758:773, 782:784, 799:814, 823:825, 840:855, 864:866, 881:896, 905:907, 922:937, 946:948, 963:978, 987:989, 1004:1019, 1028:1030, 1045:1060, 1069:1071, 1086:1101, 1110:1112, 1127:1142, 1151:1153, 1168:1183, 1192:1194, 1209:1224, 1233:1235, 1250:1265, 1274:1276, 1291:1306, 1315:1317), training_frame=cm2.train.raw.hex, validation_frame=cm2.cv.raw.hex, ntrees=700, balance_classes = FALSE, seed=7777777)
cm2.RF.750   <- h2o.randomForest(y = 2393, x =  c(1, 3, 11:34, 38:45, 471:486, 495:497, 512:527, 526:538, 553:568, 577:579, 594:609, 618:620, 635:650, 659:661, 676:691, 700:702, 717:732, 741:743, 758:773, 782:784, 799:814, 823:825, 840:855, 864:866, 881:896, 905:907, 922:937, 946:948, 963:978, 987:989, 1004:1019, 1028:1030, 1045:1060, 1069:1071, 1086:1101, 1110:1112, 1127:1142, 1151:1153, 1168:1183, 1192:1194, 1209:1224, 1233:1235, 1250:1265, 1274:1276, 1291:1306, 1315:1317), training_frame=cm2.train.raw.hex, validation_frame=cm2.cv.raw.hex, ntrees=750, balance_classes = FALSE, seed=7777777)
cm2.RF.800   <- h2o.randomForest(y = 2393, x =  c(1, 3, 11:34, 38:45, 471:486, 495:497, 512:527, 526:538, 553:568, 577:579, 594:609, 618:620, 635:650, 659:661, 676:691, 700:702, 717:732, 741:743, 758:773, 782:784, 799:814, 823:825, 840:855, 864:866, 881:896, 905:907, 922:937, 946:948, 963:978, 987:989, 1004:1019, 1028:1030, 1045:1060, 1069:1071, 1086:1101, 1110:1112, 1127:1142, 1151:1153, 1168:1183, 1192:1194, 1209:1224, 1233:1235, 1250:1265, 1274:1276, 1291:1306, 1315:1317), training_frame=cm2.train.raw.hex, validation_frame=cm2.cv.raw.hex, ntrees=800, balance_classes = FALSE, seed=7777777)
cm2.RF.850   <- h2o.randomForest(y = 2393, x =  c(1, 3, 11:34, 38:45, 471:486, 495:497, 512:527, 526:538, 553:568, 577:579, 594:609, 618:620, 635:650, 659:661, 676:691, 700:702, 717:732, 741:743, 758:773, 782:784, 799:814, 823:825, 840:855, 864:866, 881:896, 905:907, 922:937, 946:948, 963:978, 987:989, 1004:1019, 1028:1030, 1045:1060, 1069:1071, 1086:1101, 1110:1112, 1127:1142, 1151:1153, 1168:1183, 1192:1194, 1209:1224, 1233:1235, 1250:1265, 1274:1276, 1291:1306, 1315:1317), training_frame=cm2.train.raw.hex, validation_frame=cm2.cv.raw.hex, ntrees=850, balance_classes = FALSE, seed=7777777)
cm2.RF.900   <- h2o.randomForest(y = 2393, x =  c(1, 3, 11:34, 38:45, 471:486, 495:497, 512:527, 526:538, 553:568, 577:579, 594:609, 618:620, 635:650, 659:661, 676:691, 700:702, 717:732, 741:743, 758:773, 782:784, 799:814, 823:825, 840:855, 864:866, 881:896, 905:907, 922:937, 946:948, 963:978, 987:989, 1004:1019, 1028:1030, 1045:1060, 1069:1071, 1086:1101, 1110:1112, 1127:1142, 1151:1153, 1168:1183, 1192:1194, 1209:1224, 1233:1235, 1250:1265, 1274:1276, 1291:1306, 1315:1317), training_frame=cm2.train.raw.hex, validation_frame=cm2.cv.raw.hex, ntrees=900, balance_classes = FALSE, seed=7777777)
cm2.RF.950   <- h2o.randomForest(y = 2393, x =  c(1, 3, 11:34, 38:45, 471:486, 495:497, 512:527, 526:538, 553:568, 577:579, 594:609, 618:620, 635:650, 659:661, 676:691, 700:702, 717:732, 741:743, 758:773, 782:784, 799:814, 823:825, 840:855, 864:866, 881:896, 905:907, 922:937, 946:948, 963:978, 987:989, 1004:1019, 1028:1030, 1045:1060, 1069:1071, 1086:1101, 1110:1112, 1127:1142, 1151:1153, 1168:1183, 1192:1194, 1209:1224, 1233:1235, 1250:1265, 1274:1276, 1291:1306, 1315:1317), training_frame=cm2.train.raw.hex, validation_frame=cm2.cv.raw.hex, ntrees=950, balance_classes = FALSE, seed=7777777)
cm2.RF.1000   <- h2o.randomForest(y = 2393, x =  c(1, 3, 11:34, 38:45, 471:486, 495:497, 512:527, 526:538, 553:568, 577:579, 594:609, 618:620, 635:650, 659:661, 676:691, 700:702, 717:732, 741:743, 758:773, 782:784, 799:814, 823:825, 840:855, 864:866, 881:896, 905:907, 922:937, 946:948, 963:978, 987:989, 1004:1019, 1028:1030, 1045:1060, 1069:1071, 1086:1101, 1110:1112, 1127:1142, 1151:1153, 1168:1183, 1192:1194, 1209:1224, 1233:1235, 1250:1265, 1274:1276, 1291:1306, 1315:1317), training_frame=cm2.train.raw.hex, validation_frame=cm2.cv.raw.hex, ntrees=1000, balance_classes = FALSE, seed=7777777)
cm2.RF.1050   <- h2o.randomForest(y = 2393, x =  c(1, 3, 11:34, 38:45, 471:486, 495:497, 512:527, 526:538, 553:568, 577:579, 594:609, 618:620, 635:650, 659:661, 676:691, 700:702, 717:732, 741:743, 758:773, 782:784, 799:814, 823:825, 840:855, 864:866, 881:896, 905:907, 922:937, 946:948, 963:978, 987:989, 1004:1019, 1028:1030, 1045:1060, 1069:1071, 1086:1101, 1110:1112, 1127:1142, 1151:1153, 1168:1183, 1192:1194, 1209:1224, 1233:1235, 1250:1265, 1274:1276, 1291:1306, 1315:1317), training_frame=cm2.train.raw.hex, validation_frame=cm2.cv.raw.hex, ntrees=1050, balance_classes = FALSE, seed=7777777)
cm2.RF.1100   <- h2o.randomForest(y = 2393, x =  c(1, 3, 11:34, 38:45, 471:486, 495:497, 512:527, 526:538, 553:568, 577:579, 594:609, 618:620, 635:650, 659:661, 676:691, 700:702, 717:732, 741:743, 758:773, 782:784, 799:814, 823:825, 840:855, 864:866, 881:896, 905:907, 922:937, 946:948, 963:978, 987:989, 1004:1019, 1028:1030, 1045:1060, 1069:1071, 1086:1101, 1110:1112, 1127:1142, 1151:1153, 1168:1183, 1192:1194, 1209:1224, 1233:1235, 1250:1265, 1274:1276, 1291:1306, 1315:1317), training_frame=cm2.train.raw.hex, validation_frame=cm2.cv.raw.hex, ntrees=1100, balance_classes = FALSE, seed=7777777)
cm2.RF.1150   <- h2o.randomForest(y = 2393, x =  c(1, 3, 11:34, 38:45, 471:486, 495:497, 512:527, 526:538, 553:568, 577:579, 594:609, 618:620, 635:650, 659:661, 676:691, 700:702, 717:732, 741:743, 758:773, 782:784, 799:814, 823:825, 840:855, 864:866, 881:896, 905:907, 922:937, 946:948, 963:978, 987:989, 1004:1019, 1028:1030, 1045:1060, 1069:1071, 1086:1101, 1110:1112, 1127:1142, 1151:1153, 1168:1183, 1192:1194, 1209:1224, 1233:1235, 1250:1265, 1274:1276, 1291:1306, 1315:1317), training_frame=cm2.train.raw.hex, validation_frame=cm2.cv.raw.hex, ntrees=1150, balance_classes = FALSE, seed=7777777)
cm2.RF.1200   <- h2o.randomForest(y = 2393, x =  c(1, 3, 11:34, 38:45, 471:486, 495:497, 512:527, 526:538, 553:568, 577:579, 594:609, 618:620, 635:650, 659:661, 676:691, 700:702, 717:732, 741:743, 758:773, 782:784, 799:814, 823:825, 840:855, 864:866, 881:896, 905:907, 922:937, 946:948, 963:978, 987:989, 1004:1019, 1028:1030, 1045:1060, 1069:1071, 1086:1101, 1110:1112, 1127:1142, 1151:1153, 1168:1183, 1192:1194, 1209:1224, 1233:1235, 1250:1265, 1274:1276, 1291:1306, 1315:1317), training_frame=cm2.train.raw.hex, validation_frame=cm2.cv.raw.hex, ntrees=1200, balance_classes = FALSE, seed=7777777)
cm2.RF.1250   <- h2o.randomForest(y = 2393, x =  c(1, 3, 11:34, 38:45, 471:486, 495:497, 512:527, 526:538, 553:568, 577:579, 594:609, 618:620, 635:650, 659:661, 676:691, 700:702, 717:732, 741:743, 758:773, 782:784, 799:814, 823:825, 840:855, 864:866, 881:896, 905:907, 922:937, 946:948, 963:978, 987:989, 1004:1019, 1028:1030, 1045:1060, 1069:1071, 1086:1101, 1110:1112, 1127:1142, 1151:1153, 1168:1183, 1192:1194, 1209:1224, 1233:1235, 1250:1265, 1274:1276, 1291:1306, 1315:1317), training_frame=cm2.train.raw.hex, validation_frame=cm2.cv.raw.hex, ntrees=1250, balance_classes = FALSE, seed=7777777)
cm2.RF.1300   <- h2o.randomForest(y = 2393, x =  c(1, 3, 11:34, 38:45, 471:486, 495:497, 512:527, 526:538, 553:568, 577:579, 594:609, 618:620, 635:650, 659:661, 676:691, 700:702, 717:732, 741:743, 758:773, 782:784, 799:814, 823:825, 840:855, 864:866, 881:896, 905:907, 922:937, 946:948, 963:978, 987:989, 1004:1019, 1028:1030, 1045:1060, 1069:1071, 1086:1101, 1110:1112, 1127:1142, 1151:1153, 1168:1183, 1192:1194, 1209:1224, 1233:1235, 1250:1265, 1274:1276, 1291:1306, 1315:1317), training_frame=cm2.train.raw.hex, validation_frame=cm2.cv.raw.hex, ntrees=1300, balance_classes = FALSE, seed=7777777)
cm2.RF.1350   <- h2o.randomForest(y = 2393, x =  c(1, 3, 11:34, 38:45, 471:486, 495:497, 512:527, 526:538, 553:568, 577:579, 594:609, 618:620, 635:650, 659:661, 676:691, 700:702, 717:732, 741:743, 758:773, 782:784, 799:814, 823:825, 840:855, 864:866, 881:896, 905:907, 922:937, 946:948, 963:978, 987:989, 1004:1019, 1028:1030, 1045:1060, 1069:1071, 1086:1101, 1110:1112, 1127:1142, 1151:1153, 1168:1183, 1192:1194, 1209:1224, 1233:1235, 1250:1265, 1274:1276, 1291:1306, 1315:1317), training_frame=cm2.train.raw.hex, validation_frame=cm2.cv.raw.hex, ntrees=1350, balance_classes = FALSE, seed=7777777)
cm2.RF.1400   <- h2o.randomForest(y = 2393, x =  c(1, 3, 11:34, 38:45, 471:486, 495:497, 512:527, 526:538, 553:568, 577:579, 594:609, 618:620, 635:650, 659:661, 676:691, 700:702, 717:732, 741:743, 758:773, 782:784, 799:814, 823:825, 840:855, 864:866, 881:896, 905:907, 922:937, 946:948, 963:978, 987:989, 1004:1019, 1028:1030, 1045:1060, 1069:1071, 1086:1101, 1110:1112, 1127:1142, 1151:1153, 1168:1183, 1192:1194, 1209:1224, 1233:1235, 1250:1265, 1274:1276, 1291:1306, 1315:1317), training_frame=cm2.train.raw.hex, validation_frame=cm2.cv.raw.hex, ntrees=1400, balance_classes = FALSE, seed=7777777)
cm2.RF.1450   <- h2o.randomForest(y = 2393, x =  c(1, 3, 11:34, 38:45, 471:486, 495:497, 512:527, 526:538, 553:568, 577:579, 594:609, 618:620, 635:650, 659:661, 676:691, 700:702, 717:732, 741:743, 758:773, 782:784, 799:814, 823:825, 840:855, 864:866, 881:896, 905:907, 922:937, 946:948, 963:978, 987:989, 1004:1019, 1028:1030, 1045:1060, 1069:1071, 1086:1101, 1110:1112, 1127:1142, 1151:1153, 1168:1183, 1192:1194, 1209:1224, 1233:1235, 1250:1265, 1274:1276, 1291:1306, 1315:1317), training_frame=cm2.train.raw.hex, validation_frame=cm2.cv.raw.hex, ntrees=1450, balance_classes = FALSE, seed=7777777)
cm2.RF.1500   <- h2o.randomForest(y = 2393, x =  c(1, 3, 11:34, 38:45, 471:486, 495:497, 512:527, 526:538, 553:568, 577:579, 594:609, 618:620, 635:650, 659:661, 676:691, 700:702, 717:732, 741:743, 758:773, 782:784, 799:814, 823:825, 840:855, 864:866, 881:896, 905:907, 922:937, 946:948, 963:978, 987:989, 1004:1019, 1028:1030, 1045:1060, 1069:1071, 1086:1101, 1110:1112, 1127:1142, 1151:1153, 1168:1183, 1192:1194, 1209:1224, 1233:1235, 1250:1265, 1274:1276, 1291:1306, 1315:1317), training_frame=cm2.train.raw.hex, validation_frame=cm2.cv.raw.hex, ntrees=1500, balance_classes = FALSE, seed=7777777)

cm2.RF.50@model$training_metrics@metrics$AUC
cm2.RF.100@model$training_metrics@metrics$AUC
cm2.RF.150@model$training_metrics@metrics$AUC
cm2.RF.200@model$training_metrics@metrics$AUC
cm2.RF.250@model$training_metrics@metrics$AUC
cm2.RF.300@model$training_metrics@metrics$AUC
cm2.RF.350@model$training_metrics@metrics$AUC
cm2.RF.400@model$training_metrics@metrics$AUC
cm2.RF.450@model$training_metrics@metrics$AUC
cm2.RF.500@model$training_metrics@metrics$AUC
cm2.RF.550@model$training_metrics@metrics$AUC
cm2.RF.600@model$training_metrics@metrics$AUC
cm2.RF.650@model$training_metrics@metrics$AUC
cm2.RF.700@model$training_metrics@metrics$AUC
cm2.RF.750@model$training_metrics@metrics$AUC
cm2.RF.800@model$training_metrics@metrics$AUC
cm2.RF.850@model$training_metrics@metrics$AUC
cm2.RF.900@model$training_metrics@metrics$AUC
cm2.RF.950@model$training_metrics@metrics$AUC
cm2.RF.1000@model$training_metrics@metrics$AUC
cm2.RF.1050@model$training_metrics@metrics$AUC
cm2.RF.1100@model$training_metrics@metrics$AUC
cm2.RF.1150@model$training_metrics@metrics$AUC
cm2.RF.1200@model$training_metrics@metrics$AUC
cm2.RF.1250@model$training_metrics@metrics$AUC
cm2.RF.1300@model$training_metrics@metrics$AUC
cm2.RF.1350@model$training_metrics@metrics$AUC
cm2.RF.1400@model$training_metrics@metrics$AUC
cm2.RF.1450@model$training_metrics@metrics$AUC
cm2.RF.1500@model$training_metrics@metrics$AUC

cm2.RF.50@model$validation_metrics@metrics$AUC
cm2.RF.100@model$validation_metrics@metrics$AUC
cm2.RF.150@model$validation_metrics@metrics$AUC
cm2.RF.200@model$validation_metrics@metrics$AUC
cm2.RF.250@model$validation_metrics@metrics$AUC
cm2.RF.300@model$validation_metrics@metrics$AUC
cm2.RF.350@model$validation_metrics@metrics$AUC
cm2.RF.400@model$validation_metrics@metrics$AUC
cm2.RF.450@model$validation_metrics@metrics$AUC
cm2.RF.500@model$validation_metrics@metrics$AUC
cm2.RF.550@model$validation_metrics@metrics$AUC
cm2.RF.600@model$validation_metrics@metrics$AUC
cm2.RF.650@model$validation_metrics@metrics$AUC
cm2.RF.700@model$validation_metrics@metrics$AUC
cm2.RF.750@model$validation_metrics@metrics$AUC
cm2.RF.800@model$validation_metrics@metrics$AUC
cm2.RF.850@model$validation_metrics@metrics$AUC
cm2.RF.900@model$validation_metrics@metrics$AUC
cm2.RF.950@model$validation_metrics@metrics$AUC
cm2.RF.1000@model$validation_metrics@metrics$AUC
cm2.RF.1050@model$validation_metrics@metrics$AUC
cm2.RF.1100@model$validation_metrics@metrics$AUC
cm2.RF.1150@model$validation_metrics@metrics$AUC
cm2.RF.1200@model$validation_metrics@metrics$AUC
cm2.RF.1250@model$validation_metrics@metrics$AUC
cm2.RF.1300@model$validation_metrics@metrics$AUC
cm2.RF.1350@model$validation_metrics@metrics$AUC
cm2.RF.1400@model$validation_metrics@metrics$AUC
cm2.RF.1450@model$validation_metrics@metrics$AUC
cm2.RF.1500@model$validation_metrics@metrics$AUC

head(h2o.varimp(cm2.RF.600),n=20)

cm2.var.imp.600<-as.data.frame(h2o.varimp(cm2.RF.600))
write.csv(cm2.var.imp.600,file="/export/valenfs/projects/final_results/prokaryotic_riboseq/ribo_seq/matrix/sets_24_11_2016_ecoli_combined/CM2.varimp.600.csv")

cm2.var.imp.950<-as.data.frame(h2o.varimp(cm2.RF.950))
write.csv(cm2.var.imp.950,file="/export/valenfs/projects/final_results/prokaryotic_riboseq/ribo_seq/matrix/sets_24_11_2016_ecoli_combined/CM2.varimp.950.csv")

###################################################################################################
cm2.stop2stop<-read.csv("/export/valenfs/projects/final_results/prokaryotic_riboseq/ribo_seq/matrix/sets_24_11_2016_ecoli_combined/CM2.all.stop2.stop.canonical", header=FALSE)

rownames(cm2.stop2stop) <- cm2.stop2stop[, 1]
cm2.stop2stop <- cm2.stop2stop[,-1]
cm2.stop2stop[,15:55] <- lapply(cm2.stop2stop[,15:55], factor)
cm2.stop2stop[,56:2392] <- lapply(cm2.stop2stop[,56:2392], as.numeric)
#add headers back to filtered csv
colnames(cm2.stop2stop) <- colnames(cm2.pos.raw)

cm2.stop2stop.hex <- as.h2o(cm2.stop2stop, destination_frame = "cm2.stop2stop.hex")

cm2.stop2stop.RF.600 <- h2o.predict(cm2.RF.600, cm2.stop2stop.hex)
cm2.stop2stop$pred    <-as.data.frame(cm2.stop2stop.RF.600$predict)[,1]
cm2.stop2stop$pred_pos<-as.data.frame(cm2.stop2stop.RF.600$pos)[,1]
write.csv(cm2.stop2stop,file="/export/valenfs/projects/final_results/prokaryotic_riboseq/ribo_seq/matrix/sets_24_11_2016_ecoli_combined/CM2.stop2stop.predictions.600.csv")

cm2.stop2stop.RF.950 <- h2o.predict(cm2.RF.950, cm2.stop2stop.hex)
cm2.stop2stop$pred    <-as.data.frame(cm2.stop2stop.RF.950$predict)[,1]
cm2.stop2stop$pred_pos<-as.data.frame(cm2.stop2stop.RF.950$pos)[,1]
write.csv(cm2.stop2stop,file="/export/valenfs/projects/final_results/prokaryotic_riboseq/ribo_seq/matrix/sets_24_11_2016_ecoli_combined/CM2.stop2stop.predictions.950.csv")




###################################################################################################

