#!/usr/bin/perl -w
#use strict;

##to do 27/07/2016
##script to take a csv matrix and to scale values in rows by the row sum, filter out the lower 10% of genes by expression, and then sum per meta position.

my $csv=$ARGV[0];
my @meta;
my @header;
my $gene_count=0;

#parse once for cds values;
open (IN, $csv) || die;
while (<IN>){
	chomp();
	if (/^gene_id/){
        my @a=split(",");
		#remove gene name and scaling factor
		shift(@a); pop(@a);
		for (@a){
			push (@header,$_);
		}
	}else{

        $gene_count++;

		my @a=split(",");	
		#remove gene name
		my $gene=shift(@a);

		#cds sum is last position in array
		my $cdsSum=pop(@a); #keep so we don't include
		my $rowSum=0;

		#scaling factor is the sum of genes
		for (@a){
			$rowSum+=$_;
		}

        my $count=0;
        for (@a){

#            #added method here to initalise positions 17/10/2016
#            unless ($meta[$count]) {            
#                $meta[$count]=0;
#            }else{
                $meta[$count]+=eval {($_/$rowSum)} || 0;
#            }
                $count++;
		}
	}
}
close (IN);

my $outCount=0;
for (@header){

    my $sum=eval { ($meta[$outCount]/$gene_count) } || 0;    
	print "$_\t$sum\n";   #I could further devide these by the number of sequence (the count...) 
	$outCount++;
}

#print "size=$#header\n";

#output
#meta_pos	#scaled_sum_counts
#-50		1.223
#-49		3.545
