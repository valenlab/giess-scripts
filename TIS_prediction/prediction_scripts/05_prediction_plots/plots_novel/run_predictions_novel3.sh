#!/bin/bash

#script to process predictions

#1) Extract fasta sequenes for motif analysis. Matching anotations. Truncations. Extensions
#	at predicted TISs
#	at annotated TISs

#2) make 3' metaplots. Matching anotations. Truncations. Extensions
#   at predicted TISs
#   at annotated TISs

#3) make 5' metaplots. Matching anotations. Truncations. Extensions
#	at predicted TISs
#	at annotated TISs

#4) Calculate GC content in 3rd codon positions.  Matching anotations. Truncations. Extensions
#	at predicted TISs
#	at annotated TISs

#5) Calcuate free energies with vienna fold. Matching anotations. Truncations. Extensions
#   at predicted TISs
#   at annotated TISs 

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
# input
#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#

usage(){
cat << EOF
usage: $0 options

script to trim and align riboseq fastq datasets 

OPTIONS:
    -p      path to predictions bed
    -n      path to novel predictions bed
    -f      path to genome fasta
    -g      path to genome gtf
    -o      output directory
    -r      3 prime file
    -i      5 prime file
    -s      sam file  
    -h      this help message
EOF
}

while getopts ":p:f:g:o:r:i:s:n:h" opt; do
    case $opt in 
    p)
        pred_bed=$OPTARG
        echo "-p predicted ORFs $OPTARG"
        ;;
    n)
        novel_bed=$OPTARG
        echo "-n novel predicted ORFs $OPTARG"
        ;;
    f)
        genome_fa=$OPTARG
        echo "-f genome fasta $OPTARG"
        ;;
    g)
        genes_gtf=$OPTARG
        echo "-g genome gtf $OPTARG"
        ;;
    o)      
        out_dir=$OPTARG
        echo "-o output dir $OPTARG"
        ;;
    r)
        three_prime=$OPTARG
        echo "-r three prime $OPTARG"
        ;;
    i)  five_prime=$OPTARG
        echo "-i five prime $OPTARG"
        ;;
    s)  
        sam_file=$OPTARG
        echo "-s sam file $OPTARG" 
        ;;
    h)
        usage
        exit
        ;;
    ?) 
        echo "Invalid option: -$OPTARG"
        usage
        exit 1
        ;;
    esac
done

if [ ! -e $pred_bed ]  || [ ! $novel_bed ] || [ ! $out_dir ] || [ ! $genome_fa ] || [ ! $genes_gtf ] || [ ! $three_prime ] || [ ! $five_prime ] || [ ! $sam_file ]; then
                echo "something's missing - check your command lines args"
        usage
        exit 1
fi

if [ ! -d $out_dir ]; then
	mkdir $out_dir
fi

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#1 - Fastas
#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#

if [ ! -d ${out_dir}/fasta ]; then
	mkdir ${out_dir}/fasta
fi

#in relation to predicted and annotated ORFs
nice -n 10 perl fasta_from_bed.pl $genes_gtf $pred_bed $genome_fa ${out_dir}/fasta

nice -n 10 perl fasta_from_bed_novel.pl $genes_gtf $novel_bed $genome_fa ${out_dir}/fasta

echo "Fasta's done"

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#2 - unshifted 3' metaplots
#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#

if [ ! -d ${out_dir}/3_prime ]; then
	mkdir ${out_dir}/3_prime
fi

### In relation to annotated genes

if [ ! -d ${out_dir}/3_prime/annotated ]; then
	mkdir ${out_dir}/3_prime/annotated
fi

nice -n 10 perl gene_list_from_bed.pl $genes_gtf $pred_bed > ${out_dir}/3_prime/annotated/genelist.txt 

grep "Annotated" ${out_dir}/3_prime/annotated/genelist.txt | awk -F, {'print $2'} > ${out_dir}/3_prime/annotated/genes.ann.txt
grep "Truncation" ${out_dir}/3_prime/annotated/genelist.txt | awk -F, {'print $2'} > ${out_dir}/3_prime/annotated/genes.tru.txt
grep "Extension" ${out_dir}/3_prime/annotated/genelist.txt | awk -F, {'print $2'} > ${out_dir}/3_prime/annotated/genes.ext.txt

while read line; do grep ${line}, $three_prime >> ${out_dir}/3_prime/annotated/start_codon_ano.csv; done < ${out_dir}/3_prime/annotated/genes.ann.txt 
while read line; do grep ${line}, $three_prime >> ${out_dir}/3_prime/annotated/start_codon_tru.csv; done < ${out_dir}/3_prime/annotated/genes.tru.txt
while read line; do grep ${line}, $three_prime >> ${out_dir}/3_prime/annotated/start_codon_ext.csv; done < ${out_dir}/3_prime/annotated/genes.ext.txt

sed -i -e '1igene_id,-50,-49,-48,-47,-46,-45,-44,-43,-42,-41,-40,-39,-38,-37,-36,-35,-34,-33,-32,-31,-30,-29,-28,-27,-26,-25,-24,-23,-22,-21,-20,-19,-18,-17,-16,-15,-14,-13,-12,-11,-10,-9,-8,-7,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,247,248,249,250,251,252,253,254,255,256,257,258,259,260,261,262,263,264,265,266,267,268,269,270,271,272,273,274,275,276,277,278,279,280,281,282,283,284,285,286,287,288,289,290,291,292,293,294,295,296,297,298,299,300,301,302,303,304,305,306,307,308,309,310,311,312,313,314,315,316,317,318,319,320,321,322,323,324,325,326,327,328,329,330,331,332,333,334,335,336,337,338,339,340,341,342,343,344,345,346,347,348,349,350,351,352,353,354,355,356,357,358,359,360,361,362,363,364,365,366,367,368,369,370,371,372,373,374,375,376,377,378,379,380,381,382,383,384,385,386,387,388,389,390,391,392,393,394,395,396,397,398,399,400,401,402,403,404,405,406,407,408,409,410,411,412,413,414,415,416,417,418,419,420,421,422,423,424,425,426,427,428,429,430,431,432,433,434,435,436,437,438,439,440,441,442,443,444,445,446,447,448,449,450,451,452,453,454,455,456,457,458,459,460,461,462,463,464,465,466,467,468,469,470,471,472,473,474,475,476,477,478,479,480,481,482,483,484,485,486,487,488,489,490,491,492,493,494,495,496,497,498,499,500,sum\' ${out_dir}/3_prime/annotated/start_codon_ano.csv
sed -i -e '1igene_id,-50,-49,-48,-47,-46,-45,-44,-43,-42,-41,-40,-39,-38,-37,-36,-35,-34,-33,-32,-31,-30,-29,-28,-27,-26,-25,-24,-23,-22,-21,-20,-19,-18,-17,-16,-15,-14,-13,-12,-11,-10,-9,-8,-7,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,247,248,249,250,251,252,253,254,255,256,257,258,259,260,261,262,263,264,265,266,267,268,269,270,271,272,273,274,275,276,277,278,279,280,281,282,283,284,285,286,287,288,289,290,291,292,293,294,295,296,297,298,299,300,301,302,303,304,305,306,307,308,309,310,311,312,313,314,315,316,317,318,319,320,321,322,323,324,325,326,327,328,329,330,331,332,333,334,335,336,337,338,339,340,341,342,343,344,345,346,347,348,349,350,351,352,353,354,355,356,357,358,359,360,361,362,363,364,365,366,367,368,369,370,371,372,373,374,375,376,377,378,379,380,381,382,383,384,385,386,387,388,389,390,391,392,393,394,395,396,397,398,399,400,401,402,403,404,405,406,407,408,409,410,411,412,413,414,415,416,417,418,419,420,421,422,423,424,425,426,427,428,429,430,431,432,433,434,435,436,437,438,439,440,441,442,443,444,445,446,447,448,449,450,451,452,453,454,455,456,457,458,459,460,461,462,463,464,465,466,467,468,469,470,471,472,473,474,475,476,477,478,479,480,481,482,483,484,485,486,487,488,489,490,491,492,493,494,495,496,497,498,499,500,sum\' ${out_dir}/3_prime/annotated/start_codon_tru.csv
sed -i -e '1igene_id,-50,-49,-48,-47,-46,-45,-44,-43,-42,-41,-40,-39,-38,-37,-36,-35,-34,-33,-32,-31,-30,-29,-28,-27,-26,-25,-24,-23,-22,-21,-20,-19,-18,-17,-16,-15,-14,-13,-12,-11,-10,-9,-8,-7,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,247,248,249,250,251,252,253,254,255,256,257,258,259,260,261,262,263,264,265,266,267,268,269,270,271,272,273,274,275,276,277,278,279,280,281,282,283,284,285,286,287,288,289,290,291,292,293,294,295,296,297,298,299,300,301,302,303,304,305,306,307,308,309,310,311,312,313,314,315,316,317,318,319,320,321,322,323,324,325,326,327,328,329,330,331,332,333,334,335,336,337,338,339,340,341,342,343,344,345,346,347,348,349,350,351,352,353,354,355,356,357,358,359,360,361,362,363,364,365,366,367,368,369,370,371,372,373,374,375,376,377,378,379,380,381,382,383,384,385,386,387,388,389,390,391,392,393,394,395,396,397,398,399,400,401,402,403,404,405,406,407,408,409,410,411,412,413,414,415,416,417,418,419,420,421,422,423,424,425,426,427,428,429,430,431,432,433,434,435,436,437,438,439,440,441,442,443,444,445,446,447,448,449,450,451,452,453,454,455,456,457,458,459,460,461,462,463,464,465,466,467,468,469,470,471,472,473,474,475,476,477,478,479,480,481,482,483,484,485,486,487,488,489,490,491,492,493,494,495,496,497,498,499,500,sum\' ${out_dir}/3_prime/annotated/start_codon_ext.csv

#then run as usual:	
nice -n 10 perl meta_plot_matrix_scale.pl ${out_dir}/3_prime/annotated/start_codon_ano.csv > ${out_dir}/3_prime/annotated/ann_start_meta_scaled.csv
nice -n 10 perl meta_plot_matrix_scale.pl ${out_dir}/3_prime/annotated/start_codon_tru.csv > ${out_dir}/3_prime/annotated/tru_start_meta_scaled.csv
nice -n 10 perl meta_plot_matrix_scale.pl ${out_dir}/3_prime/annotated/start_codon_ext.csv > ${out_dir}/3_prime/annotated/ext_start_meta_scaled.csv

####At predictions:

if [ ! -d ${out_dir}/3_prime/predicted ]; then
	mkdir ${out_dir}/3_prime/predicted
fi

prefix=${pred_bed%.bed}
name=$(basename $prefix)

#change to 3'
#add novels
nice -n 10 perl meta_gene_plot_bed_matrix_scaled_three_prime.pl $pred_bed $novel_bed $sam_file ${out_dir}/3_prime/predicted/

perl meta_plot_matrix_scale.pl ${out_dir}/3_prime/predicted/${name}_start_match.csv > ${out_dir}/3_prime/predicted/${name}_start_meta_scaled_match.csv
perl meta_plot_matrix_scale.pl ${out_dir}/3_prime/predicted/${name}_start_truncation.csv > ${out_dir}/3_prime/predicted/${name}_start_meta_scaled_truncation.csv
perl meta_plot_matrix_scale.pl ${out_dir}/3_prime/predicted/${name}_start_elongation.csv > ${out_dir}/3_prime/predicted/${name}_start_meta_scaled_elongation.csv
perl meta_plot_matrix_scale.pl ${out_dir}/3_prime/predicted/${name}_start_novel.csv > ${out_dir}/3_prime/predicted/${name}_start_meta_scaled_novel.csv

echo "Three primes done"

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#2 - unshifted 5' metaplots
#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#

if [ ! -d ${out_dir}/5_prime ]; then
    mkdir ${out_dir}/5_prime
fi

### In relation to annotated genes

if [ ! -d ${out_dir}/5_prime/annotated ]; then
    mkdir ${out_dir}/5_prime/annotated
fi

nice -n 10 perl gene_list_from_bed.pl $genes_gtf $pred_bed > ${out_dir}/5_prime/annotated/genelist.txt

grep "Annotated" ${out_dir}/5_prime/annotated/genelist.txt | awk -F, {'print $2'} > ${out_dir}/5_prime/annotated/genes.ann.txt
grep "Truncation" ${out_dir}/5_prime/annotated/genelist.txt | awk -F, {'print $2'} > ${out_dir}/5_prime/annotated/genes.tru.txt
grep "Extension" ${out_dir}/5_prime/annotated/genelist.txt | awk -F, {'print $2'} > ${out_dir}/5_prime/annotated/genes.ext.txt

while read line; do grep ${line}, $five_prime >> ${out_dir}/5_prime/annotated/start_codon_ano.csv; done < ${out_dir}/5_prime/annotated/genes.ann.txt
while read line; do grep ${line}, $five_prime >> ${out_dir}/5_prime/annotated/start_codon_tru.csv; done < ${out_dir}/5_prime/annotated/genes.tru.txt
while read line; do grep ${line}, $five_prime >> ${out_dir}/5_prime/annotated/start_codon_ext.csv; done < ${out_dir}/5_prime/annotated/genes.ext.txt

sed -i -e '1igene_id,-50,-49,-48,-47,-46,-45,-44,-43,-42,-41,-40,-39,-38,-37,-36,-35,-34,-33,-32,-31,-30,-29,-28,-27,-26,-25,-24,-23,-22,-21,-20,-19,-18,-17,-16,-15,-14,-13,-12,-11,-10,-9,-8,-7,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,247,248,249,250,251,252,253,254,255,256,257,258,259,260,261,262,263,264,265,266,267,268,269,270,271,272,273,274,275,276,277,278,279,280,281,282,283,284,285,286,287,288,289,290,291,292,293,294,295,296,297,298,299,300,301,302,303,304,305,306,307,308,309,310,311,312,313,314,315,316,317,318,319,320,321,322,323,324,325,326,327,328,329,330,331,332,333,334,335,336,337,338,339,340,341,342,343,344,345,346,347,348,349,350,351,352,353,354,355,356,357,358,359,360,361,362,363,364,365,366,367,368,369,370,371,372,373,374,375,376,377,378,379,380,381,382,383,384,385,386,387,388,389,390,391,392,393,394,395,396,397,398,399,400,401,402,403,404,405,406,407,408,409,410,411,412,413,414,415,416,417,418,419,420,421,422,423,424,425,426,427,428,429,430,431,432,433,434,435,436,437,438,439,440,441,442,443,444,445,446,447,448,449,450,451,452,453,454,455,456,457,458,459,460,461,462,463,464,465,466,467,468,469,470,471,472,473,474,475,476,477,478,479,480,481,482,483,484,485,486,487,488,489,490,491,492,493,494,495,496,497,498,499,500,sum\' ${out_dir}/5_prime/annotated/start_codon_ano.csv
sed -i -e '1igene_id,-50,-49,-48,-47,-46,-45,-44,-43,-42,-41,-40,-39,-38,-37,-36,-35,-34,-33,-32,-31,-30,-29,-28,-27,-26,-25,-24,-23,-22,-21,-20,-19,-18,-17,-16,-15,-14,-13,-12,-11,-10,-9,-8,-7,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,247,248,249,250,251,252,253,254,255,256,257,258,259,260,261,262,263,264,265,266,267,268,269,270,271,272,273,274,275,276,277,278,279,280,281,282,283,284,285,286,287,288,289,290,291,292,293,294,295,296,297,298,299,300,301,302,303,304,305,306,307,308,309,310,311,312,313,314,315,316,317,318,319,320,321,322,323,324,325,326,327,328,329,330,331,332,333,334,335,336,337,338,339,340,341,342,343,344,345,346,347,348,349,350,351,352,353,354,355,356,357,358,359,360,361,362,363,364,365,366,367,368,369,370,371,372,373,374,375,376,377,378,379,380,381,382,383,384,385,386,387,388,389,390,391,392,393,394,395,396,397,398,399,400,401,402,403,404,405,406,407,408,409,410,411,412,413,414,415,416,417,418,419,420,421,422,423,424,425,426,427,428,429,430,431,432,433,434,435,436,437,438,439,440,441,442,443,444,445,446,447,448,449,450,451,452,453,454,455,456,457,458,459,460,461,462,463,464,465,466,467,468,469,470,471,472,473,474,475,476,477,478,479,480,481,482,483,484,485,486,487,488,489,490,491,492,493,494,495,496,497,498,499,500,sum\' ${out_dir}/5_prime/annotated/start_codon_tru.csv
sed -i -e '1igene_id,-50,-49,-48,-47,-46,-45,-44,-43,-42,-41,-40,-39,-38,-37,-36,-35,-34,-33,-32,-31,-30,-29,-28,-27,-26,-25,-24,-23,-22,-21,-20,-19,-18,-17,-16,-15,-14,-13,-12,-11,-10,-9,-8,-7,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,247,248,249,250,251,252,253,254,255,256,257,258,259,260,261,262,263,264,265,266,267,268,269,270,271,272,273,274,275,276,277,278,279,280,281,282,283,284,285,286,287,288,289,290,291,292,293,294,295,296,297,298,299,300,301,302,303,304,305,306,307,308,309,310,311,312,313,314,315,316,317,318,319,320,321,322,323,324,325,326,327,328,329,330,331,332,333,334,335,336,337,338,339,340,341,342,343,344,345,346,347,348,349,350,351,352,353,354,355,356,357,358,359,360,361,362,363,364,365,366,367,368,369,370,371,372,373,374,375,376,377,378,379,380,381,382,383,384,385,386,387,388,389,390,391,392,393,394,395,396,397,398,399,400,401,402,403,404,405,406,407,408,409,410,411,412,413,414,415,416,417,418,419,420,421,422,423,424,425,426,427,428,429,430,431,432,433,434,435,436,437,438,439,440,441,442,443,444,445,446,447,448,449,450,451,452,453,454,455,456,457,458,459,460,461,462,463,464,465,466,467,468,469,470,471,472,473,474,475,476,477,478,479,480,481,482,483,484,485,486,487,488,489,490,491,492,493,494,495,496,497,498,499,500,sum\' ${out_dir}/5_prime/annotated/start_codon_ext.csv

#then run as usual: 
nice -n 10 perl meta_plot_matrix_scale.pl ${out_dir}/5_prime/annotated/start_codon_ano.csv > ${out_dir}/5_prime/annotated/ann_start_meta_scaled.csv
nice -n 10 perl meta_plot_matrix_scale.pl ${out_dir}/5_prime/annotated/start_codon_tru.csv > ${out_dir}/5_prime/annotated/tru_start_meta_scaled.csv
nice -n 10 perl meta_plot_matrix_scale.pl ${out_dir}/5_prime/annotated/start_codon_ext.csv > ${out_dir}/5_prime/annotated/ext_start_meta_scaled.csv

####At predictions:

if [ ! -d ${out_dir}/5_prime/predicted ]; then
    mkdir ${out_dir}/5_prime/predicted
fi

prefix=${pred_bed%.bed}
name=$(basename $prefix)

nice -n 10 perl meta_gene_plot_bed_matrix_scaled_five_prime.pl $pred_bed $novel_bed $sam_file ${out_dir}/5_prime/predicted/

perl meta_plot_matrix_scale.pl ${out_dir}/5_prime/predicted/${name}_start_match.csv > ${out_dir}/5_prime/predicted/${name}_start_meta_scaled_match.csv
perl meta_plot_matrix_scale.pl ${out_dir}/5_prime/predicted/${name}_start_truncation.csv > ${out_dir}/5_prime/predicted/${name}_start_meta_scaled_truncation.csv
perl meta_plot_matrix_scale.pl ${out_dir}/5_prime/predicted/${name}_start_elongation.csv > ${out_dir}/5_prime/predicted/${name}_start_meta_scaled_elongation.csv
perl meta_plot_matrix_scale.pl ${out_dir}/5_prime/predicted/${name}_start_novel.csv > ${out_dir}/5_prime/predicted/${name}_start_meta_scaled_novel.csv

echo "Five primes done"

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#3 - GC bias in 3rd codon positions (9bp windows)
#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#

if [ ! -d ${out_dir}/GCbias ]; then
	mkdir ${out_dir}/GCbias
fi

####
#in relation to annotated orfs
####

nice -n 10 perl gcbias_from_fasta_at_third_codon_windows.pl ${out_dir}/fasta/anno_match.fa > ${out_dir}/GCbias/anno.match.csv
nice -n 10 perl gcbias_from_fasta_at_third_codon_windows.pl ${out_dir}/fasta/anno_trunk.fa > ${out_dir}/GCbias/anno.trunc.csv
nice -n 10 perl gcbias_from_fasta_at_third_codon_windows.pl ${out_dir}/fasta/anno_elong.fa > ${out_dir}/GCbias/anno.elong.csv

####
#in relation to predicted orfs
####

nice -n 10 perl gcbias_from_fasta_at_third_codon_windows.pl ${out_dir}/fasta/pred_match.fa > ${out_dir}/GCbias/pred.match.csv
nice -n 10 perl gcbias_from_fasta_at_third_codon_windows.pl ${out_dir}/fasta/pred_trunk.fa > ${out_dir}/GCbias/pred.trunc.csv
nice -n 10 perl gcbias_from_fasta_at_third_codon_windows.pl ${out_dir}/fasta/pred_elong.fa > ${out_dir}/GCbias/pred.elong.csv
nice -n 10 perl gcbias_from_fasta_at_third_codon_windows.pl ${out_dir}/fasta/pred_novel.fa > ${out_dir}/GCbias/pred.novel.csv


echo "Sbias done"

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#4 - vienna fold
#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#

if [ ! -d ${out_dir}/vf ]; then
    mkdir ${out_dir}/vf
fi

if [ ! -d ${out_dir}/vf/tmp_am ];  then
    mkdir ${out_dir}/vf/tmp_am
fi

nice -n 10 perl fasta_windows.pl ${out_dir}/fasta/anno_match.fa ${out_dir}/vf/tmp_am/ 39

for file in ${out_dir}/vf/tmp_am/*.fa; do
    name=$(basename $file);
    prefix=${name%.fa};
    RNAfold --noPS < $file > ${out_dir}/vf/tmp_am/${prefix}_vf.txt;
done

for file in ${out_dir}/vf/tmp_am/*_vf.txt; do
    nice -n 10 perl parse_vf.pl $file >> ${out_dir}/vf/am.csv;
done

###

if [ ! -d ${out_dir}/vf/tmp_ae ];  then
    mkdir ${out_dir}/vf/tmp_ae
fi

nice -n 10 perl fasta_windows.pl ${out_dir}/fasta/anno_elong.fa ${out_dir}/vf/tmp_ae/ 39

for file in ${out_dir}/vf/tmp_ae/*.fa; do
    name=$(basename $file);
    prefix=${name%.fa};
    RNAfold --noPS < $file > ${out_dir}/vf/tmp_ae/${prefix}_vf.txt;
done

for file in ${out_dir}/vf/tmp_ae/*_vf.txt; do
    nice -n 10 perl parse_vf.pl $file >> ${out_dir}/vf/ae.csv;
done

###

if [ ! -d ${out_dir}/vf/tmp_at ];  then
    mkdir ${out_dir}/vf/tmp_at
fi

nice -n 10 perl fasta_windows.pl ${out_dir}/fasta/anno_trunk.fa ${out_dir}/vf/tmp_at/ 39

for file in ${out_dir}/vf/tmp_at/*.fa; do
    name=$(basename $file);
    prefix=${name%.fa};
    RNAfold --noPS < $file > ${out_dir}/vf/tmp_at/${prefix}_vf.txt;
done

for file in ${out_dir}/vf/tmp_at/*_vf.txt; do
    nice -n 10 perl parse_vf.pl $file >> ${out_dir}/vf/at.csv;
done

###

if [ ! -d ${out_dir}/vf/tmp_pm ];  then
    mkdir ${out_dir}/vf/tmp_pm
fi

nice -n 10 perl fasta_windows.pl ${out_dir}/fasta/pred_match.fa ${out_dir}/vf/tmp_pm/ 39

for file in ${out_dir}/vf/tmp_pm/*.fa; do
    name=$(basename $file);
    prefix=${name%.fa};
    RNAfold --noPS < $file > ${out_dir}/vf/tmp_pm/${prefix}_vf.txt;
done

for file in ${out_dir}/vf/tmp_pm/*_vf.txt; do
    nice -n 10 perl parse_vf.pl $file >> ${out_dir}/vf/pm.csv;
done

###

if [ ! -d ${out_dir}/vf/tmp_pe ];  then
    mkdir ${out_dir}/vf/tmp_pe
fi

nice -n 10 perl fasta_windows.pl ${out_dir}/fasta/pred_elong.fa ${out_dir}/vf/tmp_pe/ 39

for file in ${out_dir}/vf/tmp_pe/*.fa; do
    name=$(basename $file);
    prefix=${name%.fa};
    RNAfold --noPS < $file > ${out_dir}/vf/tmp_pe/${prefix}_vf.txt;
done

for file in ${out_dir}/vf/tmp_pe/*_vf.txt; do
    nice -n 10 perl parse_vf.pl $file >> ${out_dir}/vf/pe.csv;
done

###

if [ ! -d ${out_dir}/vf/tmp_pt ];  then
    mkdir ${out_dir}/vf/tmp_pt
fi

nice -n 10 perl fasta_windows.pl ${out_dir}/fasta/pred_trunk.fa ${out_dir}/vf/tmp_pt/ 39

for file in ${out_dir}/vf/tmp_pt/*.fa; do
    name=$(basename $file);
    prefix=${name%.fa};
    RNAfold --noPS < $file > ${out_dir}/vf/tmp_pt/${prefix}_vf.txt;
done

for file in ${out_dir}/vf/tmp_pt/*_vf.txt; do
    nice -n 10 perl parse_vf.pl $file >> ${out_dir}/vf/pt.csv;
done

###

if [ ! -d ${out_dir}/vf/tmp_pn ];  then
    mkdir ${out_dir}/vf/tmp_pn
fi

nice -n 10 perl fasta_windows.pl ${out_dir}/fasta/pred_novel.fa ${out_dir}/vf/tmp_pn/ 39

for file in ${out_dir}/vf/tmp_pn/*.fa; do
    name=$(basename $file);
    prefix=${name%.fa};
    RNAfold --noPS < $file > ${out_dir}/vf/tmp_pn/${prefix}_vf.txt;
done

for file in ${out_dir}/vf/tmp_pn/*_vf.txt; do
    nice -n 10 perl parse_vf.pl $file >> ${out_dir}/vf/pn.csv;
done

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#better plotting
#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#

prefix=${pred_bed%.bed}
name=$(basename $prefix)

nice -n 10 Rscript make_better_plots4.R ${out_dir}/GCbias/anno.match.csv ${out_dir}/GCbias/anno.trunc.csv ${out_dir}/GCbias/anno.elong.csv ${out_dir}/GCbias/pred.match.csv ${out_dir}/GCbias/pred.trunc.csv ${out_dir}/GCbias/pred.elong.csv ${out_dir}/GCbias/pred.novel.csv ${out_dir}/3_prime/annotated/ann_start_meta_scaled.csv ${out_dir}/3_prime/annotated/tru_start_meta_scaled.csv ${out_dir}/3_prime/annotated/ext_start_meta_scaled.csv ${out_dir}/3_prime/predicted/${name}_start_meta_scaled_match.csv ${out_dir}/3_prime/predicted/${name}_start_meta_scaled_truncation.csv ${out_dir}/3_prime/predicted/${name}_start_meta_scaled_elongation.csv ${out_dir}/3_prime/predicted/${name}_start_meta_scaled_novel.csv ${out_dir}/5_prime/annotated/ann_start_meta_scaled.csv ${out_dir}/5_prime/annotated/tru_start_meta_scaled.csv ${out_dir}/5_prime/annotated/ext_start_meta_scaled.csv ${out_dir}/5_prime/predicted/${name}_start_meta_scaled_match.csv ${out_dir}/5_prime/predicted/${name}_start_meta_scaled_truncation.csv ${out_dir}/5_prime/predicted/${name}_start_meta_scaled_elongation.csv ${out_dir}/vf/am.csv ${out_dir}/vf/ae.csv ${out_dir}/vf/at.csv ${out_dir}/vf/pm.csv ${out_dir}/vf/pe.csv ${out_dir}/vf/pt.csv ${out_dir}/vf/pn.csv ${out_dir}/figure2.pdf ${out_dir}/figure2_bars.pdf ${out_dir}/figure3.pdf ${out_dir}/figure4.pdf ${out_dir}/figure5.pdf

