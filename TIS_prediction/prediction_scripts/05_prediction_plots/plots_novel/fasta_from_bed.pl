#!/usr/bin/perl -w
use strict;

#to do 08/07/2016
#script to take a bed file of orf predictions and a fastq file and report start codon usage

my $gtf=$ARGV[0];
my $bed=$ARGV[1]; #of predictions
my $fasta=$ARGV[2];
my $out_dir=$ARGV[3];

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#open fasta
my %fasta_sequences; #key = sequence_name, value=sequence
my $name;
open (FA, $fasta) || die "can't open $fasta";
while (<FA>){
    chomp;
    if (/^>([^\s]+)/){ #take header up to the first space
        $name=$1;
    }else{
        $fasta_sequences{$name}.=$_;
    }
}
close(FA);

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#open gtf file setup genomic hashes, to store start and stop coords per gene, per direction
my %gene_regions_fwd_start;
my %gene_regions_fwd_stop;
my %gene_regions_rev_start;
my %gene_regions_rev_stop;

open(GENES,$gtf) || die "can't open $gtf";        #gtf is 1 based
while (<GENES>){
    unless(/^#/){
        my @b=split("\t");
        my $class=$b[2];
        my $chr=$b[0];
        my $prim5=$b[3];
        my $prim3=$b[4];
        my $dir=$b[6];
        my $gene_id="unknown";
        ($gene_id) = $b[8] =~ /gene_id\s"([^\"]+)";/;

        if ($class eq "CDS"){

            if ($dir eq "+"){ #use start positions as begining of feature
                $gene_regions_fwd_start{$gene_id}=$prim5;
                $gene_regions_fwd_stop{$chr}{$prim3+3}=$gene_id; #+3 because the cds does not include the stop codon
            }else{
                $gene_regions_rev_start{$gene_id}=$prim3;
                $gene_regions_rev_stop{$chr}{$prim5-3}=$gene_id; #-3 becuase the cds does not include the stop codon
            }
        }
    }
}
close(GENES);

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#open bed of predictions
my %match;
my %truncation;
my %elongation;
my %novel;

#windows_in_relation_to_annotated_start
my %gene_match;
my %gene_truncation;
my %gene_elongation;

open (BED, $bed) || die;
while (<BED>){

    unless (/^track/){  #skip header
        my @b=split("\t");
        my $chr=$b[0];
        my $start=$b[1]+1;     #start is zero based
        my $stop=$b[2];
        my $count=$b[4];
        my $dir=$b[5];

        if ($dir eq "+"){
            if (exists ($fasta_sequences{$chr})){ 

                #window in relation to predicition
                my $seq=substr($fasta_sequences{$chr},($start-100),201); #zero based slice (-100 to +100)
              
                #check if the region shares a stop codon with a gene
                if (exists ($gene_regions_fwd_stop{$chr}{$stop})){
                    my $end=$gene_regions_fwd_stop{$chr}{$stop};

                    #start of gene from plot in rlation to anotated start sites
                    my $gene_seq=substr($fasta_sequences{$chr},($gene_regions_fwd_start{$end}-100),201); #zero based slice (-100 to +100)
                    my $gene_name=$gene_regions_fwd_stop{$chr}{$stop};

                    #check if the start matches
                    if ($start == $gene_regions_fwd_start{$end}){
                        $match{$seq}=1;
                        $gene_match{$gene_name}=$seq; #gene_seq gene start matches the predition start

                    #check for extensions
                    }elsif( $start < $gene_regions_fwd_start{$end}){
                        $elongation{$seq}=1;
                        $gene_elongation{$gene_name}=$gene_seq;

                    #otherwise truncation
                    }else{
                        $truncation{$seq}=1;
                        $gene_truncation{$gene_name}=$gene_seq; #this was incorrect

                    }
                }else{
                    #this is a novel orf prediction
                    $novel{$seq}=1;
                }
            }

        #rev
        }else{
            if (exists ($fasta_sequences{$chr})){

                #for rev, start is 3' (stop)
                #stop is 5' (start)

                #fasta window in relation to prediction
                my $seq=reverse(substr($fasta_sequences{$chr},$stop-102,201));  #-100 to + 100 (start_codon = 1)          
                $seq=~tr/ACGTacgt/TGCAtgca/;

                #if the region shares a stop codon with a gene
                if (exists ($gene_regions_rev_stop{$chr}{$start})){

                    my $end=$gene_regions_rev_stop{$chr}{$start};
                   
                    #start of gene from plot in relation to anotated start sites
                    my $gene_seq=reverse(substr($fasta_sequences{$chr},$gene_regions_rev_start{$end}-102,201));  #-100 to + 100 (start_codon = 1)          
                    $gene_seq=~tr/ACGTacgt/TGCAtgca/;
                    my $gene_name=$gene_regions_rev_stop{$chr}{$start};

                    #check if the start matches
                    if ($stop == $gene_regions_rev_start{$end}){
                        $match{$seq}=1;
                        $gene_match{$gene_name}=$gene_seq;

                    #check for extensions
                    }elsif( $stop > $gene_regions_rev_start{$end}){
                        $elongation{$seq}=1;
                        $gene_elongation{$gene_name}=$gene_seq;
                
                    #otherwise truncation
                    }else{
                        $truncation{$seq}=1;
                        $gene_truncation{$gene_name}=$gene_seq;
                    }
                }else{
                    #this is a novel orf prediction
                    $novel{$seq}=1;
                }
            }    
        }
    }
}
close(BED);

#fasta out, start codons in relation to predictions
my $pred_match_out=$out_dir."/pred_match.fa";
my $pred_elong_out=$out_dir."/pred_elong.fa";
my $pred_trunk_out=$out_dir."/pred_trunk.fa";
#my $pred_novel_out=$out_dir."/pred_novel.fa";

#fasta out, start codons in relation to annotations
my $anno_match_out=$out_dir."/anno_match.fa";
my $anno_elong_out=$out_dir."/anno_elong.fa";
my $anno_trunk_out=$out_dir."/anno_trunk.fa";

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#Output1, region -50 to +50 nt of annotated gene TISs, split into matching, truncations, elongations, raw

#windows_in_relation_to_annotated_start
open (A1, ">$anno_match_out") || die;
for my $gene (keys %gene_match){
    print A1 ">$gene\n$gene_match{$gene}\n";
}close(A1);

open (A2, ">$anno_elong_out") || die;
for my $gene (keys %gene_elongation){
    print A2 ">$gene\n$gene_elongation{$gene}\n";
}close(A2);

open (A3, ">$anno_trunk_out") || die;
for my $gene (keys %gene_truncation){
    print A3 ">$gene\n$gene_truncation{$gene}\n";
}close(A3);


#windows_in_relation_to_predicted_start
open (P1, ">$pred_match_out") || die;
my $count=1;
for my $seq (keys %match){
    print P1 ">$count\n$seq\n";
    $count++;
}close(P1);

open (P2, ">$pred_elong_out") || die;
$count=1;
for my $seq (keys %elongation){
    print P2 ">$count\n$seq\n";
    $count++;
}close(P2);

open (P3, ">$pred_trunk_out") || die;
$count=1;
for my $seq (keys %truncation){
    print P3 ">$count\n$seq\n";
    $count++;
}close(P3);

#open (P4, ">$pred_novel_out") || die;
#$count=1;
#for my $seq (keys %novel){
#    print P4 ">$count\n$seq\n";
#    $count++;
#}close(P4);


exit;
#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#


