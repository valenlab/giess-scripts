#!/usr/bin/perl -w
use strict;

#to do 11/10/2016
#script to take a multi fasta file and produce an output matrix of the GC bias for 3rd codon positios 

my $fasta=$ARGV[0]; #regions around the gene starts

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#open fasta file, #assume that the start codons are 1-100nt centered at 51nt 

#for the 100 nt spans 51=ATG = frame0 
print "gene,upstream_GC,downstreamGC\n";
my $gene;

open (FA, $fasta) || die;
while (<FA>){

    if (/^>(.*)$/){
        $gene=$1;
    }else{
        my @seq=split("",$_);

        my $count=0;
        my $upCount=0;
        my $downCount=0;
        my $midway=($#seq-0)/2;
        my $up_nt=0;
        my $down_nt=0;

        while ($count < ($#seq-8)){
 
            for my $pos ($count .. $count+8){
                my $window_start=$count+5;
   #             print "$window_start\n";

                 if ($pos%3 == 2){                   #this position in the window is codon frame 3
                    if ($pos <= $midway){
                        $up_nt++;
                        if ($seq[$pos] eq "C" || $seq[$pos] eq "G" ){   
                            $upCount++;
                        }
                   }else{ 
                        $down_nt++;
                        if ($seq[$pos] eq "C" || $seq[$pos] eq "G" ){   
                            $downCount++;
                        }
                    }
                }
            }  
            $count++;
        }        
        #print "$gene,".($upCount/$up_nt).",".($up_nt/8).",".($downCount/$down_nt).",".($down_nt/8)."\n";
        print "$gene,".($upCount/$up_nt).",".($downCount/$down_nt)."\n";
    }
}

exit;
