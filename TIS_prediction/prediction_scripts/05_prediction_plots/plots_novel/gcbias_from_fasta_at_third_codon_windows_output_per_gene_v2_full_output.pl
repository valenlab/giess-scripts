#!/usr/bin/perl -w
use strict;

#to do 11/10/2016
#script to take a multi fasta file and produce an output matrix of the GC bias for 3rd codon positios 

my $fasta=$ARGV[0]; #regions around the gene starts

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#open fasta file, #assume that the start codons are 1-100nt centered at 51nt 

#for the 100 nt spans 51=ATG = frame0 
#print "gene,upstream_GC,downstreamGC\n";
#print "gene,5,8,11,14,17,20,23,26,29,32,35,38,41,44,47,50,53,56,59,62,65,68,71,74,77,80,83,86,89,92,95,98,101,104,107,110,113,116,119,122,125,128,131,134,137,140,143,146,149,152,155,158,161,164,167,170,173,176,179,182,185,188,191,194,197";
print "gene,-96,-93,-90,-87,-84,-81,-78,-75,-72,-69,-66,-63,-60,-57,-54,-51,-48,-45,-42,-39,-36,-33,-30,-27,-24,-21,-18,-15,-12,-9,-6,-3,0,3,6,9,12,15,18,21,24,27,30,33,36,39,42,45,48,51,54,57,60,63,66,69,72,75,78,81,84,87,90,93,96\n";

my $gene;
my $matrix;

open (FA, $fasta) || die;
while (<FA>){

    if (/^>(.*)$/){
        $gene=$1;
        print "$gene"
    }else{
        my @seq=split("",$_);
        my $count=0;
        while ($count < ($#seq-8)){

            my $windowGC=0; 
            for my $pos ($count .. $count+8){
                my $window_start=$count+5;

                 if ($pos%3 == 2){                   #this position in the window is codon frame 3
                    if ($seq[$pos] eq "C" || $seq[$pos] eq "G" ){   
                        $windowGC++;
                    }
                }
            }  
            if ($count%3==0){
                my $windowRatio=$windowGC/3;
                print ",$windowRatio";
            }
            $count++;
        }
        print "\n";        
    }
}

exit;
