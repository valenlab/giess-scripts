#!/usr/bin/perl -w
use strict;

#to do 11/10/2016
#script to take a multi fasta file and produce an output matrix of the GC bias for 3rd codon positios 

my $fasta=$ARGV[0]; #regions around the gene starts

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#open fasta file, #assume that tghe start codons are 1-100nt centered at 51nt 

#for the 100 nt spans 51=ATG = frame0 
my %matrix;
my %window_sum_GC;
my $seq_count; #for normalisation;
my $gene;

open (FA, $fasta) || die;
while (<FA>){

    if (/^>(.*)$/){
        $gene=$1;
    }else{
        my @seq=split("",$_);

        my $count=0;
        while ($count < ($#seq-8)){
 
            for my $pos ($count .. $count+8){
                my $window_start=$count+5;
   #             print "$window_start\n";

                if ($seq[$pos] eq "C" || $seq[$pos] eq "G" ){   
                    if ($pos%3 == 2){                   #this position in the window is codon frame 3
                        $matrix{$window_start}++;
                    }
                }
            }  
            $count++; 
        }        
        $seq_count++;
    }
}

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
print "position,frame3_GC\n";     #header

for my $window_position (sort {$a <=> $b} keys %matrix){
    my $out_pos=$window_position+1;
    my $sum=(((0+$matrix{$window_position})/$seq_count)/3);
    if ($out_pos%3==0 ){
        print "$out_pos,$sum\n";
    }
}

exit;
