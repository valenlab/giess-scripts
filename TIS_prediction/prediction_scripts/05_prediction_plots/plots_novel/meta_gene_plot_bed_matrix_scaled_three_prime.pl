#!/usr/bin/perl -w
use strict;

#to do 02/11/2016
#script to take gtf get coords around start/stop codons, and assign counts to them from a bed (n termini peptides for example)
#scale each
#gene, so that the reads coming from it sum to 1 (disparding the drop bottom 10% of genes on CDS expression)
#where a read overlaps multiple in strand genes counts it towards all those genes

#input files:
#gtf exons, start, stop codons
my $bed_file=$ARGV[0];
my $novel_bed_file=$ARGV[1];
my $sam_file=$ARGV[2];
my $outDir=$ARGV[3];
my ($prefix)=$bed_file=~/([^\/]+).bed$/;

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#open bed of predictions

my %start_ann_fwd;
my %start_ann_rev;
my %start_trk_fwd;
my %start_trk_rev;
my %start_elo_fwd;
my %start_elo_rev; #key1 = chr, key2 = position, value = gene_id(s)

my %startM_ann; #key1 = gene_id, key2=metapos, key3=value
my %startM_trk; #key1 = gene_id, key2=metapos, key3=value
my %startM_elo; #key1 = gene_id, key2=metapos, key3=value

my $bed_count=0;

open (BED, $bed_file) || die "can't open $bed_file";      #bed is O based at start, 1 based at end
while (<BED>){
    unless (/^track/){  #skip header
        my @b=split("\t");
        my $chr=$b[0];
        my $start=$b[1]+1;     #start is zero bases
        my $stop=$b[2];
        my $class=$b[3];    #Annotated,Truncation,Extension
        my $count=$b[4];
        my $dir=$b[5];
              
        $bed_count++;

        my $gene_id=$chr."_".$bed_count;      
        #assign to metaplots 
        if ($dir eq "+"){    

            if ($class eq "Annotated"){
                my $cdsLength=-51;    
                for (($start-50) .. ($start+500)){ #50bp upstream, 100bp downstream
                    $cdsLength+=1;    
                    if (exists ($start_ann_fwd{$chr}{$_})){
                        $start_ann_fwd{$chr}{$_}.=",".$gene_id.";".$cdsLength; 
                    }else{
                        $start_ann_fwd{$chr}{$_}=$gene_id.";".$cdsLength;
                    }
                    $startM_ann{$gene_id}{$cdsLength}=0;  #initalise
                }    
            }

            if ($class eq "Truncation"){
                my $cdsLength=-51;    
                for (($start-50) .. ($start+500)){ #50bp upstream, 100bp downstream
                    $cdsLength+=1;    
                    if (exists ($start_trk_fwd{$chr}{$_})){
                        $start_trk_fwd{$chr}{$_}.=",".$gene_id.";".$cdsLength; 
                    }else{
                        $start_trk_fwd{$chr}{$_}=$gene_id.";".$cdsLength;
                    }
                    $startM_trk{$gene_id}{$cdsLength}=0;  #initalise
                }
            }

            if ($class eq "Extension"){
                my $cdsLength=-51;    
                for (($start-50) .. ($start+500)){ #50bp upstream, 100bp downstream
                    $cdsLength+=1;    
                    if (exists ($start_elo_fwd{$chr}{$_})){
                        $start_elo_fwd{$chr}{$_}.=",".$gene_id.";".$cdsLength; 
                    }else{
                        $start_elo_fwd{$chr}{$_}=$gene_id.";".$cdsLength;
                    }
                    $startM_elo{$gene_id}{$cdsLength}=0;  #initalise
                }
            }
        }elsif($dir eq "-"){

            if ($class eq "Annotated"){
                my $upstream=$stop+50;
                my $downstream=$stop-500;
                my $cdsLength=-51;
                while ($upstream >= $downstream){ #50bp upstream, 100bp downstream
                    $cdsLength+=1;
                    if (exists ($start_ann_rev{$chr}{$_})){
                        $start_ann_rev{$chr}{$_}.=",".$gene_id.";".$cdsLength;
                    }else{
                        $start_ann_rev{$chr}{$_}=$gene_id.";".$cdsLength;
                    }
                    $startM_ann{$gene_id}{$cdsLength}=0;  #initialise
                    $upstream--;
                }   
            }

            if ($class eq "Truncation"){
                my $upstream=$stop+50;
                my $downstream=$stop-500;
                my $cdsLength=-51;
                while ($upstream >= $downstream){ #50bp upstream, 100bp downstream
                    $cdsLength+=1;
                    if (exists ($start_trk_rev{$chr}{$_})){
                        $start_trk_rev{$chr}{$_}.=",".$gene_id.";".$cdsLength;
                    }else{
                        $start_trk_rev{$chr}{$_}=$gene_id.";".$cdsLength;
                    }
                    $startM_trk{$gene_id}{$cdsLength}=0;  #initialise
                    $upstream--;
                }
            }

            if ($class eq "Extension"){
                my $upstream=$stop+50;
                my $downstream=$stop-500;
                my $cdsLength=-51;
                while ($upstream >= $downstream){ #50bp upstream, 100bp downstream
                    $cdsLength+=1;
                    if (exists ($start_elo_rev{$chr}{$_})){
                        $start_elo_rev{$chr}{$_}.=",".$gene_id.";".$cdsLength;
                    }else{
                        $start_elo_rev{$chr}{$_}=$gene_id.";".$cdsLength;
                    }
                    $startM_elo{$gene_id}{$cdsLength}=0;  #initialise
                    $upstream--;
                }
            }
        }
    }
}
close(BED);

print "bed parsed\n";

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#open bed of novel

my %start_new_fwd;
my %start_new_rev;
my %startM_new; #key1 = gene_id, key2=metapos, key3=value

$bed_count=0;

open (BED, $novel_bed_file) || die "can't open $novel_bed_file";      #bed is O based at start, 1 based at end
while (<BED>){
    unless (/^track/){  #skip header
        my @b=split("\t");
        my $chr=$b[0];
        my $start=$b[1]+1;     #start is zero bases
        my $stop=$b[2];
        my $class=$b[3];    #Annotated,Truncation,Extension
        my $count=$b[4];
        my $dir=$b[5];

        $bed_count++;

        my $gene_id=$chr."_".$bed_count;
        #assign to metaplots 
        if ($dir eq "+"){

            if ($class eq "Novel"){
                my $cdsLength=-51;
                for (($start-50) .. ($start+500)){ #50bp upstream, 100bp downstream
                    $cdsLength+=1;
                    if (exists ($start_new_fwd{$chr}{$_})){
                        $start_new_fwd{$chr}{$_}.=",".$gene_id.";".$cdsLength;
                    }else{
                        $start_new_fwd{$chr}{$_}=$gene_id.";".$cdsLength;
                    }
                    $startM_new{$gene_id}{$cdsLength}=0;  #initalise
                }
            }
        }elsif($dir eq "-"){

            if ($class eq "Novel"){
                my $upstream=$stop+50;
                my $downstream=$stop-500;
                my $cdsLength=-51;
                while ($upstream >= $downstream){ #50bp upstream, 100bp downstream
                    $cdsLength+=1;
                    if (exists ($start_new_rev{$chr}{$_})){
                        $start_new_rev{$chr}{$_}.=",".$gene_id.";".$cdsLength;
                    }else{
                        $start_new_rev{$chr}{$_}=$gene_id.";".$cdsLength;
                    }
                    $startM_new{$gene_id}{$cdsLength}=0;  #initialise
                    $upstream--;
                }
            }
        }
    }
}
close(BED);

print "novel bed parsed\n";

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#open sam file

open(SAM, $sam_file) || die "can't open $sam_file";      #sam is 1 based
while (<SAM>){
    #skip headers
    unless(/^#/){
        my @b=split("\t");
        my $leftMost=$b[3]; #leftmost position of match 5' for fwd, 3' for rev
        my $flag=$b[1];
        my $chr=$b[2];
        my $mapq=$b[4];
        my $cig=$b[5];
        my $seq=$b[9];
        my $fivePrime;
        #mapping uniqness filter
        if ($mapq >= 10){
            unless ($flag & 0x4){   #if aligned
 
               if ($flag & 0x10){  #if rev calculate 5' from 

                    $fivePrime=$leftMost;  #this is actually three prime

                    #assign to metaplots
                    if (exists ($start_ann_rev{$chr}{$fivePrime})){
                        my @over1=split(",",$start_ann_rev{$chr}{$fivePrime});
                        for (@over1){
                            my ($gene,$meta_pos)=$_=~/(^[^;]+);(\d+)$/;  #/(\w+)_(-?\w+)/;
                            $startM_ann{$gene}{$meta_pos}+=1;
                        }
                    }

                    if (exists ($start_trk_rev{$chr}{$fivePrime})){
                        my @over1=split(",",$start_trk_rev{$chr}{$fivePrime});
                        for (@over1){
                            my ($gene,$meta_pos)=$_=~/(^[^;]+);(\d+)$/;  #/(\w+)_(-?\w+)/;
                            $startM_trk{$gene}{$meta_pos}+=1;
                        }
                    }

                    if (exists ($start_elo_rev{$chr}{$fivePrime})){
                        my @over1=split(",",$start_elo_rev{$chr}{$fivePrime});
                        for (@over1){
                            my ($gene,$meta_pos)=$_=~/(^[^;]+);(\d+)$/;  #/(\w+)_(-?\w+)/;
                            $startM_elo{$gene}{$meta_pos}+=1;
                        }
                    }

                    if (exists ($start_new_rev{$chr}{$fivePrime})){
                        my @over1=split(",",$start_new_rev{$chr}{$fivePrime});
                        for (@over1){
                            my ($gene,$meta_pos)=$_=~/(^[^;]+);(\d+)$/;  #/(\w+)_(-?\w+)/;
                            $startM_new{$gene}{$meta_pos}+=1;
                        }
                    }

                }else{ #if fwd this is easy

                    #parse cigar for indels and adjust the length of the alignment             
                    my $length=length($seq);
                    while ($cig =~/(\d+)I/g){   #add to length for insertions
                        $length+=$1;
                    }
                    while ($cig =~/(\d+)D/g){   #substact from length for deletions
                        $length-=$1;
                    }
                    $fivePrime=$leftMost+$length-1;   #sam is 1 based          #this is acutally three prime

                    #assign to metaplots
                    if (exists ($start_ann_fwd{$chr}{$fivePrime})){
                        my @over4=split(",",$start_ann_fwd{$chr}{$fivePrime});
                        for (@over4){
                            my ($gene,$meta_pos)=$_=~/(^[^;]+);(-?\w+$)/;
                            $startM_ann{$gene}{$meta_pos}+=1;
                        }
                    }

                    if (exists ($start_trk_fwd{$chr}{$fivePrime})){
                        my @over4=split(",",$start_trk_fwd{$chr}{$fivePrime});
                        for (@over4){
                            my ($gene,$meta_pos)=$_=~/(^[^;]+);(-?\w+$)/;
                            $startM_trk{$gene}{$meta_pos}+=1;
                        }
                    }

                    if (exists ($start_elo_fwd{$chr}{$fivePrime})){
                        my @over4=split(",",$start_elo_fwd{$chr}{$fivePrime});
                        for (@over4){
                            my ($gene,$meta_pos)=$_=~/(^[^;]+);(-?\w+$)/;
                            $startM_elo{$gene}{$meta_pos}+=1;
                        }
                    }

                    if (exists ($start_new_fwd{$chr}{$fivePrime})){
                        my @over4=split(",",$start_new_fwd{$chr}{$fivePrime});
                        for (@over4){
                            my ($gene,$meta_pos)=$_=~/(^[^;]+);(-?\w+$)/;
                            $startM_new{$gene}{$meta_pos}+=1;
                        }
                    }
                }
            }
        }
    }
}
close (SAM);

print "5' sam parsed\n";

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
my $outStart_ann=$outDir."/".$prefix."_start_match.csv";
my $outStart_trk=$outDir."/".$prefix."_start_truncation.csv";
my $outStart_elo=$outDir."/".$prefix."_start_elongation.csv";
my $outStart_new=$outDir."/".$prefix."_start_novel.csv";

#calcuate cdssum as window sums!!! or skip the 10% filter in the next step!

open (OUT1,">$outStart_ann") || die "can't open $outStart_ann\n";
open (OUT2,">$outStart_trk") || die "can't open $outStart_trk\n";
open (OUT3,">$outStart_elo") || die "can't open $outStart_elo\n";
open (OUT4,">$outStart_new") || die "can't open $outStart_new\n";

#meta plots output per gene
print OUT1 "gene_id"; for (-50 .. 500){     print OUT1 ",$_"; } print OUT1 "\n"; #header
for my $gene (sort keys %startM_ann){
    print OUT1 "$gene";
    for my $pos (sort {$a <=> $b} keys %{$startM_ann{$gene}}){
        print OUT1 ",$startM_ann{$gene}{$pos}";
    }
    print OUT1 "\n";
}
close(OUT1);


print OUT2 "gene_id"; for (-50 .. 500){     print OUT2 ",$_"; } print OUT2 "\n"; #header
for my $gene (sort keys %startM_trk){
    print OUT2 "$gene";
    for my $pos (sort {$a <=> $b} keys %{$startM_trk{$gene}}){
        print OUT2 ",$startM_trk{$gene}{$pos}";
    }
    print OUT2 "\n";
}
close(OUT2);


print OUT3 "gene_id"; for (-50 .. 500){     print OUT3 ",$_"; } print OUT3 "\n"; #header
for my $gene (sort keys %startM_elo){
    print OUT3 "$gene";
    for my $pos (sort {$a <=> $b} keys %{$startM_elo{$gene}}){
        print OUT3 ",$startM_elo{$gene}{$pos}";
    }
    print OUT3 "\n";
}
close(OUT3);


print OUT4 "gene_id"; for (-50 .. 500){     print OUT4 ",$_"; } print OUT4 "\n"; #header
for my $gene (sort keys %startM_new){
    print OUT4 "$gene";
    for my $pos (sort {$a <=> $b} keys %{$startM_new{$gene}}){
        print OUT4 ",$startM_new{$gene}{$pos}";
    }
    print OUT4 "\n";
}
close(OUT4);

exit;
