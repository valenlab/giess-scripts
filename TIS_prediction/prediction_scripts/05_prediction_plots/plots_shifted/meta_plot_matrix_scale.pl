#!/usr/bin/perl -w
#use strict;

##to do 29/11/2016
##script to take a csv matrix and to sum per meta position.

my $csv=$ARGV[0];
my @meta;
my @header;
my $gene_count=0;

#parse once for cds values;
open (IN, $csv) || die;
while (<IN>){
	chomp();
	if (/^gene_id/){
        my @a=split(",");
		#remove gene name and scaling factor
		shift(@a);
		for (@a){
			push (@header,$_);
		}
	}else{

        $gene_count++;

		my @a=split(",");	
		#remove gene name
		my $gene=shift(@a);

		my $rowSum=0;

		#scaling factor is the sum of genes
		for (@a){
			$rowSum+=$_;
		}

        my $count=0;
        for (@a){
            $meta[$count]+=eval {($_/$rowSum)} || 0;
            $count++;
		}
	}
}
close (IN);

my $outCount=0;
for (@header){
    my $sum=eval { ($meta[$outCount]/$gene_count) } || 0;    
	print "$_\t$sum\n";   
	$outCount++;
}
