#!/bin/bash

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
# input
#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#

usage(){
cat << EOF
usage: $0 options

script to trim and align riboseq fastq datasets 

OPTIONS:
    -p      path to predictions bed
    -g      path to genome gtf
    -o      output directoy
    -f      fwd shoelaces wig
    -r      rev shoelaced wig  
    -h      this help message
EOF
}

while getopts ":p:g:o:f:r:h" opt; do
    case $opt in
    p)
        pred_bed=$OPTARG
        echo "-p predicted ORFs $OPTARG"
        ;;
    g)
        genes_gtf=$OPTARG
        echo "-g genome gtf $OPTARG"
        ;;
    o)
        out_dir=$OPTARG
        echo "-o output dir $OPTARG"
        ;;
    f)
        fwd_wig=$OPTARG
        echo "-f forward wig $OPTARG"
        ;;
    r)
        rev_wig=$OPTARG
        echo "-r reverse wig $OPTARG" 
        ;;
    h)
        usage
        exit
        ;;
    ?)
        echo "Invalid option: -$OPTARG"
        usage
        exit 1
        ;;
    esac
done

if [ ! -e $pred_bed ] || [ ! $out_dir ] || [ ! $genes_gtf ] || [ ! $fwd_wig ] || [ ! $rev_wig ]; then
        echo "something's missing - check your command lines args"
        usage
        exit 1
fi

if [ ! -d $out_dir ]; then
    mkdir $out_dir
fi

#------------------------------------------------------------------------------------------
#1 get counts and prediodicity 
#------------------------------------------------------------------------------------------

nice -n 10 perl meta_gene_plots_shifted_wigs.pl $genes_gtf $pred_bed $fwd_wig $rev_wig $out_dir

nice -n 10 perl meta_plot_matrix_scale.pl ${out_dir}/annotated_matching.csv > ${out_dir}/annotated_matching_scaled.csv 
nice -n 10 perl meta_plot_matrix_scale.pl ${out_dir}/annotated_elongation.csv > ${out_dir}/annotated_elongation_scaled.csv
nice -n 10 perl meta_plot_matrix_scale.pl ${out_dir}/annotated_truncation.csv > ${out_dir}/annotated_truncation_scaled.csv
nice -n 10 perl meta_plot_matrix_scale.pl ${out_dir}/predicted_matching.csv > ${out_dir}/predicted_matching_scaled.csv
nice -n 10 perl meta_plot_matrix_scale.pl ${out_dir}/predicted_elongation.csv > ${out_dir}/predicted_elongation_scaled.csv
nice -n 10 perl meta_plot_matrix_scale.pl ${out_dir}/predicted_truncation.csv > ${out_dir}/predicted_truncation_scaled.csv


#------------------------------------------------------------------------------------------ 
#2 Make metagene plots
#------------------------------------------------------------------------------------------

Rscript make_plots.R ${out_dir}/annotated_matching_scaled.csv ${out_dir}/annotated_truncation_scaled.csv ${out_dir}/annotated_elongation_scaled.csv ${out_dir}/predicted_matching_scaled.csv ${out_dir}/predicted_truncation_scaled.csv ${out_dir}/predicted_elongation_scaled.csv ${out_dir}/periodicity_truncation_upstream.csv ${out_dir}/periodicity_truncation_between.csv ${out_dir}/periodicity_truncation_downstream.csv ${out_dir}/periodicity_elongation_upstream.csv ${out_dir}/periodicity_elongation_between.csv ${out_dir}/periodicity_elongation_downstream.csv ${out_dir}/figure3.shifted.pdf ${out_dir}/figure3.shifted.periodicity.pdf
