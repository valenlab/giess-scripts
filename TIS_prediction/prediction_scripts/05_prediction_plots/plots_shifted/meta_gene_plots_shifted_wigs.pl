#!/usr/bin/perl -w
use strict;

#to do 25/11/2016
#script to take gtf get coords around start codons, and assign counts to them from wig

#input files:
#gtf exons, start, stop codons
my $gtf=$ARGV[0];
my $predicted_bed=$ARGV[1];
my $wig_fwd=$ARGV[2];
my $wig_rev=$ARGV[3];
my $outDir=$ARGV[4];

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#open forward wig file
my %fwd_wig_counts; #chr,pos=count
my $chr;

open (FWD, $wig_fwd) || die "can't open $wig_fwd";      #wig is 1 based
while (<FWD>){

    chomp;
    if (/^variableStep\schrom=(.*)$/){
        $chr=$1
    }else{
        my @w=split("\t");
        my $pos=$w[0];                        #shoelaces wigs could be 0 based
        my $count=$w[1];
        $fwd_wig_counts{$chr}{$pos}=$count; 
    }
}
close (FWD);

print "Forward wig parsed\n";

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#open rev wig file
my %rev_wig_counts; #chr,pos=count

open (REV, $wig_rev) || die "can't open $wig_rev";      #wig is 1 based
while (<REV>){

    chomp;
    if (/^variableStep\schrom=(.*)$/){
        $chr=$1
    }else{ 
        my @w=split("\t");
        my $pos=$w[0];                #shoelaces wigs might be 0 based
        my $count=$w[1];
        $rev_wig_counts{$chr}{$pos}=$count;
    }
}
close (REV);

print "Reverse wig parsed\n";

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#open gtf file setup genomic hashes, to store start and stop coords per gene, per direction
my %gene_regions_fwd_start;
my %gene_regions_rev_start;
my %stops_fwd; #chr,pos,gene=1
my %stops_rev; #chr,pos,gene=1;

open(GENES,$gtf) || die "can't open $gtf";        #gtf is 1 based
while (<GENES>){
    unless(/^#/){
        my @b=split("\t");
        my $class=$b[2];
        my $chr=$b[0];
        my $prim5=$b[3];
        my $prim3=$b[4];
        my $dir=$b[6];
        my $gene_id="unknown";
        ($gene_id) = $b[8] =~ /gene_id\s"([^\"]+)";/;
        if ($class eq "CDS"){
            if ($dir eq "+"){ #use start positions as begining of feature
                $gene_regions_fwd_start{$gene_id}=$prim5;
                $stops_fwd{$chr}{$prim3+3}=$gene_id;
            }else{
                $gene_regions_rev_start{$gene_id}=$prim3;
                $stops_rev{$chr}{$prim5-3}=$gene_id;
            }
        }
    }
}
close(GENES);

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#open bed of predictions

#windows:
#1) Annotated Match
#2) Annotated Elongation
#3) Annotated Truncation
#4) Predicted Match
#5) Predicted Elongation
#6) Predicted Truncation

#7) upstream elongation
#8) middle elongation
#9) downstream elongation
#10) upstream truncation
#11) middle truncation
#12) downstream truncation #		do we want these for anotated and predicted seperatly??  No!

my %anno_match; #gene, offset, count
my %anno_elo;
my %anno_tru;
my %pred_match;
my %pred_elo;
my %pred_tru;  

my %elong_upstream;     #upstream of predicted TIS
my %elong_middle;       #between predicted and annotated TIS
my %elong_downstream;   #downstream of annotated TIS

my %trunc_upstream;     #upstream of annotated TIS
my %trunc_middle;       #between annotated and predicted TIS
my %trunc_downstream;   #downstream of annotated TIS

open (BED, $predicted_bed) || die "can't open $predicted_bed";      #bed is O based at start, 1 based at end
while (<BED>){
    unless (/^track/){  #skip header
        my @b=split("\t");
        my $chr=$b[0];
        my $start=$b[1]+1;     #start is zero bases
        my $stop=$b[2];
        my $class=$b[3];    #Annotated,Truncation,Extension
        my $count=$b[4];
        my $dir=$b[5];

        if ($dir eq "+"){

            if ($class eq "Annotated"){
                if (exists ($stops_fwd{$chr}{$stop})){
                    my $gene_id=$stops_fwd{$chr}{$stop};
                    my $geneStart=$gene_regions_fwd_start{$gene_id};
                    for my $count (-29 .. 30){   #formerly -50 to 100
                        my $anno_count=0;
                        my $pred_count=0;
                        if ( exists ( $fwd_wig_counts{$chr}{$geneStart+$count} ) ) { $anno_count=$fwd_wig_counts{$chr}{$geneStart+$count}; }
                        if ( exists ( $fwd_wig_counts{$chr}{$start+$count} ) )     { $pred_count=$fwd_wig_counts{$chr}{$start+$count};     }
                        $anno_match{$gene_id}{$count}=$anno_count;
                        $pred_match{$gene_id}{$count}=$pred_count;
                    }
                }
            }
            if ($class eq "Truncation"){
                if (exists ($stops_fwd{$chr}{$stop})){
                    my $gene_id=$stops_fwd{$chr}{$stop};
                    my $geneStart=$gene_regions_fwd_start{$gene_id};
                    for my $count (-29 .. 30){
                        my $anno_count=0;
                        my $pred_count=0;
                        if ( exists ( $fwd_wig_counts{$chr}{$geneStart+$count} ) ) { $anno_count=$fwd_wig_counts{$chr}{$geneStart+$count}; }
                        if ( exists ( $fwd_wig_counts{$chr}{$start+$count} ) )     { $pred_count=$fwd_wig_counts{$chr}{$start+$count};     }
                        $anno_tru{$gene_id}{$count}=$anno_count;
                        $pred_tru{$gene_id}{$count}=$pred_count;
                    }

                    #truncation periodicity                 
                    my $frameCount=2;           #upstream begin on 0, end on 2

							#it looks like the offsets are all consistantly off (formerly starting at 0)

                    for my $count (($geneStart-30) .. ($geneStart-1)){    #10 codons                        
                        my $anno_count=0;
                        if ( exists ( $fwd_wig_counts{$chr}{$count} ) ) { $anno_count=$fwd_wig_counts{$chr}{$count}; }

                        if ($frameCount%3 == 0){
                            $trunc_upstream{$gene_id}[0]+=$anno_count;
                        }elsif( $frameCount%3 == 1){
                            $trunc_upstream{$gene_id}[1]+=$anno_count;
                        }elsif( $frameCount%3 == 2){
                            $trunc_upstream{$gene_id}[2]+=$anno_count;
                        }
                        $frameCount++;
                    }

                    $frameCount=2;
                    for my $count ($geneStart .. $start-1){  #annotated is lower
                        my $mid_count=0;
                       if ( exists ( $fwd_wig_counts{$chr}{$count} ) )     { $mid_count=$fwd_wig_counts{$chr}{$count};     }

                        if ($frameCount%3 == 0){
                            $trunc_middle{$gene_id}[0]+=$mid_count;
                        }elsif( $frameCount%3 == 1){
                            $trunc_middle{$gene_id}[1]+=$mid_count;
                        }elsif( $frameCount%3 == 2){
                            $trunc_middle{$gene_id}[2]+=$mid_count;
                        }
                        $frameCount++;
                    }

                    $frameCount=2;
                    for my $count ($start .. ($start+29)){    #10 codons
                        my $pred_count=0;
                        if ( exists ( $fwd_wig_counts{$chr}{$count} ) )     { $pred_count=$fwd_wig_counts{$chr}{$count};     }
 
                        if ($frameCount%3 == 0){ 
                            $trunc_downstream{$gene_id}[0]+=$pred_count;
                        }elsif( $frameCount%3 == 1){
                            $trunc_downstream{$gene_id}[1]+=$pred_count;
                        }elsif( $frameCount%3 == 2){
                            $trunc_downstream{$gene_id}[2]+=$pred_count;
                        }
                        $frameCount++;
                    }

                    #truncation lengths for distribution
                }
            }
            if ($class eq "Extension"){
                if (exists ($stops_fwd{$chr}{$stop})){
                    my $gene_id=$stops_fwd{$chr}{$stop};
                    my $geneStart=$gene_regions_fwd_start{$gene_id};
                    for my $count (-29 .. 30){
                        my $anno_count=0;
                        my $pred_count=0;
                        if ( exists ( $fwd_wig_counts{$chr}{$geneStart+$count} ) ) { $anno_count=$fwd_wig_counts{$chr}{$geneStart+$count}; }
                        if ( exists ( $fwd_wig_counts{$chr}{$start+$count} ) )     { $pred_count=$fwd_wig_counts{$chr}{$start+$count};     }
                        $anno_elo{$gene_id}{$count}=$anno_count;
                        $pred_elo{$gene_id}{$count}=$pred_count;
                    }

                    #elongation periodicity
                    my $frameCount=2;
                    for my $count (($start-30) .. ($start-1)){  #10 codons
                        my $anno_count=0;
                        if ( exists ( $fwd_wig_counts{$chr}{$count} ) ) { $anno_count=$fwd_wig_counts{$chr}{$count}; }

                        if ($frameCount%3 == 0){
                            $elong_upstream{$gene_id}[0]+=$anno_count;
                        }elsif( $frameCount%3 == 1){
                            $elong_upstream{$gene_id}[1]+=$anno_count;
                        }elsif( $frameCount%3 == 2){
                            $elong_upstream{$gene_id}[2]+=$anno_count;
                        }
                        $frameCount++;
                    }

                    $frameCount=2;
                    for my $count ($start .. $geneStart-1){  #predicted is lower
                        my $mid_count=0;
                        if ( exists ( $fwd_wig_counts{$chr}{$count} ) )     { $mid_count=$fwd_wig_counts{$chr}{$count};     }

                        if ($frameCount%3 == 0){
                            $elong_middle{$gene_id}[0]+=$mid_count;
                        }elsif( $frameCount%3 == 1){
                            $elong_middle{$gene_id}[1]+=$mid_count;
                        }elsif( $frameCount%3 == 2){
                            $elong_middle{$gene_id}[2]+=$mid_count;
                        }
                        $frameCount++;
                    }

                    $frameCount=2;  #starts on frame 1
                    for my $count ($geneStart .. ($geneStart+29)){   #10 codons
                        my $pred_count=0;
                        if ( exists ( $fwd_wig_counts{$chr}{$count} ) )     { $pred_count=$fwd_wig_counts{$chr}{$count};     }

                        if ($frameCount%3 == 0){
                            $elong_downstream{$gene_id}[0]+=$pred_count;
                        }elsif( $frameCount%3 == 1){
                            $elong_downstream{$gene_id}[1]+=$pred_count;
                        }elsif( $frameCount%3 == 2){
                            $elong_downstream{$gene_id}[2]+=$pred_count;
                        }
                        $frameCount++;
                    }
                    #elongation lengths for distribution
                }
            }
        }elsif($dir eq "-"){

            if ($class eq "Annotated"){
               if (exists ($stops_rev{$chr}{$start})){
                    my $gene_id=$stops_rev{$chr}{$start};
                    my $geneStart=$gene_regions_rev_start{$gene_id};          #rev starts are in frame 2 (2nt downstream - in the cds
                    my $rev_count=-29;
                    my $count=30;
                    while ($count > -30){  #go from -29 to 30
                        my $anno_count=0;
                        my $pred_count=0;
                        if ( exists ( $rev_wig_counts{$chr}{$geneStart+$count} ) ) { $anno_count=$rev_wig_counts{$chr}{$geneStart+$count}; }
                        if ( exists ( $rev_wig_counts{$chr}{$stop+$count} ) )      { $pred_count=$rev_wig_counts{$chr}{$stop+$count};      }
                        $anno_match{$gene_id}{$rev_count}=$anno_count;
                        $pred_match{$gene_id}{$rev_count}=$pred_count;
                        $rev_count++;
                        $count--;
                    }
                }
            }

            if ($class eq "Truncation"){
                if (exists ($stops_rev{$chr}{$start})){
                    my $gene_id=$stops_rev{$chr}{$start};
                    my $geneStart=$gene_regions_rev_start{$gene_id};           #rev starts are in frame 2 (2nt downstream - in the cds)
                    my $rev_count=-29;
                    my $count=30;
                    while ($count > -30){  #go from -29 to 30
                        my $anno_count=0;
                        my $pred_count=0;
                        if ( exists ( $rev_wig_counts{$chr}{$geneStart+$count} ) ) { $anno_count=$rev_wig_counts{$chr}{$geneStart+$count}; }
                        if ( exists ( $rev_wig_counts{$chr}{$stop+$count} ) )      { $pred_count=$rev_wig_counts{$chr}{$stop+$count};      }
                        $anno_tru{$gene_id}{$rev_count}=$anno_count;
                        $pred_tru{$gene_id}{$rev_count}=$pred_count;
                        $rev_count++;
                        $count--;

#                        my $poos=$geneStart+$count;
#                        print "$gene_id,$count,$chr,$poos,$anno_count\n";
#                        my $pooos=$stop+$count;
#                        print "$gene_id,$count,$chr,$pooos,$pred_count\n";

                    }
                
                    #truncation periodicity
                    my $frameCount=29;     #upstream begin on 2, end on 0 for rev
                    for my $count (($geneStart+1) .. ($geneStart+30)){    #10 codons    #formerly +1 to +30         #upstream of annotated (rev)            
                        my $anno_count=0;
                        if ( exists ( $rev_wig_counts{$chr}{$count} ) ) { $anno_count=$rev_wig_counts{$chr}{$count}; }

#                        my $frame=$frameCount%3;
#                        print "$gene_id,$frame,$frameCount,$chr,$count,$anno_count\n";

                        if ($frameCount%3 == 0){
                            $trunc_upstream{$gene_id}[0]+=$anno_count;
                        }elsif( $frameCount%3 == 1){
                            $trunc_upstream{$gene_id}[1]+=$anno_count;
                        }elsif( $frameCount%3 == 2){
                            $trunc_upstream{$gene_id}[2]+=$anno_count;
                        }
                        $frameCount--;
                    }

                    $frameCount=62;					
                    for my $count ($stop+1 .. $geneStart){  #predicted is lower    #between preidcted and annotated

                        my $mid_count=0;
                        if ( exists ( $rev_wig_counts{$chr}{$count} ) )     { $mid_count=$rev_wig_counts{$chr}{$count};     }

#                        my $frame=$frameCount%3;
#                        print "$gene_id,$frame,$frameCount,$chr,$count,$mid_count\n";

                        if ($frameCount%3 == 0){
                            $trunc_middle{$gene_id}[0]+=$mid_count;
                        }elsif( $frameCount%3 == 1){
                            $trunc_middle{$gene_id}[1]+=$mid_count;
                        }elsif( $frameCount%3 == 2){
                            $trunc_middle{$gene_id}[2]+=$mid_count;
                        }
                        $frameCount--;
                    }
 
                    $frameCount=29;   
                    for my $count (($stop-29) .. $stop){    #10 codons  #formerly -20 to stop     #downstream of annotated
                        my $pred_count=0;
                        if ( exists ( $rev_wig_counts{$chr}{$count} ) )     { $pred_count=$rev_wig_counts{$chr}{$count};     }

#                        my $frame=$frameCount%3;
#                        print "$gene_id,$frame,$frameCount,$chr,$count,$pred_count\n";

                        if ($frameCount%3 == 0){
                            $trunc_downstream{$gene_id}[0]+=$pred_count;
                        }elsif( $frameCount%3 == 1){
                            $trunc_downstream{$gene_id}[1]+=$pred_count;
                        }elsif( $frameCount%3 == 2){
                            $trunc_downstream{$gene_id}[2]+=$pred_count;
                        }
                        $frameCount--;
                    }
                                    
                    #truncation lengths for periodicity

                }
            }

            if ($class eq "Extension"){
                if (exists ($stops_rev{$chr}{$start})){
                    my $gene_id=$stops_rev{$chr}{$start};
                    my $geneStart=$gene_regions_rev_start{$gene_id};          #rev starts are in frame 2 (2nt downstream - in the cds
                     my $rev_count=-29;
                     my $count=30;
                     while ($count > -30){ #this needs to go from +30 to -29       
                        my $anno_count=0;
                        my $pred_count=0;
                        if ( exists ( $rev_wig_counts{$chr}{$geneStart+$count} ) ) { $anno_count=$rev_wig_counts{$chr}{$geneStart+$count}; }
                        if ( exists ( $rev_wig_counts{$chr}{$stop+$count} ) )      { $pred_count=$rev_wig_counts{$chr}{$stop+$count};      }
                        $anno_elo{$gene_id}{$rev_count}=$anno_count;
                        $pred_elo{$gene_id}{$rev_count}=$pred_count;
                        $rev_count++;
                        $count--;

#                        my $poos=$geneStart+$count;
#                        print "$gene_id,$count,$chr,$poos,$anno_count\n";
#                        my $pooos=$stop+$count;
#                        print "$gene_id,$count,$chr,$pooos,$pred_count\n";

                    }

                    #elongation periodicity
                    my $frameCount=29;           #upstream begin on 2, end on 0 for rev
                    for my $count (($stop+1) .. ($stop+30)){    #10 codons       #upstream of predicted TIS                 
                        my $anno_count=0;
                        if ( exists ( $rev_wig_counts{$chr}{$count} ) ) { $anno_count=$rev_wig_counts{$chr}{$count}; }

#                        my $frame=$frameCount%3;
#                        print "$gene_id,$frame,$frameCount,$chr,$count,$anno_count\n";

                        if ($frameCount%3 == 0){
                            $elong_upstream{$gene_id}[0]+=$anno_count;
                        }elsif( $frameCount%3 == 1){
                            $elong_upstream{$gene_id}[1]+=$anno_count;
                        }elsif( $frameCount%3 == 2){
                            $elong_upstream{$gene_id}[2]+=$anno_count;
                        }
                        $frameCount--;
                    }

                    $frameCount=29;
                    for my $count ($geneStart+1 .. $stop){  #predicted is lower
                        my $mid_count=0;
                        if ( exists ( $rev_wig_counts{$chr}{$count} ) )     { $mid_count=$rev_wig_counts{$chr}{$count};     }

#                       my $frame=$frameCount%3;
#                       print "$gene_id,$frame,$frameCount,$chr,$count,$mid_count\n";

                        if ($frameCount%3 == 0){
                            $elong_middle{$gene_id}[0]+=$mid_count;
                        }elsif( $frameCount%3 == 1){
                            $elong_middle{$gene_id}[1]+=$mid_count;
                        }elsif( $frameCount%3 == 2){
                            $elong_middle{$gene_id}[2]+=$mid_count;
                        }
                        $frameCount--;
                    }

                    $frameCount=62;
                    for my $count (($geneStart-29) .. $geneStart){    #10 codons     #downstream of annotated TIS
                        my $pred_count=0;
                        if ( exists ( $rev_wig_counts{$chr}{$count} ) )     { $pred_count=$rev_wig_counts{$chr}{$count};     }

#                       my $frame=$frameCount%3;
#                       print "$gene_id,$frame,$frameCount,$chr,$count,$pred_count\n";

                        if ($frameCount%3 == 0){
                            $elong_downstream{$gene_id}[0]+=$pred_count;
                        }elsif( $frameCount%3 == 1){
                            $elong_downstream{$gene_id}[1]+=$pred_count;
                        }elsif( $frameCount%3 == 2){
                            $elong_downstream{$gene_id}[2]+=$pred_count;
                        }
                        $frameCount--;
                    }

                    #elongation lenghts for periodicity

                }
            }
        }
    }
}
close(BED);

print "bed parsed\n";

###################################################################################################
#scale lengths

#drop the lowest 10% of genes
#my %black_list;
#my $count=0;
#my $number_of_genes=keys %cdsSum;
#my $ten_percent=$number_of_genes*0.1;

#sort the gene list by total cds TE, lowest to highest
#for my $gene ( sort { $cdsSum{$a} <=> $cdsSum{$b} } keys(%cdsSum) ){
#    if ($count <= $ten_percent){ $black_list{$gene}=1; }
#    $count++;
#}

###################################################################################################
my $out1=$outDir."/annotated_matching.csv";
my $out2=$outDir."/annotated_elongation.csv";
my $out3=$outDir."/annotated_truncation.csv";
my $out4=$outDir."/predicted_matching.csv";
my $out5=$outDir."/predicted_elongation.csv";
my $out6=$outDir."/predicted_truncation.csv";

#for bar plots

open (OUT1,">$out1") || die "can't open $out1\n";
print OUT1 "gene_id"; for (-30 .. 29){ print OUT1 ",$_"; } print OUT1 "\n"; #header
for my $gene (sort keys %anno_match){ print OUT1 "$gene"; for my $pos ( sort { $a <=> $b } keys %{ $anno_match{$gene} } )  { print OUT1 ",$anno_match{$gene}{$pos}"; } print OUT1 "\n"; }

open (OUT2,">$out2") || die "can't open $out2\n";
print OUT2 "gene_id"; for (-30 .. 29){ print OUT2 ",$_"; } print OUT2 "\n"; #header
for my $gene (sort keys %anno_elo){   print OUT2 "$gene"; for my $pos ( sort { $a <=> $b } keys %{ $anno_elo{$gene} } )    { print OUT2 ",$anno_elo{$gene}{$pos}";   } print OUT2 "\n"; }

open (OUT3,">$out3") || die "can't open $out3\n";
print OUT3 "gene_id"; for (-30 .. 29){ print OUT3 ",$_"; } print OUT3 "\n"; #header
for my $gene (sort keys %anno_tru){   print OUT3 "$gene"; for my $pos ( sort { $a <=> $b } keys %{ $anno_tru{$gene} } )    { print OUT3 ",$anno_tru{$gene}{$pos}";   } print OUT3 "\n"; }

open (OUT4,">$out4") || die "can't open $out4\n";
print OUT4 "gene_id"; for (-30 .. 29){ print OUT4 ",$_"; } print OUT4 "\n"; #header
for my $gene (sort keys %pred_match){ print OUT4 "$gene"; for my $pos ( sort { $a <=> $b } keys %{ $pred_match{$gene} } )  { print OUT4 ",$pred_match{$gene}{$pos}"; } print OUT4 "\n"; }

open (OUT5,">$out5") || die "can't open $out5\n";
print OUT5 "gene_id"; for (-30 .. 29){ print OUT5 ",$_"; } print OUT5 "\n"; #header
for my $gene (sort keys %pred_elo){   print OUT5 "$gene"; for my $pos ( sort { $a <=> $b } keys %{ $pred_elo{$gene} } )    { print OUT5 ",$pred_elo{$gene}{$pos}";   } print OUT5 "\n"; }

open (OUT6,">$out6") || die "can't open $out6\n";
print OUT6 "gene_id"; for (-30 .. 29){ print OUT6 ",$_"; } print OUT6 "\n"; #header
for my $gene (sort keys %pred_tru){   print OUT6 "$gene"; for my $pos ( sort { $a <=> $b } keys %{ $pred_tru{$gene} } )    { print OUT6 ",$pred_tru{$gene}{$pos}";   } print OUT6 "\n"; }

my $out7=$outDir."/periodicity_truncation_upstream.csv";
my $out8=$outDir."/periodicity_truncation_between.csv";
my $out9=$outDir."/periodicity_truncation_downstream.csv";
my $out10=$outDir."/periodicity_elongation_upstream.csv";
my $out11=$outDir."/periodicity_elongation_between.csv";
my $out12=$outDir."/periodicity_elongation_downstream.csv";

#for periodicity plots

open (OUT7,">$out7") || die "can't open $out7\n";
print OUT7 "gene,codon1,codon2,codon3\n";
for my $gene (sort keys %trunc_upstream){
    print OUT7 "$gene";
    my $gene_sum=0; for (0..2){ $gene_sum+=$trunc_upstream{$gene}[$_]; }
    for (0..2){
        my $proportional_value=eval{ $trunc_upstream{$gene}[$_]/$gene_sum } || 0;  
        print OUT7 ",$proportional_value";
    }
    print OUT7 "\n";
}

open (OUT8,">$out8") || die "can't open $out8\n";
print OUT8 "gene,codon1,codon2,codon3\n";
for my $gene (sort keys %trunc_middle){
    print OUT8 "$gene";
    my $gene_sum=0; for (0..2){ $gene_sum+=$trunc_middle{$gene}[$_]; }
    for (0..2){
        my $proportional_value=eval{ $trunc_middle{$gene}[$_]/$gene_sum } || 0;
        print OUT8 ",$proportional_value";

    }
    print OUT8 "\n";
}

open (OUT9,">$out9") || die "can't open $out9\n";
print OUT9 "gene,codon1,codon2,codon3\n";
for my $gene (sort keys %trunc_downstream){
    print OUT9 "$gene";
    my $gene_sum=0; for (0..2){ $gene_sum+=$trunc_downstream{$gene}[$_]; }
    for (0..2){
        my $proportional_value=eval{ $trunc_downstream{$gene}[$_]/$gene_sum } || 0;
        print OUT9 ",$proportional_value";
    }
    print OUT9 "\n";
}

open (OUT10,">$out10") || die "can't open $out10\n";
print OUT10 "gene,codon1,codon2,codon3\n";
for my $gene (sort keys %elong_upstream){
    print OUT10 "$gene";
    my $gene_sum=0; for (0..2){ $gene_sum+=$elong_upstream{$gene}[$_]; }
    for (0..2){
        my $proportional_value=eval{ $elong_upstream{$gene}[$_]/$gene_sum } || 0;
        print OUT10 ",$proportional_value";
    }
    print OUT10 "\n";
}

open (OUT11,">$out11") || die "can't open $out11\n";
print OUT11 "gene,codon1,codon2,codon3\n";
for my $gene (sort keys %elong_middle){
    print OUT11 "$gene";
    my $gene_sum=0; for (0..2){ $gene_sum+=$elong_middle{$gene}[$_]; }
    for (0..2){
        my $proportional_value=eval{ $elong_middle{$gene}[$_]/$gene_sum } || 0;
        print OUT11 ",$proportional_value";
    }
    print OUT11 "\n";
}

open (OUT12,">$out12") || die "can't open $out12\n";
print OUT12 "gene,codon1,codon2,codon3\n";
for my $gene (sort keys %elong_downstream){
    print OUT12 "$gene";
    my $gene_sum=0; for (0..2){ $gene_sum+=$elong_downstream{$gene}[$_]; }
    for (0..2){
        my $proportional_value=eval{ $elong_downstream{$gene}[$_]/$gene_sum } || 0;
        print OUT12 ",$proportional_value";
    }
    print OUT12 "\n";
}
                     
exit;
