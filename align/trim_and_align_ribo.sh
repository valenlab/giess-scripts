#!/bin/bash

#AG 08/10/15
#script to process genomic non paired fastq datasets 
#1 Trim adaptors
#2 Remove rRNA
#3 Remove organism specific ncRNA
#4 Map to organism specific reference

#PATH=$PATH:/Home/ii/adamg/bin/
top2=/net/apps/cbu/stow/tophat-2.0.13/bin/tophat2 

usage(){
cat << EOF
usage: $0 options

script to trim and align riboseq fastq datasets 

OPTIONS:
	-f	path to directory containing input fastq files (gzipped)
	-o	path to output dir
	-s	genome - one of:
			GRCz10	(GRCz10_zebrabish)
			Zv9		(Zv9_zebrafish)
			GRCm38	(GRCm38_mouse)
			GRCh38	(GRCh38_human)
			R64_1_1	(R64_1_1_yeast)
			BDGP6	(BDGP6_fruitfly)
	-a	path to file containing adaptor sequences (defaults to those I have seen in other riboseq datasets)
	-h	this help message

example usage: trim_and_align.sh -f <in.fasta.dir> -o <out_dir> -s Zv9

EOF
}

while getopts ":f:o:s:a:h" opt; do
	case $opt in 
		f)
			in_dir=$OPTARG
		  	echo "-f input folder $OPTARG"
		  	;;
        o)
            out_dir=$OPTARG
            echo "-o output folder $OPTARG"
            ;;
        s)
            species=$OPTARG
            echo "-s genome $OPTARG"
            ;;
		a)	
			adapt=$OPTARG
			echo "-a adaptor file $OPTARG"
			;;
		h)
			usage
			exit
			;;
		?) 
			echo "Invalid option: -$OPTARG"
			usage
			exit 1
		  	;;
	esac
done

if [ ! -e $in_dir ]  || [ ! $out_dir ] || [ ! $species ]; then
		echo "something's missing - check your command lines args"
        usage
        exit 1
fi

if [ ! -d $out_dir ]; then
    mkdir $out_dir
fi

rRNA=/export/kjempetujafs/valen-group/data/genome/adam/rrna/SILVA_119_bothSURef

if [ $species == "GRCz10" ]; then
	ncRNA=/export/kjempetujafs/valen-group/data/genome/adam/Zv10_zebrafish/ncrna_edited/Danio_rerio.GRCz10.ncrna
	genome=/export/kjempetujafs/valen-group/data/genome/adam/Zv10_zebrafish/bowtie2_index/Danio_rerio.GRCz10
	transome=/export/kjempetujafs/valen-group/data/genome/adam/Zv10_zebrafish/tophat2_transcriptome/Danio_rerio.GRCz10
elif [ $species == "Zv9" ]; then
    ncRNA=/export/kjempetujafs/valen-group/data/genome/adam/Zv9_zebrafish/ncrna_edited/Danio_rerio.Zv9.ncrna
    genome=/export/kjempetujafs/valen-group/data/genome/adam/Zv9_zebrafish/bowtie2_index/Danio_rerio.Zv9
    transome=/export/kjempetujafs/valen-group/data/genome/adam/Zv9_zebrafish/tophat2_transcriptome/Danio_rerio.Zv9.79
elif [ $species == "GRCm38" ]; then
    ncRNA=/export/kjempetujafs/valen-group/data/genome/adam/GRCm38_mouse/ncrna_edited/Mus_musculus.GRCm38.ncrna
    genome=/export/kjempetujafs/valen-group/data/genome/adam/GRCm38_mouse/bowtie2_index/Mus_musculus.GRCm38.dna.primary_assembly.chr
    transome=/export/kjempetujafs/valen-group/data/genome/adam/GRCm38_mouse/tophat2_transcriptome/Mus_musculus.GRCm38.79.chr
elif [ $species == "GRCh38" ]; then
    ncRNA=/export/kjempetujafs/valen-group/data/genome/adam/GRCh38_human/ncrna_edited/Homo_sapiens.GRCh38.ncrna
    genome=/export/kjempetujafs/valen-group/data/genome/adam/GRCh38_human/bowtie2_index/Homo_sapiens.GRCh38.dna.primary_assembly.chr
    transome=/export/kjempetujafs/valen-group/data/genome/adam/GRCh38_human/tophat2_transcriptome/Homo_sapiens.GRCh38.79.chr
elif [ $species == "R64_1_1" ]; then
    ncRNA=/export/kjempetujafs/valen-group/data/genome/adam/R64_1_1_yeast/ncrna_edited/Saccharomyces_cerevisiae.R64-1-1.ncrna
    genome=/export/kjempetujafs/valen-group/data/genome/adam/R64_1_1_yeast/bowtie2_index/Saccharomyces_cerevisiae.R64-1-1.dna.toplevel
    transome=/export/kjempetujafs/valen-group/data/genome/adam/R64_1_1_yeast/tophat2_transcriptome/Saccharomyces_cerevisiae.R64-1-1.79
elif [ $species == "BDGP6" ]; then
    ncRNA=/export/kjempetujafs/valen-group/data/genome/adam/BDGP6_fruitfly/ncrna_edited/Drosophila_melanogaster.BDGP6.ncrna
    genome=/export/kjempetujafs/valen-group/data/genome/adam/BDGP6_fruitfly/bowtie2_index/Drosophila_melanogaster.BDGP6.dna.toplevel
    transome=/export/kjempetujafs/valen-group/data/genome/adam/BDGP6_fruitfly/tophat2_transcriptome/Drosophila_melanogaster.BDGP6.79
else 
	echo "invalid species selected : $species"
	usage
	exit 1
fi

if [ ! $adapt ]; then
	echo "using default adaptor file"
	adapt=/export/kjempetujafs/valen-group/data/genome/adam/riboseq_adaptor_list.fa
fi

for file in ${in_dir}/*.fastq.gz; do

	name=$(basename $file)
	prefix=${name%.fastq.gz}
	echo $prefix

	#------------------------------------------------------------------------------------------
	#1 Trim adaptors
	#------------------------------------------------------------------------------------------
	#-n 4 = 4 iterations of adaptor removal
	#-e 0.1 = error rate, 1 missmatch per 10 bp
	#-O 5 = minimium overlap with an adaptor sequence
	#-m -20 = discard reads shorter than 20 bp

	if [ ! -d ${out_dir}/trim ]; then
		mkdir ${out_dir}/trim
	fi

#	cutadapt -a file:${adapt} -n 4 -e 0.1 -O 5 -m 20 -o ${out_dir}/trim/${prefix}_trim.fastq.gz $file
	
	#------------------------------------------------------------------------------------------
	#2 Remove rRNA
	#------------------------------------------------------------------------------------------
	#-p 8 = 8 processors
	#-N 1 = allow 1 missmatch in seed
	#-L 15 = use 15bp seeds

	if [ ! -d ${out_dir}/rRNA_depletion ]; then
		mkdir ${out_dir}/rRNA_depletion
	fi

	if [ ! -d ${out_dir}/rRNA_match ]; then
		mkdir ${out_dir}/rRNA_match
	fi

	echo "out = ${out_dir}/rRNA_depletion/${prefix}_no_rRNA.fastq.gz"
	echo "ref = $rRNA"
	echo "in  = ${out_dir}/trim/${prefix}_trim.fastq.gz"

#	bowtie2 -p 8 -N 1 -L 15 --un-gz ${out_dir}/rRNA_depletion/${prefix}_no_rRNA.fastq.gz -x $rRNA -U ${out_dir}/trim/${prefix}_trim.fastq.gz | samtools view -bSu - | samtools sort - ${out_dir}/rRNA_match/${prefix}_rRNA

	#------------------------------------------------------------------------------------------
	#3 Remove organism specific ncRNA
	#------------------------------------------------------------------------------------------
	#-p 8 = use 8 processors
	#-N 1 = allow 1 missmatch in seed
	#-L 15 = use 15bp seeds

	if [ ! -d ${out_dir}/ncRNA_depletion_$species ]; then
		mkdir ${out_dir}/ncRNA_depletion_$species
	fi

	if [ ! -d ${out_dir}/ncRNA_match_$species ]; then
		mkdir ${out_dir}/ncRNA_match_$species
	fi

#	bowtie2 -p 8 -N 1 -L 15 --un-gz ${out_dir}/ncRNA_depletion/${prefix}_no_ncRNA.fastq.gz -x $ncRNA -U ${out_dir}/rRNA_depletion/${prefix}_no_rRNA.fastq.gz | samtools view -bSu - | samtools sort - ${out_dir}/ncRNA_match/${prefix}_ncRNA

	#------------------------------------------------------------------------------------------
	#4 Map to organism specfic reference
	#------------------------------------------------------------------------------------------
	#-g 1 = max genome multihits 1
	#-x 1 = max transcriptome multihits 1
	#--prefilter-multihits = don't pass multihits to transcriptome
	#-p 8 = use 8 processors
	#--transcriptome-index = use pre built transriptome (from tophat2 -G <<org.gtf>> --transcriptome-index=<<org.trans>>)

	if [ ! -d ${out_dir}/aligned_$species ]; then
		mkdir ${out_dir}/aligned_$species
	fi

	if [ ! -d ${out_dir}/aligned_${species}/$prefix ]; then
		mkdir ${out_dir}/aligned_${species}/$prefix
	fi
 
	$top2 -g 1 --transcriptome-index=$transome -p 8 -o ${out_dir}/aligned_${species}/$prefix $genome ${out_dir}/ncRNA_depletion/${prefix}_no_ncRNA.fastq.gz

done
