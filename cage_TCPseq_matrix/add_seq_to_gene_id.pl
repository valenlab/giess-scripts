#!/usr/bin/perl -w
use strict;

#to do 10/01/2018
#script to parse genes id'd from matrix and to add start codons sequences and surrounding Kozak context 

#input files:
#gtf exons, start, stop codons
#sam = aligned 
my $gtf=$ARGV[0];
my $fasta=$ARGV[1];
my $matrix=$ARGV[2];


#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#open gtf and get transcript lengths
my %transcripts; #key = gene_id, transcript_id, #value = sum_exon_lengths;

open(GENES1,$gtf) || die "can't open $gtf";      #gft is 1 based
while (<GENES1>){
    unless(/^#/){
        my @b=split("\t");
        my $chr=$b[0];
        my $class=$b[2];
        my $start=$b[3];
        my $end=$b[4];
        my $dir=$b[6];
        my ($gene_id) = $b[8] =~ /gene_id\s"([^\"]+)";/;
        my ($transcript_id) = $b[8] =~ /transcript_id\s"([^\"]+)";/;

        if ($gene_id && $transcript_id){

            if ($class eq "exon"){
                if ($dir eq "+"){        
                    for ($start .. $end){
                        $transcripts{$gene_id}{$transcript_id}++;   
                    }
                }else{
                    for ($start .. $end){
                        $transcripts{$gene_id}{$transcript_id}++;
                    }
                }
            }
        }
    }
}
close (GENES1);

#select longest transcripts per gene
my %longest_transcript; #key=gene_id, value=transcript_id

for my $gene (keys %transcripts){
    my $longest=0;
    for my $transcript (keys %{ $transcripts{$gene}} ){
        if ($transcripts{$gene}{$transcript} > $longest) {
            $longest_transcript{$gene}=$transcript;
            $longest=$transcripts{$gene}{$transcript};
        }
    }
}

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#second pass through the genome, find annotated start codons and setup transcript models for longest transcript of each gene

my %gene_start_codon_fwd;
my %gene_stop_codon_fwd;
my %gene_exons_fwd;

my %gene_start_codon_rev;
my %gene_stop_codon_rev;
my %gene_exons_rev;

my %gene_2_chr; #key = gene_id; value = chr

open(GENES2,$gtf) || die "can't open $gtf";      #gft is 1 based
while (<GENES2>){
    unless(/^#/){
        my @b=split("\t");
        my $chr=$b[0];
        my $class=$b[2];
        my $start=$b[3];
        my $end=$b[4];
        my $dir=$b[6];
        my ($gene_id) = $b[8] =~ /gene_id\s"([^\"]+)";/;
        my ($transcript_id) = $b[8] =~ /transcript_id\s"([^\"]+)";/;

        if ($gene_id && $transcript_id){

            #if the transcript is in the list of longest transcripts
            if (exists ( $longest_transcript{$gene_id} )){

                if ($transcript_id eq $longest_transcript{$gene_id}){
 
                    $gene_2_chr{$gene_id}=$chr;

                    if ($dir eq "+"){ #fwd cases. Use start positions as 5'

                        if ($class eq "start_codon"){ 
                            $gene_start_codon_fwd{$gene_id}=$start;
                        }
                        if ($class eq "stop_codon"){ 
                            $gene_stop_codon_fwd{$gene_id}=$start;
                        }
                        if ($class eq "exon"){
                            $gene_exons_fwd{$gene_id}{$start}=$end; 
                        }

                    }else{ #revese cases use end as 5'
  
                        if ($class eq "start_codon"){
                            $gene_start_codon_rev{$gene_id}=$end;
                        }
                        if ($class eq "stop_codon"){
                            $gene_stop_codon_rev{$gene_id}=$end;
                        }
                        if ($class eq "exon"){
                            $gene_exons_rev{$gene_id}{$start}=$end;
                        }
                    }
                }
            }
        }
    }
}
close(GENES2);

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
##open fasta for codon sequeces
my %fasta_sequences; #key = sequence_name, value=sequence
my $name;
open (FA, $fasta) || die "can't open $fasta";
while (<FA>){
    chomp;
    if (/^>([^\s]+)/){ #take header up to the first space
        $name=$1;
        if ($name =~ /^chr(.*)/){
           $name=$1; #if the chr name have a chr* prefix, remove it 
        }
    }else{
        $fasta_sequences{$name}.=$_;
    }
}
close(FA);

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#setup transcript models for the longest transcript of each gene 

my %stop2stop_stop_coord_fwd; #find the index of the stop codon per gene
my %stop2stop_start_coord_fwd; #find the index of the start codon per gene
my %stop2stop_stop_coord_rev; 
my %stop2stop_start_coord_rev; 
my %gene_model_fwd;
my %gene_model_rev;

#gene_model{$gene}{1}=12345
#gene_model{$gene}{2}=12346
#gene_model{$gene}{3}=12347
#gene_model{$gene}{4}=12348
#gene_model{$gene}{5}=12349
#...
#to end of exons

for my $gene (keys %gene_exons_fwd){
    if ( (exists ($gene_start_codon_fwd{$gene})) && (exists ($gene_stop_codon_fwd{$gene})) ) { #restrict to genes with annotated start + stop codon
 
        my $model_pos=0;

        for my $exon_start (sort {$a <=> $b} keys %{ $gene_exons_fwd{$gene} } ){
            my $exon_end=$gene_exons_fwd{$gene}{$exon_start};

            #fwd exons are in ascending order
            # start(-1)-> 100958 100975
            #             101077 101715 <-end(+1)
            
            for ($exon_start .. $exon_end){
                $gene_model_fwd{$gene}{$model_pos}=$_;

                if ($_ == $gene_stop_codon_fwd{$gene}){ 
                    $stop2stop_stop_coord_fwd{$gene}=$model_pos;    #find the index of the stop codon per gene
                }

                if ($_ == $gene_start_codon_fwd{$gene}){                    
                    $stop2stop_start_coord_fwd{$gene}=$model_pos;    #find the index of the start codon per gene
                }
                $model_pos++;
            }
        }
    }
}

for my $gene (keys %gene_exons_rev){
    if ( (exists ($gene_start_codon_rev{$gene})) && (exists ($gene_stop_codon_rev{$gene})) ) { #restrict to genes with annotated start + stop codon

        my $model_pos=0;

        for my $exon_end (reverse (sort {$a <=> $b} keys %{ $gene_exons_rev{$gene} } )){
            my $exon_start=$gene_exons_rev{$gene}{$exon_end};

            #rev exons are sorted in decending order  
            #           447087 447794 <-start(+1)
            # end(-1)-> 446060 446254

            while ($exon_start >= $exon_end){
                $gene_model_rev{$gene}{$model_pos}=$exon_start;

                if ($exon_start == $gene_stop_codon_rev{$gene}){
                    $stop2stop_stop_coord_rev{$gene}=$model_pos;    #find the index of the stop codon per gene
                }
                if ($exon_start == $gene_start_codon_rev{$gene}){
                    $stop2stop_start_coord_rev{$gene}=$model_pos;    #find the index of the start codon per gene
                }
                $model_pos++;
                $exon_start--;
            }
        }
    }
}            

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#get start codons and the surrounding sequene for each gene
my %gene_initiation_codon;
my %gene_upstream_kozak_region;
my %gene_downstream_kozak_region;

############################
### Yeast Kozak sequence ###

#traditional kozak - 6 tp +6
#extended -8 to + 6
#New -15 to +6
#yeast kozak
#Yew = -15to?

#        ***
#AAAAAAAAATGTCT
#AATACACAATGCCT  -8 to +6 (ATG = 1,2,3)
#     \ \        #most important motifs (might no be so true)

############################

for my $gene (keys %stop2stop_start_coord_fwd){

    my $chr=$gene_2_chr{$gene};
    my $TIS_coord=$stop2stop_start_coord_fwd{$gene}; 
    my $upstream_coord=$stop2stop_start_coord_fwd{$gene}-6;
    my $downstream_coord=$stop2stop_start_coord_fwd{$gene}+3;
    my $seq_TIS="nnn";
    my $seq_up="nnnnnn";
    my $seq_down="nnn";

    if (exists ($gene_model_fwd{$gene}{$TIS_coord})){ #search upstream while we are within an annotated leader region.
        my $codon1=substr($fasta_sequences{$chr}, ($gene_model_fwd{$gene}{ ($TIS_coord-0)} -1), 1);     #remember the fasta sequence is zero based
        my $codon2=substr($fasta_sequences{$chr}, ($gene_model_fwd{$gene}{ ($TIS_coord+1)} -1), 1);
        my $codon3=substr($fasta_sequences{$chr}, ($gene_model_fwd{$gene}{ ($TIS_coord+2)} -1), 1);
        $seq_TIS=$codon1.$codon2.$codon3;
    }


    #because the yeast gtf does not included annotated 5' leaders I will just take the nuclotides directly upstream of the start codon.

    if (exists ($gene_model_fwd{$gene}{$TIS_coord})){ #search upstream while we are within an annotated leader region.
        my $codon1=substr($fasta_sequences{$chr}, ($gene_model_fwd{$gene}{ ($TIS_coord-0)} -16), 1);     #remember the fasta seq
        my $codon2=substr($fasta_sequences{$chr}, ($gene_model_fwd{$gene}{ ($TIS_coord-0)} -15), 1);
        my $codon3=substr($fasta_sequences{$chr}, ($gene_model_fwd{$gene}{ ($TIS_coord-0)} -14), 1);
        my $codon4=substr($fasta_sequences{$chr}, ($gene_model_fwd{$gene}{ ($TIS_coord-0)} -13), 1);
        my $codon5=substr($fasta_sequences{$chr}, ($gene_model_fwd{$gene}{ ($TIS_coord-0)} -12), 1);
        my $codon6=substr($fasta_sequences{$chr}, ($gene_model_fwd{$gene}{ ($TIS_coord-0)} -11), 1);
        my $codon7=substr($fasta_sequences{$chr}, ($gene_model_fwd{$gene}{ ($TIS_coord-0)} -10), 1);
        my $codon8=substr($fasta_sequences{$chr}, ($gene_model_fwd{$gene}{ ($TIS_coord-0)} -9), 1);
        my $codon9=substr($fasta_sequences{$chr}, ($gene_model_fwd{$gene}{ ($TIS_coord-0)} -8), 1);
        my $codon10=substr($fasta_sequences{$chr}, ($gene_model_fwd{$gene}{ ($TIS_coord-0)} -7), 1);
        my $codon11=substr($fasta_sequences{$chr}, ($gene_model_fwd{$gene}{ ($TIS_coord-0)} -6), 1);
        my $codon12=substr($fasta_sequences{$chr}, ($gene_model_fwd{$gene}{ ($TIS_coord-0)} -5), 1);
        my $codon13=substr($fasta_sequences{$chr}, ($gene_model_fwd{$gene}{ ($TIS_coord-0)} -4), 1);
        my $codon14=substr($fasta_sequences{$chr}, ($gene_model_fwd{$gene}{ ($TIS_coord-0)} -3), 1);
        my $codon15=substr($fasta_sequences{$chr}, ($gene_model_fwd{$gene}{ ($TIS_coord-0)} -2), 1);
        $seq_up=$codon1.$codon2.$codon3.$codon4.$codon5.$codon6.$codon7.$codon8.$codon9.$codon10.$codon11.$codon12.$codon13.$codon14.$codon15;

    #if (exists ($gene_model_fwd{$gene}{$upstream_coord})){ #search upstream while we are within an annotated leader region.
        #my $codon1=substr($fasta_sequences{$chr}, ($gene_model_fwd{$gene}{ ($upstream_coord-0)} -1), 1);     #remember the fasta sequence is zero based
        #my $codon2=substr($fasta_sequences{$chr}, ($gene_model_fwd{$gene}{ ($upstream_coord+1)} -1), 1);
        #my $codon3=substr($fasta_sequences{$chr}, ($gene_model_fwd{$gene}{ ($upstream_coord+2)} -1), 1);
        #my $codon4=substr($fasta_sequences{$chr}, ($gene_model_fwd{$gene}{ ($upstream_coord+3)} -1), 1);
        #my $codon5=substr($fasta_sequences{$chr}, ($gene_model_fwd{$gene}{ ($upstream_coord+4)} -1), 1);
        #my $codon6=substr($fasta_sequences{$chr}, ($gene_model_fwd{$gene}{ ($upstream_coord+5)} -1), 1);
        #$seq_up=$codon1.$codon2.$codon3.$codon4.$codon5.$codon6;
    }

    if (exists ($gene_model_fwd{$gene}{$downstream_coord})){ #search upstream while we are within an annotated leader region.
        my $codon1=substr($fasta_sequences{$chr}, ($gene_model_fwd{$gene}{ ($downstream_coord-0)} -1), 1);     #remember the fasta sequence is zero based
        my $codon2=substr($fasta_sequences{$chr}, ($gene_model_fwd{$gene}{ ($downstream_coord+1)} -1), 1);
        my $codon3=substr($fasta_sequences{$chr}, ($gene_model_fwd{$gene}{ ($downstream_coord+2)} -1), 1);
        $seq_down=$codon1.$codon2.$codon3;
    }

    $gene_initiation_codon{$gene}=$seq_TIS;
    $gene_upstream_kozak_region{$gene}=$seq_up;
    $gene_downstream_kozak_region{$gene}=$seq_down;
    #print "$gene,$seq_up,$seq_TIS,$seq_down\n"; 
}

for my $gene (keys %stop2stop_start_coord_rev){

    my $chr=$gene_2_chr{$gene};
    my $TIS_coord=$stop2stop_start_coord_rev{$gene};
    my $upstream_coord=$stop2stop_start_coord_rev{$gene}-6;
    my $downstream_coord=$stop2stop_start_coord_rev{$gene}+3;
    my $seq_TIS="nnn";
    my $seq_up="nnnnnn";
    my $seq_down="nnn";

    if (exists ($gene_model_rev{$gene}{$TIS_coord})){ #search upstream while we are within an annotated leader region.
        my $codon1=substr($fasta_sequences{$chr}, ($gene_model_rev{$gene}{ ($TIS_coord-0) } -1) ,1); #remember the fasta sequence is zero based
        my $codon2=substr($fasta_sequences{$chr}, ($gene_model_rev{$gene}{ ($TIS_coord+1) } -1) ,1);
        my $codon3=substr($fasta_sequences{$chr}, ($gene_model_rev{$gene}{ ($TIS_coord+2) } -1) ,1);
        $seq_TIS=$codon1.$codon2.$codon3;
        $seq_TIS=~tr/ACGTacgt/TGCAtgca/;     
    }

    #because the yeast gtf does not included annotated 5' leaders I will just take the nuclotides directly upstream of the start cod
    #if (exists ($gene_model_rev{$gene}{$upstream_coord})){ #search upstream while we are within an annotated leader region.
        #my $codon1=substr($fasta_sequences{$chr}, ($gene_model_rev{$gene}{ ($upstream_coord-0)} -1), 1);     #remember the fasta seq
        #my $codon2=substr($fasta_sequences{$chr}, ($gene_model_rev{$gene}{ ($upstream_coord+1)} -1), 1);
        #my $codon3=substr($fasta_sequences{$chr}, ($gene_model_rev{$gene}{ ($upstream_coord+2)} -1), 1);
        #my $codon4=substr($fasta_sequences{$chr}, ($gene_model_rev{$gene}{ ($upstream_coord+3)} -1), 1);
        #my $codon5=substr($fasta_sequences{$chr}, ($gene_model_rev{$gene}{ ($upstream_coord+4)} -1), 1);
        #my $codon6=substr($fasta_sequences{$chr}, ($gene_model_rev{$gene}{ ($upstream_coord+5)} -1), 1);
        #$seq_up=$codon1.$codon2.$codon3.$codon4.$codon5.$codon6;
        #$seq_up=~tr/ACGTacgt/TGCAtgca/;
    
    if (exists ($gene_model_rev{$gene}{$TIS_coord})){ #search upstream while we are within an annotated leader region.
        my $codon1=substr($fasta_sequences{$chr}, ($gene_model_rev{$gene}{ ($TIS_coord-0)} +14), 1);     #remember the fasta sequence is zero based
        my $codon2=substr($fasta_sequences{$chr}, ($gene_model_rev{$gene}{ ($TIS_coord-0)} +13), 1);
        my $codon3=substr($fasta_sequences{$chr}, ($gene_model_rev{$gene}{ ($TIS_coord-0)} +12), 1);
        my $codon4=substr($fasta_sequences{$chr}, ($gene_model_rev{$gene}{ ($TIS_coord-0)} +11), 1);
        my $codon5=substr($fasta_sequences{$chr}, ($gene_model_rev{$gene}{ ($TIS_coord-0)} +10), 1);
        my $codon6=substr($fasta_sequences{$chr}, ($gene_model_rev{$gene}{ ($TIS_coord-0)} +9), 1);
        my $codon7=substr($fasta_sequences{$chr}, ($gene_model_rev{$gene}{ ($TIS_coord-0)} +8), 1);
        my $codon8=substr($fasta_sequences{$chr}, ($gene_model_rev{$gene}{ ($TIS_coord-0)} +7), 1);
        my $codon9=substr($fasta_sequences{$chr}, ($gene_model_rev{$gene}{ ($TIS_coord-0)} +6), 1);
        my $codon10=substr($fasta_sequences{$chr}, ($gene_model_rev{$gene}{ ($TIS_coord-0)} +5), 1);
        my $codon11=substr($fasta_sequences{$chr}, ($gene_model_rev{$gene}{ ($TIS_coord-0)} +4), 1);
        my $codon12=substr($fasta_sequences{$chr}, ($gene_model_rev{$gene}{ ($TIS_coord-0)} +3), 1);
        my $codon13=substr($fasta_sequences{$chr}, ($gene_model_rev{$gene}{ ($TIS_coord-0)} +2), 1);
        my $codon14=substr($fasta_sequences{$chr}, ($gene_model_rev{$gene}{ ($TIS_coord-0)} +1), 1);
        my $codon15=substr($fasta_sequences{$chr}, ($gene_model_rev{$gene}{ ($TIS_coord-0)} +0), 1);
        $seq_up=$codon1.$codon2.$codon3.$codon4.$codon5.$codon6.$codon7.$codon8.$codon9.$codon10.$codon11.$codon12.$codon13.$codon14.$codon15;
        $seq_up=~tr/ACGTacgt/TGCAtgca/;
    }

    if (exists ($gene_model_rev{$gene}{$downstream_coord})){ #search upstream while we are within an annotated leader region.
        my $codon1=substr($fasta_sequences{$chr}, ($gene_model_rev{$gene}{ ($downstream_coord-0)} -1), 1);     #remember the fasta sequence is zero based
        my $codon2=substr($fasta_sequences{$chr}, ($gene_model_rev{$gene}{ ($downstream_coord+1)} -1), 1);
        my $codon3=substr($fasta_sequences{$chr}, ($gene_model_rev{$gene}{ ($downstream_coord+2)} -1), 1);
        $seq_down=$codon1.$codon2.$codon3;
        $seq_down=~tr/ACGTacgt/TGCAtgca/;
    }

    $gene_initiation_codon{$gene}=$seq_TIS;
    $gene_upstream_kozak_region{$gene}=$seq_up;
    $gene_downstream_kozak_region{$gene}=$seq_down;
    #print "$gene,$seq_up,$seq_TIS,$seq_down\n"; 
}

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#open matrix and add sequence columns

#transcript_id, gene_id, leader_length, CDS_length, leader_RNA_FPKM, leader_RFP_FPKM, leader_TE, leader_SSU_FPKM, leader_LSU_FPKM, leader_StoL_ratio, CDS_RNA_FPKM, CDS_RFP_FPKM, CDS_TE, CDS_SSU_FPKM, CDS_LSU_FPKM, CDS_StoL_ratio

open(MAT,$matrix) || die "can't open $matrix"; 
while (<MAT>){

    chomp;
    my @b=split(",");
    my $gene=$b[0];

    unless(/^#/){
        print "$_,$gene_upstream_kozak_region{$gene},$gene_initiation_codon{$gene},$gene_downstream_kozak_region{$gene}\n";
    }else{ #header
        print "$_,Upstream_TIS_sequence,Initiation_codon,Downstream_TIS_sequence\n";
    }
}
close(MAT);

exit:
