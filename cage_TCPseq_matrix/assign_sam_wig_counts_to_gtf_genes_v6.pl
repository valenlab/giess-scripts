#!/usr/bin/perl -w
use strict;

#19/01/18
#script to assign counts to CDS and leader models and calcuate fpkm over leaders and CDS regions

my $inGtf=$ARGV[0]; 
my $wig_RIBOseq_fwd=$ARGV[1];
my $wig_RIBOseq_rev=$ARGV[2];
my $bam_RNAseq=$ARGV[3];
my $bam_TCPseq_SSU=$ARGV[4];
my $bam_TCPseq_LSU=$ARGV[5];
my $leaders=$ARGV[6]; #from cageR (cage_peaks_assignment_gtf_transcript_coords_for_TCPseq_v8.pl)

#excluding:
#genes where the TSS is downstream of the start codon
#genes without a detectable cage peak 
#genes that are annotated as protien_coding

#reads are assigned to leaders/CDS on a stand specific basis (including the RNA-seq) as follows:
#Ribo-seq:  Using a shoelaces shifted bed, assign to exact overlaps regions only
#RNA-seq:   Assign fragments that overlap leader and CDS to both.
#TCP-seq_SSU:   Assign fragments that overlap leader and CDS to leader.
#TCP-seq_LSU:   Assign fragments that overlap leader and CDS to CDS.

#Flags:
#overlapping_gene: The longest transcript of this gene overlaps with the longest transcript of another gene
#leader_potentially_overlaps_upstream_gene: There is an upstream gene within 500nt of the start codon of this gene 
#gene_potentially_overlaps_downstream_leader: There is an downstream gene start codon withing 500nt of the 3' most position of this gene (in yeast this is the stop codon).

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#open gtf and get transcript lengths
my %transcripts; #key = gene_id, transcript_id, #value = sum_exon_lengths;

open(GENES1,$inGtf) || die "can't open $inGtf";      #gft is 1 based
while (<GENES1>){
    unless(/^#/){
        my @b=split("\t");
        my $chr=$b[0];
        my $class=$b[2];
        my $start=$b[3];
        my $end=$b[4];
        my $dir=$b[6];
        my ($gene_id) = $b[8] =~ /gene_id\s"([^\"]+)";/;
        my ($transcript_id) = $b[8] =~ /transcript_id\s"([^\"]+)";/;
        my $gene_biotype="NA";
        if ($b[8] =~ /gene_biotype\s"([^\"]+)";/){
            $gene_biotype=$1;
        }

        if ($gene_id && $transcript_id){

            if ($gene_biotype eq "protein_coding"){ #restrict to protien coding genes (control for ncRNAs)

                if ($class eq "exon"){
                    if ($dir eq "+"){
                        for ($start .. $end){
                            $transcripts{$gene_id}{$transcript_id}++;
                        }
                    }else{
                        for ($start .. $end){
                            $transcripts{$gene_id}{$transcript_id}++;
                        }
                    }
                }
            }
        }
    }
}
close (GENES1);

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#select longest transcripts per gene
my %longest_transcript; #key=gene_id, value=transcript_id

for my $gene (keys %transcripts){
    my $longest=0;
    for my $transcript (keys %{ $transcripts{$gene}} ){
        if ($transcripts{$gene}{$transcript} > $longest) {
            $longest_transcript{$gene}=$transcript;
            $longest=$transcripts{$gene}{$transcript};
        }
    }
}

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#second pass through the genome, find annotated start codons and setup transcript models for longest transcript of each gene
my %gene_start_codon_fwd;
my %gene_stop_codon_fwd;
my %gene_exons_fwd;
my %gene_start_codon_rev;
my %gene_stop_codon_rev;
my %gene_exons_rev;
my %gene_2_chr; #key = gene_id; value = chr

open(GENES2,$inGtf) || die "can't open $inGtf";      #gft is 1 based
while (<GENES2>){
    unless(/^#/){
        my @b=split("\t");
        my $chr=$b[0];
        my $class=$b[2];
        my $start=$b[3];
        my $end=$b[4];
        my $dir=$b[6];
        my ($gene_id) = $b[8] =~ /gene_id\s"([^\"]+)";/;
        my ($transcript_id) = $b[8] =~ /transcript_id\s"([^\"]+)";/;

        if ($gene_id && $transcript_id){

            if (exists ( $longest_transcript{$gene_id} )){    #if the transcript is in the list of longest transcripts

                if ($transcript_id eq $longest_transcript{$gene_id}){

                    $gene_2_chr{$gene_id}=$chr;

                    if ($dir eq "+"){ #fwd cases. Use start positions as 5'

                        if ($class eq "start_codon"){
                            $gene_start_codon_fwd{$gene_id}=$start;
                        }
                        if ($class eq "stop_codon"){
                            $gene_stop_codon_fwd{$gene_id}=$start;
                        }
                        if ($class eq "exon"){
                            $gene_exons_fwd{$gene_id}{$start}=$end;
                        }

                    }else{ #revese cases use end as 5'

                        if ($class eq "start_codon"){
                            $gene_start_codon_rev{$gene_id}=$end;
                        }
                        if ($class eq "stop_codon"){
                            $gene_stop_codon_rev{$gene_id}=$end;
                        }
                        if ($class eq "exon"){
                            $gene_exons_rev{$gene_id}{$start}=$end;
                        }

                    }
                }
            }
        }
    }
}
close(GENES2);

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#setup trascript models

my %gene_model_fwd;
my %start_coord_fwd;
my %stop_coord_fwd;

#5' is the #gene_model{$gene}{0}
#3' is the last coord
my %three_prime_most_coord_fwd;

#gene_model{$gene}{0}=12345   #1st nt of start codon
#gene_model{$gene}{1}=12346
#gene_model{$gene}{2}=12347
#gene_model{$gene}{3}=12348
#gene_model{$gene}{4}=12349
#...
#to end of exons             #last nt of stop codon

for my $gene (keys %gene_exons_fwd){
    if ( (exists ($gene_start_codon_fwd{$gene})) && (exists ($gene_stop_codon_fwd{$gene})) ) { #restrict to genes with annotated start + stop codon

        my $model_pos=0;

        for my $exon_start (sort {$a <=> $b} keys %{ $gene_exons_fwd{$gene} } ){
            my $exon_end=$gene_exons_fwd{$gene}{$exon_start};

            #fwd exons are in ascending order
            # start(-1)-> 100958 100975
            #             101077 101715 <-end(+1)

            for ($exon_start .. $exon_end){
                $gene_model_fwd{$gene}{$model_pos}=$_;

                if ($_ == $gene_stop_codon_fwd{$gene}){
                    $stop_coord_fwd{$gene}=$model_pos;    #find the index of the stop codon per gene
                }

                if ($_ == $gene_start_codon_fwd{$gene}){
                    $start_coord_fwd{$gene}=$model_pos;    #find the index of the start codon per gene
                }
                $model_pos++;
            }
        }
        $three_prime_most_coord_fwd{$gene}=$model_pos-1; #store the 3 prime most position of each gene
    }
}

my %gene_model_rev;
my %start_coord_rev;
my %stop_coord_rev;

#5' is the #gene_model{$gene}{0}
#3' is the last coord
my %three_prime_most_coord_rev;

for my $gene (keys %gene_exons_rev){
    if ( (exists ($gene_start_codon_rev{$gene})) && (exists ($gene_stop_codon_rev{$gene})) ) { #restrict to genes with annotated start + stop codon

        my $model_pos=0;

        for my $exon_end (reverse (sort {$a <=> $b} keys %{ $gene_exons_rev{$gene} } )){
            my $exon_start=$gene_exons_rev{$gene}{$exon_end};

            #rev exons are sorted in decending order  
            #           447087 447794 <-start(+1)
            # end(-1)-> 446060 446254

            while ($exon_start >= $exon_end){
                $gene_model_rev{$gene}{$model_pos}=$exon_start;

                if ($exon_start == $gene_stop_codon_rev{$gene}){
                    $stop_coord_rev{$gene}=$model_pos;    #find the index of the stop codon per gene
                }
                if ($exon_start == $gene_start_codon_rev{$gene}){
                    $start_coord_rev{$gene}=$model_pos;    #find the index of the start codon per gene
                }
                $model_pos++;
                $exon_start--;
            }
        }
        $three_prime_most_coord_rev{$gene}=$model_pos-1; #store the 3 prime most position of each gene
    }
}

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#parse leaders
my %leader_positions_fwd;
my %leader_positions_rev;

#filters:
my %leader_length;
my %cage_peak_value;
my %overlaps_inframe_gene;
my %leader_overlaps_upstream;
my %gene_overlaps_downstream_leader;

#gene_id,       0
#transcript_id, 1
#chr,           2
#dir,           3
#overlaps_inframe_gene,           4
#leader_overlaps_upstream,        5
#gene_overlaps_downstream_leader, 6
#highest_cage_peak,               7
#count_at_highest_cage_peak,      8
#leader_length                    9

#ENSDARG00000037917,ENSDART00000161963,3,fwd,FALSE,FALSE,FALSE,34716685,2.30440468389324,143
#ENSDARG00000104069,ENSDART00000167982,5,fwd,FALSE,FALSE,FALSE,337237,9.98331428397882,122
#ENSDARG00000037925,ENSDART00000130591,3,fwd,FALSE,FALSE,FALSE,36250098,0.346429817638395,-679
#ENSDARG00000029263,ENSDART00000078466,3,fwd,FALSE,FALSE,FALSE,NaN,0,NaN

open(LEAD, $leaders) || die "can't open $leaders";
while (<LEAD>){
    unless(/^#/){
 
        chomp;
        my @b=split(",");

        my $gene=$b[0];
        my $transcript=$b[1];
        my $chr=$b[2];
        my $dir=$b[3];
        my $overlaps_inframe_gene=$b[4];
        my $leader_overlaps_upstream=$b[5];
        my $gene_overlaps_downstream_leader=$b[6];
        my $highest_cage_peak=$b[7];
        my $count_at_highest_cage_peak=$b[8];
        my $leader_length=$b[9];

        if ($overlaps_inframe_gene eq "TRUE"){ $overlaps_inframe_gene{$gene}=1; }
        if ($leader_overlaps_upstream eq "TRUE"){ $leader_overlaps_upstream=1; }
        if ($gene_overlaps_downstream_leader eq "TRUE"){ $gene_overlaps_downstream_leader=1; }

        unless ($leader_length eq "NaN"){  #only take genes that have a detectable cage peak

            unless ($leader_length < 0){  #exlude genes with negative leader sizes, as they cuase problems with FPKM

                if ($dir eq "fwd"){ 
                    if (exists ($start_coord_fwd{$gene})){
                        unless ($highest_cage_peak >=  $gene_model_fwd{$gene}{$start_coord_fwd{$gene}}){  #exclude genes where the TSS is downstream of the start codon
                            $leader_positions_fwd{$gene}=$highest_cage_peak;
                            $cage_peak_value{$gene}=$count_at_highest_cage_peak;
                            $leader_length{$gene}=$leader_length;
                        } 
                    }
                }else{
                    if (exists ($start_coord_rev{$gene})){
                        unless ($highest_cage_peak <=  $gene_model_rev{$gene}{$start_coord_rev{$gene}}){  #exclude genes where the TSS is downstream of the start codon
                            $leader_positions_rev{$gene}=$highest_cage_peak;
                            $cage_peak_value{$gene}=$count_at_highest_cage_peak;
                            $leader_length{$gene}=$leader_length;
                        }
                    }
                }             
            }
        }
    }
}
close(LEAD);

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#extend transcript models to incorportate cage derived leaders
my %leader_start_coord; #key=gene, value=coord

for my $gene (keys %leader_positions_fwd){

    if (exists ($gene_2_chr{$gene})){  #to restict to protien_coding genes
        my $leader_start=$leader_positions_fwd{$gene};
        my $three_prime_coord=$three_prime_most_coord_fwd{$gene};
        my $three_prime_pos=$gene_model_fwd{$gene}{$three_prime_coord};
        my $five_prime_coord=0;
        my $five_prime_pos=$gene_model_fwd{$gene}{$five_prime_coord};

        if ($leader_start >= $five_prime_pos){  #great, just find and save the coordinate
 
            for my $coord ($five_prime_coord .. $three_prime_coord){
                my $pos=$gene_model_fwd{$gene}{$coord};       
                if ($pos == $leader_start){
                    $leader_start_coord{$gene}=$coord;
                    last;
                }
            }

        }else{  #extend the coords

            my $extended_coord=0; 
            while ($five_prime_pos > $leader_start){        
                $extended_coord--;
                $five_prime_pos--;
                $gene_model_fwd{$gene}{$extended_coord}=$five_prime_pos;
            }
            $leader_start_coord{$gene}=$extended_coord;
        }
    }
} 

for my $gene (keys %leader_positions_rev){

    if (exists ($gene_2_chr{$gene})){  #to restict to protien_coding genes
        my $leader_start=$leader_positions_rev{$gene};
        my $three_prime_coord=$three_prime_most_coord_rev{$gene};
        my $three_prime_pos=$gene_model_rev{$gene}{$three_prime_coord};
        my $five_prime_coord=0;
        my $five_prime_pos=$gene_model_rev{$gene}{$five_prime_coord};
 
        if ($leader_start <= $five_prime_pos){  #great, just find and save the coordinate

            for my $coord ($five_prime_coord .. $three_prime_coord){
                my $pos=$gene_model_rev{$gene}{$coord};
                if ($pos == $leader_start){
                    $leader_start_coord{$gene}=$coord;
                    last;
                }
            }

        }else{   #extend the coords

            my $extended_coord=0;
            while ($five_prime_pos < $leader_start){
                $extended_coord--;
                $five_prime_pos++;
                $gene_model_rev{$gene}{$extended_coord}=$five_prime_pos;
            }
            $leader_start_coord{$gene}=$extended_coord;
        }
    }
}

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#Transcript coord key
#Transcript 5' coord   0                                  #1st nt of annotated transcript
#Transcript 3' coord   $three_prime_most_coord_???{$gene} #last nt of transcript
#Start codon coord     $start_coord_???{$gene}            #1st nt in start codon
#Stop codon coord      $stop_coord_???{$gene}             #1st nt in stop codon
#Leader start coord    $leader_start_coord{$gene}         #1st nt of cage defined leader

#$gene_model_fwd{$gene}{$coord}==genomic position

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#loop though genes and assign leader and CDS search hashes
my %leader_search_fwd; #key1=chr, key2=pos, value=gene
my %leader_search_rev; #key1=chr, key2=pos, value=gene
my %CDS_search_fwd; #key1=chr, key2=pos, value=gene
my %CDS_search_rev; #key1=chr, key2=pos, value=gene

my %CDS_counts_RIBOseq; #key = gene, value = count sum
my %leader_counts_RIBOseq; #key, value = count sum

my %CDS_counts_RNAseq; 
my %leader_counts_RNAseq; 

my %CDS_counts_TCPseq_SSU; 
my %leader_counts_TCPseq_SSU;

my %CDS_counts_TCPseq_LSU;
my %leader_counts_TCPseq_LSU;

for my $gene (keys %gene_model_fwd){
    my $chr=$gene_2_chr{$gene};
    my $start_coord=$start_coord_fwd{$gene};
    my $stop_coord=$stop_coord_fwd{$gene};

    $CDS_counts_RIBOseq{$gene}=0;
    $leader_counts_RIBOseq{$gene}=0;

    $CDS_counts_RNAseq{$gene}=0; 
    $leader_counts_RNAseq{$gene}=0; 

    $CDS_counts_TCPseq_SSU{$gene}=0;
    $leader_counts_TCPseq_SSU{$gene}=0;

    $CDS_counts_TCPseq_LSU{$gene}=0;
    $leader_counts_TCPseq_LSU{$gene}=0;

    for my $coord (sort {$a <=> $b} keys %{ $gene_model_fwd{$gene} } ){
        my $pos=$gene_model_fwd{$gene}{$coord};
        
        if ($coord < $start_coord){
            $leader_search_fwd{$chr}{$pos}=$gene;
        }elsif($coord <= ($stop_coord+2)){  #limit to the stop codon
            $CDS_search_fwd{$chr}{$pos}=$gene;
        }
    }
}

for my $gene (keys %gene_model_rev){
    my $chr=$gene_2_chr{$gene};
    my $start_coord=$start_coord_rev{$gene};
    my $stop_coord=$stop_coord_rev{$gene};

    $CDS_counts_RIBOseq{$gene}=0;    
    $leader_counts_RIBOseq{$gene}=0;

    $CDS_counts_RNAseq{$gene}=0;
    $leader_counts_RNAseq{$gene}=0;

    $CDS_counts_TCPseq_SSU{$gene}=0;
    $leader_counts_TCPseq_SSU{$gene}=0;

    $CDS_counts_TCPseq_LSU{$gene}=0;
    $leader_counts_TCPseq_LSU{$gene}=0;

    for my $coord (sort {$a <=> $b} keys %{ $gene_model_rev{$gene} } ){
        my $pos=$gene_model_rev{$gene}{$coord};

        if ($coord < $start_coord){
            $leader_search_rev{$chr}{$pos}=$gene;
        }elsif($coord <= ($stop_coord+2)){
            $CDS_search_rev{$chr}{$pos}=$gene;
        }
    }
}      

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#open riboseq and assign
my $total_RIBOseq_wig_count_fwd=0;
my $total_RIBOseq_wig_count_rev=0;

my $chr;
open (WIG1, $wig_RIBOseq_fwd) || die "Can't open $wig_RIBOseq_fwd";
while (<WIG1>){

    if (/variableStep\schrom=(.*)/){
        $chr=$1;
    }else{
        #split line
        my @line=split("\t");

        my $pos=$line[0];
        my $count=$line[1];

        #This is gtf dependant (required for Zv10)
        if ($chr=~/chr(.*)/){ #ensembl chromosome names do not contain the "chr" prefix
            $chr=$1;
        }

        if (exists ( $leader_search_fwd{$chr}{$pos})){   #is riboseq directional?
            $leader_counts_RIBOseq{$leader_search_fwd{$chr}{$pos}}+=$count;
        }elsif( exists($CDS_search_fwd{$chr}{$pos})){
            $CDS_counts_RIBOseq{$CDS_search_fwd{$chr}{$pos}}+=$count;
        }
        $total_RIBOseq_wig_count_fwd+=$count;
    }
}
close(WIG1);

open (WIG2, $wig_RIBOseq_rev) || die "Can't open $wig_RIBOseq_rev";
while (<WIG2>){

    if (/variableStep\schrom=(.*)/){
        $chr=$1;
    }else{
        #split line
        my @line=split("\t");

        my $pos=$line[0];
        my $count=$line[1];

        #This is gtf dependant (required for Zv10)
        if ($chr=~/chr(.*)/){ #ensembl chromosome names do not contain the "chr" prefix
            $chr=$1;
        }

        if (exists ( $leader_search_rev{$chr}{$pos})){   #riboseq is directional so only search in strand
            $leader_counts_RIBOseq{$leader_search_rev{$chr}{$pos}}+=$count;
        }elsif( exists($CDS_search_rev{$chr}{$pos})){
            $CDS_counts_RIBOseq{$CDS_search_rev{$chr}{$pos}}+=$count;
        }
        $total_RIBOseq_wig_count_rev+=$count;
    }
}
close(WIG2);

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#open and store RNAseq counts
my $total_RNAseq_count=0;

open BAM,"samtools view $bam_RNAseq |";

while(<BAM>){

    next if(/^(\@)/);  ## skipping the header lines (if you used -h in the samools command)
    s/\n//;  s/\r//;  ## removing new line
    my @sam = split(/\t+/);  ## splitting SAM line into array

    my $leftMost=$sam[3]; #leftmost position of match 5' for fwd, 3' for rev
    my $flag=$sam[1];
    my $chr=$sam[2];
    my $mapq=$sam[4];
    my $cigar=$sam[5];
    my $CDS_hit=0;
    my $leader_hit=0;

    unless ($flag & 0x4){   #if aligned

        $total_RNAseq_count++;

        if ($flag & 0x10){  #Reverse reads. Starting from the leftmost position parse the cigar and check if matching positions overlap leaders or cds's

            while ($cigar !~ /^$/){
                if ($cigar =~ /^([0-9]+[MIDN])/){
                    my $cigar_part = $1;
                    if ($cigar_part =~ /(\d+)M/){   #alignment matching 
                        for my $pos ($leftMost .. ($leftMost+$1-1)){ #search though this position
                            if (exists ( $leader_search_rev{$chr}{$pos})){   #The yeast rnaseq is directional so only search in strand
                                $leader_hit=$leader_search_rev{$chr}{$pos};
                            }elsif( exists($CDS_search_rev{$chr}{$pos})){
                                $CDS_hit=$CDS_search_rev{$chr}{$pos};
                            }
                        }
                        $leftMost+=$1;
                    } elsif ($cigar_part =~ /(\d+)I/){  #insertion (to the reference) #do nothing this region is not in the reference
                    } elsif ($cigar_part =~ /(\d+)D/){  #deletion (from the reference)
                        $leftMost+=$1; #skip this position. Add to position count but do not search
                    } elsif ($cigar_part =~ /(\d+)N/){  #Skipped region from the reference
                        $leftMost+=$1; #skip this position. Add to position count but do not search  
                    }
                    $cigar =~ s/$cigar_part//;
                }
            }

        }else{ #Forward reads. Starting from the leftmost position parse the cigar and check if matching positions overlap leaders or cds's
             while ($cigar !~ /^$/){
                if ($cigar =~ /^([0-9]+[MIDN])/){
                    my $cigar_part = $1;
                    if ($cigar_part =~ /(\d+)M/){   #alignment matching 
                        for my $pos ($leftMost .. ($leftMost+$1-1)){ #search though this position
                            if (exists ( $leader_search_fwd{$chr}{$pos})){   #The yeast rnaseq is directional so only search in strand
                                $leader_hit=$leader_search_fwd{$chr}{$pos};
                            }elsif( exists($CDS_search_fwd{$chr}{$pos})){
                                $CDS_hit=$CDS_search_fwd{$chr}{$pos};
                            }
                        }
                        $leftMost+=$1;
                    } elsif ($cigar_part =~ /(\d+)I/){  #insertion (to the reference) #do nothing this region is not in the reference
                    } elsif ($cigar_part =~ /(\d+)D/){  #deletion (from the reference)
                        $leftMost+=$1; #skip this position. Add to position count but do not search
                    } elsif ($cigar_part =~ /(\d+)N/){  #Skipped region from the reference
                        $leftMost+=$1; #skip this position. Add to position count but do not search  
                    }
                    $cigar =~ s/$cigar_part//;
                }
            }
        }

        if ($leader_hit){ #now contains gene name
            $leader_counts_RNAseq{$leader_hit}+=1;
        }

        if ($CDS_hit){ #now contains gene name
            $CDS_counts_RNAseq{$CDS_hit}+=1;
        }
    }
}
close (BAM);

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#open and store TCPseq SSU counts
my $total_TCPseq_SSU_count=0;

open BAM2,"samtools view $bam_TCPseq_SSU |";

while(<BAM2>){

    next if(/^(\@)/);  ## skipping the header lines (if you used -h in the samools command)
    s/\n//;  s/\r//;  ## removing new line
    my @sam = split(/\t+/);  ## splitting SAM line into array

    my $leftMost=$sam[3]; #leftmost position of match 5' for fwd, 3' for rev
    my $flag=$sam[1];
    my $chr=$sam[2];
    my $mapq=$sam[4];
    my $cigar=$sam[5];
    my $CDS_hit=0;
    my $leader_hit=0;

    unless ($flag & 0x4){   #if aligned

        $total_TCPseq_SSU_count++;

        if ($flag & 0x10){  #Reverse reads. Starting from the leftmost position parse the cigar and check if matching positions overlap leaders or cds's

            while ($cigar !~ /^$/){
                if ($cigar =~ /^([0-9]+[MIDN])/){
                    my $cigar_part = $1;
                    if ($cigar_part =~ /(\d+)M/){   #alignment matching 
                        for my $pos ($leftMost .. ($leftMost+$1-1)){ #search though this position
                            if (exists ( $leader_search_rev{$chr}{$pos})){   #The yeast rnaseq is directional so only search in strand                              
                                $leader_hit=$leader_search_rev{$chr}{$pos};
                            }elsif( exists($CDS_search_rev{$chr}{$pos})){
                                $CDS_hit=$CDS_search_rev{$chr}{$pos};
                            }
                        }
                        $leftMost+=$1;
                    } elsif ($cigar_part =~ /(\d+)I/){  #insertion (to the reference) #do nothing this region is not in the reference
                    } elsif ($cigar_part =~ /(\d+)D/){  #deletion (from the reference)
                        $leftMost+=$1; #skip this position. Add to position count but do not search
                    } elsif ($cigar_part =~ /(\d+)N/){  #Skipped region from the reference
                        $leftMost+=$1; #skip this position. Add to position count but do not search  
                    }
                    $cigar =~ s/$cigar_part//;
                }
            }

        }else{ #Forward reads. Starting from the leftmost position parse the cigar and check if matching positions overlap leaders or cds's
             while ($cigar !~ /^$/){
                if ($cigar =~ /^([0-9]+[MIDN])/){
                    my $cigar_part = $1;
                    if ($cigar_part =~ /(\d+)M/){   #alignment matching 
                        for my $pos ($leftMost .. ($leftMost+$1-1)){ #search though this position
                            if (exists ( $leader_search_fwd{$chr}{$pos})){   #The yeast rnaseq is directional so only search in strand                              
                                $leader_hit=$leader_search_fwd{$chr}{$pos};
                            }elsif( exists($CDS_search_fwd{$chr}{$pos})){
                                $CDS_hit=$CDS_search_fwd{$chr}{$pos};
                            }
                        }
                        $leftMost+=$1;
                    } elsif ($cigar_part =~ /(\d+)I/){  #insertion (to the reference) #do nothing this region is not in the reference
                    } elsif ($cigar_part =~ /(\d+)D/){  #deletion (from the reference)
                        $leftMost+=$1; #skip this position. Add to position count but do not search
                    } elsif ($cigar_part =~ /(\d+)N/){  #Skipped region from the reference
                        $leftMost+=$1; #skip this position. Add to position count but do not search  
                    }
                    $cigar =~ s/$cigar_part//;
                }
            }
        }

        if ($leader_hit){ #now contains gene name   #assign counts preferencially to leaders
            $leader_counts_TCPseq_SSU{$leader_hit}+=1;
        }elsif ($CDS_hit){ #now contains gene name
            $CDS_counts_TCPseq_SSU{$CDS_hit}+=1;
        }
    }
}
close (BAM2);

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#open and store TPCseq LSU counts
my $total_TCPseq_LSU_count=0;

open BAM3,"samtools view $bam_TCPseq_LSU |";

while(<BAM3>){

    next if(/^(\@)/);  ## skipping the header lines (if you used -h in the samools command)
    s/\n//;  s/\r//;  ## removing new line
    my @sam = split(/\t+/);  ## splitting SAM line into array

    my $leftMost=$sam[3]; #leftmost position of match 5' for fwd, 3' for rev
    my $flag=$sam[1];
    my $chr=$sam[2];
    my $mapq=$sam[4];
    my $cigar=$sam[5];
    my $CDS_hit=0;
    my $leader_hit=0;

    unless ($flag & 0x4){   #if aligned

        $total_TCPseq_LSU_count++;
        
        if ($flag & 0x10){  #Reverse reads. Starting from the leftmost position parse the cigar and check if matching positions overlap leaders or cds's
            
            while ($cigar !~ /^$/){
                if ($cigar =~ /^([0-9]+[MIDN])/){
                    my $cigar_part = $1;
                    if ($cigar_part =~ /(\d+)M/){   #alignment matching 
                        for my $pos ($leftMost .. ($leftMost+$1-1)){ #search though this position
                            if (exists ( $leader_search_rev{$chr}{$pos})){   #The yeast rnaseq is directional so only search in strand                              
                                $leader_hit=$leader_search_rev{$chr}{$pos};
                            }elsif( exists($CDS_search_rev{$chr}{$pos})){
                                $CDS_hit=$CDS_search_rev{$chr}{$pos};
                            }
                        }
                        $leftMost+=$1;
                    } elsif ($cigar_part =~ /(\d+)I/){  #insertion (to the reference) #do nothing this region is not in the reference
                    } elsif ($cigar_part =~ /(\d+)D/){  #deletion (from the reference)
                        $leftMost+=$1; #skip this position. Add to position count but do not search
                    } elsif ($cigar_part =~ /(\d+)N/){  #Skipped region from the reference
                        $leftMost+=$1; #skip this position. Add to position count but do not search  
                    }
                    $cigar =~ s/$cigar_part//;
                }
            }
        
        }else{ #Forward reads. Starting from the leftmost position parse the cigar and check if matching positions overlap leaders or cds's
             while ($cigar !~ /^$/){
                if ($cigar =~ /^([0-9]+[MIDN])/){
                    my $cigar_part = $1;
                    if ($cigar_part =~ /(\d+)M/){   #alignment matching 
                        for my $pos ($leftMost .. ($leftMost+$1-1)){ #search though this position
                            if (exists ( $leader_search_fwd{$chr}{$pos})){   #The yeast rnaseq is directional so only search in strand                              
                                $leader_hit=$leader_search_fwd{$chr}{$pos};
                            }elsif( exists($CDS_search_fwd{$chr}{$pos})){
                                $CDS_hit=$CDS_search_fwd{$chr}{$pos};
                            }
                        }
                        $leftMost+=$1;
                    } elsif ($cigar_part =~ /(\d+)I/){  #insertion (to the reference) #do nothing this region is not in the reference
                    } elsif ($cigar_part =~ /(\d+)D/){  #deletion (from the reference)
                        $leftMost+=$1; #skip this position. Add to position count but do not search
                    } elsif ($cigar_part =~ /(\d+)N/){  #Skipped region from the reference
                        $leftMost+=$1; #skip this position. Add to position count but do not search  
                    }
                    $cigar =~ s/$cigar_part//;
                }
            }
        }

        if ($CDS_hit){ #now contains gene name  #assign counts preferencially to CDSs
            $CDS_counts_TCPseq_LSU{$CDS_hit}+=1;
        }elsif($leader_hit){
            $leader_counts_TCPseq_LSU{$leader_hit}+=1;
        }
    }
}
close (BAM3);

#¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤#
#output
my $total_RIBOseq_aligned_sum=$total_RIBOseq_wig_count_fwd+$total_RIBOseq_wig_count_rev;
my $total_RNAseq_aligned_sum=$total_RNAseq_count;
my $total_TCPseq_SSU_aligned_sum=$total_TCPseq_SSU_count;
my $total_TCPseq_LSU_aligned_sum=$total_TCPseq_LSU_count;

#header
#print "#gene_id,transcript_id,chr,dir,leader_start_position,cage_tags_at_leader_peak,leader_length,CDS_length,leader_count_RFP,leader_count_RNA,leader_RPF_FPKM,leader_RNA_FPKM,CDS_count_RPF,CDS_count_RNA,CDS_RPF_FPKM,CDS_RNA_FPKM,overlapping_gene,leader_potentially_overlaps_upstream_gene,gene_potentially_overlaps_downstream_leader\n";

print "#gene_id,transcript_id,cage_tags_at_leader_peak,leader_length,CDS_length,leader_RNA_FPKM,leader_RFP_FPKM,leader_SSU_FPKM,leader_LSU_FPKM,CDS_RNA_FPKM,CDS_RFP_FPKM,CDS_SSU_FPKM,CDS_LSU_FPKM,overlapping_gene,leader_potentially_overlaps_upstream_gene,gene_potentially_overlaps_downstream_leader\n";

for my $gene (keys %leader_positions_fwd){ #restrict to genes with cage defined leaders

    if (exists ($gene_2_chr{$gene})){  #restict to protien_coding genes
        my $chr=$gene_2_chr{$gene};
        my $transcript=$longest_transcript{$gene};
        my $start_coord=$start_coord_fwd{$gene};
        my $stop_coord=$stop_coord_fwd{$gene};
        my $CDS_length=(($stop_coord+2)-$start_coord)+1;
        my $leader_length=$leader_length{$gene};
        my $highest_cage_peak_value=$cage_peak_value{$gene};
        my $leader_start=$leader_positions_fwd{$gene};
        my $overlapping_gene="FALSE";
        my $leader_potentially_overlaps_upstream_gene="FALSE";
        my $gene_potentially_overlaps_downstream_leader="FALSE";
        
        if (exists ($overlaps_inframe_gene{$gene})){           $overlapping_gene="TRUE"; }
        if (exists ($leader_overlaps_upstream{$gene})){        $leader_potentially_overlaps_upstream_gene="TRUE"; }
        if (exists ($gene_overlaps_downstream_leader{$gene})){ $gene_potentially_overlaps_downstream_leader="TRUE"; }

        my $leader_count_RIBOseq=$leader_counts_RIBOseq{$gene};
        my $CDS_count_RIBOseq=$CDS_counts_RIBOseq{$gene};

        my $leader_count_RNAseq=$leader_counts_RNAseq{$gene};
        my $CDS_count_RNAseq=$CDS_counts_RNAseq{$gene};

        my $leader_count_TCPseq_SSU=$leader_counts_TCPseq_SSU{$gene};
        my $CDS_count_TCPseq_SSU=$CDS_counts_TCPseq_SSU{$gene};

        my $leader_count_TCPseq_LSU=$leader_counts_TCPseq_LSU{$gene};
        my $CDS_count_TCPseq_LSU=$CDS_counts_TCPseq_LSU{$gene};

        #my $scaling_factor=$total_aligned_sum/1000000;
        #my $leader_RMP=$leader_count/$scaling_factor;
        #my $leader_FPKM_manual=$leader_RMP/($leader_length/1000);

        my $leader_RIBOseq_FPKM=eval{ $leader_count_RIBOseq/($leader_length*$total_RIBOseq_aligned_sum)*1000000000 } || 0;
        my $CDS_RIBOseq_FPKM=eval{ $CDS_count_RIBOseq/($CDS_length*$total_RIBOseq_aligned_sum)*1000000000 } || 0;

        my $leader_RNAseq_FPKM=eval{ $leader_count_RNAseq/($leader_length*$total_RNAseq_aligned_sum)*1000000000 } || 0;
        my $CDS_RNAseq_FPKM=eval{ $CDS_count_RNAseq/($CDS_length*$total_RNAseq_aligned_sum)*1000000000 } || 0;

        my $leader_TCPseq_SSU_FPKM=eval{ $leader_count_TCPseq_SSU/($leader_length*$total_TCPseq_SSU_aligned_sum)*1000000000 } || 0;
        my $CDS_TCPseq_SSU_FPKM=eval{ $CDS_count_TCPseq_SSU/($CDS_length*$total_TCPseq_SSU_aligned_sum)*1000000000 } || 0;

        my $leader_TCPseq_LSU_FPKM=eval{ $leader_count_TCPseq_LSU/($leader_length*$total_TCPseq_LSU_aligned_sum)*1000000000 } || 0;
        my $CDS_TCPseq_LSU_FPKM=eval{ $CDS_count_TCPseq_LSU/($CDS_length*$total_TCPseq_LSU_aligned_sum)*1000000000 } || 0;

        print "$gene,$transcript,$highest_cage_peak_value,$leader_length,$CDS_length,$leader_RNAseq_FPKM,$leader_RIBOseq_FPKM,$leader_TCPseq_SSU_FPKM,$leader_TCPseq_LSU_FPKM,$CDS_RNAseq_FPKM,$CDS_RIBOseq_FPKM,$CDS_TCPseq_SSU_FPKM,$CDS_TCPseq_LSU_FPKM,$overlapping_gene,$leader_potentially_overlaps_upstream_gene,$gene_potentially_overlaps_downstream_leader\n";

        #print "$gene,$transcript,$chr,fwd,$leader_start,$highest_cage_peak_value,$leader_length,$CDS_length,$leader_count_RIBOseq,$leader_count_RNAseq,$leader_count_TCPseq_SSU,$leader_count_TCPseq_LSU,$leader_RIBOseq_FPKM,$leader_RNAseq_FPKM,$leader_TCPseq_SSU_FPKM,$leader_TCPseq_LSU_FPKM,$CDS_count_RIBOseq,$CDS_count_RNAseq,$CDS_count_TCPseq_SSU,$CDS_count_TCPseq_LSU,$CDS_RIBOseq_FPKM,$CDS_RNAseq_FPKM,$CDS_TCPseq_SSU_FPKM,$CDS_TCPseq_LSU_FPKM,$overlapping_gene,$leader_potentially_overlaps_upstream_gene,$gene_potentially_overlaps_downstream_leader\n";
    }
}

for my $gene (keys %leader_positions_rev){ #restrict to genes with cage defined leaders

    if (exists ($gene_2_chr{$gene})){  #restict to protien_coding genes
        my $chr=$gene_2_chr{$gene};
        my $transcript=$longest_transcript{$gene};
        my $start_coord=$start_coord_rev{$gene};
        my $stop_coord=$stop_coord_rev{$gene};
        my $CDS_length=(($stop_coord+2)-$start_coord)+1;
        my $leader_length=$leader_length{$gene};
        my $highest_cage_peak_value=$cage_peak_value{$gene};
        my $leader_start=$leader_positions_rev{$gene};
        my $overlapping_gene="FALSE";
        my $leader_potentially_overlaps_upstream_gene="FALSE";
        my $gene_potentially_overlaps_downstream_leader="FALSE";
        
        if (exists ($overlaps_inframe_gene{$gene})){           $overlapping_gene="TRUE"; }
        if (exists ($leader_overlaps_upstream{$gene})){        $leader_potentially_overlaps_upstream_gene="TRUE"; }
        if (exists ($gene_overlaps_downstream_leader{$gene})){ $gene_potentially_overlaps_downstream_leader="TRUE"; }

        my $leader_count_RIBOseq=$leader_counts_RIBOseq{$gene};
        my $CDS_count_RIBOseq=$CDS_counts_RIBOseq{$gene};

        my $leader_count_RNAseq=$leader_counts_RNAseq{$gene};
        my $CDS_count_RNAseq=$CDS_counts_RNAseq{$gene};

        my $leader_count_TCPseq_SSU=$leader_counts_TCPseq_SSU{$gene};
        my $CDS_count_TCPseq_SSU=$CDS_counts_TCPseq_SSU{$gene};

        my $leader_count_TCPseq_LSU=$leader_counts_TCPseq_LSU{$gene};
        my $CDS_count_TCPseq_LSU=$CDS_counts_TCPseq_LSU{$gene};

        #my $scaling_factor=$total_aligned_sum/1000000;
        #my $leader_RMP=$leader_count/$scaling_factor;
        #my $leader_FPKM_new=$leader_RMP/($leader_length/1000);

        my $leader_RIBOseq_FPKM=eval{ $leader_count_RIBOseq/($leader_length*$total_RIBOseq_aligned_sum)*1000000000 } || 0;
        my $CDS_RIBOseq_FPKM=eval{ $CDS_count_RIBOseq/($CDS_length*$total_RIBOseq_aligned_sum)*1000000000 } || 0;

        my $leader_RNAseq_FPKM=eval{ $leader_count_RNAseq/($leader_length*$total_RNAseq_aligned_sum)*1000000000 } || 0;
        my $CDS_RNAseq_FPKM=eval{ $CDS_count_RNAseq/($CDS_length*$total_RNAseq_aligned_sum)*1000000000 } || 0;

        my $leader_TCPseq_SSU_FPKM=eval{ $leader_count_TCPseq_SSU/($leader_length*$total_TCPseq_SSU_aligned_sum)*1000000000 } || 0;
        my $CDS_TCPseq_SSU_FPKM=eval{ $CDS_count_TCPseq_SSU/($CDS_length*$total_TCPseq_SSU_aligned_sum)*1000000000 } || 0;

        my $leader_TCPseq_LSU_FPKM=eval{ $leader_count_TCPseq_LSU/($leader_length*$total_TCPseq_LSU_aligned_sum)*1000000000 } || 0;
        my $CDS_TCPseq_LSU_FPKM=eval{ $CDS_count_TCPseq_LSU/($CDS_length*$total_TCPseq_LSU_aligned_sum)*1000000000 } || 0;

        print "$gene,$transcript,$leader_start,$highest_cage_peak_value,$leader_length,$CDS_length,$leader_RNAseq_FPKM,$leader_RIBOseq_FPKM,$leader_TCPseq_SSU_FPKM,$leader_TCPseq_LSU_FPKM,$CDS_RNAseq_FPKM,$CDS_RIBOseq_FPKM,$CDS_TCPseq_SSU_FPKM,$CDS_TCPseq_LSU_FPKM,$overlapping_gene,$leader_potentially_overlaps_upstream_gene,$gene_potentially_overlaps_downstream_leader\n";

        #print "$gene,$transcript,$chr,fwd,$highest_cage_peak_value,$leader_length,$CDS_length,$leader_count_RIBOseq,$leader_count_RNAseq,$leader_count_TCPseq_SSU,$leader_count_TCPseq_LSU,$leader_RIBOseq_FPKM,$leader_RNAseq_FPKM,$leader_TCPseq_SSU_FPKM,$leader_TCPseq_LSU_FPKM,$CDS_count_RIBOseq,$CDS_count_RNAseq,$CDS_count_TCPseq_SSU,$CDS_count_TCPseq_LSU,$CDS_RIBOseq_FPKM,$CDS_RNAseq_FPKM,$CDS_TCPseq_SSU_FPKM,$CDS_TCPseq_LSU_FPKM,$overlapping_gene,$leader_potentially_overlaps_upstream_gene,$gene_potentially_overlaps_downstream_leader\n";
    }
}

exit;
