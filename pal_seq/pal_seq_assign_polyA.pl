#!/usr/bin/perl -w
use strict;
#05/10/15
#take fitted values, gff and a sam file then assign polyA lenghts to transcripts

my $inGFF=$ARGV[0];	#bartel transcript models
my $inFit=$ARGV[2]; #fitted polyA lenghts per illimina spot
my $inSam=$ARGV[1]; #aligned samfile
my %three_prime; #key = chr, key2 = coords, value=transcript_id,possible othertranscript with 3' utr at this coord
my %aligned2utr; #key = read identifier, value = transcript_id(s)
my %tail2trans; #key = transcript, value = tail length(s)

#####
#1 open gff file get the coords or 3' UTR for each gene
#####

open (IN1, $inGFF) || die;
while (<IN1>){	

	#skip headers
	unless (/^ENS/){
		next;
	}
	
	my @a=split("\t",$_);
	my $t_id=$a[0];
	my $chr=$a[2];
	my $dir=$a[3];
	my $txStart=$a[4];
	my $txEnd=$a[5];
	my $cdsStart=$a[6];
	my $cdsEnd=$a[7];

	#check for introns, and exclude these posotion
	my %introns;
	my $exonN=$a[8];
	my @exonS=split(",",$a[9]);
	my @exonE=split(",",$a[10]);

	#n=3
	#e[0]-s[1]
	#e[1]-s[2]

	#setup introns, by exon count
	for (0 .. ($exonN-2)){
		$introns{$exonE[$_]}=$exonS[($_+1)];    
	}

    if ($dir eq "+"){
		for my $nuc ($cdsEnd .. $txEnd){
			my $intronic=0;
			#check that the position is not intronic
			for (sort { $a <=> $b } keys %introns){
				if ($nuc > $_ && $nuc < $introns{$_} ){
					$intronic=1;
				}
			}
			unless ($intronic){
				if (exists ($three_prime{$chr}{$nuc})){
					$three_prime{$chr}{$nuc}.=",".$t_id;
				}else{
					$three_prime{$chr}{$nuc}=$t_id;
				}
			}
		}
	}

	if ($dir eq "-"){
		for my $nuc ($txStart .. $cdsStart){
			my $intronic=0;
			#check that the position is not intronic
			for (sort { $a <=> $b } keys %introns){
				if ($nuc > $_ && $nuc < $introns{$_} ){
					$intronic=1;
				}	
			}
			unless ($intronic){
				if (exists ($three_prime{$chr}{$nuc})){
					$three_prime{$chr}{$nuc}.=",".$t_id;                         
				}else{
					$three_prime{$chr}{$nuc}=$t_id;
				}
			}
		}
	}
}

#####
#2 Open sam file and get read ids for spots matching at transcript 3' utrs
#####

open (IN2, $inSam) || die;
while(<IN2>){

	#reset for each read;
	my %used_transcripts;

	my @sam=split("\t",$_);
 	my @id=split('\.',$sam[0]);  
	my $id_ed=$id[0]."_".$id[1];
	my $flag=$sam[1];
	my $chr=$sam[2];
	my $pos=$sam[3];
	my $cig=$sam[5];

    #get length from seq. I should really adjust this for indels + missmatches
	my $length;
	my $seq=$sam[9];
	$length=length($seq);

	#get length of match from cigar #deal with indel/missmatch
	#if ($cig =~ /(\d+)M/ ){
	#	$length=$1;
	#}		

    #store id if it overlaps with 3'utr. (Sam position shows leftmost coords or a fwd or rev read)
	for ($pos .. ($pos+$length)){
		if (exists ($three_prime{$chr}{$_})){	
			#for each trasncript with a 3' UTR at this position (some transcripts in the bartel gtf overlap)
			my @trans=split(",",$three_prime{$chr}{$_});
			for (@trans){
				#avoid assigning the same read to the same transcript more than once		
				unless (exists $used_transcripts{$_} ){
					if (exists ($aligned2utr{$id_ed})){
						$aligned2utr{$id_ed}.=",".$_;
					}else{
               			$aligned2utr{$id_ed}=$_;
					}
					$used_transcripts{$_}=1;
				}
			}
		}
	}
}

#####
#3 get fitted tail lenghts for each transcript
#####

my $countEx;

open (IN3, $inFit) || die;
while(<IN3>){
	unless(/^""/){
		chomp();
		my $exclude=0;
		my @b=split(",",$_);
		my $read_id=$b[1];
		#remove quotes
		my $read_id_clean=substr $read_id, 1,-1;
		my $e1=$b[4]; #estimated from all polyA standard sizes
		my $e2=$b[5]; #estimated from 1,50,100 polyA standard sizes (as bartel paper)

		#deal with NA's in tail lenghts = set to zero
		if ($e1 =~ /NA/){ $e1=0;}
        if ($e2 =~ /NA/){ $e1=0;}

		#in the paper they exclude reads with a calcualted length of less than -50 or more than 1000 
		if ($e1 < -50){ $exclude=1; }
		if ($e1 > 1000){ $exclude=1; } 

		unless ($exclude){
			if (exists($aligned2utr{$read_id_clean})){
				my @transcripts=split(",",$aligned2utr{$read_id_clean});
				for (@transcripts){
					if (exists ($tail2trans{$_})){
						$tail2trans{$_}.=",".$e1;
					}else{
						$tail2trans{$_}=$e1;
					}
				}
			}
		}
	}
}

print "#transcript_id\ttail_lenghts(comma_seperated)\ttag_count\tmean_length\n";
for (sort keys %tail2trans){
	my $count=0;
	my $sum=0;
	for (my @means=split(",",$tail2trans{$_})){
		unless ($_ eq "NA"){
			$sum+=$_;
			$count++;	
		}
	}
	my $mean=eval{$sum/$count} || 0;
	print "$_\t$tail2trans{$_}\t$count\t$mean\n";
}

exit;
