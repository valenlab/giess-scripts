#!/bin/bash

#AG 15/09/15
#script to process pal seq (from bartel group) fastq datasets. with q64 and 5bp trimming from 5'
#1 Trim adaptors
#2 Map trimmed reads to organism specific reference
#3 Map unmapped reads to mRNA standards

#PATH=$PATH:/Home/ii/adamg/bin/
top2=/net/apps/cbu/stow/tophat-2.0.13/bin/tophat2 

usage(){
cat << EOF
usage: $0 options

script to trim and align riboseq fastq datasets 

OPTIONS:
	-f	path to directory containing input fastq files (gzipped)
	-o	path to output dir
	-s	organism - one of:
			fishz10 (GRCz10_zebrabish)
			fishz9	(Zv9_zebrafish)
			mouse	(GRCm38_mouse)
			human	(GRCh38_human)
			yeast	(R64_1_1_yeast)
			fly	(BDGP6_fruitfly)
	-a	path to file containing adaptor sequences (defaults to bartel PAL-seq adaptor)
	-h	this help message

example usage: trim_and_aling.sh -f <in.fasta.dir> -o <out_dir> -s fish

EOF
}

while getopts ":f:o:s:a:h" opt; do
	case $opt in 
		f)
			in_dir=$OPTARG
		  	echo "-f input folder $OPTARG"
		  	;;
        o)
            out_dir=$OPTARG
            echo "-o output folder $OPTARG"
            ;;
        s)
            species=$OPTARG
            echo "-s speices $OPTARG"
            ;;
		a)	
			adapt=$OPTARG
			echo "-a adaptor file $OPTARG"
			;;
		h)
			usage
			exit
			;;
		?) 
			echo "Invalid option: -$OPTARG"
			usage
			exit 1
		  	;;
	esac
done

if [ ! -e $in_dir ]  || [ ! $out_dir ] || [ ! $species ]; then
		echo "something's missing - check your command lines args"
        usage
        exit 1
fi

if [ ! -d $out_dir ]; then
    mkdir $out_dir
fi

if [ $species == "fishz10" ]; then
	ncRNA=/export/kjempetujafs/valen-group/data/genome/adam/Zv10_zebrafish/ncrna_edited/Danio_rerio.GRCz10.ncrna
	genome=/export/kjempetujafs/valen-group/data/genome/adam/Zv10_zebrafish/bowtie2_index/Danio_rerio.GRCz10
	transome=/export/kjempetujafs/valen-group/data/genome/adam/Zv10_zebrafish/tophat2_transcriptome/Danio_rerio.GRCz10
elif [ $species == "fishz9" ]; then
    ncRNA=/export/kjempetujafs/valen-group/data/genome/adam/Zv9_zebrafish/ncrna_edited/Danio_rerio.Zv9.ncrna
    genome=/export/kjempetujafs/valen-group/data/genome/adam/Zv9_zebrafish/bowtie2_index/Danio_rerio.Zv9
    transome=/export/kjempetujafs/valen-group/data/genome/adam/Zv9_zebrafish/tophat2_transcriptome/Danio_rerio.Zv9.79
else 
	echo "invalid species selected : $species"
	usage
	exit 1
fi

poly_std=/export/kjempetujafs/valen-group/data/genome/adam/PAL_seq_standards/Tail_length_standard_sequences_edited

if [ ! $adapt ]; then
	echo "using default adaptor file"
	adapt=/export/kjempetujafs/valen-group/data/genome/adam/bartel_pal_adaptor_list.fa
fi

for file in ${in_dir}/*.fastq.gz; do

	name=$(basename $file)
	prefix=${name%.fastq.gz}
	echo $prefix

	#------------------------------------------------------------------------------------------
	#1 Trim adaptors
	#------------------------------------------------------------------------------------------
	#-n 4 = 4 iterations of adaptor removal
	#-e 0.1 = error rate, 1 missmatch per 10 bp
	#-O 5 = minimium overlap with an adaptor sequence
	#-m 20 = discard reads shorter than 20 bp
	#-u 8 = trim 5 bases from 3'

	if [ ! -d ${out_dir}/trim ]; then
		mkdir ${out_dir}/trim
	fi

	cutadapt -a file:${adapt} -n 1 -e 0.1 -O 5 -m 20 -u -5 -o ${out_dir}/trim/${prefix}_trim.fastq.gz $file
	
	#------------------------------------------------------------------------------------------
	#2 Map to organism specfic reference
	#------------------------------------------------------------------------------------------
	#-g 1 = max genome multihits 1
	#-x 1 = max transcriptome multihits 1
	#--prefilter-multihits = don't pass multihits to transcriptome
	#-p 8 = use 8 processors
	#--transcriptome-index = use pre built transriptome (from tophat2 -G <<org.gtf>> --transcriptome-index=<<org.trans>>)

	if [ ! -d ${out_dir}/aligned ]; then
		mkdir ${out_dir}/aligned
	fi

	if [ ! -d ${out_dir}/aligned/$prefix ]; then
		mkdir ${out_dir}/aligned/$prefix
	fi
 
	$top2 -g 1 --solexa1.3-quals --no-coverage-search --segment-length=15 --transcriptome-index=$transome -p 16 -o ${out_dir}/aligned/$prefix $genome ${out_dir}/trim/${prefix}_trim.fastq.gz

    #------------------------------------------------------------------------------------------
    #3 Map to poly(A) standards
    #------------------------------------------------------------------------------------------
    #-p 8 = use 8 processors
    #-N 1 = allow 1 missmatch in seed
    #-L 15 = use 15bp seeds

    if [ ! -d ${out_dir}/aligned_std ]; then
        mkdir ${out_dir}/aligned_std
    fi

	bamToFastq -i ${out_dir}/aligned/${prefix}/unmapped.bam -fq ${out_dir}/aligned/${prefix}/unmapped.fastq

	bowtie2 -p 16 -N 1 -L 15 -x $poly_std -U ${out_dir}/aligned/${prefix}/unmapped.fastq | samtools view -bSu - | samtools sort - ${out_dir}/aligned_std/${prefix}_standards

	#----------------------------------------------------------------------------------------------
	#get unique hits from bams
	#----------------------------------------------------------------------------------------------
	#samtools view -q 5 -b


done
