#!/usr/bin/perl -w
use strict;
#17/09/15
#produce a csv of read_id (coords) and intensity values

my $inFq=$ARGV[0];	#fastq of raw reads
my $inInt=$ARGV[1]; #normalised iniesity values tab seperated
my $inSam=$ARGV[2]; #aligned samfile
my %ident; #key = SRR1146562.n identifier, value = x y coords
my %coords2int; #key = x y coords, value = normalised intensity

#open fastq, link idenifiers to xy coords
open (IN1, $inFq) || die;
while (<IN1>){	
	#take every 4th line
	if ($. % 4 == 1){
	chomp;
		if (/@(\w+)\.(\d+)\s\w+-\w+:(\d+):(\d+):(\d+):(\d+)/){
			my $machine=$1;
			my $id=$2;
			my $lane=$3;
			my $tile=$4;
			my $x=$5;
			my $y=$6;
			$ident{$machine."_".$id}=$lane."_".$tile."_".$x."_".$y;
		}
	}
}

#open intensities. link coord to int
open (IN2, $inInt) || die;
while(<IN2>){
	chomp;
	my @int=split("\t",$_);
    my $lane=$int[0];
    my $tile=$int[1];
    my $x=$int[2];
    my $y=$int[3];
	my $spot=$int[4];
	$coords2int{$lane."_".$tile."_".$x."_".$y}=$spot;
}

#for my $k1 (keys %ident){
#	if (exists($coords2int{$ident{$k1}})){ 
#		print "$k1,$coords2int{$ident{$k1}}\n";
#	}
#}

#open sam file
open (IN3, $inSam) || die;
while(<IN3>){
	my @sam=split("\t",$_);

 	my @id=split('\.',$sam[0]);  
	my $id_ed=$id[0]."_".$id[1];

	my @len=split('_',$sam[2]);

	if (exists$ident{$id_ed}){
		#machine_id,lane_tile_x_y,int,length	
		print "$id_ed,$ident{$id_ed},$coords2int{$ident{$id_ed}},$len[1]\n";
	}
}

