#####
##Process PAl-seq datasets from  Subtelny et al. 2014
#####

#Trim and Align reads
trim_and_align_pal_q64_remove6.sh

#make csv's for standard calabration
perl pal_seq_poly_A_cvs.pl <fastq_of_raw_reads> <normalised_streptavidin_intenisty_values> <aligned_sam_file> > <PAL_standards_filtered.csv>

#make csv's for model fitting
perl pal_seq_poly_A_cvs_fitting.pl  <fastq_of_raw_reads> <normalised_streptavidin_intenisty_values> <aligned_sam_file> > <PAL_for_fit.csv>

#----
#R calibrate intensites of standard lengths and fit to genomic data with a linear regression.
Library(dplyr)

std.<-read.csv("PAL_standards_filtered.csv",header=TRUE)
std.filter<-subset(std, (LENGTH %in% c(10,50,100)))
genomic<-read.csv("PAL_for_fit.csv",header=TRUE)

fit<-lm(LENGTH~INTENSITY,data=std)
fit.filtered<-lm(LENGTH~INTENSITY,data=std.filter)
genomic$estimate1<-predict(fit,genomic)
genomic$estimate2<-predict(fit.filtered,genomic.2h)
write.csv(genomic, file="PAL_fitted.csv")
#----

#get polyA lengths per transcript
perl pal_seq_assign_polyA.pl <bartel_zebrafish_refFlat.txt> <aligned_sam_file> <PAL_fitted.csv> > <PAL_length_dist.csv>

