#!/usr/bin/perl -w
use strict;

#to do 25/01/2016
#script to take gtf get coords around start/stop codons, and assign 5' regions to them from sam
#scale each gene, so that the reads coming from it sum to 1 (disparding the drop bottom 10% of genes on CDS expression)
#where a read overlaps multiple in strand genes counts it towards all those genes

#input files:
#gtf exons, start, stop codons
#sam = aligned 
my $gtf=$ARGV[0];
my $sam=$ARGV[1];
my $outDir=$ARGV[2];
my ($prefix)=$sam=~/([^\/]+).sam$/;

###################################################################################################
#open gtf file setup genomic cds positions
my %startPosFwd; #key1 = chr, key2 = position, value = gene_id(s)
my %startPosRev; #key1 = chr, key2 = position, value = gene_id(s)
my %stopPosFwd; #key1 = chr, key2 = position, value = gene_id(s)
my %stopPosRev; #key1 = chr, key2 = position, value = gene_id(s)
my %cdsFwd; #key1 = chr, key2 = position, value = gene_ids(s)
my %cdsRev; #key1 = chr, key2 = position, value = gene_ids(s)
my %startM; #key1 = gene_id, key2=metapos, key3=value
my %stopM; #key1 = gene_id, key2=metapos, key3=value
my %cdsSum; #key = gene_id, value=total CDS signal

open(GENES,$gtf) || die "can't open $gtf";
while (<GENES>){
	unless(/^#/){
		my @b=split("\t");
		my $class=$b[2];
		my $chr=$b[0];
		my $start=$b[3];
		my $end=$b[4];
		my $dir=$b[6];
		my $gene_id="unknown";
		if ($dir eq "+"){
			if ($class eq "start_codon"){
                ($gene_id) = $b[8] =~ /gene_id\s"([^\"]+)";/;
				my $cdsLength=-51;	
				for (($start-50) .. ($start+500)){ #50bp upstream, 100bp downstream
					$cdsLength+=1;	
					if (exists ($startPosFwd{$chr}{$_})){
                        $startPosFwd{$chr}{$_}.=",".$gene_id.";".$cdsLength;  #change linker from "_" to ";"
					}else{
						$startPosFwd{$chr}{$_}=$gene_id.";".$cdsLength;
					}
					$startM{$gene_id}{$cdsLength}=0;
				}	
			}elsif($class eq "stop_codon"){
                ($gene_id) = $b[8] =~ /gene_id\s"([^\"]+)";/;
                my $cdsLength=-501;
                for (($start-500) .. ($start+50)){ #50bp upstream, 100bp downstream
                    $cdsLength+=1;
					if (exists ($stopPosFwd{$chr}{$_})){
                    	$stopPosFwd{$chr}{$_}.=",".$gene_id.";".$cdsLength;
					}else{
                        $stopPosFwd{$chr}{$_}=$gene_id.";".$cdsLength;
					}
                    $stopM{$gene_id}{$cdsLength}=0;
                } 				
			}elsif($class eq "CDS"){
                ($gene_id) = $b[8] =~ /gene_id\s"([^\"]+)";/;
                $cdsSum{$gene_id}=0;
				for ($start .. $end){
					if (exists ($cdsFwd{$chr}{$_})){
						$cdsFwd{$chr}{$_}.=",".$gene_id; 
					}else{
					 	$cdsFwd{$chr}{$_}=$gene_id;
					}
				}
			}
		}elsif($dir eq "-"){
			if ($class eq "start_codon"){
				($gene_id) = $b[8] =~ /gene_id\s"([^\"]+)";/;
				my $upstream=$end+50;
				my $downstream=$end-500;
                my $cdsLength=-51;
				while ($upstream >= $downstream){ #50bp upstream, 100bp downstream
					$cdsLength+=1;
					if (exists ($startPosRev{$chr}{$_})){
						$startPosRev{$chr}{$_}.=",".$gene_id.";".$cdsLength;
					}else{
						$startPosRev{$chr}{$_}=$gene_id.";".$cdsLength;
					}
                    $startM{$gene_id}{$cdsLength}=0;
					$upstream--;
				}   
			}elsif($class eq "stop_codon"){
				($gene_id) = $b[8] =~ /gene_id\s"([^\"]+)";/;   
                my $upstream=$end+500;
                my $downstream=$end-50;
				my $cdsLength=-501;
                while ($upstream >= $downstream){ #50bp upstream, 100bp downstream
					$cdsLength+=1;			
					if (exists ($stopPosRev{$chr}{$_})){
						$stopPosRev{$chr}{$_}.=",".$gene_id.";".$cdsLength;
    				}else{
                        $stopPosRev{$chr}{$_}=$gene_id.";".$cdsLength;
					}		
	                $stopM{$gene_id}{$cdsLength}=0;
					$upstream--;
				}
			}elsif($class eq "CDS"){
                ($gene_id) = $b[8] =~ /gene_id\s"([^\"]+)";/;
				$cdsSum{$gene_id}=0;
				for ($start .. $end){
					if (exists ($cdsRev{$chr})){
						$cdsRev{$chr}{$_}.=",".$gene_id;
					}else{
						$cdsRev{$chr}{$_}=$gene_id; 
					}
				}   
			}
		}
	}
}
close(GENES);

print "5' gtf parsed\n";

###################################################################################################
#open sam file
my %start_lengths_scale; 	#gene, meta position (-50 to 500), length = count
my %stop_lengths_scale; 	#gene, meta position (-500 to 50), length = count

open (SAM, $sam) || die "can't open $sam";
while (<SAM>){
	#skip headers
	unless(/^#/){
		my @b=split("\t");
		my $leftMost=$b[3]; #leftmost position of match 5' for fwd, 3' for rev
		my $flag=$b[1];
		my $chr=$b[2];
		my $mapq=$b[4];
		my $cig=$b[5];
		my $seq=$b[9];
		my $fivePrime;
		#mapping uniqness filter
		if ($mapq >= 10){
			unless ($flag & 0x4){   #if aligned
				if ($flag & 0x10){  #if rev calculate 5' from 
					#parse cigar for indels and adjust the length of the alignment             
					my $length=length($seq);
					while ($cig =~/(\d+)I/g){   #add to length for insertions
						$length+=$1;
					}
					while ($cig =~/(\d+)D/g){   #substact from length for deletions
						$length-=$1;
					}
					$fivePrime=$leftMost+$length;
					#assign to metaplots
					if (exists ($startPosRev{$chr}{$fivePrime})){
						my @over1=split(",",$startPosRev{$chr}{$fivePrime});
						for (@over1){
							my ($gene,$meta_pos)=$_=~/(^[^;]+);(-?\w+)$/;  #/(\w+)_(-?\w+)/;
							$startM{$gene}{$meta_pos}+=1;    		
                            $start_lengths_scale{$gene}{$meta_pos}{length($seq)}+=1;   
						}	
					}
					if (exists ($stopPosRev{$chr}{$fivePrime})){
						my @over2=split(",",$stopPosRev{$chr}{$fivePrime});
						for (@over2){
							my ($gene,$meta_pos)=$_=~/(^[^;]+);(-?\w+)$/;
							$stopM{$gene}{$meta_pos}+=1;    
                            $stop_lengths_scale{$gene}{$meta_pos}{length($seq)}+=1;
						}
					}
					if (exists ($cdsRev{$chr}{$fivePrime})){
						my @over3=split(",",$cdsRev{$chr}{$fivePrime});
						for (@over3){
							$cdsSum{$_}++;
						}	
					}
				}else{ #if fwd this is easy
					$fivePrime=$leftMost;
					#assign to metaplots
					if (exists ($startPosFwd{$chr}{$fivePrime})){
						my @over4=split(",",$startPosFwd{$chr}{$fivePrime});
						for (@over4){
                            my ($gene,$meta_pos)=$_=~/(^[^;]+);(-?\w+$)/;
							$startM{$gene}{$meta_pos}+=1;	
                            $start_lengths_scale{$gene}{$meta_pos}{length($seq)}+=1;
						}
					}
                    if (exists ($stopPosFwd{$chr}{$fivePrime})){
						my @over5=split(",",$stopPosFwd{$chr}{$fivePrime});
						for (@over5){
							my ($gene,$meta_pos)=$_=~/(^[^;]+);(-?\w+$)/;
							$stopM{$gene}{$meta_pos}+=1;    
                            $stop_lengths_scale{$gene}{$meta_pos}{length($seq)}+=1;
						}
					}
					if (exists ($cdsFwd{$chr}{$fivePrime})){
						my @over6=split(",",$cdsFwd{$chr}{$fivePrime}); 
						for (@over6){
							$cdsSum{$_}++;
						}
					}
				}
			}
		}
	}					
}
close (SAM);

print "5' sam parsed\n";

###################################################################################################
#set groups bases on peaks in the profile
#group 1 = peak at -20 to -10 start_codon
#group 2 = peak at -5 to +5 start_codon
#group 3 = peak at 10 to 20 start_codon
#group 4 = peak at stop_codons 1 to 10

my %group1;
my %group2;
my %group3;
my %group4;

for my $gene (sort keys %startM){
	my $preCdsSum=0; 
	my $g1Sum=0; 
	my $g2Sum=0; 
	my $g3Sum=0;
	for my $pos (sort {$a <=> $b} keys %{$startM{$gene}}){
		$preCdsSum+=$startM{$gene}{$pos}; #sum over window (not just gene cds)
		if ($pos >=-20 && $pos <=-10){ $g1Sum+=$startM{$gene}{$pos}; }
		if ($pos >=-5  && $pos <= 5 ){ $g2Sum+=$startM{$gene}{$pos}; }
		if ($pos >= 10 && $pos <= 20){ $g3Sum+=$startM{$gene}{$pos}; }
	}   
	#Split genes into subgroups based on a proportion of reads at peak positions #propertion = 0.05 / 5%
	if ($preCdsSum){ #avoid division by zero
		if ($g1Sum/$preCdsSum >= 0.05){ $group1{$gene}=1; }
		if ($g2Sum/$preCdsSum >= 0.05){ $group2{$gene}=1; }
		if ($g3Sum/$preCdsSum >= 0.05){ $group3{$gene}=1; }
	}
}

for my $gene (sort keys %stopM){
	my $preCdsSum=0;
	my $g4Sum=0;
	for my $pos (sort {$a <=> $b} keys %{$stopM{$gene}}){
	$preCdsSum+=$stopM{$gene}{$pos};
		if ($pos >=1 && $pos <=10){ $g4Sum+=$stopM{$gene}{$pos}; }
	}
	if ($preCdsSum){ #avoid division by zero
		if ($g4Sum/$preCdsSum >= 0.05){ $group4{$gene}=1; }
	}
}

#edited to just print out which genes belong to which group.
my $outGroup1=$outDir."/".$prefix."_group1.csv";
my $outGroup2=$outDir."/".$prefix."_group2.csv";
my $outGroup3=$outDir."/".$prefix."_group3.csv";
my $outGroup4=$outDir."/".$prefix."_group4.csv";
open (OUT1, ">$outGroup1") || die;
open (OUT2, ">$outGroup2") || die;
open (OUT3, ">$outGroup3") || die;
open (OUT4, ">$outGroup4") || die;

for (sort keys %group1){
	print OUT1 "$_\n";
}

for (sort keys %group2){
    print OUT2 "$_\n";
}

for (sort keys %group3){
    print OUT3 "$_\n";
}

for (sort keys %group4){
    print OUT4 "$_\n";
}

exit;

###################################################################################################
#scale lengths
my %scaled_start;
my %scaled_stop;
my %raw_start;
my %raw_stop;

my %r1_start;
my %r2_start;
my %r3_start;
my %r4_stop;

my %s1_start;
my %s2_start;
my %s3_start;
my %s4_stop;

#drop the lowest 10% of genes
my %black_list;
my $count=0;
my $number_of_genes= keys %cdsSum;
my $ten_percent=$number_of_genes*0.1;

#sort the gene list by total cds riboseq sum, lowest to highest
for my $gene ( sort { $cdsSum{$a} <=> $cdsSum{$b} } keys(%cdsSum) ){
	if ($count <= $ten_percent){
		$black_list{$gene}=1;
	}
	$count++;
}

for my $gene (sort keys %start_lengths_scale){    	
	for my $pos (sort {$a <=> $b} keys %{$start_lengths_scale{$gene}}){
		for my $length (sort {$a <=> $b} keys %{$start_lengths_scale{$gene}{$pos}}){
			#devide count by the total cds count for this gene;
			my $scaled_count=eval { $start_lengths_scale{$gene}{$pos}{$length}/$cdsSum{$gene}} || 0 ;

			$raw_start{$pos}{$length}+=$start_lengths_scale{$gene}{$pos}{$length};

			#lengths per profile group
			if (exists ($group1{$gene})){ $r1_start{$pos}{$length}+=$start_lengths_scale{$gene}{$pos}{$length};}
            if (exists ($group2{$gene})){ $r2_start{$pos}{$length}+=$start_lengths_scale{$gene}{$pos}{$length};}
            if (exists ($group3{$gene})){ $r3_start{$pos}{$length}+=$start_lengths_scale{$gene}{$pos}{$length};}

			unless(exists($black_list{$gene})){ #exclude the lowest 10% of genes bases of total riboseq cds sum when scaling
	            #devide count by the total cds count for this gene;
    	        my $scaled_count=eval { $start_lengths_scale{$gene}{$pos}{$length}/$cdsSum{$gene}} || 0 ;
		        $scaled_start{$pos}{$length}+=$scaled_count;
            
				#lengths per profile group
				if (exists ($group1{$gene})){ $s1_start{$pos}{$length}+=$scaled_count; }
                if (exists ($group2{$gene})){ $s2_start{$pos}{$length}+=$scaled_count; }  
                if (exists ($group3{$gene})){ $s3_start{$pos}{$length}+=$scaled_count; }  
			}
		}
	}
}

for my $gene (sort keys %stop_lengths_scale){
	for my $pos (sort {$a <=> $b} keys %{$stop_lengths_scale{$gene}}){
		for my $length (sort {$a <=> $b} keys %{$stop_lengths_scale{$gene}{$pos}}){
			#devide count by the total cds count for this gene;
			my $scaled_count=eval { $stop_lengths_scale{$gene}{$pos}{$length}/$cdsSum{$gene}} || 0 ;

            $raw_stop{$pos}{$length}+=$stop_lengths_scale{$gene}{$pos}{$length};

			#lenghts per group
			if (exists ($group4{$gene})){ $r4_stop{$pos}{$length}+=$stop_lengths_scale{$gene}{$pos}{$length}; }

		    unless(exists($black_list{$gene})){ #exclude the lowest 10% of genes bases of total riboseq cds sum
            	#devide count by the total cds count for this gene;
            	my $scaled_count=eval { $stop_lengths_scale{$gene}{$pos}{$length}/$cdsSum{$gene}} || 0 ; 
				$scaled_stop{$pos}{$length}+=$scaled_count;

				#lengths per profile group
				if (exists ($group4{$gene})){ $s4_stop{$pos}{$length}+=$scaled_count; }
			}
		}
	}
}

###################################################################################################
my $outStart=$outDir."/".$prefix."_start_codon.csv";
my $outStop=$outDir."/".$prefix."_stop_codon.csv";
my $outStartLengths=$outDir."/".$prefix."_start_codon_lengths.csv";
my $outStopLengths=$outDir."/".$prefix."_stop_codon_lengths.csv";
my $outStartLengthsScale=$outDir."/".$prefix."_start_codon_lengths_scale.csv";
my $outStopLengthsScale=$outDir."/".$prefix."_stop_codon_lengths_scale.csv";

my $outGroup1_raw=$outDir."/".$prefix."_start_codon_group1_raw.csv";
my $outGroup2_raw=$outDir."/".$prefix."_start_codon_group2_raw.csv";
my $outGroup3_raw=$outDir."/".$prefix."_start_codon_group3_raw.csv";
my $outGroup4_raw=$outDir."/".$prefix."_stop_codon_group4_raw.csv";
my $outGroup1_scale=$outDir."/".$prefix."_start_codon_group1_scale.csv";
my $outGroup2_scale=$outDir."/".$prefix."_start_codon_group2_scale.csv";
my $outGroup3_scale=$outDir."/".$prefix."_start_codon_group3_scale.csv";
my $outGroup4_scale=$outDir."/".$prefix."_stop_codon_group4_scale.csv";

open (OUT1,">$outStart") || die "can't open $outStart\n";
open (OUT2,">$outStop")  || die "can't open $outStop\n";
open (OUT3,">$outStartLengths") || die "can't open $outStartLengths\n";
open (OUT4,">$outStopLengths")  || die "can't open $outStopLengths\n";
open (OUT5,">$outStartLengthsScale") || die "can't open $outStartLengthsScale\n";
open (OUT6,">$outStopLengthsScale")  || die "can't open $outStopLengthsScale\n";

open (OUT7,">$outGroup1_raw") || die "can't open $outGroup1_raw\n";
open (OUT8,">$outGroup2_raw") || die "can't open $outGroup2_raw\n";
open (OUT9,">$outGroup3_raw") || die "can't open $outGroup3_raw\n";
open (OUT10,">$outGroup4_raw") || die "can't open $outGroup4_raw\n";
open (OUT11,">$outGroup1_scale") || die "can't open $outGroup1_scale\n";
open (OUT12,">$outGroup2_scale") || die "can't open $outGroup2_scale\n";
open (OUT13,">$outGroup3_scale") || die "can't open $outGroup3_scale\n";
open (OUT14,">$outGroup4_scale") || die "can't open $outGroup4_scale\n";

#####
## meta plots fwd
#####
print OUT1 "gene_id"; for (-50 .. 500){ 	print OUT1 ",$_"; } print OUT1 ",sum\n"; #header
for my $gene (sort keys %startM){
	print OUT1 "$gene";
	for my $pos (sort {$a <=> $b} keys %{$startM{$gene}}){
		print OUT1 ",$startM{$gene}{$pos}";
	}
	print OUT1 ",$cdsSum{$gene}\n";
}

#####
## meta_plots rev
#####
print OUT2 "gene_id"; for (-500 .. 50){     print OUT2 ",$_"; } print OUT2 "sum\n"; #header
for my $gene (sort keys %stopM){
    print OUT2 "$gene";
    for my $pos (sort {$a <=> $b} keys %{$stopM{$gene}}){
        print OUT2 ",$stopM{$gene}{$pos}";
    }
    print OUT2 ",$cdsSum{$gene}\n";
}

#####
## output absoute length values
#####
for my $k1 (sort {$a <=> $b} keys %raw_start){ for my $k2 (sort {$a <=> $b} keys %{$raw_start{$k1}}){ print OUT3 "$k1\t$k2\t$raw_start{$k1}{$k2}\n"; } }
for my $k1 (sort {$a <=> $b} keys %raw_stop){  for my $k2 (sort {$a <=> $b} keys %{$raw_stop{$k1}}){  print OUT4 "$k1\t$k2\t$raw_stop{$k1}{$k2}\n"; } }

##### Absolute lengths per group
for my $k1 (sort {$a <=> $b} keys %r1_start){ for my $k2 (sort {$a <=> $b} keys %{$r1_start{$k1}}){ print OUT7 "$k1\t$k2\t$r1_start{$k1}{$k2}\n"; } }
for my $k1 (sort {$a <=> $b} keys %r2_start){ for my $k2 (sort {$a <=> $b} keys %{$r2_start{$k1}}){ print OUT8 "$k1\t$k2\t$r2_start{$k1}{$k2}\n"; } }
for my $k1 (sort {$a <=> $b} keys %r3_start){ for my $k2 (sort {$a <=> $b} keys %{$r3_start{$k1}}){ print OUT9 "$k1\t$k2\t$r3_start{$k1}{$k2}\n"; } }
for my $k1 (sort {$a <=> $b} keys %r4_stop){  for my $k2 (sort {$a <=> $b} keys %{$r4_stop{$k1}}){  print OUT10 "$k1\t$k2\t$r4_stop{$k1}{$k2}\n"; } }

#####
## output scaled length values
#####
for my $pos (sort {$a <=> $b} keys %scaled_start){ for my $length (sort {$a <=> $b} keys %{$scaled_start{$pos}}){ print OUT5 "$pos\t$length\t$scaled_start{$pos}{$length}\n"; } }
for my $pos (sort {$a <=> $b} keys %scaled_stop){  for my $length (sort {$a <=> $b} keys %{$scaled_stop{$pos}}){  print OUT6 "$pos\t$length\t$scaled_stop{$pos}{$length}\n"; } }

#### Scaled lengths per group
for my $pos (sort {$a <=> $b} keys %s1_start){ for my $length (sort {$a <=> $b} keys %{$s1_start{$pos}}){ print OUT11 "$pos\t$length\t$s1_start{$pos}{$length}\n"; } }
for my $pos (sort {$a <=> $b} keys %s2_start){ for my $length (sort {$a <=> $b} keys %{$s2_start{$pos}}){ print OUT12 "$pos\t$length\t$s2_start{$pos}{$length}\n"; } }
for my $pos (sort {$a <=> $b} keys %s3_start){ for my $length (sort {$a <=> $b} keys %{$s3_start{$pos}}){ print OUT13 "$pos\t$length\t$s3_start{$pos}{$length}\n"; } }
for my $pos (sort {$a <=> $b} keys %s4_stop){  for my $length (sort {$a <=> $b} keys %{$s4_stop{$pos}}){  print OUT14 "$pos\t$length\t$s4_stop{$pos}{$length}\n"; } }

exit;
