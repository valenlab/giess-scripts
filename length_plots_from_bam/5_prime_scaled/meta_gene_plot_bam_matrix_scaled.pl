#!/usr/bin/perl -w
use strict;

#to do 21/01/2016
#script to take gtf get coords around start/stop codons, and assign 5' regions to them from sam
#scale each gene, so that the reads coming from it sum to 1 (disparding the drop bottom 10% of genes on CDS expression)
#where a read overlaps multiple in strand genes counts it towards all those genes

#input files:
#gtf exons, start, stop codons
#sam = aligned 
my $gtf=$ARGV[0];
my $sam=$ARGV[1];
my $outDir=$ARGV[2];
my ($prefix)=$sam=~/([^\/]+).sam$/;


###################################################################################################
#open gtf and get inton positions
my %introns; #key=gene_id, #key2=position, #value = 1;
my %previous_exon; #key=gene_id. value=exon_position

#setup a hash of intron positions
open(GENES2,$gtf) || die "can't open $gtf";      #gft is 1 based
while (<GENES2>){
    unless(/^#/){
        my @b=split("\t");
        my $class=$b[2];
        my $start=$b[3];
        my $end=$b[4];
        my $dir=$b[6];
        if ($class eq "exon"){
            my ($gene_id) = $b[8] =~ /gene_id\s"(\w+)"/;

            #fwd exons are sorted in ascending order 
            #             100958 100975 <-end(+1)
            # satrt(-1)-> 101077 101715 
            if ($dir eq "+"){
                if (exists $previous_exon{$gene_id}){   #if there is a previous exon, there is an intron between this one and the last one
                     for my $pos ($previous_exon{$gene_id} .. ($start-1)){   #end of previous exon to start of this one = sorted ascending
                         $introns{$gene_id}{$pos}=1;
                     }
                     $previous_exon{$gene_id}=$end+1;
                }else{
                     $previous_exon{$gene_id}=$end+1;
                }

            }else{
            #rev exons are sorted in decending order  
            # end(-1)-> 447087 447794
            #           446060 446254 <-start(+1)
                if (exists $previous_exon{$gene_id}){   #if there is a previous exon, there is an intron between this one and the last one
                     for my $pos (($end+1) .. $previous_exon{$gene_id}){   #end of this exon to start of the pervious one = sorted decending
                         $introns{$gene_id}{$pos}=1;
                     }
                     $previous_exon{$gene_id}=$start-1;
                }else{
                     $previous_exon{$gene_id}=$start-1;
                }
            }
        }
    }
}
close (GENES2);

###################################################################################################
#open gtf file setup genomic cds positions
my %startPosFwd; #key1 = chr, key2 = position, value = gene_id(s)
my %startPosRev; #key1 = chr, key2 = position, value = gene_id(s)
my %stopPosFwd; #key1 = chr, key2 = position, value = gene_id(s)
my %stopPosRev; #key1 = chr, key2 = position, value = gene_id(s)
my %cdsFwd; #key1 = chr, key2 = position, value = gene_ids(s)
my %cdsRev; #key1 = chr, key2 = position, value = gene_ids(s)
my %startM; #key1 = gene_id, key2=metapos, key3=value
my %stopM; #key1 = gene_id, key2=metapos, key3=value
my %cdsSum; #key = gene_id, value=total CDS signal

open(GENES,$gtf) || die "can't open $gtf";        #gtf is 1 based
while (<GENES>){
    unless(/^#/){
        my @b=split("\t");
        my $class=$b[2];
        my $chr=$b[0];
        my $start=$b[3];
        my $end=$b[4];
        my $dir=$b[6];
        my $gene_id="unknown";
        if ($dir eq "+"){
            if ($class eq "start_codon"){
                ($gene_id) = $b[8] =~ /gene_id\s"([^\"]+)";/;
                my $cdsLength=-51;    
                for (($start-50) .. ($start+500)){ #50bp upstream, 100bp downstream
                    unless (exists $introns{$gene_id}{$_}){ #check if this position is intronic
                        $cdsLength+=1;    
                        if (exists ($startPosFwd{$chr}{$_})){
                            $startPosFwd{$chr}{$_}.=",".$gene_id.";".$cdsLength;  #change linker from "_" to ";"
                        }else{
                            $startPosFwd{$chr}{$_}=$gene_id.";".$cdsLength;
                        }
                        $startM{$gene_id}{$cdsLength}=0;
                    }
                }    
            }elsif($class eq "stop_codon"){
                ($gene_id) = $b[8] =~ /gene_id\s"([^\"]+)";/;
                my $cdsLength=-501;
                for (($start-500) .. ($start+50)){ #50bp upstream, 100bp downstream
                    unless (exists $introns{$gene_id}{$_}){ #check if this position is intonic
                        $cdsLength+=1;
                        if (exists ($stopPosFwd{$chr}{$_})){
                            $stopPosFwd{$chr}{$_}.=",".$gene_id.";".$cdsLength;
                        }else{
                            $stopPosFwd{$chr}{$_}=$gene_id.";".$cdsLength;
                        }
                        $stopM{$gene_id}{$cdsLength}=0;
                    }
                }                 
            }elsif($class eq "CDS"){
                ($gene_id) = $b[8] =~ /gene_id\s"([^\"]+)";/;
                $cdsSum{$gene_id}=0;
                for ($start .. $end){
                    if (exists ($cdsFwd{$chr}{$_})){
                        $cdsFwd{$chr}{$_}.=",".$gene_id; 
                    }else{
                         $cdsFwd{$chr}{$_}=$gene_id;
                    }
                }
            }
        }elsif($dir eq "-"){
            if ($class eq "start_codon"){
                ($gene_id) = $b[8] =~ /gene_id\s"([^\"]+)";/;
                my $upstream=$end+50;
                my $downstream=$end-500;
                my $cdsLength=-51;
                while ($upstream >= $downstream){ #50bp upstream, 100bp downstream
                    unless (exists $introns{$gene_id}{$_}){ #check if this position is intonic
                        $cdsLength+=1;
                        if (exists ($startPosRev{$chr}{$_})){
                            $startPosRev{$chr}{$_}.=",".$gene_id.";".$cdsLength;
                        }else{
                            $startPosRev{$chr}{$_}=$gene_id.";".$cdsLength;
                        }
                        $startM{$gene_id}{$cdsLength}=0;
                        $upstream--;
                    }
                }   
            }elsif($class eq "stop_codon"){
                ($gene_id) = $b[8] =~ /gene_id\s"([^\"]+)";/;   #=~ /gene_id\s"(\w+)"/;
                my $upstream=$end+500;
                my $downstream=$end-50;
                my $cdsLength=-501;
                while ($upstream >= $downstream){ #50bp upstream, 100bp downstream
                    unless (exists $introns{$gene_id}{$_}){ #check if this position is intonic
                        $cdsLength+=1;            
                        if (exists ($stopPosRev{$chr}{$_})){
                            $stopPosRev{$chr}{$_}.=",".$gene_id.";".$cdsLength;
                        }else{
                            $stopPosRev{$chr}{$_}=$gene_id.";".$cdsLength;
                        }        
                        $stopM{$gene_id}{$cdsLength}=0;
                        $upstream--;
                    }
                }
            }elsif($class eq "CDS"){
                ($gene_id) = $b[8] =~ /gene_id\s"([^\"]+)";/;  #underscrores here?
                $cdsSum{$gene_id}=0;
                for ($start .. $end){
                    if (exists ($cdsRev{$chr})){
                        $cdsRev{$chr}{$_}.=",".$gene_id;
                    }else{
                        $cdsRev{$chr}{$_}=$gene_id; 
                    }
                }   
            }
        }
    }
}
close(GENES);

print "5' gtf parsed\n";

###################################################################################################
#open sam file
my %start_lengths_scale;     #gene, meta position (-50 to 500), length = count
my %stop_lengths_scale;     #gene, meta position (-500 to 50), length = count
my %start_lengths;             #meta position (-500 to 50), length = count
my %stop_lengths;             #meta position (-500 to 50), length = count

open (SAM, $sam) || die "can't open $sam";      #sam is 1 based
while (<SAM>){
    #skip headers
    unless(/^#/){
        my @b=split("\t");
        my $leftMost=$b[3]; #leftmost position of match 5' for fwd, 3' for rev
        my $flag=$b[1];
        my $chr=$b[2];
        my $mapq=$b[4];
        my $cig=$b[5];
        my $seq=$b[9];
        my $fivePrime;
        #mapping uniqness filter
        if ($mapq >= 10){
            unless ($flag & 0x4){   #if aligned
                if ($flag & 0x10){  #if rev calculate 5' from 
                    #parse cigar for indels and adjust the length of the alignment             
                    my $length=length($seq);
                    while ($cig =~/(\d+)I/g){   #add to length for insertions
                        $length+=$1;
                    }
                    while ($cig =~/(\d+)D/g){   #substact from length for deletions
                        $length-=$1;
                    }
                    $fivePrime=$leftMost+$length-1;   #sam is 1 based
                    #assign to metaplots
                    if (exists ($startPosRev{$chr}{$fivePrime})){
                        my @over1=split(",",$startPosRev{$chr}{$fivePrime});
                        for (@over1){
                            my ($gene,$meta_pos)=$_=~/(^[^;]+);(-?\w+)$/;  #/(\w+)_(-?\w+)/;
                            $startM{$gene}{$meta_pos}+=1;            
                            $start_lengths{$meta_pos}{length($seq)}+=1;
                            $start_lengths_scale{$gene}{$meta_pos}{length($seq)}+=1;   
                        }    
                    }
                    if (exists ($stopPosRev{$chr}{$fivePrime})){
                        my @over2=split(",",$stopPosRev{$chr}{$fivePrime});
                        for (@over2){
                            my ($gene,$meta_pos)=$_=~/(^[^;]+);(-?\w+)$/;
                            $stopM{$gene}{$meta_pos}+=1;    
                            $stop_lengths{$meta_pos}{length($seq)}+=1;
                            $stop_lengths_scale{$gene}{$meta_pos}{length($seq)}+=1;
                        }
                    }
                    if (exists ($cdsRev{$chr}{$fivePrime})){
                        my @over3=split(",",$cdsRev{$chr}{$fivePrime});
                        for (@over3){
                            $cdsSum{$_}++;
                        }    
                    }
                }else{ #if fwd this is easy
                    $fivePrime=$leftMost;
                    #assign to metaplots
                    if (exists ($startPosFwd{$chr}{$fivePrime})){
                        my @over4=split(",",$startPosFwd{$chr}{$fivePrime});
                        for (@over4){
                            my ($gene,$meta_pos)=$_=~/(^[^;]+);(-?\w+$)/;
                            $startM{$gene}{$meta_pos}+=1;    
                            $start_lengths{$meta_pos}{length($seq)}+=1;
                            $start_lengths_scale{$gene}{$meta_pos}{length($seq)}+=1;
                        }
                    }
                    if (exists ($stopPosFwd{$chr}{$fivePrime})){
                        my @over5=split(",",$stopPosFwd{$chr}{$fivePrime});
                        for (@over5){
                            my ($gene,$meta_pos)=$_=~/(^[^;]+);(-?\w+$)/;
                            $stopM{$gene}{$meta_pos}+=1;    
                            $stop_lengths{$meta_pos}{length($seq)}+=1;
                            $stop_lengths_scale{$gene}{$meta_pos}{length($seq)}+=1;
                        }
                    }
                    if (exists ($cdsFwd{$chr}{$fivePrime})){
                        my @over6=split(",",$cdsFwd{$chr}{$fivePrime}); 
                        for (@over6){
                            $cdsSum{$_}++;
                        }
                    }
                }
            }
        }
    }                    
}
close (SAM);

print "5' sam parsed\n";

###################################################################################################
#scale lengths
my %scaled_start;
my %scaled_stop;

#drop the lowest 10% of genes
my %black_list;
my $count=0;
my $number_of_genes= keys %cdsSum;
my $ten_percent=$number_of_genes*0.1;

#sort the gene list by total cds riboseq sum, lowest to highest
for my $gene ( sort { $cdsSum{$a} <=> $cdsSum{$b} } keys(%cdsSum) ){
    if ($count <= $ten_percent){
        $black_list{$gene}=1;
    }
    $count++;
}

for my $gene (sort keys %start_lengths_scale){    

    unless(exists($black_list{$gene})){ #exclude the lowest 10% of genes bases of total riboseq cds sum
            
        for my $pos (sort {$a <=> $b} keys %{$start_lengths_scale{$gene}}){
            for my $length (sort {$a <=> $b} keys %{$start_lengths_scale{$gene}{$pos}}){
                #devide count by the total cds count for this gene;
                my $scaled_count=eval { $start_lengths_scale{$gene}{$pos}{$length}/$cdsSum{$gene}} || 0 ;
                $scaled_start{$pos}{$length}+=$scaled_count;
            }
        }
    }
}

for my $gene (sort keys %stop_lengths_scale){

    unless(exists($black_list{$gene})){ #exclude the lowest 10% of genes bases of total riboseq cds sum

        for my $pos (sort {$a <=> $b} keys %{$stop_lengths_scale{$gene}}){
            for my $length (sort {$a <=> $b} keys %{$stop_lengths_scale{$gene}{$pos}}){
                #devide count by the total cds count for this gene;
                my $scaled_count=eval { $stop_lengths_scale{$gene}{$pos}{$length}/$cdsSum{$gene}} || 0 ;
                $scaled_stop{$pos}{$length}+=$scaled_count;
            }
        }
    }
}

###################################################################################################
my $outStart=$outDir."/".$prefix."_start_codon.csv";
my $outStop=$outDir."/".$prefix."_stop_codon.csv";
my $outStartLengths=$outDir."/".$prefix."_start_codon_lengths.csv";
my $outStopLengths=$outDir."/".$prefix."_stop_codon_lengths.csv";
my $outStartLengthsScale=$outDir."/".$prefix."_start_codon_lengths_scale.csv";
my $outStopLengthsScale=$outDir."/".$prefix."_stop_codon_lengths_scale.csv";

open (OUT1,">$outStart") || die "can't open $outStart\n";
open (OUT2,">$outStop")  || die "can't open $outStop\n";
open (OUT3,">$outStartLengths") || die "can't open $outStartLengths\n";
open (OUT4,">$outStopLengths")  || die "can't open $outStopLengths\n";
open (OUT5,">$outStartLengthsScale") || die "can't open $outStartLengthsScale\n";
open (OUT6,">$outStopLengthsScale")  || die "can't open $outStopLengthsScale\n";

#meta plots output per gene
print OUT1 "gene_id"; for (-50 .. 500){     print OUT1 ",$_"; } print OUT1 ",sum\n"; #header
for my $gene (sort keys %startM){
    print OUT1 "$gene";
    for my $pos (sort {$a <=> $b} keys %{$startM{$gene}}){
        print OUT1 ",$startM{$gene}{$pos}";
    }
    print OUT1 ",$cdsSum{$gene}\n";
}

print OUT2 "gene_id"; for (-500 .. 50){     print OUT2 ",$_"; } print OUT2 "sum\n"; #header
for my $gene (sort keys %stopM){
    print OUT2 "$gene";
    for my $pos (sort {$a <=> $b} keys %{$stopM{$gene}}){
        print OUT2 ",$stopM{$gene}{$pos}";
    }
    print OUT2 ",$cdsSum{$gene}\n";
}


#output absoute length values
for my $k1 (sort {$a <=> $b} keys %start_lengths){
    for my $k2 (sort {$a <=> $b} keys %{$start_lengths{$k1}}){
        print OUT3 "$k1\t$k2\t$start_lengths{$k1}{$k2}\n";
    }
}

for my $k1 (sort {$a <=> $b} keys %stop_lengths){
    for my $k2 (sort {$a <=> $b} keys %{$stop_lengths{$k1}}){
        print OUT4 "$k1\t$k2\t$stop_lengths{$k1}{$k2}\n";
    }
}


#output scaled length values
for my $pos (sort {$a <=> $b} keys %scaled_start){
    for my $length (sort {$a <=> $b} keys %{$scaled_start{$pos}}){
        print OUT5 "$pos\t$length\t$scaled_start{$pos}{$length}\n";
    }
}

for my $pos (sort {$a <=> $b} keys %scaled_stop){
    for my $length (sort {$a <=> $b} keys %{$scaled_stop{$pos}}){
        print OUT6 "$pos\t$length\t$scaled_stop{$pos}{$length}\n";
    }
}

exit;
