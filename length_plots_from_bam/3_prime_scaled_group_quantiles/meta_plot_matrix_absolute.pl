#!/usr/bin/perl -w
#use strict;

##to do 27/01/2016
##script to take a csv matrix and then sum per meta position.

my $csv=$ARGV[0];
my @meta;
my @header;
open (IN, $csv) || die;
while (<IN>){
	chomp();
	if (/^gene/){
        my @a=split(",");
		#remove gene name and scaling factor
		shift(@a); pop(@a);
		for (@a){
			push (@header,$_);
		}
	}else{

		my @a=split(",");	

		#remove gene name
		shift(@a);

		my $count=0;
		for (@a){
			$meta[$count]+=$_;
			$count++;
		}
	}
}
close (IN);

my $outSum=0;
for (@header){
	print "$_\t$meta[$outSum]\n";
	$outSum++;
}

#output
#meta_pos	#scaled_sum_counts
#-50		1.223
#-49		3.545
