#!/usr/bin/env Rscript
args = commandArgs(trailingOnly=TRUE)

#usage = Rscript plot_lenghts.R in.csv out.pdf

#test if there is at least one argument: if not, return an error
if (! length(args)==2) {
  stop("2 arguments must be supplied (input.csv, output.pdf).n", call.=FALSE)
}

#load libraries
library(ggplot2)

#read csv
lengths.start<-read.csv(args[1],header=FALSE,check.names=FALSE,sep="\t")

#sort out csv headers
colnames(lengths.start)<-c("position","length","count")

#make plot
a=ggplot(data=lengths.start,aes(x=position,y=count,fill=factor(position%%3))) + geom_bar(stat="identity") + geom_vline(xintercept = 0, linetype="dotted") + scale_x_continuous(limits=c(-60,30),minor_breaks = seq(-60, 30, 3), breaks = seq(-60, 30, 15)) + facet_wrap(~ length,ncol=1) + theme(legend.position="none")

#save pdf
ggsave(args[2], a, width=10,height=200,limitsize=FALSE)
