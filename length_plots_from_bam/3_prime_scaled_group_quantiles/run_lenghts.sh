#!/bin/bash

#AG 21/01/15
#script to produce a plot of 5' riboseq length distributions from a bam file
#1 Bam to sam
#2 Get 5' reads
#3 plot scaled and unscaled lenghts relative to start and stop codons

#dependanices (must be in path)
#samtools
#r
#Rscript

usage(){
cat << EOF
usage: $0 options

script to trim and align riboseq fastq datasets 

OPTIONS:
	-b	path to the input bam files
	-o	path to output dir
	-g	gtf file (matched to the bam gemone)
	-h	this help message

example usage: run_lenghts.sh -b <in.bam> -g in.gtf -o <out_dir>

EOF
}

while getopts ":b:o:g:h" opt; do
	case $opt in 
		b)
			in_bam=$OPTARG
		  	echo "-b input bam file $OPTARG"
		  	;;
        o)
            out_dir=$OPTARG
            echo "-o output folder $OPTARG"
            ;;
        g)
            in_gtf=$OPTARG
            echo "-g gtf file $OPTARG"
            ;;
		h)
			usage
			exit
			;;
		?) 
			echo "Invalid option: -$OPTARG"
			usage
			exit 1
		  	;;
	esac
done

if [ ! -e $in_bam ]  || [ ! $out_dir ] || [ ! $in_gtf ]; then
		echo "something's missing - check your command lines args"
        usage
        exit 1
fi

if [ ! -d $out_dir ]; then
    mkdir $out_dir
fi

if [ ! -d ${out_dir}/tmp ]; then
    mkdir ${out_dir}/tmp
fi

if [ ! -d ${out_dir}/plots ]; then
    mkdir ${out_dir}/plots
fi

if [ ! -d ${out_dir}/png ]; then
    mkdir ${out_dir}/png
fi

for file in $in_bam/*.bam; do

	name=$(basename $file)
	prefix=${name%.bam}
	echo starting $prefix

	#------------------------------------------------------------------------------------------
	#1 Bam to sam
	#------------------------------------------------------------------------------------------

	samtools view $file > ${out_dir}/tmp/${prefix}.sam
	
	#------------------------------------------------------------------------------------------
	#2 Count 5' ends of reads
	#------------------------------------------------------------------------------------------

	nice -n 10 perl meta_gene_plot_bam_matrix_scaled_group.pl $in_gtf ${out_dir}/tmp/${prefix}.sam $out_dir

	#------------------------------------------------------------------------------------------	
	#3 Make aboslute length plots
	#------------------------------------------------------------------------------------------

	Rscript plot_lengths.start.R ${out_dir}/${prefix}_start_codon_lengths.csv ${out_dir}/plots/${prefix}_lengths_absolute_start.pdf
    Rscript plot_lengths.stop.R ${out_dir}/${prefix}_stop_codon_lengths.csv ${out_dir}/plots/${prefix}_lengths_absolute_stop.pdf

    Rscript plot_lengths.start.png.R ${out_dir}/${prefix}_start_codon_lengths.csv ${out_dir}/png/${prefix}_lengths_absolute_start.png
    Rscript plot_lengths.stop.png.R ${out_dir}/${prefix}_stop_codon_lengths.csv ${out_dir}/png/${prefix}_lengths_absolute_stop.png

    #for profile groups
	
    Rscript plot_lengths.start.R ${out_dir}/${prefix}_start_codon_group1_raw.csv ${out_dir}/plots/${prefix}_lengths_absolute_start_group1.pdf
    Rscript plot_lengths.start.R ${out_dir}/${prefix}_start_codon_group2_raw.csv ${out_dir}/plots/${prefix}_lengths_absolute_start_group2.pdf
    Rscript plot_lengths.start.R ${out_dir}/${prefix}_start_codon_group3_raw.csv ${out_dir}/plots/${prefix}_lengths_absolute_start_group3.pdf
    Rscript plot_lengths.stop.R ${out_dir}/${prefix}_stop_codon_group4_raw.csv ${out_dir}/plots/${prefix}_lengths_absolute_stop_group4.pdf

    Rscript plot_lengths.start.png.R ${out_dir}/${prefix}_start_codon_group1_raw.csv ${out_dir}/png/${prefix}_lengths_absolute_start_group1.png
    Rscript plot_lengths.start.png.R ${out_dir}/${prefix}_start_codon_group2_raw.csv ${out_dir}/png/${prefix}_lengths_absolute_start_group2.png
    Rscript plot_lengths.start.png.R ${out_dir}/${prefix}_start_codon_group3_raw.csv ${out_dir}/png/${prefix}_lengths_absolute_start_group3.png
    Rscript plot_lengths.stop.png.R ${out_dir}/${prefix}_stop_codon_group4_raw.csv ${out_dir}/png/${prefix}_lengths_absolute_stop_group4.png

	echo absolute length plots produced

    #------------------------------------------------------------------------------------------ 
    #4 Make scaled length plots
    #------------------------------------------------------------------------------------------

    Rscript plot_lengths.start.R ${out_dir}/${prefix}_start_codon_lengths_scale.csv ${out_dir}/plots/${prefix}_lengths_scaled_start.pdf
    Rscript plot_lengths.stop.R ${out_dir}/${prefix}_stop_codon_lengths_scale.csv ${out_dir}/plots/${prefix}_lengths_scaled_stop.pdf

    Rscript plot_lengths.start.png.R ${out_dir}/${prefix}_start_codon_lengths_scale.csv ${out_dir}/png/${prefix}_lengths_scaled_start.png
    Rscript plot_lengths.stop.png.R ${out_dir}/${prefix}_stop_codon_lengths_scale.csv ${out_dir}/png/${prefix}_lengths_scaled_stop.png

	#for profile groups

	Rscript plot_lengths.start.R ${out_dir}/${prefix}_start_codon_group1_scale.csv ${out_dir}/plots/${prefix}_lengths_scaled_start_group1.pdf
    Rscript plot_lengths.start.R ${out_dir}/${prefix}_start_codon_group2_scale.csv ${out_dir}/plots/${prefix}_lengths_scaled_start_group2.pdf
    Rscript plot_lengths.start.R ${out_dir}/${prefix}_start_codon_group3_scale.csv ${out_dir}/plots/${prefix}_lengths_scaled_start_group3.pdf
    Rscript plot_lengths.stop.R ${out_dir}/${prefix}_stop_codon_group4_scale.csv ${out_dir}/plots/${prefix}_lengths_scaled_stop_group4.pdf

    Rscript plot_lengths.start.png.R ${out_dir}/${prefix}_start_codon_group1_scale.csv ${out_dir}/png/${prefix}_lengths_scaled_start_group1.png
    Rscript plot_lengths.start.png.R ${out_dir}/${prefix}_start_codon_group2_scale.csv ${out_dir}/png/${prefix}_lengths_scaled_start_group2.png
    Rscript plot_lengths.start.png.R ${out_dir}/${prefix}_start_codon_group3_scale.csv ${out_dir}/png/${prefix}_lengths_scaled_start_group3.png
    Rscript plot_lengths.stop.png.R ${out_dir}/${prefix}_stop_codon_group4_scale.csv ${out_dir}/png/${prefix}_lengths_scaled_stop_group4.png

    echo scaled length plots produced

	#------------------------------------------------------------------------------------------ 
	#5 Make metagene plots (bonus)
	#------------------------------------------------------------------------------------------

	perl meta_plot_matrix_absolute.pl ${out_dir}/${prefix}_start_codon.csv > ${out_dir}/${prefix}_start_meta_absolute.csv  
	perl meta_plot_matrix_absolute.pl ${out_dir}/${prefix}_stop_codon.csv > ${out_dir}/${prefix}_stop_meta_absolute.csv

    perl meta_plot_matrix_scale.pl ${out_dir}/${prefix}_start_codon.csv > ${out_dir}/${prefix}_start_meta_scaled.csv
    perl meta_plot_matrix_scale.pl ${out_dir}/${prefix}_stop_codon.csv > ${out_dir}/${prefix}_stop_meta_scaled.csv

	Rscript plot_meta.R ${out_dir}/${prefix}_start_meta_absolute.csv ${out_dir}/${prefix}_stop_meta_absolute.csv ${out_dir}/plots/${prefix}_meta_absolute.pdf ${out_dir}/png/${prefix}_meta_absolute.png
    Rscript plot_meta.R ${out_dir}/${prefix}_start_meta_scaled.csv ${out_dir}/${prefix}_stop_meta_scaled.csv ${out_dir}/plots/${prefix}_meta_scaled.pdf ${out_dir}/png/${prefix}_meta_scaled.png

	#quantiles
    perl meta_plot_matrix_absolute.pl ${out_dir}/${prefix}_start_codon_q1.csv > ${out_dir}/${prefix}_start_meta_absolute_q1.csv
    perl meta_plot_matrix_absolute.pl ${out_dir}/${prefix}_start_codon_q4.csv > ${out_dir}/${prefix}_start_meta_absolute_q4.csv
    perl meta_plot_matrix_absolute.pl ${out_dir}/${prefix}_start_codon_q1_group1.csv > ${out_dir}/${prefix}_start_meta_absolute_q1_group1.csv
    perl meta_plot_matrix_absolute.pl ${out_dir}/${prefix}_start_codon_q4_group1.csv > ${out_dir}/${prefix}_start_meta_absolute_q4_group1.csv
    perl meta_plot_matrix_absolute.pl ${out_dir}/${prefix}_start_codon_q1_group2.csv > ${out_dir}/${prefix}_start_meta_absolute_q1_group2.csv
    perl meta_plot_matrix_absolute.pl ${out_dir}/${prefix}_start_codon_q4_group2.csv > ${out_dir}/${prefix}_start_meta_absolute_q4_group2.csv
    perl meta_plot_matrix_absolute.pl ${out_dir}/${prefix}_start_codon_q1_group3.csv > ${out_dir}/${prefix}_start_meta_absolute_q1_group3.csv
    perl meta_plot_matrix_absolute.pl ${out_dir}/${prefix}_start_codon_q4_group3.csv > ${out_dir}/${prefix}_start_meta_absolute_q4_group3.csv

    perl meta_plot_matrix_scale.pl ${out_dir}/${prefix}_start_codon_q1.csv > ${out_dir}/${prefix}_start_meta_scale_q1.csv
    perl meta_plot_matrix_scale.pl ${out_dir}/${prefix}_start_codon_q4.csv > ${out_dir}/${prefix}_start_meta_scale_q4.csv
    perl meta_plot_matrix_scale.pl ${out_dir}/${prefix}_start_codon_q1_group1.csv > ${out_dir}/${prefix}_start_meta_scale_q1_group1.csv
    perl meta_plot_matrix_scale.pl ${out_dir}/${prefix}_start_codon_q4_group1.csv > ${out_dir}/${prefix}_start_meta_scale_q4_group1.csv
    perl meta_plot_matrix_scale.pl ${out_dir}/${prefix}_start_codon_q1_group2.csv > ${out_dir}/${prefix}_start_meta_scale_q1_group2.csv
    perl meta_plot_matrix_scale.pl ${out_dir}/${prefix}_start_codon_q4_group2.csv > ${out_dir}/${prefix}_start_meta_scale_q4_group2.csv
    perl meta_plot_matrix_scale.pl ${out_dir}/${prefix}_start_codon_q1_group3.csv > ${out_dir}/${prefix}_start_meta_scale_q1_group3.csv
    perl meta_plot_matrix_scale.pl ${out_dir}/${prefix}_start_codon_q4_group3.csv > ${out_dir}/${prefix}_start_meta_scale_q4_group3.csv

    Rscript plot_meta_4.R ${out_dir}/${prefix}_start_meta_absolute_q1.csv ${out_dir}/${prefix}_start_meta_scale_q1.csv ${out_dir}/${prefix}_start_meta_absolute_q4.csv ${out_dir}/${prefix}_start_meta_scale_q4.csv ${out_dir}/plots/${prefix}_meta_qualtile.pdf ${out_dir}/png/${prefix}_meta_qualtile.png 

	Rscript plot_meta_4.R ${out_dir}/${prefix}_start_meta_absolute_q1_group1.csv ${out_dir}/${prefix}_start_meta_scale_q1_group1.csv ${out_dir}/${prefix}_start_meta_absolute_q4_group1.csv ${out_dir}/${prefix}_start_meta_scale_q4_group1.csv ${out_dir}/plots/${prefix}_meta_qualtile_group1.pdf ${out_dir}/png/${prefix}_meta_qualtile_group1.png

    Rscript plot_meta_4.R ${out_dir}/${prefix}_start_meta_absolute_q1_group2.csv ${out_dir}/${prefix}_start_meta_scale_q1_group2.csv ${out_dir}/${prefix}_start_meta_absolute_q4_group2.csv ${out_dir}/${prefix}_start_meta_scale_q4_group2.csv ${out_dir}/plots/${prefix}_meta_qualtile_group2.pdf ${out_dir}/png/${prefix}_meta_qualtile_group2.png

    Rscript plot_meta_4.R ${out_dir}/${prefix}_start_meta_absolute_q1_group3.csv ${out_dir}/${prefix}_start_meta_scale_q1_group3.csv ${out_dir}/${prefix}_start_meta_absolute_q4_group3.csv ${out_dir}/${prefix}_start_meta_scale_q4_group3.csv ${out_dir}/plots/${prefix}_meta_qualtile_group3.pdf ${out_dir}/png/${prefix}_meta_qualtile_group3.png

	echo meta plots done

	#----------------------------------------------------------------------------------------------
	#tidy up
	#---------------------------------------------------------------------------------------------_

	rm ${out_dir}/tmp/${prefix}.sam

done

