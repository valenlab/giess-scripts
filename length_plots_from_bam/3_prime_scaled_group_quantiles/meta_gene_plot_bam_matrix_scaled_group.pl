#!/usr/bin/perl -w
use strict;

#to do 04/02/2016
#script to take gtf get coords around start/stop codons, and assign 5' regions to them from sam
#scale each gene, so that the reads coming from it sum to 1 (disparding the drop bottom 10% of genes on CDS expression)
#where a read overlaps multiple in strand genes counts it towards all those genes
#edited to output quantile groups of gene groups

#input files:
#gtf exons, start, stop codons
#sam = aligned 
my $gtf=$ARGV[0];
my $sam=$ARGV[1];
my $outDir=$ARGV[2];
my ($prefix)=$sam=~/([^\/]+).sam$/;

###################################################################################################
#open gtf and get inton positions
my %introns; #key=gene_id, #key2=position, #value = 1;
my %previous_exon; #key=gene_id. value=exon_position

#setup a hash of intron positions
open(GENES2,$gtf) || die "can't open $gtf";      #gft is 1 based
while (<GENES2>){
    unless(/^#/){
        my @b=split("\t");
        my $class=$b[2];
        my $start=$b[3];
        my $end=$b[4];
        my $dir=$b[6];
        if ($class eq "exon"){
            my ($gene_id) = $b[8] =~ /gene_id\s"(\w+)"/;

            #fwd exons are sorted in ascending order 
            #             100958 100975 <-end(+1)
            # satrt(-1)-> 101077 101715 
            if ($dir eq "+"){
                if (exists $previous_exon{$gene_id}){   #if there is a previous exon, there is an intron between this one and the last one
                     for my $pos ($previous_exon{$gene_id} .. ($start-1)){   #end of previous exon to start of this one = sorted ascending
                         $introns{$gene_id}{$pos}=1;
                     }
                     $previous_exon{$gene_id}=$end+1;
                }else{
                     $previous_exon{$gene_id}=$end+1;
                }

            }else{
            #rev exons are sorted in decending order  
            # end(-1)-> 447087 447794
            #           446060 446254 <-start(+1)
                if (exists $previous_exon{$gene_id}){   #if there is a previous exon, there is an intron between this one and the last one
                     for my $pos (($end+1) .. $previous_exon{$gene_id}){   #end of this exon to start of the pervious one = sorted decending
                         $introns{$gene_id}{$pos}=1;
                     }
                     $previous_exon{$gene_id}=$start-1;
                }else{
                     $previous_exon{$gene_id}=$start-1;
                }
            }
        }
    }
}
close (GENES2);

###################################################################################################
#open gtf file setup genomic cds positions
my %startPosFwd; #key1 = chr, key2 = position, value = gene_id(s)
my %startPosRev; #key1 = chr, key2 = position, value = gene_id(s)
my %stopPosFwd; #key1 = chr, key2 = position, value = gene_id(s)
my %stopPosRev; #key1 = chr, key2 = position, value = gene_id(s)
my %cdsFwd; #key1 = chr, key2 = position, value = gene_ids(s)
my %cdsRev; #key1 = chr, key2 = position, value = gene_ids(s)
my %startM; #key1 = gene_id, key2=metapos, key3=value
my %stopM; #key1 = gene_id, key2=metapos, key3=value
my %cdsSum; #key = gene_id, value=total CDS signal

open(GENES,$gtf) || die "can't open $gtf";
while (<GENES>){
    unless(/^#/){
        my @b=split("\t");
        my $class=$b[2];
        my $chr=$b[0];
        my $start=$b[3];
        my $end=$b[4];
        my $dir=$b[6];
        my $gene_id="unknown";
        if ($dir eq "+"){
            if ($class eq "start_codon"){
                ($gene_id) = $b[8] =~ /gene_id\s"([^\"]+)";/;
                my $cdsLength=-51;    
                for (($start-50) .. ($start+500)){ #50bp upstream, 100bp downstream
                    unless (exists $introns{$gene_id}{$_}){ #check if this position is intronic
                        $cdsLength+=1;    
                        if (exists ($startPosFwd{$chr}{$_})){
                             $startPosFwd{$chr}{$_}.=",".$gene_id.";".$cdsLength;  #change linker from "_" to ";"
                        }else{
                            $startPosFwd{$chr}{$_}=$gene_id.";".$cdsLength;
                        }
                        $startM{$gene_id}{$cdsLength}=0;
                    }
                }    
            }elsif($class eq "stop_codon"){
                ($gene_id) = $b[8] =~ /gene_id\s"([^\"]+)";/;
                my $cdsLength=-501;
                for (($start-500) .. ($start+50)){ #50bp upstream, 100bp downstream
                    unless (exists $introns{$gene_id}{$_}){ #check if this position is intronic
                        $cdsLength+=1;
                        if (exists ($stopPosFwd{$chr}{$_})){
                            $stopPosFwd{$chr}{$_}.=",".$gene_id.";".$cdsLength;
                        }else{
                            $stopPosFwd{$chr}{$_}=$gene_id.";".$cdsLength;
                        }
                        $stopM{$gene_id}{$cdsLength}=0;
                    }
                }                 
            }elsif($class eq "CDS"){
                ($gene_id) = $b[8] =~ /gene_id\s"([^\"]+)";/;
                $cdsSum{$gene_id}=0;
                for ($start .. $end){
                    if (exists ($cdsFwd{$chr}{$_})){
                        $cdsFwd{$chr}{$_}.=",".$gene_id; 
                    }else{
                         $cdsFwd{$chr}{$_}=$gene_id;
                    }
                }
            }
        }elsif($dir eq "-"){
            if ($class eq "start_codon"){
                ($gene_id) = $b[8] =~ /gene_id\s"([^\"]+)";/;
                my $upstream=$end+50;
                my $downstream=$end-500;
                my $cdsLength=-51;
                while ($upstream >= $downstream){ #50bp upstream, 100bp downstream
                    unless (exists $introns{$gene_id}{$_}){ #check if this position is intronic
                        $cdsLength+=1;
                        if (exists ($startPosRev{$chr}{$_})){
                            $startPosRev{$chr}{$_}.=",".$gene_id.";".$cdsLength;
                        }else{
                            $startPosRev{$chr}{$_}=$gene_id.";".$cdsLength;
                        }
                        $startM{$gene_id}{$cdsLength}=0;
                        $upstream--;
                    }
                }   
            }elsif($class eq "stop_codon"){
                ($gene_id) = $b[8] =~ /gene_id\s"([^\"]+)";/;   
                my $upstream=$end+500;
                my $downstream=$end-50;
                my $cdsLength=-501;
                while ($upstream >= $downstream){ #50bp upstream, 100bp downstream
                    unless (exists $introns{$gene_id}{$_}){ #check if this position is intronic
                        $cdsLength+=1;            
                        if (exists ($stopPosRev{$chr}{$_})){
                            $stopPosRev{$chr}{$_}.=",".$gene_id.";".$cdsLength;
                        }else{
                            $stopPosRev{$chr}{$_}=$gene_id.";".$cdsLength;
                        }        
                        $stopM{$gene_id}{$cdsLength}=0;
                        $upstream--;
                    } 
                }
            }elsif($class eq "CDS"){
                ($gene_id) = $b[8] =~ /gene_id\s"([^\"]+)";/;
                $cdsSum{$gene_id}=0;
                for ($start .. $end){
                    if (exists ($cdsRev{$chr})){
                        $cdsRev{$chr}{$_}.=",".$gene_id;
                    }else{
                        $cdsRev{$chr}{$_}=$gene_id; 
                    }
                }   
            }
        }
    }
}
close(GENES);

print "gtf parsed\n";

###################################################################################################
#open sam file
my %start_lengths_scale;     #gene, meta position (-50 to 500), length = count
my %stop_lengths_scale;     #gene, meta position (-500 to 50), length = count

open (SAM, $sam) || die "can't open $sam";
while (<SAM>){
    #skip headers
    unless(/^#/){
        my @b=split("\t");
        my $leftMost=$b[3]; #leftmost position of match 5' for fwd, 3' for rev
        my $flag=$b[1];
        my $chr=$b[2];
        my $mapq=$b[4];
        my $cig=$b[5];
        my $seq=$b[9];
        my $threePrime;
        #mapping uniqness filter
        if ($mapq >= 10){
            unless ($flag & 0x4){   #if aligned
                if ($flag & 0x10){  #if rev calculate 3' == sam coordinate (leftmost)

                    $threePrime=$leftMost;

                    #assign to metaplots
                    if (exists ($startPosRev{$chr}{$threePrime})){
                        my @over1=split(",",$startPosRev{$chr}{$threePrime});
                        for (@over1){
                            my ($gene,$meta_pos)=$_=~/(^[^;]+);(-?\w+)$/;  #/(\w+)_(-?\w+)/;
                            $startM{$gene}{$meta_pos}+=1;            
                            $start_lengths_scale{$gene}{$meta_pos}{length($seq)}+=1;   
                        }    
                    }
                    if (exists ($stopPosRev{$chr}{$threePrime})){
                        my @over2=split(",",$stopPosRev{$chr}{$threePrime});
                        for (@over2){
                            my ($gene,$meta_pos)=$_=~/(^[^;]+);(-?\w+)$/;
                            $stopM{$gene}{$meta_pos}+=1;    
                            $stop_lengths_scale{$gene}{$meta_pos}{length($seq)}+=1;
                        }
                    }
                    if (exists ($cdsRev{$chr}{$threePrime})){
                        my @over3=split(",",$cdsRev{$chr}{$threePrime});
                        for (@over3){
                            $cdsSum{$_}++;
                        }    
                    }

                }else{ #if fwd 3' == sam coordinate (leftmost) + read length 

                    #parse cigar for indels and adjust the length of the alignment             
                    my $length=length($seq);
                    while ($cig =~/(\d+)I/g){   #add to length for insertions
                        $length+=$1;
                    }
                    while ($cig =~/(\d+)D/g){   #substact from length for deletions
                        $length-=$1;
                    }

                    $threePrime=$leftMost+($length-1);              #SAM is 1 based
                    #assign to metaplots
                    if (exists ($startPosFwd{$chr}{$threePrime})){
                        my @over4=split(",",$startPosFwd{$chr}{$threePrime});
                        for (@over4){
                            my ($gene,$meta_pos)=$_=~/(^[^;]+);(-?\w+$)/;
                            $startM{$gene}{$meta_pos}+=1;    
                            $start_lengths_scale{$gene}{$meta_pos}{length($seq)}+=1;
                        }
                    }
                    if (exists ($stopPosFwd{$chr}{$threePrime})){
                        my @over5=split(",",$stopPosFwd{$chr}{$threePrime});
                        for (@over5){
                            my ($gene,$meta_pos)=$_=~/(^[^;]+);(-?\w+$)/;
                            $stopM{$gene}{$meta_pos}+=1;    
                            $stop_lengths_scale{$gene}{$meta_pos}{length($seq)}+=1;
                        }
                    }
                    if (exists ($cdsFwd{$chr}{$threePrime})){
                        my @over6=split(",",$cdsFwd{$chr}{$threePrime}); 
                        for (@over6){
                            $cdsSum{$_}++;
                        }
                    }
                }
            }
        }
    }                    
}
close (SAM);

print "sam parsed for 3'\n";

###################################################################################################
#set groups bases on peaks in the profile
#group 1 = peak at -20 to -10 start_codon
#group 2 = peak at -5 to +5 start_codon
#group 3 = peak at 10 to 20 start_codon
#group 4 = peak at stop_codons 1 to 10

my %group1;
my %group2;
my %group3;
my %group4;

for my $gene (sort keys %startM){
    my $preCdsSum=0; 
    my $g1Sum=0; 
    my $g2Sum=0; 
    my $g3Sum=0;
    for my $pos (sort {$a <=> $b} keys %{$startM{$gene}}){
        $preCdsSum+=$startM{$gene}{$pos}; #sum over window (not just gene cds)
        if ($pos >=-20 && $pos <=-10){ $g1Sum+=$startM{$gene}{$pos}; }
        if ($pos >=-5  && $pos <= 5 ){ $g2Sum+=$startM{$gene}{$pos}; }
        if ($pos >= 10 && $pos <= 20){ $g3Sum+=$startM{$gene}{$pos}; }
    }   
    #Split genes into subgroups based on a proportion of reads at peak positions #propertion = 0.05 / 5%
    if ($preCdsSum){ #avoid division by zero
        if ($g1Sum/$preCdsSum >= 0.05){ $group1{$gene}=$g1Sum/$preCdsSum; } #use this to out quantiles
        if ($g2Sum/$preCdsSum >= 0.05){ $group2{$gene}=$g2Sum/$preCdsSum; }
        if ($g3Sum/$preCdsSum >= 0.05){ $group3{$gene}=$g3Sum/$preCdsSum; }
    }
}

for my $gene (sort keys %stopM){
    my $preCdsSum=0;
    my $g4Sum=0;
    for my $pos (sort {$a <=> $b} keys %{$stopM{$gene}}){
    $preCdsSum+=$stopM{$gene}{$pos};
        if ($pos >=1 && $pos <=10){ $g4Sum+=$stopM{$gene}{$pos}; }
    }
    if ($preCdsSum){ #avoid division by zero
        if ($g4Sum/$preCdsSum >= 0.05){ $group4{$gene}=$g4Sum/$preCdsSum; } #use this to output quantiles
    }
}

###################################################################################################
#scale lengths
my %scaled_start;
my %scaled_stop;
my %raw_start;
my %raw_stop;

my %r1_start;
my %r2_start;
my %r3_start;
my %r4_stop;

my %s1_start;
my %s2_start;
my %s3_start;
my %s4_stop;

my %q1; #top 25%   #cds sums
my %q4; #bottow 25%
my %g1_q1;
my %g1_q4;
my %g2_q1;
my %g2_q4;
my %g3_q1;
my %g3_q4;

#drop the lowest 10% of genes
my %black_list;
my $count=0;
my $number_of_genes=keys %cdsSum;
my $ten_percent=$number_of_genes*0.1;

#sort the gene list by total cds riboseq sum, lowest to highest
for my $gene ( sort { $cdsSum{$a} <=> $cdsSum{$b} } keys(%cdsSum) ){
    if ($count <= $ten_percent){ $black_list{$gene}=1; }

    #add quntiles for all genes here
    if ($count <= ($number_of_genes*0.25)){ $q4{$gene}=1;    
    }elsif($count > ($number_of_genes*0.75)){ $q1{$gene}=1; }
    $count++;
}

####################
#quantiles here#####

#count group sums
#sort each group on the proportional value   
#get 0-25%
#get 26-50
#get 51-75
#get 76-100  #print seperate meta plots  || output the the list (per group) and seperate quantiles in R?

my $g1_total=keys %group1;
my $g1_count=0;
for my $gene ( sort { $group1{$a} <=> $group1{$b} }  keys %group1){
    if ($g1_count <= ($g1_total*0.25) ){ $g1_q4{$gene}=1;
    }elsif ($g1_count > ($g1_total*0.75) ){ $g1_q1{$gene}=1; }
    $g1_count++;
}

my $g2_total=keys %group2;
my $g2_count=0;
for my $gene ( sort { $group2{$a} <=> $group2{$b} }  keys %group2){
    if ($g2_count <= ($g2_total*0.25) ){ $g2_q4{$gene}=1;
    }elsif ($g2_count > ($g2_total*0.75) ){ $g2_q1{$gene}=1; }  
    $g2_count++;
}

my $g3_total=keys %group3;
my $g3_count=0;
for my $gene ( sort { $group3{$a} <=> $group3{$b} }  keys %group3){
    if ($g3_count <= ($g3_total*0.25) ){ $g3_q4{$gene}=1;
    }elsif ($g3_count > ($g3_total*0.75) ){ $g3_q1{$gene}=1; }  
    $g3_count++;
}

for my $gene (sort keys %start_lengths_scale){        
    for my $pos (sort {$a <=> $b} keys %{$start_lengths_scale{$gene}}){
        for my $length (sort {$a <=> $b} keys %{$start_lengths_scale{$gene}{$pos}}){
            #devide count by the total cds count for this gene;
            my $scaled_count=eval { $start_lengths_scale{$gene}{$pos}{$length}/$cdsSum{$gene}} || 0 ;

            $raw_start{$pos}{$length}+=$start_lengths_scale{$gene}{$pos}{$length};

            #lengths per profile group
            if (exists ($group1{$gene})){ $r1_start{$pos}{$length}+=$start_lengths_scale{$gene}{$pos}{$length};}
            if (exists ($group2{$gene})){ $r2_start{$pos}{$length}+=$start_lengths_scale{$gene}{$pos}{$length};}
            if (exists ($group3{$gene})){ $r3_start{$pos}{$length}+=$start_lengths_scale{$gene}{$pos}{$length};}

            unless(exists($black_list{$gene})){ #exclude the lowest 10% of genes bases of total riboseq cds sum when scaling
                #devide count by the total cds count for this gene;
                my $scaled_count=eval { $start_lengths_scale{$gene}{$pos}{$length}/$cdsSum{$gene}} || 0 ;
                $scaled_start{$pos}{$length}+=$scaled_count;
            
                #lengths per profile group
                if (exists ($group1{$gene})){ $s1_start{$pos}{$length}+=$scaled_count; }
                if (exists ($group2{$gene})){ $s2_start{$pos}{$length}+=$scaled_count; }  
                if (exists ($group3{$gene})){ $s3_start{$pos}{$length}+=$scaled_count; }  
            }
        }
    }
}

for my $gene (sort keys %stop_lengths_scale){
    for my $pos (sort {$a <=> $b} keys %{$stop_lengths_scale{$gene}}){
        for my $length (sort {$a <=> $b} keys %{$stop_lengths_scale{$gene}{$pos}}){
            #devide count by the total cds count for this gene;
            my $scaled_count=eval { $stop_lengths_scale{$gene}{$pos}{$length}/$cdsSum{$gene}} || 0 ;

            $raw_stop{$pos}{$length}+=$stop_lengths_scale{$gene}{$pos}{$length};

            #lenghts per group
            if (exists ($group4{$gene})){ $r4_stop{$pos}{$length}+=$stop_lengths_scale{$gene}{$pos}{$length}; }

            unless(exists($black_list{$gene})){ #exclude the lowest 10% of genes bases of total riboseq cds sum
                #devide count by the total cds count for this gene;
                my $scaled_count=eval { $stop_lengths_scale{$gene}{$pos}{$length}/$cdsSum{$gene}} || 0 ; 
                $scaled_stop{$pos}{$length}+=$scaled_count;

                #lengths per profile group
                if (exists ($group4{$gene})){ $s4_stop{$pos}{$length}+=$scaled_count; }
            }
        }
    }
}

###################################################################################################
my $outStart=$outDir."/".$prefix."_start_codon.csv";
my $outStop=$outDir."/".$prefix."_stop_codon.csv";
my $outStartLengths=$outDir."/".$prefix."_start_codon_lengths.csv";
my $outStopLengths=$outDir."/".$prefix."_stop_codon_lengths.csv";
my $outStartLengthsScale=$outDir."/".$prefix."_start_codon_lengths_scale.csv";
my $outStopLengthsScale=$outDir."/".$prefix."_stop_codon_lengths_scale.csv";

my $outGroup1_raw=$outDir."/".$prefix."_start_codon_group1_raw.csv";
my $outGroup2_raw=$outDir."/".$prefix."_start_codon_group2_raw.csv";
my $outGroup3_raw=$outDir."/".$prefix."_start_codon_group3_raw.csv";
my $outGroup4_raw=$outDir."/".$prefix."_stop_codon_group4_raw.csv";
my $outGroup1_scale=$outDir."/".$prefix."_start_codon_group1_scale.csv";
my $outGroup2_scale=$outDir."/".$prefix."_start_codon_group2_scale.csv";
my $outGroup3_scale=$outDir."/".$prefix."_start_codon_group3_scale.csv";
my $outGroup4_scale=$outDir."/".$prefix."_stop_codon_group4_scale.csv";

#quanities
my $quantile_raw_q1=$outDir."/".$prefix."_start_codon_q1.csv";
my $quantile_raw_q4=$outDir."/".$prefix."_start_codon_q4.csv";
my $quantile_raw_group1_q1=$outDir."/".$prefix."_start_codon_q1_group1.csv";
my $quantile_raw_group1_q4=$outDir."/".$prefix."_start_codon_q4_group1.csv";
my $quantile_raw_group2_q1=$outDir."/".$prefix."_start_codon_q1_group2.csv";
my $quantile_raw_group2_q4=$outDir."/".$prefix."_start_codon_q4_group2.csv";
my $quantile_raw_group3_q1=$outDir."/".$prefix."_start_codon_q1_group3.csv";
my $quantile_raw_group3_q4=$outDir."/".$prefix."_start_codon_q4_group3.csv";

open (OUT1,">$outStart") || die "can't open $outStart\n";
open (OUT2,">$outStop")  || die "can't open $outStop\n";
open (OUT3,">$outStartLengths") || die "can't open $outStartLengths\n";
open (OUT4,">$outStopLengths")  || die "can't open $outStopLengths\n";
open (OUT5,">$outStartLengthsScale") || die "can't open $outStartLengthsScale\n";
open (OUT6,">$outStopLengthsScale")  || die "can't open $outStopLengthsScale\n";

open (OUT7,">$outGroup1_raw") || die "can't open $outGroup1_raw\n";
open (OUT8,">$outGroup2_raw") || die "can't open $outGroup2_raw\n";
open (OUT9,">$outGroup3_raw") || die "can't open $outGroup3_raw\n";
open (OUT10,">$outGroup4_raw") || die "can't open $outGroup4_raw\n";
open (OUT11,">$outGroup1_scale") || die "can't open $outGroup1_scale\n";
open (OUT12,">$outGroup2_scale") || die "can't open $outGroup2_scale\n";
open (OUT13,">$outGroup3_scale") || die "can't open $outGroup3_scale\n";
open (OUT14,">$outGroup4_scale") || die "can't open $outGroup4_scale\n";

open (OUT15,">$quantile_raw_q1") || die "can't open $quantile_raw_q1\n";
open (OUT16,">$quantile_raw_q4") || die "can't open $quantile_raw_q4\n";
open (OUT17,">$quantile_raw_group1_q1") || die "can't open $quantile_raw_group1_q1\n";
open (OUT18,">$quantile_raw_group1_q4") || die "can't open $quantile_raw_group1_q4\n";
open (OUT19,">$quantile_raw_group2_q1") || die "can't open $quantile_raw_group2_q1\n";
open (OUT20,">$quantile_raw_group2_q4") || die "can't open $quantile_raw_group2_q4\n";
open (OUT21,">$quantile_raw_group3_q1") || die "can't open $quantile_raw_group3_q1\n";
open (OUT22,">$quantile_raw_group3_q4") || die "can't open $quantile_raw_group3_q4\n";

#####
## meta plots start
#####
print OUT1 "gene_id"; for (-50 .. 500){     print OUT1 ",$_"; } print OUT1 ",sum\n"; #header
print OUT15 "gene_id"; for (-50 .. 500){     print OUT15 ",$_"; } print OUT15 ",sum\n"; #header
print OUT16 "gene_id"; for (-50 .. 500){     print OUT16 ",$_"; } print OUT16 ",sum\n"; #header
print OUT17 "gene_id"; for (-50 .. 500){     print OUT17 ",$_"; } print OUT17 ",sum\n"; #header
print OUT18 "gene_id"; for (-50 .. 500){     print OUT18 ",$_"; } print OUT18 ",sum\n"; #header
print OUT19 "gene_id"; for (-50 .. 500){     print OUT19 ",$_"; } print OUT19 ",sum\n"; #header
print OUT20 "gene_id"; for (-50 .. 500){     print OUT20 ",$_"; } print OUT20 ",sum\n"; #header
print OUT21 "gene_id"; for (-50 .. 500){     print OUT21 ",$_"; } print OUT21 ",sum\n"; #header
print OUT22 "gene_id"; for (-50 .. 500){     print OUT22 ",$_"; } print OUT22 ",sum\n"; #header

for my $gene (sort keys %startM){
    print OUT1 "$gene";
    for my $pos (sort {$a <=> $b} keys %{$startM{$gene}}){
        print OUT1 ",$startM{$gene}{$pos}";
    }
    print OUT1 ",$cdsSum{$gene}\n";

    if ($q1{$gene}){
        print OUT15 "$gene";
        for my $pos (sort {$a <=> $b} keys %{$startM{$gene}}){ print OUT15 ",$startM{$gene}{$pos}"; }
        print OUT15 ",$cdsSum{$gene}\n";
    }
    if ($q4{$gene}){
        print OUT16 "$gene";
        for my $pos (sort {$a <=> $b} keys %{$startM{$gene}}){ print OUT16 ",$startM{$gene}{$pos}"; }   
        print OUT16 ",$cdsSum{$gene}\n";
    }
    if ($g1_q1{$gene}){
        print OUT17 "$gene";
        for my $pos (sort {$a <=> $b} keys %{$startM{$gene}}){ print OUT17 ",$startM{$gene}{$pos}"; }   
        print OUT17 ",$cdsSum{$gene}\n";
    }
    if ($g1_q4{$gene}){
        print OUT18 "$gene";
        for my $pos (sort {$a <=> $b} keys %{$startM{$gene}}){ print OUT18 ",$startM{$gene}{$pos}"; }   
         print OUT18 ",$cdsSum{$gene}\n";
    }
    if ($g2_q1{$gene}){
        print OUT19 "$gene";
        for my $pos (sort {$a <=> $b} keys %{$startM{$gene}}){ print OUT19 ",$startM{$gene}{$pos}"; }   
        print OUT19 ",$cdsSum{$gene}\n";
    }
    if ($g2_q4{$gene}){
        print OUT20 "$gene";
        for my $pos (sort {$a <=> $b} keys %{$startM{$gene}}){ print OUT20 ",$startM{$gene}{$pos}"; }   
        print OUT20 ",$cdsSum{$gene}\n";
    }
    if ($g3_q1{$gene}){
        print OUT21 "$gene";
        for my $pos (sort {$a <=> $b} keys %{$startM{$gene}}){ print OUT21 ",$startM{$gene}{$pos}"; }   
        print OUT21 ",$cdsSum{$gene}\n";
    }
    if ($g3_q4{$gene}){
        print OUT22 "$gene";
        for my $pos (sort {$a <=> $b} keys %{$startM{$gene}}){ print OUT22 ",$startM{$gene}{$pos}"; }   
        print OUT22 ",$cdsSum{$gene}\n";
    }
}

#####
## meta_plots stop
#####
print OUT2 "gene_id"; for (-500 .. 50){     print OUT2 ",$_"; } print OUT2 "sum\n"; #header
for my $gene (sort keys %stopM){
    print OUT2 "$gene";
    for my $pos (sort {$a <=> $b} keys %{$stopM{$gene}}){
        print OUT2 ",$stopM{$gene}{$pos}";
    }
    print OUT2 ",$cdsSum{$gene}\n";
}

#####
## output absoute length values
#####
for my $k1 (sort {$a <=> $b} keys %raw_start){ for my $k2 (sort {$a <=> $b} keys %{$raw_start{$k1}}){ print OUT3 "$k1\t$k2\t$raw_start{$k1}{$k2}\n"; } }
for my $k1 (sort {$a <=> $b} keys %raw_stop){  for my $k2 (sort {$a <=> $b} keys %{$raw_stop{$k1}}){  print OUT4 "$k1\t$k2\t$raw_stop{$k1}{$k2}\n"; } }

##### Absolute lengths per group
for my $k1 (sort {$a <=> $b} keys %r1_start){ for my $k2 (sort {$a <=> $b} keys %{$r1_start{$k1}}){ print OUT7 "$k1\t$k2\t$r1_start{$k1}{$k2}\n"; } }
for my $k1 (sort {$a <=> $b} keys %r2_start){ for my $k2 (sort {$a <=> $b} keys %{$r2_start{$k1}}){ print OUT8 "$k1\t$k2\t$r2_start{$k1}{$k2}\n"; } }
for my $k1 (sort {$a <=> $b} keys %r3_start){ for my $k2 (sort {$a <=> $b} keys %{$r3_start{$k1}}){ print OUT9 "$k1\t$k2\t$r3_start{$k1}{$k2}\n"; } }
for my $k1 (sort {$a <=> $b} keys %r4_stop){  for my $k2 (sort {$a <=> $b} keys %{$r4_stop{$k1}}){  print OUT10 "$k1\t$k2\t$r4_stop{$k1}{$k2}\n"; } }

#####
## output scaled length values
#####
for my $pos (sort {$a <=> $b} keys %scaled_start){ for my $length (sort {$a <=> $b} keys %{$scaled_start{$pos}}){ print OUT5 "$pos\t$length\t$scaled_start{$pos}{$length}\n"; } }
for my $pos (sort {$a <=> $b} keys %scaled_stop){  for my $length (sort {$a <=> $b} keys %{$scaled_stop{$pos}}){  print OUT6 "$pos\t$length\t$scaled_stop{$pos}{$length}\n"; } }

#### Scaled lengths per group
for my $pos (sort {$a <=> $b} keys %s1_start){ for my $length (sort {$a <=> $b} keys %{$s1_start{$pos}}){ print OUT11 "$pos\t$length\t$s1_start{$pos}{$length}\n"; } }
for my $pos (sort {$a <=> $b} keys %s2_start){ for my $length (sort {$a <=> $b} keys %{$s2_start{$pos}}){ print OUT12 "$pos\t$length\t$s2_start{$pos}{$length}\n"; } }
for my $pos (sort {$a <=> $b} keys %s3_start){ for my $length (sort {$a <=> $b} keys %{$s3_start{$pos}}){ print OUT13 "$pos\t$length\t$s3_start{$pos}{$length}\n"; } }
for my $pos (sort {$a <=> $b} keys %s4_stop){  for my $length (sort {$a <=> $b} keys %{$s4_stop{$pos}}){  print OUT14 "$pos\t$length\t$s4_stop{$pos}{$length}\n"; } }

exit;
