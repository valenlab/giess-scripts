#!/bin/bash

#AG 02/12/15
#script to produce a plot of 5' riboseq length distributions from a bam file
#1 Bam to sam
#2 Get 3' reads
#3 plot lenghts erelative to start and stop codons

#dependanices (must be in path)
#samtools
#r
#Rscript

usage(){
cat << EOF
usage: $0 options

script to trim and align riboseq fastq datasets 

OPTIONS:
	-b	path to the input bam files
	-o	path to output dir
	-g	gtf file (matched to the bam gemone)
	-h	this help message

example usage: run_lenghts.sh -b <in.bam> -g in.gtf -o <out_dir>

EOF
}

while getopts ":b:o:g:h" opt; do
	case $opt in 
		b)
			in_bam=$OPTARG
		  	echo "-b input bam file $OPTARG"
		  	;;
        o)
            out_dir=$OPTARG
            echo "-o output folder $OPTARG"
            ;;
        g)
            in_gtf=$OPTARG
            echo "-g gtf file $OPTARG"
            ;;
		h)
			usage
			exit
			;;
		?) 
			echo "Invalid option: -$OPTARG"
			usage
			exit 1
		  	;;
	esac
done

if [ ! -e $in_bam ]  || [ ! $out_dir ] || [ ! $in_gtf ]; then
		echo "something's missing - check your command lines args"
        usage
        exit 1
fi

if [ ! -d $out_dir ]; then
    mkdir $out_dir
fi

if [ ! -d ${out_dir}/tmp ]; then
    mkdir ${out_dir}/tmp
fi

if [ ! -d ${out_dir}/plots ]; then
    mkdir ${out_dir}/plots
fi

for file in $in_bam/*.bam; do

	name=$(basename $file)
	prefix=${name%.bam}
	echo starting $prefix

	#------------------------------------------------------------------------------------------
	#1 Bam to sam
	#------------------------------------------------------------------------------------------

	samtools view $file > ${out_dir}/tmp/${prefix}.sam
	
	#------------------------------------------------------------------------------------------
	#2 Count 3' ends of reads
	#------------------------------------------------------------------------------------------

	nice -n 10 perl meta_gene_plot_bam_matrix_overlap_3_prime.pl $in_gtf ${out_dir}/tmp/${prefix}.sam $out_dir

	#------------------------------------------------------------------------------------------	
	#3 Make length plots
	#------------------------------------------------------------------------------------------

	Rscript plot_lengths.start.R ${out_dir}/${prefix}_start_codon_lengths.csv ${out_dir}/plots/${prefix}_start.pdf

    Rscript plot_lengths.stop.R ${out_dir}/${prefix}_stop_codon_lengths.csv ${out_dir}/plots/${prefix}_stop.pdf
	
	echo plots produced

	#------------------------------------------------------------------------------------------ 
	#4 Make metagene plots (bonus)
	#------------------------------------------------------------------------------------------

	perl scale_meta_plot_matrix_row_filter.pl ${out_dir}/${prefix}_start_codon.csv >  ${out_dir}/tmp/${prefix}_start_meta_scaled.csv
	perl scale_meta_plot_matrix_row_filter.pl ${out_dir}/${prefix}_stop_codon.csv > ${out_dir}/tmp/${prefix}_stop_meta_scaled.csv 

	Rscript plot_meta.R ${out_dir}/tmp/${prefix}_start_meta_scaled.csv ${out_dir}/tmp/${prefix}_stop_meta_scaled.csv ${out_dir}/plots/${prefix}_meta.pdf

done

