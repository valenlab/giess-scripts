#!/usr/bin/perl -w
use strict;

#to do 07/12/2015
#script to take gtf get coords around start/stop codons, and assign 3' regions to them from sam
#where a read overlaps multiple in strand genes counts it towards all those genes

#input files:
#gtf exons, start, stop codons
#sam = aligned 
my $gtf=$ARGV[0];
my $sam=$ARGV[1];
my $outDir=$ARGV[2];

my ($prefix)=$sam=~/([^\/]+).sam$/;

###################################################################################################
#open gtf file setup genomic cds positions
my %startPosFwd; #key1 = chr, key2 = position, value = gene_id(s)
my %startPosRev; #key1 = chr, key2 = position, value = gene_id(s)
my %stopPosFwd; #key1 = chr, key2 = position, value = gene_id(s)
my %stopPosRev; #key1 = chr, key2 = position, value = gene_id(s)

my %cdsFwd; #key1 = chr, key2 = position, value = gene_ids(s)
my %cdsRev; #key1 = chr, key2 = position, value = gene_ids(s)

my %startM; #key1 = gene_id, key2=metapos, key3=value
my %stopM; #key1 = gene_id, key2=metapos, key3=value
my %cdsSum; #key = gene_id, value=total CDS signal

my $head=0;

open(GENES,$gtf) || die "can't open $gtf";
while (<GENES>){
	unless(/^#/){
		my @b=split("\t");
		my $class=$b[2];
		my $chr=$b[0];
		my $start=$b[3];
		my $end=$b[4];
		my $dir=$b[6];
		if ($dir eq "+"){
			if ($class eq "start_codon"){
                my ($gene_id) = $b[8] =~ /gene_id\s"(\w+)"/;
				my $cdsLength=-51;	
				for (($start-50) .. ($start+500)){ #50bp upstream, 100bp downstream
					$cdsLength+=1;
		
					if (exists ($startPosFwd{$chr}{$_})){
                        $startPosFwd{$chr}{$_}.=",".$gene_id."_".$cdsLength;
					}else{
						$startPosFwd{$chr}{$_}=$gene_id."_".$cdsLength;
					}
					$startM{$gene_id}{$cdsLength}=0;
				}	
			}elsif($class eq "stop_codon"){
                my ($gene_id) = $b[8] =~ /gene_id\s"(\w+)"/;
                my $cdsLength=-501;
                for (($start-500) .. ($start+50)){ #50bp upstream, 100bp downstream
                    $cdsLength+=1;
		
					if (exists ($stopPosFwd{$chr}{$_})){
                    	$stopPosFwd{$chr}{$_}.=",".$gene_id."_".$cdsLength;
					}else{
                        $stopPosFwd{$chr}{$_}=$gene_id."_".$cdsLength;
					}
                    $stopM{$gene_id}{$cdsLength}=0;
                } 				
			}elsif($class eq "CDS"){
                my ($gene_id) = $b[8] =~ /gene_id\s"(\w+)"/;
                $cdsSum{$gene_id}=0;
				for ($start .. $end){
		
					if (exists ($cdsFwd{$chr}{$_})){
						$cdsFwd{$chr}{$_}.=",".$gene_id; #overlapping genes
					}else{
					 	$cdsFwd{$chr}{$_}=$gene_id;
					}
				}
			}

		}elsif($dir eq "-"){
			if ($class eq "start_codon"){
				my ($gene_id) = $b[8] =~ /gene_id\s"(\w+)"/;
				my $upstream=$end+50;
				my $downstream=$end-500;
                my $cdsLength=-51;
				while ($upstream >= $downstream){ #50bp upstream, 100bp downstream
					$cdsLength+=1;
		
					if (exists ($startPosRev{$chr}{$_})){
						$startPosRev{$chr}{$_}.=",".$gene_id."_".$cdsLength;
					}else{
						$startPosRev{$chr}{$_}=$gene_id."_".$cdsLength;
					}
	
                    $startM{$gene_id}{$cdsLength}=0;
					$upstream--;
				}   
			}elsif($class eq "stop_codon"){
				my ($gene_id) = $b[8] =~ /gene_id\s"(\w+)"/;
                my $upstream=$end+500;
                my $downstream=$end-50;
				my $cdsLength=-501;
                while ($upstream >= $downstream){ #50bp upstream, 100bp downstream
					$cdsLength+=1;	
		
					if (exists ($stopPosRev{$chr}{$_})){
						$stopPosRev{$chr}{$_}.=",".$gene_id."_".$cdsLength;
    				}else{
                        $stopPosRev{$chr}{$_}=$gene_id."_".$cdsLength;
					}		
	                $stopM{$gene_id}{$cdsLength}=0;
					$upstream--;
				}
			}elsif($class eq "CDS"){
                my ($gene_id) = $b[8] =~ /gene_id\s"(\w+)"/;
				$cdsSum{$gene_id}=0;
				for ($start .. $end){
		
					if (exists ($cdsRev{$chr})){
						$cdsRev{$chr}{$_}.=",".$gene_id;
					}else{
						$cdsRev{$chr}{$_}=$gene_id; 
					}
				}   
			}
		}
	}
}
close(GENES);

print "5' gtf parsed\n";

###################################################################################################
#open sam file
my %start_lengths;
my %stop_lengths;

open (SAM, $sam) || die "can't open $sam";
while (<SAM>){
	#skip headers
	unless(/^#/){
		my @b=split("\t");
		my $leftMost=$b[3]; #leftmost position of match 5' for fwd, 3' for rev
		my $flag=$b[1];
		my $chr=$b[2];
		my $mapq=$b[4];
		my $cig=$b[5];
		my $seq=$b[9];
		my $threePrime;

		#mapping uniqness filter
		if ($mapq >= 10){

			unless ($flag & 0x4){   #if aligned

				if ($flag & 0x10){  #if rev 3' = leftmost 

					$threePrime=$leftMost;
					
					#assign to metaplots
					if (exists ($startPosRev{$chr}{$threePrime})){
						my @over1=split(",",$startPosRev{$chr}{$threePrime});
						for (@over1){
							my ($gene,$meta_pos)=$_=~/(\w+)_(-?\w+)/;
							$startM{$gene}{$meta_pos}+=1;   
							$start_lengths{$meta_pos}{length($seq)}+=1;
						}	
					}
					if (exists ($stopPosRev{$chr}{$threePrime})){
						my @over2=split(",",$stopPosRev{$chr}{$threePrime});
						for (@over2){
							my ($gene,$meta_pos)=$_=~/(\w+)_(-?\w+)/;
							$stopM{$gene}{$meta_pos}+=1;    
							$stop_lengths{$meta_pos}{length($seq)}+=1;
						}
					}
					if (exists ($cdsRev{$chr}{$threePrime})){
						my @over3=split(",",$cdsRev{$chr}{$threePrime});
						for (@over3){
							$cdsSum{$_}++;
						}	
					}

				}else{ #if fwd 3' = leftmost plus sequence length adjusted for indels

                    #parse cigar for indels and adjust the length of the alignment             
                    my $length=length($seq);
                    while ($cig =~/(\d+)I/g){   #add to length for insertions
                        $length+=$1;
                    }
                    while ($cig =~/(\d+)D/g){   #substact from length for deletions
                        $length-=$1;
                    }

                    $threePrime=$leftMost+$length;

					#assign to metaplots
					if (exists ($startPosFwd{$chr}{$threePrime})){
						my @over4=split(",",$startPosFwd{$chr}{$threePrime});
						for (@over4){
                            my ($gene,$meta_pos)=$_=~/(\w+)_(-?\w+)/;
							$startM{$gene}{$meta_pos}+=1;	
							$start_lengths{$meta_pos}{length($seq)}+=1;
						}
					}
                    if (exists ($stopPosFwd{$chr}{$threePrime})){
						my @over5=split(",",$stopPosFwd{$chr}{$threePrime});
						for (@over5){
							my ($gene,$meta_pos)=$_=~/(\w+)_(-?\w+)/;
							$stopM{$gene}{$meta_pos}+=1;    
							$stop_lengths{$meta_pos}{length($seq)}+=1;
						}
					}
					if (exists ($cdsFwd{$chr}{$threePrime})){
						my @over6=split(",",$cdsFwd{$chr}{$threePrime}); 
						for (@over6){
							$cdsSum{$_}++;
						}
					}
				}
			}
		}
	}					
}
close (SAM);

print "3' sam parsed\n";

###################################################################################################
my $outStart=$outDir."/".$prefix."_start_codon.csv";
my $outStop=$outDir."/".$prefix."_stop_codon.csv";
my $outStartLengths=$outDir."/".$prefix."_start_codon_lengths.csv";
my $outStopLengths=$outDir."/".$prefix."_stop_codon_lengths.csv";

open (OUT1,">$outStart") || die "can't open $outStart\n";
open (OUT2,">$outStop") || die "can't open $outStart\n";
open (OUT3,">$outStartLengths") || die "can't open $outStart\n";
open (OUT4,">$outStopLengths") || die "can't open $outStart\n";

#gene,position,count_at_position
#Header
print OUT1 "gene_id"; for (-50 .. 500){ 	print OUT1 ",$_"; } print OUT1 ",sum\n";
for my $gene (sort keys %startM){
	print OUT1 "$gene";
	for my $pos (sort {$a <=> $b} keys %{$startM{$gene}}){
		print OUT1 ",$startM{$gene}{$pos}";
	}
	print OUT1 ",$cdsSum{$gene}\n";
}

print OUT2 "gene_id"; for (-500 .. 50){     print OUT2 ",$_"; } print OUT2 "sum\n";
for my $gene (sort keys %stopM){
    print OUT2 "$gene";
    for my $pos (sort {$a <=> $b} keys %{$stopM{$gene}}){
        print OUT2 ",$stopM{$gene}{$pos}";
    }
    print OUT2 ",$cdsSum{$gene}\n";
}

for my $k1 (sort {$a <=> $b} keys %start_lengths){
	for my $k2 (sort {$a <=> $b} keys %{$start_lengths{$k1}}){
		print OUT3 "$k1\t$k2\t$start_lengths{$k1}{$k2}\n";
	}
}

for my $k1 (sort {$a <=> $b} keys %stop_lengths){
    for my $k2 (sort {$a <=> $b} keys %{$stop_lengths{$k1}}){
        print OUT4 "$k1\t$k2\t$stop_lengths{$k1}{$k2}\n";
    }
}

exit;
