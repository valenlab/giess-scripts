#!/usr/bin/perl -w
#use strict;

##to do 11/11/2015
##script to take a csv matrix and to scale values in rows by the cds total, and then sum per meta position.
##And filter on expression value

my $csv=$ARGV[0];
my @meta;
my @header;
open (IN, $csv) || die;
while (<IN>){
	chomp();
	if (/^gene/){
        my @a=split(",");
		#remove gene name and scaling factor
		shift(@a); pop(@a);
		for (@a){
			push (@header,$_);
		}
	}else{

		my @a=split(",");	

		#remove gene name
		shift(@a);

		#scaling factor is last position in array
		my $scale=pop(@a);
		my $rowSum=0;

		#scaling factor is the sum of genes
		for (@a){
			$rowSum+=$_;
		}

		if ($rowSum <= 1000){

			my $count=0;
			for (@a){
				$meta[$count]+=eval {($_/$rowSum)} || 0;
				$count++;
			}
		}
	}
}
close (IN);

my $outSum=0;
for (@header){
	print "$_\t$meta[$outSum]\n";
	$outSum++;
}

#output
#meta_pos	#scaled_sum_counts
#-50		1.223
#-49		3.545
