#!/usr/bin/perl -w
#use strict;

##to do 27/01/2016
##script to take a csv matrix and to scale values in rows by the row sum, filter out the lower 10% of genes by expression, and then sum per meta position.

my $csv=$ARGV[0];
my @meta;
my @header;
my %cdsSum;
my %black_list;

#parse once for cds values;
open (IN, $csv) || die;
while (<IN>){
	chomp();
	unless (/^gene/){
		my @a=split(",");
		#name is 1st position, cds sum is last position in array	
		$cdsSum{shift(@a)}=pop(@a);
	}
}

#sort the gene list by total cds riboseq sum, lowest to highest
my $count=0;
my $number_of_genes= keys %cdsSum;
my $ten_percent=$number_of_genes*0.1;
for my $gene ( sort { $cdsSum{$a} <=> $cdsSum{$b} } keys(%cdsSum) ){
#   print "$gene,$cdsSum{$gene}\n";
    if ($count <= $ten_percent){
        $black_list{$gene}=1;
    }
    $count++;
}
close(IN);

#parse 2nd time to filter
open (IN, $csv) || die;
while (<IN>){
	chomp();
	if (/^gene/){
        my @a=split(",");
		#remove gene name and scaling factor
		shift(@a); pop(@a);
		for (@a){
			push (@header,$_);
		}
	}else{
		my @a=split(",");	
		#remove gene name
		my $gene=shift(@a);

		#cds sum is last position in array
		my $cdsSum=pop(@a); #keep so we don't include
		my $rowSum=0;

		#scaling factor is the sum of genes
		for (@a){
			$rowSum+=$_;
		}

		#exclude lowest 10% of genes by cds expressiopn (last row)
		unless (exists ($black_list{$gene})){
			my $count=0;
			for (@a){
				$meta[$count]+=eval {($_/$rowSum)} || 0;
				$count++;
			}
		}
	}
}
close (IN);

my $outSum=0;
for (@header){
	print "$_\t$meta[$outSum]\n";
	$outSum++;
}

#output
#meta_pos	#scaled_sum_counts
#-50		1.223
#-49		3.545
