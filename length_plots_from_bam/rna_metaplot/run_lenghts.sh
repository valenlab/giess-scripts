#!/bin/bash

#AG 09/02/16
#script to produce a plot of 5' riboseq length distributions from a bam file
#1 Bam to sam
#2 Get 5' reads
#3 meta plots around the start and stop codons

#dependanices (must be in path)
#samtools
#r
#Rscript

usage(){
cat << EOF
usage: $0 options

script to trim and align riboseq fastq datasets 

OPTIONS:
    -b    path to the input bam files
    -o    path to output dir
    -g    gtf file (matched to the bam gemone)
    -h    this help message

example usage: run_lenghts.sh -b <in.bam> -g in.gtf -o <out_dir>

EOF
}

while getopts ":b:o:g:h" opt; do
    case $opt in 
        b)
            in_bam=$OPTARG
              echo "-b input bam file $OPTARG"
              ;;
        o)
            out_dir=$OPTARG
            echo "-o output folder $OPTARG"
            ;;
        g)
            in_gtf=$OPTARG
            echo "-g gtf file $OPTARG"
            ;;
        h)
            usage
            exit
            ;;
        ?) 
            echo "Invalid option: -$OPTARG"
            usage
            exit 1
              ;;
    esac
done

if [ ! -e $in_bam ]  || [ ! $out_dir ] || [ ! $in_gtf ]; then
        echo "something's missing - check your command lines args"
        usage
        exit 1
fi

if [ ! -d $out_dir ]; then
    mkdir $out_dir
fi

if [ ! -d ${out_dir}/tmp ]; then
    mkdir ${out_dir}/tmp
fi

if [ ! -d ${out_dir}/plots ]; then
    mkdir ${out_dir}/plots
fi

if [ ! -d ${out_dir}/png ]; then
    mkdir ${out_dir}/png
fi

for file in $in_bam/*.bam; do

    name=$(basename $file)
    prefix=${name%.bam}
    echo starting $prefix

    #------------------------------------------------------------------------------------------
    #1 Bam to sam
    #------------------------------------------------------------------------------------------

    samtools view $file > ${out_dir}/tmp/${prefix}.sam
    
    #------------------------------------------------------------------------------------------
    #2 Count 5' ends of reads
    #------------------------------------------------------------------------------------------

    nice -n 10 perl meta_gene_plot_bam_matrix_overlap_subgroups_for_rna_seq.pl $in_gtf ${out_dir}/tmp/${prefix}.sam $out_dir

    #------------------------------------------------------------------------------------------ 
    #3 Make metagene plots
    #------------------------------------------------------------------------------------------

    perl meta_plot_matrix_absolute.pl ${out_dir}/${prefix}_start_codon.csv > ${out_dir}/${prefix}_start_meta_absolute.csv
    perl meta_plot_matrix_absolute.pl ${out_dir}/${prefix}_stop_codon.csv > ${out_dir}/${prefix}_stop_meta_absolute.csv

    perl meta_plot_matrix_scale.pl ${out_dir}/${prefix}_start_codon.csv > ${out_dir}/${prefix}_start_meta_scaled.csv
    perl meta_plot_matrix_scale.pl ${out_dir}/${prefix}_stop_codon.csv > ${out_dir}/${prefix}_stop_meta_scaled.csv

    Rscript plot_meta.R ${out_dir}/${prefix}_start_meta_absolute.csv ${out_dir}/${prefix}_stop_meta_absolute.csv ${out_dir}/plots/${prefix}_meta_absolute.pdf ${out_dir}/png/${prefix}_meta_absolute.png
    Rscript plot_meta.R ${out_dir}/${prefix}_start_meta_scaled.csv ${out_dir}/${prefix}_stop_meta_scaled.csv ${out_dir}/plots/${prefix}_meta_scaled.pdf ${out_dir}/png/${prefix}_meta_scaled.png

    #------------------------------------------------------------------------------------------ 
    #4 Make metagene plots for groups
    #------------------------------------------------------------------------------------------

    perl meta_plot_matrix_absolute.pl ${out_dir}/${prefix}_start_codon_group1.csv > ${out_dir}/${prefix}_start_meta_g1_absolute.csv
    perl meta_plot_matrix_absolute.pl ${out_dir}/${prefix}_start_codon_group2.csv > ${out_dir}/${prefix}_start_meta_g2_absolute.csv
    perl meta_plot_matrix_absolute.pl ${out_dir}/${prefix}_start_codon_group3.csv > ${out_dir}/${prefix}_start_meta_g3_absolute.csv
    perl meta_plot_matrix_absolute.pl ${out_dir}/${prefix}_stop_codon_group4.csv > ${out_dir}/${prefix}_stop_meta_g4_absolute.csv

    perl meta_plot_matrix_scale.pl ${out_dir}/${prefix}_start_codon_group1.csv > ${out_dir}/${prefix}_start_meta_g1_scaled.csv
    perl meta_plot_matrix_scale.pl ${out_dir}/${prefix}_start_codon_group2.csv > ${out_dir}/${prefix}_start_meta_g2_scaled.csv
    perl meta_plot_matrix_scale.pl ${out_dir}/${prefix}_start_codon_group3.csv > ${out_dir}/${prefix}_start_meta_g3_scaled.csv
    perl meta_plot_matrix_scale.pl ${out_dir}/${prefix}_stop_codon_group4.csv > ${out_dir}/${prefix}_stop_meta_g4_scaled.csv

    Rscript plot_meta_groups.R ${out_dir}/${prefix}_start_meta_g1_absolute.csv ${out_dir}/${prefix}_start_meta_g2_absolute.csv ${out_dir}/${prefix}_start_meta_g3_absolute.csv ${out_dir}/${prefix}_stop_meta_g4_absolute.csv ${out_dir}/plots/${prefix}_meta_groups_absolute.pdf ${out_dir}/png/${prefix}_meta_groups_absolute.png
    Rscript plot_meta_groups.R ${out_dir}/${prefix}_start_meta_g1_scaled.csv ${out_dir}/${prefix}_start_meta_g2_scaled.csv ${out_dir}/${prefix}_start_meta_g3_scaled.csv ${out_dir}/${prefix}_stop_meta_g4_scaled.csv ${out_dir}/plots/${prefix}_meta_groups_scaled.pdf ${out_dir}/png/${prefix}_meta_groups_scaled.png

    #------------------------------------------------------------------------------------------ 
    #4 Tidy up
    #------------------------------------------------------------------------------------------

#   rm ${out_dir}/tmp/${prefix}.sam

done
