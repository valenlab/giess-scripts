#!/usr/bin/perl -w
use strict;

#to do 02/02/2016
#script to take gtf get coords around start/stop codons, and assign overlapping postions to them from bam
#where a read overlaps multiple in starnd genes counts it towards all those genes
#also split genes into subgroups based on a proportion of reads at peak positions
#group 1 = peak at -20 to -10 start_codon
#group 2 = peak at -5 to +5 start_codon
#group 3 = peak at 10 to 20 start_codon

#input files:  
my $gtf=$ARGV[0];
my $sam=$ARGV[1];
my $outDir=$ARGV[2];

my ($prefix)=$sam=~/([^\/]+).sam$/;
print "$prefix\n";

###################################################################################################
#open gtf and get inton positions
my %introns; #key=gene_id, #key2=position, #value = 1;
my %previous_exon; #key=gene_id. value=exon_position

#setup a hash of intron positions
open(GENES2,$gtf) || die "can't open $gtf";      #gft is 1 based
while (<GENES2>){
    unless(/^#/){
        my @b=split("\t");
        my $class=$b[2];
        my $start=$b[3];
        my $end=$b[4];
        my $dir=$b[6];
        if ($class eq "exon"){
            my ($gene_id) = $b[8] =~ /gene_id\s"(\w+)"/;

            #fwd exons are sorted in ascending order 
            #             100958 100975 <-end(+1)
            # satrt(-1)-> 101077 101715 
            if ($dir eq "+"){
                if (exists $previous_exon{$gene_id}){   #if there is a previous exon, there is an intron between this one and the last one
                     for my $pos ($previous_exon{$gene_id} .. ($start-1)){   #end of previous exon to start of this one = sorted ascending
                         $introns{$gene_id}{$pos}=1;
                     }
                     $previous_exon{$gene_id}=$end+1;
                }else{
                     $previous_exon{$gene_id}=$end+1;
                }

            }else{
            #rev exons are sorted in decending order  
            # end(-1)-> 447087 447794
            #           446060 446254 <-start(+1)
                if (exists $previous_exon{$gene_id}){   #if there is a previous exon, there is an intron between this one and the last one
                     for my $pos (($end+1) .. $previous_exon{$gene_id}){   #end of this exon to start of the pervious one = sorted decending
                         $introns{$gene_id}{$pos}=1;
                     }
                     $previous_exon{$gene_id}=$start-1;
                }else{
                     $previous_exon{$gene_id}=$start-1;
                }
            }
        }
    }
}
close (GENES2);

###################################################################################################
#open gtf file setup genomic cds positions
my %startPosFwd; #key1 = chr, key2 = position, value = gene_id(s)
my %startPosRev; #key1 = chr, key2 = position, value = gene_id(s)
my %stopPosFwd; #key1 = chr, key2 = position, value = gene_id(s)
my %stopPosRev; #key1 = chr, key2 = position, value = gene_id(s)
my %cdsFwd; #key1 = chr, key2 = position, value = gene_ids(s)
my %cdsRev; #key1 = chr, key2 = position, value = gene_ids(s)
my %startM; #key1 = gene_id, key2=metapos, key3=value
my %stopM; #key1 = gene_id, key2=metapos, key3=value
my %cdsSum; #key = gene_id, value=total CDS signal

my $head=0;

open(GENES,$gtf) || die "can't open $gtf";
while (<GENES>){
    unless(/^#/){
        my @b=split("\t");
        my $class=$b[2];
        my $chr=$b[0];
        my $start=$b[3];
        my $end=$b[4];
        my $dir=$b[6];
        if ($dir eq "+"){
            if ($class eq "start_codon"){
                my ($gene_id) = $b[8] =~ /gene_id\s"(\w+)"/;
                my $cdsLength=-51;    
                for (($start-50) .. ($start+500)){ #50bp upstream, 100bp downstream
                    unless (exists $introns{$gene_id}{$_}){ #check if this position is intronic
                        $cdsLength+=1;
                            if (exists ($startPosFwd{$chr}{$_})){
                            $startPosFwd{$chr}{$_}.=",".$gene_id."_".$cdsLength;
                        }else{
                            $startPosFwd{$chr}{$_}=$gene_id."_".$cdsLength;
                        }
                        $startM{$gene_id}{$cdsLength}=0;
                    }
                }    
            }elsif($class eq "stop_codon"){
                my ($gene_id) = $b[8] =~ /gene_id\s"(\w+)"/;
                my $cdsLength=-501;
                for (($start-500) .. ($start+50)){ #50bp upstream, 100bp downstream
                    unless (exists $introns{$gene_id}{$_}){ #check if this position is intronic
                        $cdsLength+=1;
                        if (exists ($stopPosFwd{$chr}{$_})){
                            $stopPosFwd{$chr}{$_}.=",".$gene_id."_".$cdsLength;
                        }else{
                            $stopPosFwd{$chr}{$_}=$gene_id."_".$cdsLength;
                        }
                        $stopM{$gene_id}{$cdsLength}=0;
                    }
                }                 
            }elsif($class eq "CDS"){
                my ($gene_id) = $b[8] =~ /gene_id\s"(\w+)"/;
                $cdsSum{$gene_id}=0;
                for ($start .. $end){
                    if (exists ($cdsFwd{$chr}{$_})){
                        $cdsFwd{$chr}{$_}.=",".$gene_id; #overlapping genes
                    }else{
                         $cdsFwd{$chr}{$_}=$gene_id;
                    }
                }
            }

        }elsif($dir eq "-"){
            if ($class eq "start_codon"){
                my ($gene_id) = $b[8] =~ /gene_id\s"(\w+)"/;
                my $upstream=$end+50;
                my $downstream=$end-500;
                my $cdsLength=-51;
                while ($upstream >= $downstream){ #50bp upstream, 100bp downstream
                    unless (exists $introns{$gene_id}{$_}){ #check if this position is intronic
                        $cdsLength+=1;
                        if (exists ($startPosRev{$chr}{$_})){
                            $startPosRev{$chr}{$_}.=",".$gene_id."_".$cdsLength;
                        }else{
                            $startPosRev{$chr}{$_}=$gene_id."_".$cdsLength;
                        } 
                        $startM{$gene_id}{$cdsLength}=0;
                        $upstream--;
                    }
                }   
            }elsif($class eq "stop_codon"){
                my ($gene_id) = $b[8] =~ /gene_id\s"(\w+)"/;
                my $upstream=$end+500;
                my $downstream=$end-50;
                my $cdsLength=-501;
                while ($upstream >= $downstream){ #50bp upstream, 100bp downstream
                    unless (exists $introns{$gene_id}{$_}){ #check if this position is intronic
                        $cdsLength+=1;    
                        if (exists ($stopPosRev{$chr}{$_})){
                            $stopPosRev{$chr}{$_}.=",".$gene_id."_".$cdsLength;
                        }else{
                            $stopPosRev{$chr}{$_}=$gene_id."_".$cdsLength;
                        }        
                        $stopM{$gene_id}{$cdsLength}=0;
                        $upstream--;
                    }
                }
            }elsif($class eq "CDS"){
                my ($gene_id) = $b[8] =~ /gene_id\s"(\w+)"/;
                $cdsSum{$gene_id}=0;
                for ($start .. $end){
                    if (exists ($cdsRev{$chr})){
                        $cdsRev{$chr}{$_}.=",".$gene_id;
                    }else{
                        $cdsRev{$chr}{$_}=$gene_id; 
                    }
                }   
            }
        }
    }
}
close(GENES);

print "gtf finished\n";

###################################################################################################
#open sam file
my %start_lengths;
my %stop_lengths;

open (SAM, $sam) || die "can't open $sam";
while (<SAM>){
    #skip headers
    unless(/^#/){
        my @b=split("\t");
        my $leftMost=$b[3]; #leftmost position of match. 5' for fwd. 3' for rev
        my $flag=$b[1];
        my $chr=$b[2];
        my $mapq=$b[4];
        my $cig=$b[5];
        my $seq=$b[9];
        
        #mapping uniqness filter
        if ($mapq >= 10){
            unless ($flag & 0x4){   #if aligned

                #parse cigar for indels and adjust the length of the alignment     
                my $length=length($seq);
                while ($cig =~/(\d+)I/g){   #add to length for insertions
                     $length+=$1;
                }
                while ($cig =~/(\d+)D/g){   #substact from length for deletions
                    $length-=$1;
                }

                for my $base ($leftMost .. (($leftMost+$length-1))){    #assign to metaplots  #sam is 1 based

                    if ($flag & 0x10){ #if rev #in the bam files fwd reads align to the reverse strand
                        if (exists ($startPosFwd{$chr}{$base})){
                            my @over1=split(",",$startPosFwd{$chr}{$base});
                            for (@over1){
                                my ($gene,$meta_pos)=$_=~/(\w+)_(-?\w+)/;
                                $startM{$gene}{$meta_pos}+=1;   
                            }    
                        }
                        if (exists ($stopPosFwd{$chr}{$base})){
                            my @over2=split(",",$stopPosFwd{$chr}{$base});
                            for (@over2){
                                my ($gene,$meta_pos)=$_=~/(\w+)_(-?\w+)/;
                                $stopM{$gene}{$meta_pos}+=1;    
                            }
                        }
                        if (exists ($cdsFwd{$chr}{$base})){
                            my @over3=split(",",$cdsFwd{$chr}{$base});
                            for (@over3){
                                $cdsSum{$_}++;
                            }    
                        }    

                    }else{ #if fwd  #in the bam file rev reads align to the forward strand
                        if (exists ($startPosRev{$chr}{$base})){
                            my @over4=split(",",$startPosRev{$chr}{$base});
                            for (@over4){
                                my ($gene,$meta_pos)=$_=~/(\w+)_(-?\w+)/;
                                $startM{$gene}{$meta_pos}+=1;    
                            }
                        }
                        if (exists ($stopPosRev{$chr}{$base})){
                            my @over5=split(",",$stopPosRev{$chr}{$base});
                            for (@over5){
                                my ($gene,$meta_pos)=$_=~/(\w+)_(-?\w+)/;
                                $stopM{$gene}{$meta_pos}+=1;    
                            }
                        }
                        if (exists ($cdsRev{$chr}{$base})){
                            my @over6=split(",",$cdsRev{$chr}{$base}); 
                            for (@over6){
                                $cdsSum{$_}++;
                            }
                        }
                    }
                }
            }
        }
    }                    
}
close (SAM);

print "sam finished\n";

###################################################################################################
#get gene list for groups from riboseq
my %group1;
my %group2;
my %group3;


###################################################################################################
my $outStart=$outDir."/".$prefix."_start_codon.csv";
my $outStop=$outDir."/".$prefix."_stop_codon.csv";
my $outGroup1=$outDir."/".$prefix."_start_codon_group1.csv";
my $outGroup2=$outDir."/".$prefix."_start_codon_group2.csv";
my $outGroup3=$outDir."/".$prefix."_start_codon_group3.csv";
my $outGroup4=$outDir."/".$prefix."_stop_codon_group4.csv";

open (OUT1,">$outStart") || die "can't open $outStart\n";
open (OUT2,">$outStop") || die "can't open $outStart\n";
open (G1,">$outGroup1") || die "can't open $outGroup1\n";
open (G2,">$outGroup2") || die "can't open $outGroup2\n";
open (G3,">$outGroup3") || die "can't open $outGroup3\n";
open (G4,">$outGroup4") || die "can't open $outGroup4\n";

#gene,position,count_at_position
#Header
print OUT1 "gene_id"; for (-50 .. 500){     print OUT1 ",$_"; } print OUT1 ",sum\n";
print G1 "gene_id"; for (-50 .. 500){     print G1 ",$_"; } print G1 ",sum\n";
print G2 "gene_id"; for (-50 .. 500){     print G2 ",$_"; } print G2 ",sum\n";
print G3 "gene_id"; for (-50 .. 500){     print G3 ",$_"; } print G3 ",sum\n";

for my $gene (sort keys %startM){
    print OUT1 "$gene";
    my $outLine=$gene;
    my $preCdsSum=0;
    my $g1Sum=0;
    my $g2Sum=0;
    my $g3Sum=0;

    for my $pos (sort {$a <=> $b} keys %{$startM{$gene}}){
        print OUT1 ",$startM{$gene}{$pos}";
        $outLine.=",$startM{$gene}{$pos}"; #hack to get the coverage over the window. -50 to + 500 of start codon.
        $preCdsSum+=$startM{$gene}{$pos};
        #a gene can be assigned to multiple groups
        if ($pos >=-20 && $pos <=-10){     $g1Sum+=$startM{$gene}{$pos}; }
        if ($pos >=-5 && $pos <=5){     $g2Sum+=$startM{$gene}{$pos}; }
        if ($pos >=10 && $pos <=20){    $g3Sum+=$startM{$gene}{$pos}; }
    }
    print OUT1 ",$cdsSum{$gene}\n";

    #peak assignments
    #also split genes into subgroups based on a proportion of reads at peak positions
    #group 1 = peak at -20 to -10 start_codon
    #group 2 = peak at -5 to +5 start_codon
    #group 3 = peak at 10 to 20 start_codon
    #propertion = 0.05 / 5%
    #if ($g1Sum/($cdsSum{$gene}+$preCdsSum) >= 0.1){ #this was cds + the -50 region. now window
    if ($preCdsSum){ #avoid division by zero
        if ($g1Sum/$preCdsSum >= 0.05){    print G1 "$outLine".",$cdsSum{$gene}\n"; }
        if ($g2Sum/$preCdsSum >= 0.05){ print G2 "$outLine".",$cdsSum{$gene}\n"; }
        if ($g3Sum/$preCdsSum >= 0.05){ print G3 "$outLine".",$cdsSum{$gene}\n"; }
    }
}

print OUT2 "gene_id"; for (-500 .. 50){     print OUT2 ",$_"; } print OUT2 ",sum\n";
print G4 "gene_id"; for (-500 .. 50){     print G4 ",$_"; } print G4 ",sum\n";

for my $gene (sort keys %stopM){
    print OUT2 "$gene";
    my $outLine=$gene;
    my $preCdsSum=0;
    my $g4Sum=0;

    for my $pos (sort {$a <=> $b} keys %{$stopM{$gene}}){
        print OUT2 ",$stopM{$gene}{$pos}";
        $outLine.=",$stopM{$gene}{$pos}";         #hack to get the coverage at this window. = -500 and + 50 of stop codon 
        $preCdsSum+=$stopM{$gene}{$pos};
        if ($pos >=1 && $pos <=10){ $g4Sum+=$stopM{$gene}{$pos}; }
    }
    print OUT2 ",$cdsSum{$gene}\n";

    if ($preCdsSum){ #avoid division by zero
        if ($g4Sum/$preCdsSum >= 0.05){ print G4 "$outLine".",$cdsSum{$gene}\n"; }
    }
}

exit;
