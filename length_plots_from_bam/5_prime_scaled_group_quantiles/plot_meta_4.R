#!/usr/bin/env Rscript
args = commandArgs(trailingOnly=TRUE)

#usage = Rscript plot_lenghts.R start.csv stop.csv out.pdf out.png

#test if there is at least one argument: if not, return an error
if (! length(args)==6) {
  stop("6 arguments must be supplied (4 x input.csv, 2 x output.pdf/png).n", call.=FALSE)
}

#load libraries

#read csv
q1.raw<-read.csv(args[1],header=FALSE,check.names=FALSE,sep="\t")
q1.sca<-read.csv(args[2],header=FALSE,check.names=FALSE,sep="\t")
q4.raw<-read.csv(args[3],header=FALSE,check.names=FALSE,sep="\t")
q4.sca<-read.csv(args[4],header=FALSE,check.names=FALSE,sep="\t")

###################################################################################################

#open pdf
pdf(args[5], width=20,height=20)

#make plot
par(mfrow=c(2,2))

plot(x=q1.raw[1:150,1],y=q1.raw[1:150,2],type="l",xlim=c(-50,100),main="Metaplot start codon upper quartile",xlab="position relative to start codon (nt)",ylab="sum signal")
abline(v=0, lty=3)

plot(x=q1.sca[1:150,1],y=q1.sca[1:150,2],type="l",xlim=c(-50,100),main="Metaplot start codon upper quartile",xlab="position relative to start codon (nt)",ylab="sum scaled signal")
abline(v=0, lty=3)

plot(x=q4.raw[1:150,1],y=q4.raw[1:150,2],type="l",xlim=c(-50,100),main="Metaplot start codon lower quartile",xlab="position relative to start codon (nt)",ylab="sum signal")
abline(v=0, lty=3)

plot(x=q4.sca[1:150,1],y=q4.sca[1:150,2],type="l",xlim=c(-50,100),main="Metaplot start codon lower quartile",xlab="position relative to start codon (nt)",ylab="sum scaled signal")
abline(v=0, lty=3)

#save pdf
dev.off()

###################################################################################################

#open png
png(args[6], width=800,height=800)

#make plot
par(mfrow=c(2,2))

plot(x=q1.raw[1:150,1],y=q1.raw[1:150,2],type="l",xlim=c(-50,100),main="Metaplot start codon upper quartile",xlab="position relative to start codon (nt)",ylab="sum signal")
abline(v=0, lty=3)

plot(x=q1.sca[1:150,1],y=q1.sca[1:150,2],type="l",xlim=c(-50,100),main="Metaplot start codon upper quartile",xlab="position relative to start codon (nt)",ylab="sum scaled signal")
abline(v=0, lty=3)

plot(x=q4.raw[1:150,1],y=q4.raw[1:150,2],type="l",xlim=c(-50,100),main="Metaplot start codon lower quartile",xlab="position relative to start codon (nt)",ylab="sum signal")
abline(v=0, lty=3)

plot(x=q4.sca[1:150,1],y=q4.sca[1:150,2],type="l",xlim=c(-50,100),main="Metaplot start codon lower quartile",xlab="position relative to start codon (nt)",ylab="sum scaled signal")
abline(v=0, lty=3)

#save pdf
dev.off()
